# HeapQuestions

This repository contains files for the course of Web Applications, MD in Computer Engineering and MD in ICT for Internet and multimedia, DEI Padova.

Team: _The StackOverflowers_, alias
[@eliabntt](https://bitbucket.org/eliabntt),
[@rig8f](https://bitbucket.org/rig8f),
[@iamalme](https://bitbucket.org/iamalme) and
[@zovomat](https://bitbucket.org/zovomat).

## On this repo

Inside `src/main`:

- `database` folder contains SQL files about the schema, sample data insertion and dumps.
- `java` folder contains "back-end" code in the form of Java classes (_resources_), _servlets_ and _database_ managers.
- `report` contains report files, including
    - The report itself in DOCX and PDF format
    - Visio files and images/PDF conversions of ER and relational schemas
    - Mockups of the UI
    - WebML schemas of some actions
    - UML diagrams for packages and flows
- `tests` contains several html pages useful for API testing.
- `webapp` contains configuration information.

## Private server

- Host `8labs.mooo.com`
- SSH Port `2222`
- Tomcat Port `8088`
- PgSql Port `54320`
- User `sql`
- Pass `stackoverflowers18`

---
(C) 2018 The StackOverflowers.