<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page trimDirectiveWhitespaces="true" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <%@include file="../template/meta.html" %>
    <%@include file="../template/links-css.html" %>
    <script type="text/javascript" src="../js/sessionTransfer.js"></script>
    <script type="text/javascript" src="../js/checkLogged.js"></script>
    <script>checkLogged("home.jsp");</script>
    <title>HeapQuestions - Show Note</title>
</head>
<body>
<%@include file="../template/navbar.html" %>

<!-- Content -->
<main role="main" class="container-fluid">
    <div class="row">
        <div id="mainContent" class="col-xs-12 col-md-9">
            <div class="d-flex flex-column justify-content-center full-height">
                <div id="contentBody"
                     class="col-12 col-md-10 offset-md-1 col-xl-8 offset-xl-2 my-3 p-3 border border-secondary rounded">
                    <h2>Show Note(s)</h2>
                    <!-- please note that delete is not supported -->

                    <c:import url="show_message.jsp"/>

                    <!-- display the note, if any and no errors -->
                    <c:if test="${not empty note && !message.error}">
                        <p class="ml-3">
                            <!-- this is temporary because jsp pages does not accept token parameter
                                 and instead they produce it by means of a form field -->
                            <a class="btn btn-secondary text-white mr-3" href="<c:url value="update_note.jsp"/>">Edit</a>
                            <a class="btn btn-danger text-white" href="<c:url value="delete_note.jsp"/>">Delete</a>
                        </p>

                        <ul>
                            <li>id: <c:out value="${note.id}"/></li>
                            <li>userId: <c:out value="${note.userId}"/></li>
                            <li>dateTime: <c:out value="${note.dateTime}"/></li>
                            <li>content: <c:out value="${note.content}"/></li>
                        </ul>
                    </c:if>

                    <!-- else display the list of notes, if any and no errors -->
                    <c:if test="${not empty noteList && !message.error}">
                        <p class="ml-3">
                            <a class="btn btn-secondary text-white mr-3" href="<c:url value="update_note.jsp"/>">Edit</a>
                            <a class="btn btn-danger text-white" href="<c:url value="delete_note.jsp"/>">Delete</a>
                        </p>

                        <c:forEach items="${noteList}" var="note">
                            <ul>
                                <li>id: <c:out value="${note.id}"/></li>
                                <li>userId: <c:out value="${note.userId}"/></li>
                                <li>dateTime: <c:out value="${note.dateTime}"/></li>
                                <li>content: <c:out value="${note.content}"/></li>
                            </ul>
                        </c:forEach>
                    </c:if>
                </div>
            </div>
        </div>
        <%@include file="../template/sidebar.html" %>
    </div>
</main>
<%@include file="../template/footer.html" %>
<%@include file="../template/modal.html" %>
<%@include file="../template/links-js.html" %>
<script type="text/javascript" src="../js/notes.js"></script>
</body>
</html>
