<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <%@include file="../template/meta.html" %>
    <%@include file="../template/links-css.html" %>
    <script type="text/javascript" src="../js/sessionTransfer.js"></script>
    <script type="text/javascript" src="../js/checkLogged.js"></script>
    <script>checkLogged("home.jsp");</script>
    <title>HeapQuestions - Edit User</title>
</head>
<body>
<%@include file="../template/navbar.html" %>

<!-- Content -->
<main role="main" class="container-fluid">
    <div class="row">
        <div id="mainContent" class="col-xs-12 col-md-9">
            <div class="d-flex flex-column justify-content-center full-height">
                <div class="loader mx-auto" id="loader"></div>
                <div id="contentBody" class="mx-4 my-3 p-4 border border-secondary d-none rounded">
                    <h2>Edit your details</h2>
                    <%@include file="../template/alert.html" %>
                    <form id="regForm" action="javascript:void(0);" novalidate>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputUser"><strong>Username</strong>
                                    <span class="text-muted">*</span></label>
                                <input id="inputUser" type="text" class="form-control" required>
                                <div class="invalid-feedback">
                                    Invalid username
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputEmail"><strong>Email address</strong>
                                    <span class="text-muted">*</span></label>
                                <input id="inputEmail" type="email" class="form-control" required>
                                <div class="invalid-feedback">
                                    Provide your email address
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputPass1"><strong>Password</strong>
                                    <span class="text-muted">*</span></label>
                                <input id="inputPass1" type="password" class="form-control" required>
                                <div class="invalid-feedback">
                                    Provide a password
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputPass2"><strong>Retype password</strong>
                                    <span class="text-muted">*</span></label>
                                <input id="inputPass2" type="password" class="form-control" required>
                                <div class="invalid-feedback">
                                    Retype correctly your password
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputName"><strong>Name</strong>
                                    <span class="text-muted">*</span></label>
                                <input id="inputName" type="text" class="form-control" required>
                                <div class="invalid-feedback">
                                    Please provide your name!
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputSurname"><strong>Surname</strong>
                                    <span class="text-muted">*</span></label>
                                <input id="inputSurname" type="text" class="form-control" required>
                                <div class="invalid-feedback">
                                    Please provide your surname!
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputBirth"><strong>Date of birth</strong>
                                    <span class="text-muted">*</span></label>
                                <input id="inputBirth" type="date" class="form-control" required>
                                <div class="invalid-feedback">Please provide your birth date!</div>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputLoc"><strong>Location</strong>
                                    <span class="text-muted">*</span></label>
                                <input id="inputLoc" type="text" class="form-control" required>
                                <div class="invalid-feedback">Please provide your location!</div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPicture"><strong>Profile picture</strong></label>
                            <input id="inputPicture" type="file" class="form-control-file"
                                   accept="image/jpeg"/>
                            <div id="pictureDiv" class="my-2">
                                <img id="pictureBox" width="200" height="200"/>
                                <a id="pictureDelete" class="float-left" href="#">
                                    <i class="fas fa-trash"></i></a></div>

                        </div>
                        <div class="form-group text-muted"><em>* = Mandatory information</em></div>
                        <!--<div class="form-group form-check">
                            <input type="checkbox" class="form-check-input" id="checkPolicy" required>
                            <label class="form-check-label" for="checkPolicy">I accept the privacy policy of this website.</label>
                            <div class="invalid-feedback">Please accept our conditions.</div>
                        </div>-->
                        <button role="button" id="btnFormRegister" class="btn btn-secondary">Save changes</button>
                        <button role="button" id="btnFormDelete" class="btn btn-danger ml-3">Delete account</button>
                    </form>
                </div>
            </div>
        </div>
        <%@include file="../template/sidebar.html" %>
    </div>
</main>

<%@include file="../template/footer.html" %>
<%@include file="../template/modal.html" %>
<%@include file="../template/links-js.html" %>
<script type="text/javascript" src="../webjars/js-sha256/0.7.1/build/sha256.min.js"></script>
<script src="../js/editUser.js"></script>
</body>
</html>