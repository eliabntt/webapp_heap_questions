<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <%@include file="../template/meta.html" %>
    <%@include file="../template/links-css.html" %>
    <script type="text/javascript" src="../js/sessionTransfer.js"></script>
    <script type="text/javascript" src="../js/checkLogged.js"></script>
    <script>alreadyLogged();</script>
    <title>HeapQuestions - Login</title>
</head>
<body>
<%@include file="../template/navbar.html" %>

<!-- Content -->
<main role="main" class="container-fluid">
    <div class="row">
        <div id="mainContent" class="col-xs-12 col-md-9">
            <div class="d-flex flex-column justify-content-center full-height">
                <div class="mx-auto p-4 border border-secondary rounded">
                    <h2>Login</h2>
                    <%@include file="../template/alert.html" %>
                    <form action="javascript:void(0);">
                        <div class="form-group">
                            <label for="inputUserEmail">Username / Email address:</label>
                            <input type="text" class="form-control" id="inputUserEmail" required>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword">Password:</label>
                            <input type="password" class="form-control" id="inputPassword" required>
                        </div>
                        <div class="form-group form-check">
                            <input type="checkbox" class="form-check-input" id="checkRemember">
                            <label class="form-check-label" for="checkRemember">Remember me</label>
                        </div>
                        <div class="form-group text-center">
                            <a href="register.jsp">Not registered? Join us!</a>
                        </div>
                        <button id="btnFormLogin" class="btn btn-secondary">Login</button>
                    </form>
                </div>
            </div>
        </div>
        <%@include file="../template/sidebar.html" %>
    </div>
</main>

<%@include file="../template/footer.html" %>
<%@include file="../template/modal.html" %>
<%@include file="../template/links-js.html" %>
<script type="text/javascript" src="../webjars/js-sha256/0.7.1/build/sha256.min.js"></script>
<script type="text/javascript" src="../js/login.js"></script>
</body>
</html>