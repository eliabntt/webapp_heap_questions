<!DOCTYPE html>
<html lang="en">
<head>
    <%@include file="../template/meta.html" %>
    <%@include file="../template/links-css.html" %>
    <link rel="stylesheet" href="../css/tagsinput.css">
    <script type="text/javascript" src="../js/sessionTransfer.js"></script>
    <script type="text/javascript" src="../js/checkLogged.js"></script>
    <title>HeapQuestions</title>
</head>
<body>
<%@include file="../template/navbar.html" %>

<!-- Content -->
<main role="main" class="container-fluid">
    <div class="row">
        <div id="mainContent" class="col-xs-12 col-md-9">
            <div class="d-flex flex-column justify-content-around full-height">
                <div class="col-12 col-md-10 offset-md-1 col-xl-8 offset-xl-2 my-3 p-3 border border-secondary rounded">
                    <%@include file="../template/alert.html" %>

                    <!-- Form place -->
                    <div id="divEditQue">
                    </div>

                    <!-- Preview place -->
                    <div id="divShowQue">
                        <div class="row">
                            <div class="col-md-6">
                                <h3 id="queTitle" class="d-inline-block"></h3>
                            </div>
                            <div class="col-md-6">
                                <button id="queDel" class="btn btn-danger float-right ml-3">Delete</button>
                                <button id="queMod" class="btn btn-warning float-right ml-3">Moderate</button>
                                <button id="queEd" class="btn btn-secondary float-right ml-3">Edit</button>
                            </div>
                        </div>

                        <div class="border-bottom">
                            <h6 id="queDate"></h6>
                        </div>
                        <div class="row my-2">
                            <div class="col-md-6">
                                <strong>Closed:</strong>
                                <span id="queClo"></span>
                            </div>
                            <div class="col-md-6">
                                <strong>Category:</strong>
                                <span id="queCat"></span>
                            </div>
                        </div>
                        <div class="my-2 border-top">
                            <h4 class="mt-2">Content:</h4>
                        </div>
                        <div class="row">
                            <div id="queCont" class="col-md-12 my-2"></div>
                        </div>
                        <div class="my-2">
                            <h4 id="titleMySol" style="display: none">Solution:</h4>
                        </div>
                        <div class="row">
                            <div id="queSol" class="col-md-12 my-2" style="display: none"></div>
                        </div>
                        <div class="py-3 border-top"><strong>Tags: </strong><span id="queTags"></span></div>
                        <%@include file="../template/comments.html" %>
                    </div>

                    <!-- List place -->
                    <div id="divShowRecQue">
                        <span class="m-3 float-left"><h3 id="titleQue">Forum Question</h3></span>
                        <button id="newQue" class="btn btn-secondary m-3 float-right">New Question</button>
                        <div class="table-responsive">
                        <table class="table border-bottom table-hover table-striped mt-3" id="recentQue">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Title</th>
                                <th scope="col">Date</th>
                                <th scope="col">Category</th>
                                <th scope="col">State</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        </div>
                        <nav aria-label="pagination" class="d-flex flex-row justify-content-center">
                            <ul class="pagination">
                                <li id="prevPageLi" class="page-item disabled">
                                    <a id="prevPage" class="page-link" href="#" tabindex="-1">Previous</a>
                                </li>
                                <li id="nextPageLi" class="page-item">
                                    <a id="nextPage" class="page-link" href="#">Next</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <%@include file="../template/sidebar.html" %>
    </div>
</main>

<%@include file="../template/footer.html" %>
<%@include file="../template/modal.html" %>
<%@include file="../template/links-js.html" %>
<script type="text/javascript" src="../js/tagsinput.js"></script>
<script type="text/javascript" src="../js/forum.js"></script>
<script type="text/javascript" src="../js/comments.js"></script>
</body>
</html>