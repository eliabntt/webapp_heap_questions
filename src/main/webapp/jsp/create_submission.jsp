<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <%@include file="../template/meta.html" %>
    <%@include file="../template/links-css.html" %>
    <link rel="stylesheet" href="../css/tagsinput.css">
    <script type="text/javascript" src="../js/sessionTransfer.js"></script>
    <script type="text/javascript" src="../js/checkLogged.js"></script>
    <script>checkLogged("submission.jsp");</script>
    <title>HeapQuestions - Create Submission</title>
</head>
<body>
<%@include file="../template/navbar.html" %>

<!-- Content -->
<main role="main" class="container-fluid">
    <div class="row">
        <div id="mainContent" class="col-xs-12 col-md-9">
            <div class="d-flex flex-column justify-content-center full-height">
                <div class="col-12 col-md-10 offset-md-1 col-xl-8 offset-xl-2 my-3 p-3 border border-secondary rounded">
                    <h2>Create submission</h2>
                    <%@include file="../template/alert.html" %>
                    <div id="divForm">
                        <form action="javascript:void(0);" id="formInsert" novalidate>
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label for="inputTitle"><strong>Title</strong></label>
                                    <input id="inputTitle" type="text" class="form-control" required>
                                    <div class="invalid-feedback">
                                        Invalid title
                                    </div>
                                </div>
                            </div>
                            <div class="form-row mb-3">
                                <div class="input-group col-md-6">
                                    <div class="input-group-prepend">
                                        <label class="input-group-text" for="inputDiff">
                                            <i class="fas fa-level-up-alt pr-1"></i>Difficulty</label>
                                    </div>
                                    <select class="custom-select" id="inputDiff">
                                        <option value="Easy" selected>Easy</option>
                                        <option value="Medium">Medium</option>
                                        <option value="Hard">Hard</option>
                                        <option value="Extreme">Extreme</option>
                                    </select>
                                </div>

                                <div class="input-group col-md-6 mt-3 mt-md-0">
                                    <div class="input-group-prepend">
                                        <label class="input-group-text" for="inputCat">
                                            <i class="fas fa-certificate pr-1"></i>Category</label>
                                    </div>
                                    <select class="custom-select" id="inputCat">
                                        <!-- Autogen -->
                                    </select>
                                </div>
                            </div>
                            <div class="form-row mb-3">
                                <label for="inputContent"><strong>Content</strong></label>
                                <textarea id="inputContent" rows="5" class="form-control" required></textarea>
                                <div class="invalid-feedback">
                                    Please fill this field
                                </div>
                            </div>
                            <div class="form-row mb-3">
                                <label for="inputSolution"><strong>Solution</strong></label>
                                <textarea id="inputSolution" rows="5" class="form-control"></textarea>
                            </div>
                            <div class="form-row">
                                <label for="inputTagList"><strong>Tag list</strong></label>
                                <input type="text" data-role="tagsinput" id="inputTagList" class="form-control">
                            </div>
                        </form>
                    </div>
                    <div id="divPreview">
                        <div class="my-2 border-top border-bottom">
                            <h3 id="prevTitle"></h3>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-6">
                                <strong>Difficulty:</strong>
                                <span id="prevDiff"></span>
                            </div>
                            <div class="col-md-6">
                                <strong>Category:</strong>
                                <span id="prevCat"></span>
                            </div>
                        </div>
                        <div class="py-2 border-top"><h4>Content:</h4></div>
                        <div class="row">
                            <div id="prevCont" class="col-md-12 mb-2"></div>
                        </div>
                        <div class="py-2"><h4>Solution:</h4></div>
                        <div class="row">
                            <div id="prevSol" class="col-md-12"></div>
                        </div>
                        <div class="mt-3 py-2 border-bottom border-top">
                            <strong>Tags: </strong>
                            <span id="prevTags"></span>
                        </div>
                    </div>
                    <div class="form-row">
                        <button id="btnFormPreviewSub" class="btn btn-secondary mt-3">Preview</button>
                        <button id="btnFormCreateSub" class="btn btn-secondary mt-3 ml-3">Create submission</button>
                    </div>

                </div>
            </div>
        </div>
        <%@include file="../template/sidebar.html" %>
    </div>
</main>
<%@include file="../template/footer.html" %>
<%@include file="../template/modal.html" %>
<%@include file="../template/links-js.html" %>
<script type="text/javascript" src="../js/tagsinput.js"></script>
<script type="text/javascript" src="../js/createSubmission.js"></script>
</body>
</html>