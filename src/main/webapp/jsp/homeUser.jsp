<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <%@include file="../template/meta.html" %>
    <%@include file="../template/links-css.html" %>
    <script type="text/javascript" src="../js/sessionTransfer.js"></script>
    <script type="text/javascript" src="../js/checkLogged.js"></script>
    <script>checkLogged("home.jsp");</script>
    <title>HeapQuestions - Edit User</title>
</head>
<body>
<%@include file="../template/navbar.html" %>

<!-- Content -->
<main role="main" class="container-fluid">
    <div class="row">
        <div id="mainContent" class="col-xs-12 col-md-9">
            <div class="d-flex flex-column justify-content-center full-height">
                <div class="loader mx-auto" id="loader"></div>
                <div id="contentBody" class="mx-4 my-3 p-4 border border-secondary d-none rounded">
                    <div class="row p-3">
                        <div class="mr-3">
                            <img id="userPicture" class="img img-thumbnail" width="100" height="100"/>
                        </div>
                        <div>
                            <h2 class="pb-2">Hello, <strong><span id="helloUsername"></span></strong></h2>
                            <a id="btnProfile" class="btn btn-secondary mr-2" href="editUser.jsp">
                                <i class="fas fa-user-cog"></i>
                                <span class="d-inline d-md-none d-lg-inline">Edit Profile</span>
                            </a>
                            <a id="btnNotes" class="btn btn-secondary" href="list-note?token=">
                                <i class="fas fa-sticky-note"></i>
                                <span class="d-inline d-md-none d-lg-inline">Open your Notes</span>
                            </a>
                        </div>
                    </div>
                    <div class="row px-3 pb-3"><strong>Registered since: </strong>
                        <span class="ml-2" id="userSince"></span></div>
                    <div class="row px-3 pb-2"><strong>Name: </strong>
                        <span class="ml-2" id="userName"></span></div>
                    <div class="row px-3 pb-2"><strong>Surname: </strong>
                        <span class="ml-2" id="userSurname"></span></div>
                    <div class="row px-3 pb-2"><strong>E-mail address: </strong>
                        <span class="ml-2" id="userMail"></span></div>
                    <div class="row px-3 pb-2"><strong>Location: </strong>
                        <span class="ml-2" id="userLocation"></span></div>

                    <div class="row p-3">
                        <h3> Recent activities: </h3>
                    </div>
                    <div class="row">
                        <div class="mb-3 col-12 col-lg-6">
                            <div id="cardUserFQ" class="card">
                                <div class="card-header">Recent user questions</div>
                                <div id="cardUserFQBody" class="card-body">
                                    <div class="loader mx-auto my-2" id="loaderUserFQ"></div>
                                    <div id="emptyUserFQList" class="alert alert-info d-none mb-0">
                                        You have no recent forum questions!
                                    </div>
                                </div>
                                <ul id="cardUserFQList" class="list-group list-group-flush"></ul>
                            </div>
                        </div>
                        <div class="mb-3 col-12 col-lg-6">
                            <div id="cardUserSub" class="card">
                                <div class="card-header">Recent user submissions</div>
                                <div id="cardUserSubBody" class="card-body">
                                    <div class="loader mx-auto my-2" id="loaderUserS"></div>
                                    <div id="emptyUserSubList" class="alert alert-info d-none mb-0">
                                        You have no recent submissions!
                                    </div>
                                </div>
                                <ul id="cardUserSubList" class="list-group list-group-flush"></ul>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="mb-3 col-12">
                            <div id="cardUserComm" class="card">
                                <div class="card-header">Recent user comments</div>
                                <div id="cardUserCommBody" class="card-body">
                                    <div class="loader mx-auto my-2" id="loaderUserC"></div>
                                    <div id="emptyUserCommList" class="alert alert-info d-none mb-0">
                                        You have no recent comments!
                                    </div>
                                </div>
                                <ul id="cardUserCommList" class="list-group list-group-flush"></ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%@include file="../template/sidebar.html" %>
    </div>
</main>

<%@include file="../template/footer.html" %>
<%@include file="../template/modal.html" %>
<%@include file="../template/links-js.html" %>
<script type="text/javascript" src="../webjars/js-sha256/0.7.1/build/sha256.min.js"></script>
<script src="../js/homeUser.js"></script>
</body>
</html>