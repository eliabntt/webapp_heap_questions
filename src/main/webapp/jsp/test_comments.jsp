<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <%@include file="../template/meta.html" %>
    <%@include file="../template/links-css.html" %>
    <title>HeapQuestions - Test Comments</title>
</head>
<body>
    <!-- Content -->
    <main role="main" class="container-fluid">
        <div class="row">
            <div id="comment_area" class="col-xs-12 col-md-9">
                <div id="comment_input">
                <div class="form-group">
                    <label for="comment_content">Comment:</label>
                    <textarea class="form-control" rows="5" id="comment_content"></textarea>
                    <button type="button" class="btn my-2" id="send" value="send"/>Send</div>
                </div>
                <div id="comments_notLogged" class="alert alert-info">
                    <strong>You are not logged!</strong><br/>Please <a href="/jsp/login.jsp">login</a> to comment!
                </div>
                <div id="comments_alert" class="alert alert-danger"></div>
                <div id="commentToAnswer">
                    <label for="comment_answerTo">In response of:</label>
                    <div id="comment_answerTo">
                    </div>
                    <button type="button" class="btn btn-danger my-2" id="cancel" value="cancel">Cancel Answer</button>
                </div>
            </div>
        </div>
        <div class="row">
            <div id="commentsList" class="col-xs-12 col-md-9 py-2 border-top">
            </div>
        </div>
        <div class="mx-0">
            <button id="moreComments" class="btn" onclick="fetchComments(0, true);">Load other comments</button>
        </div>
    </main>
<%@include file="../template/footer.html" %>
<%@include file="../template/modal.html" %>
<%@include file="../template/links-js.html" %>
<script src="../js/comments.js"></script>
</body>
</html>