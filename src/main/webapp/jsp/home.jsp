<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <%@include file="../template/meta.html" %>
    <%@include file="../template/links-css.html" %>
    <link rel="stylesheet" href="../css/home-bg.css">
    <script type="text/javascript" src="../js/sessionTransfer.js"></script>
    <script type="text/javascript" src="../js/checkLogged.js"></script>
    <title>HeapQuestions</title>
</head>
<body>
<%@include file="../template/navbar.html" %>

<!-- Content -->
<main role="main" class="container-fluid">
    <div class="row">
        <div id="mainContent" class="col-xs-12 col-md-9">
            <div class="d-flex flex-column justify-content-around full-height text-center">
                <div class="col-md-8 offset-md-2 my-5">
                    <h1 class="display-5">Welcome to HeapQuestions!</h1>
                    <p class="lead font-weight-normal">Success is where preparation and opportunity meet.</p>
                </div>
                <div class="d-md-flex flex-md-equal mx-5 mb-5">
                    <div class="bg-dark p-2 p-md-3 mr-md-3 rounded text-white overflow-hidden" id="divSub">
                        <div class="my-4">
                            <h2 class="display-5">Submissions</h2>
                            <p class="lead">Prepare for your upcoming job interview by learning what the most
                                influential companies ask</p>
                        </div>
                    </div>
                    <div class="bg-light p-2 p-md-3 mt-5 mt-md-0 rounded overflow-hidden" id="divQue">
                        <div class="my-4">
                            <h2 class="display-5">Questions</h2>
                            <p class="lead">Refine yor knowledge by asking and answering questions on our forum</p>
                        </div>
                    </div>
                </div>
                <div class="mb-3 d-none">
                    <span id="spanLast" class="text-white px-1 rounded">Made with <i class="fab fa-java"></i> by The StackOverflowers team.</span>
                </div>
            </div>
        </div>
        <%@include file="../template/sidebar.html" %>
    </div>
</main>

<%@include file="../template/footer.html" %>
<%@include file="../template/modal.html" %>
<%@include file="../template/links-js.html" %>

<script>
    $("#divSub").click(function () {
        window.location.replace("submission.jsp");
    }).css("cursor", "pointer");
    $("#divQue").click(function () {
        window.location.replace("forum.jsp");
    }).css("cursor", "pointer");
</script>

</body>
</html>