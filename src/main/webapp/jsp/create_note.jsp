<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <%@include file="../template/meta.html" %>
    <%@include file="../template/links-css.html" %>
    <script type="text/javascript" src="../js/sessionTransfer.js"></script>
    <script type="text/javascript" src="../js/checkLogged.js"></script>
    <script>checkLogged("home.jsp");</script>
    <title>HeapQuestions - Create Note</title>
</head>
<body>
<%@include file="../template/navbar.html" %>

<!-- Content -->
<main role="main" class="container-fluid">
    <div class="row">
        <div id="mainContent" class="col-xs-12 col-md-9">
            <div class="d-flex flex-column justify-content-center full-height">
                <div id="contentBody" class="col-12 col-md-10 offset-md-1 col-xl-8 offset-xl-2 my-3 p-3 border border-secondary rounded">
                    <h2>Create Note</h2>
                    <form method="POST" action="<c:url value="/jsp/create-note"/>">
                        <div class="form-row">
                            <div class="col-12 mt-3">
                                <label for="content">Content:</label>
                                <textarea id="content" name="content" rows="5" class="form-control" required></textarea>
                                <input type="hidden" name="token" id="token">
                            </div>
                            <div class="col-12 mt-3">
                                <button type="submit" id="post" class="btn btn-secondary mr-3">Create</button>
                                <button type="reset" class="btn btn-secondary">Reset</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <%@include file="../template/sidebar.html" %>
    </div>
</main>
<%@include file="../template/footer.html" %>
<%@include file="../template/modal.html" %>
<%@include file="../template/links-js.html" %>
<script type="text/javascript" src="../js/notes.js"></script>
</body>
</html>
