$(document).ready(function () {
    // note card display
    if (token === null || token === undefined)
        $("#cardNote").addClass("d-none");
    else
        $("#cardNote a:first").attr("href", $("#cardNote a:first").attr("href") + token);

    // submission card population with modal trigger for net issues
    $.ajax({
        type: "GET",
        url: "../rest/submission",
        data: {
            from: 1,
            howMany: 5
        },
        contentType: "application/json; charset=utf-8",
        timeout: 5000
    }).done(function (data) {
        let container = $("#cardSubList");
        container.empty();
        let list = data["resource-list"];
        $.each(list, function (i, v) {
            let li = $("<li></li>").addClass("list-group-item");
            $("<a></a>")
                .attr("href", "submission.jsp?id=" + v.submission.id)
                .text(v.submission.title)
                .appendTo(li);
            li.appendTo(container);
        });

        $("#loaderS").removeClass("d-block").addClass("d-none");
        $("#contentBodyS").removeClass("d-none").addClass("d-block");
    }).fail(function (data, reason) {
        let modal = $("#modalTimeout");
        if (reason === "timeout" && !modal.hasClass("show")) {
            // connection error
            $("#loaderS").removeClass("d-block").addClass("d-none");
            $("#loaderFQ").removeClass("d-block").addClass("d-none");
            $("#cardSubList").empty();
            $("#cardFQList").empty();
            $("#navbarCollapse").remove();
            $("#cardNote").hide();
            $("#btnAddFq").hide();
            $("#btnAddSub").hide();
            $("#mainContent").empty();
            modal.modal("show");
        } else {
            $("#loaderS").removeClass("d-block").addClass("d-none");
            let alert = $("<div></div>").addClass("alert alert-danger mt-3 mx-3");
            if (data.responseJSON) {
                data = data.responseJSON;
                alert.html("<strong>Error </strong>" + data.message.errorCode + ": " + data.message.message);
            } else
                alert.html("<strong>Error</strong> retrieving the list.");
            alert.appendTo($("#contentBodyS"));
            $("#contentBodyS").removeClass("d-none").addClass("d-block");
            $("#cardSubList").empty();
        }
    });

    // fq card population
    $.ajax({
        type: "GET",
        url: "../rest/forum",
        data: {
            from: 1,
            howMany: 5
        },
        contentType: "application/json; charset=utf-8",
        timeout: 5000
    }).done(function (data) {
        let container = $("#cardFQList");
        container.empty();
        let list = data["resource-list"];
        $.each(list, function (i, v) {
            let li = $("<li></li>").addClass("list-group-item");
            $("<a></a>")
                .attr("href", "forum.jsp?id=" + v.forumQuestion.id)
                .text(v.forumQuestion.title)
                .appendTo(li);
            li.appendTo(container);
        });

        $("#loaderFQ").removeClass("d-block").addClass("d-none");
        $("#contentBodyFQ").removeClass("d-none").addClass("d-block");
    }).fail(function (data, reason) {
        let modal = $("#modalTimeout");
        if (reason === "timeout" && !modal.hasClass("show")) {
            // connection error
            $("#loaderS").removeClass("d-block").addClass("d-none");
            $("#loaderFQ").removeClass("d-block").addClass("d-none");
            $("#cardSubList").empty();
            $("#cardFQList").empty();
            $("#navbarCollapse").remove();
            $("#cardNote").hide();
            $("#btnAddFq").hide();
            $("#btnAddSub").hide();
            $("#mainContent").empty();
            modal.modal("show");
        } else {
            $("#loaderFQ").removeClass("d-block").addClass("d-none");
            let alert = $("<div></div>").addClass("alert alert-danger mt-3 mx-3");
            if (data.responseJSON) {
                data = data.responseJSON;
                alert.html("<strong>Error </strong>" + data.message.errorCode + ": " + data.message.message);
            } else
                alert.html("<strong>Error</strong> retrieving the list.");
            alert.appendTo($("#contentBodyFQ"));
            $("#contentBodyFQ").removeClass("d-none").addClass("d-block");
            $("#cardFQList").empty();
        }
    });

    $("#btnModalReload").click(function () {
        window.location.reload();
    })
});