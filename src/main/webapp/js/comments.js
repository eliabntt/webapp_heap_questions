/* Comment library for HeapQuestions
   (C) 2018 - The StackOverflowers team

  USEFUL VARIABLES:
     - comment_fromThread: keeps track, for every parent comment,
       of the last index of the last comment visualized.
     - userDB: keeps data (username & profile pic) of every user who commented
     - userIds: keeps the list of userIds involved into all comments
        (is used to populate userDB)
     - comment_answerTo: keeps the id of the comment that we want to reply
     - comment_resourceId: keeps the id of the submission/forumQuestion
     - comment_resourceType: "submission" or "question", according to REST API.
     - isAdmin: indicates if the logged user is an admin
     - ajaxInProgress: counter to keep track of running AJAX requests (useful if we
        want to visualize a loader)
 */

var comment_fromThread = {
    "parent-0": 1 //the parent of every comment is (for convention) comment 0
};

var userDB = {};
var userIds = [];

var comment_answerTo;
var comment_resourceId;
var comment_resourceType;
var comment_resourceCreatorId = null;
var comment_resourceClosed = false;

var isAdmin = false;

var ajaxInProgress = 0;

/*
    LOADER FUNCTIONS:
        - showLoader: shows the loader at the FIRST AJAX call.
        - hideLoader: hides the loader at the LAST AJAX call.
 */

function showLoader() {
    if (ajaxInProgress === 0) {
        $("#loader").show();
    }
}

function hideLoader() {
    if (ajaxInProgress === 0) {
        $("#loader").hide();
        $("#comment_area").removeClass("d-none");
    }
}

/*
    BUTTON FUNCTIONS:
        - changeAnswer: it keeps track of the id of the comment that we want
            to reply and clone it just after the form to submit a comment.
        - deleteComment: changes the comment state to "Deleted" and obscures
           controls for that comment.
 */

function changeAnswer(id) {
    comment_answerTo = id;
    var msgToClone = $("#msg-content-" + id).clone();
    var dest = $("#comment_answerTo");
    dest.empty();
    msgToClone.appendTo(dest);
    $("#commentToAnswer").show();
    $('html, body').animate({
        scrollTop: $("#comment_area").offset().top - 100
    }, 500);
}

function getAnswer(id) {
    $("#comment_input").hide();
    $("#comments_notLogged").hide();
    $.ajax({
        type: "GET", url: "../rest/forum/" + id + "/answer",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            $("#comment_answer_content")
                .append(parseSingleComment(data, false));

            $("#comment_answer_box").removeClass("d-none");
        }
    });
}

function deleteComment(id) {
    showLoader();
    ajaxInProgress++;

    var token = localStorage.token;
    if (token === undefined || token === null)
        token = sessionStorage.token;

    $.ajax({
        type: "DELETE", url: "../rest/comment/" + id,
        beforeSend: function (request) {
            request.setRequestHeader("X-TSO-Auth-Token", token);
        },
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({
            "comment": {
                "state": "Deleted",
            }
        })
    }).done(function (data) {
        $("#comment_answerTo").empty();
        $("#comment-content-text-" + data.comment.id).html("<em>Deleted message</em>");
        $("#comment-footer-" + data.comment.id).remove();
        ajaxInProgress--;
        hideLoader();
    }).fail(function (data) {
        parseErrorMessage(data);
        ajaxInProgress--;
        hideLoader();
    });
}

function markAsAnswer(id) {
    showLoader();
    ajaxInProgress++;

    var token = localStorage.token;
    if (token === undefined || token === null)
        token = sessionStorage.token;

    $.ajax({
        type: "PUT", url: "../rest/comment/" + id + "/isAnswer",
        beforeSend: function (request) {
            request.setRequestHeader("X-TSO-Auth-Token", token);
        },
        contentType: "application/json; charset=utf-8",
        dataType: "json"
    }).done(function () {
        //faster to reload everything instead of edit every comment!
        location.reload(true);
    }).fail(function (data) {
        parseErrorMessage(data);
        ajaxInProgress--;
        hideLoader();
    });
}

/*
    USER-COMMENT FUNCTIONS:
        - mapUser: inserts (if not present) the userId into userIds
        - parseUsers: gets, for every user involved in the conversation, username
           and profile picture and stores them in userDB.
 */

function mapUser(id) {
    if ($.inArray(id, userIds) < 0)
        if (id > 0)
            userIds.push(id);
}

function parseUsers() {
    $.each(userIds, function (i, userId) {
        if (userDB["user-" + userId] === undefined || userDB["user-" + userId] === null) {
            showLoader();
            ajaxInProgress++;

            $.ajax({
                type: "GET", url: "../rest/user/" + userId
            }).done(function (data) {
                userDB["user-" + userId] = {
                    username: data.user.username,
                    picture: data.user.profilePicture
                };

                $.each($(".user-" + userId), function (i, v) {

                    var tag = "user-" + userId;
                    v.innerText = userDB[tag].username;

                });
                if (userDB["user-" + userId].picture !== null && userDB["user-" + userId].picture != "null") {
                    $.each($(".picture-" + userId), function (i, v) {

                        let stringPic = "data:image/jpeg;charset=utf-8;base64," + userDB["user-" + userId].picture;
                        v.src = stringPic;
                    });
                }
                ajaxInProgress--;
                hideLoader();
            }).fail(function (data) {
                parseErrorMessage(data);
                ajaxInProgress--;
                hideLoader();
            });
        }
        else {
            $.each($(".user-" + userId), function (i, v) {
                var tag = "user-" + userId;
                v.innerText = userDB[tag].username;
            });
            if (userDB["user-" + userId].picture !== null && userDB["user-" + userId].picture != "null") {
                $.each($(".picture-" + userId), function (i, v) {
                    let stringPic = "data:image/jpeg;charset=utf-8;base64," + userDB["user-" + userId].picture;
                    v.src = stringPic;
                });
            }
        }
    });
}

/*
    VOTE FUNCTIONS:
        - parseVote: sets up/downVote to a specific comment and disables the just-used button.
 */
function parseVote(commentId, upVote) {
    showLoader();
    ajaxInProgress++;
    $.ajax({
        type: "GET", url: "../rest/comment/" + commentId,
        dataType: "json",
        data: {
            upVote: upVote
        }
    }).done(function (data) {
        comment_answerTo = null;
        $("#comment_answerTo").empty();
        $("#commentToAnswer").hide();

        if (upVote) {
            $("#upVotes-" + data.comment.id).text(data.comment.upVote);
            $("#upVotesButton-" + data.comment.id).attr("disabled", "disabled");
        }
        else {
            $("#downVotes-" + data.comment.id).text(data.comment.downVote);
            $("#downVotesButton-" + data.comment.id).attr("disabled", "disabled");
        }
        ajaxInProgress--;
        hideLoader();
    }).fail(function (data) {
        parseErrorMessage(data);
        ajaxInProgress--;
        hideLoader();
    });
}

/*
    COMMENTS FUNCTIONS:
        - parseSingleComment: creates the whole comment's html/css/js structure.
        - insertComment: inserts the structured comment into the comments' feed.
            Every comment has its content div and its childs' div.
        - parseComment: uses the 2 functions above to create the comments' feed.
 */

function parseSingleComment(comment, showVotes) {
    var commentCard = $('<div class = "card bg-light text-dark"></div>');
    var cardBody = $('<div class="card-body"></div>');
    var flexDiv = $('<div class="d-flex flex-fill"></div>');
    var rightDiv = $('<div class = "bd-highlight pr-4 border-right"></div>');

    var userName = $('<h5 class="user-' + comment.comment.userId + '"></h5>');
    userName.append("").appendTo(rightDiv);

    var picture = $('<img width="100" height="100" src="../img/default_avatar.png" class="img-thumbnail picture-' + comment.comment.userId + '" />');
    picture.appendTo(rightDiv);

    /* OLD IMPLEMENTATION OF COMMENT CARD TO HAVE UP/DOWN VOTE UNDER THE PROFILE IMAGE.
    if(showVotes) {
        var upVotesDiv = $("<div class='my-2'></div>");

        var spanIconUpVote = $("<span></span>");
        spanIconUpVote.append("<button class='btn mr-2' id='upVotesButton-" + comment.comment.id + "'onclick='parseVote(" + comment.comment.id + ",true)'><i class=\"fas fa-arrow-up\"></i></button>")
            .appendTo(upVotesDiv);

        var spanUpVotes = $("<span id='upVotes-" + comment.comment.id + "'></span>");
        spanUpVotes.append(comment.comment.upVote)
            .appendTo(upVotesDiv);

        upVotesDiv.appendTo(rightDiv);

        var downVotesDiv = $("<div class='my-2'></div>");

        var spanIconDownVote = $("<span></span>");
        spanIconDownVote.append("<button class='btn mr-2' id='downVotesButton-" + comment.comment.id + "'onclick='parseVote(" + comment.comment.id + ",false)'><i class=\"fas fa-arrow-down\"></i></button>")
            .appendTo(downVotesDiv);

        var spanDownVotes = $("<span id='downVotes-" + comment.comment.id + "'></span>");
        spanDownVotes.append(comment.comment.downVote)
            .appendTo(downVotesDiv);

        downVotesDiv.appendTo(rightDiv);
    }*/

    rightDiv.appendTo(flexDiv);
    var commentContentDiv = $('<div id="comment-content-text-' + comment.comment.id + '" class="flex-grow-1 bf-highlight pl-4"></div>');
    commentContentDiv.append(comment.comment.content).appendTo(flexDiv);
    flexDiv.appendTo(cardBody);
    cardBody.appendTo(commentCard);

    var cardFooter = $('<div id="comment-footer-' + comment.comment.id + '" class="card-footer align-middle"></div>');
    var dateTime = comment.comment.dateTime;
    var _span = $("<span class='float-left d-none d-lg-block my-1'>" + dateTime.substring(0, dateTime.lastIndexOf('.')) + "</span>");
    cardFooter.append(_span);

    if (showVotes && !comment_resourceClosed) {
        var loggedUserId = localStorage.user;
        if (loggedUserId === null || loggedUserId === undefined)
            loggedUserId = sessionStorage.user;

        if ((comment_resourceCreatorId !== null && parseInt(comment_resourceCreatorId) === parseInt(loggedUserId)))
            cardFooter.append('<button class="float-right btn btn-success ml-2" onclick="markAsAnswer(' + comment.comment.id + ');"><i class="fas fa-check-circle"></i></button>');

        if (parseInt(comment.comment.userId) === parseInt(loggedUserId) || isAdmin === true || isAdmin === "true")
            cardFooter.append('<button class="float-right btn btn-danger ml-2" onclick="deleteComment(' + comment.comment.id + ');"><i class="fas fa-trash-alt"></i></button>');
        cardFooter.append('<button class="float-right btn btn-dark ml-2" onclick="changeAnswer(' + comment.comment.id + ');"><i class="fas fa-reply"></i></button>');

        var spanDownVotes = $("<span class='float-right my-1 mx-2' id='downVotes-" + comment.comment.id + "'></span>");
        spanDownVotes.append(comment.comment.downVote)
            .appendTo(cardFooter);

        cardFooter.append("<button class='float-right btn' id='downVotesButton-" + comment.comment.id + "'onclick='parseVote(" + comment.comment.id + ",false)'><i class=\"fas fa-arrow-down\"></i></button>");

        var spanUpVotes = $("<span class='float-right my-1 mx-2 ' id='upVotes-" + comment.comment.id + "'></span>");
        spanUpVotes.append(comment.comment.upVote)
            .appendTo(cardFooter);

        cardFooter.append("<button class='float-right btn' id='upVotesButton-" + comment.comment.id + "'onclick='parseVote(" + comment.comment.id + ",true)'><i class=\"fas fa-arrow-up\"></i></button>");
    }

    cardFooter.appendTo(commentCard);

    if (comment.comment.state === "Deleted") {
        commentContentDiv.html("<em>Deleted message</em>");
        cardFooter.remove();
    }

    return commentCard;
}

function insertComment(v, showVoting, parseChildren) {
    var msgDiv = $('<div id="msg-' + v.comment.id + '"></div>');
    var msgContentDiv = $('<div id="msg-content-' + v.comment.id + '" class="my-2"></div>');

    var parentComment = parseInt(v.comment.parentComment);

    //search for right div
    if (parentComment === undefined || parentComment === null)
        parentComment = 0;

    var msgChildren = $('<div id="childrenOf-' + v.comment.id + '" class="ml-4 my-2"></div>');

    comment_fromThread["parent-" + v.comment.id] = 1;
    var comment = parseSingleComment(v, showVoting);

    comment.appendTo(msgContentDiv);
    msgContentDiv.appendTo(msgDiv);
    msgChildren.appendTo(msgDiv);

    var destinationContainer = $("#childrenOf-" + parentComment);
    var childList = $("#childrenOf-" + parentComment).children();

    var lastChild = undefined;
    var maxNum = 0;

    for (var i = 0; i < childList.length; i++) {
        if (childList.eq(i).attr("id") !== undefined) {
            var msgIdx = parseInt(childList.eq(i).attr("id").substring(4, childList.eq(i).attr("id").length));
            if (msgIdx > maxNum) {
                maxNum = msgIdx;
                lastChild = childList.eq(i);
            }
        }
    }

    if (lastChild !== undefined && destinationContainer.children().length > 0) {
        if (parseInt(maxNum) > parseInt(v.comment.id))
            lastChild.before(msgDiv);
        else {
            if (parseInt(maxNum) !== parseInt(v.comment.id))
                msgDiv.appendTo(destinationContainer);
        }
    } else {
        msgDiv.appendTo(destinationContainer);
    }

    if (parseChildren)
        getComments(comment_resourceId, comment_resourceType, v.comment.id, false);
}

function parseComments(data, parent) {
    var token = localStorage.token;
    if (token === null || token === undefined)
        token = sessionStorage.token;

    var container = $("#commentsList");
    var list = data["resource-list"];

    var root = $("#childrenOf-0").val();

    if (root === undefined || root === null)
        $('<div id="childrenOf-0"></div>').appendTo(container);

    $.each(list, function (i, v) {
        if (token === null || token === undefined) {
            insertComment(v, false, true);
        } else {
            insertComment(v, true, true);
        }
        mapUser(parseInt(v.comment.userId));
    });

    if (list.length > 1) {
        comment_fromThread["parent-" + parent] = parseInt(comment_fromThread["parent-" + list[0].comment.parentComment]) + list.length;

        if (list.length === 20)
            $("#replies-" + parent).show();
    }
    else
        $("#replies-" + parent).hide();

    if (list.length < 20)
        $("#moreComments").hide();
}

/*
    ERROR FUNCTIONS:
        - parseErrorMessage: creates a danger warning with the error occurred.
 */


function parseErrorMessage(data) {
    data = data.responseJSON;
    $("#comments_alert")
        .html("<strong>Error:</strong>" + data.message.errorCode + "<br/>" + data.message.message)
        .removeClass("d-none")
        .addClass("d-block");
    $('html, body').animate({
        scrollTop: $("#comments_alert").offset().top - 100
    }, 500);
}

/*
    COMMENTS AJAX FUNCTIONS:
        - getComments: obtain comments from REST API.
 */

function getComments(id, resType, parent, showComments) {
    $("#comments_alert").removeClass("d-block").addClass("d-none");

    showLoader();
    ajaxInProgress++;

    $.ajax({
        type: "GET", url: "../rest/" + resType + "/" + id + "/comment",
        data: {
            from: parseInt(comment_fromThread["parent-" + parent]),
            howMany: 20,
            parentComment: parseInt(parent)
        }
    }).done(function (data) {
        if (showComments) {
            parseComments(data, parent);
            parseUsers();
        }
        if (data["resource-list"].length > 0) {
            if ($("#replies-" + parent).length === 0) {
                $("#msg-content-" + parent).append('<button id="replies-' + parent + '" class="btn btn-dark mt-2" onclick="fetchComments(' + parent + ',true);"><i class="fas fa-comments"></i> Replies</button>');
                $("#replies-" + parent).show();
            }
            else {
                if (data["resource-list"].length < 20)
                    $("#replies-" + parent).hide();
            }
        }
        else {
            $("#replies-" + parent).hide();
        }

        ajaxInProgress--;
        hideLoader();
    }).fail(function (data) {
        parseErrorMessage(data);
        ajaxInProgress--;
        hideLoader();
    });
}

/*
    USER AJAX FUNCTIONS:
        - checkAdminPrivileges: checks if the logged user is admin.
 */

function checkAdminPrivileges(token, userId) {
    showLoader();
    ajaxInProgress++;

    $.ajax({
        type: "GET", url: "../rest/user/" + userId,
        beforeSend: function (request) {
            request.setRequestHeader("X-TSO-Auth-Token", token);
        },
        dataType: "json",
        async: false
    }).done(function (data) {
        isAdmin = data.user.isAdmin;

        ajaxInProgress--;
        hideLoader();
    }).fail(function (data) {
        parseErrorMessage(data);

        ajaxInProgress--;
        hideLoader();
    });
}

/*
    SETUP ROUTINES:
        - prepareCommentsEnvironment: sets up basic variables in order to make
            the library working.
        - runCommentsLibrary: the library's entrypoint to make it work.
 */


function prepareCommentsEnvironment(resourceType, resourceCreatorId, resourceClosed) {
    var token = localStorage.token;
    if (token === null || token === undefined)
        token = sessionStorage.token;

    var userId = localStorage.user;
    if (userId === null || userId === undefined)
        userId = sessionStorage.user;

    if (token === null || token === undefined) {
        $("#comments_notLogged").show();
        $("#comment_input").hide();
    } else {
        $("#comments_notLogged").hide();
        checkAdminPrivileges(token, userId);
    }

    $("#commentToAnswer").hide();

    $("#commentsList").empty();
    var queryDict = {};

    location.search.substr(1).split("&").forEach(function (item) {
        queryDict[item.split("=")[0]] = item.split("=")[1]
    });

    comment_resourceId = queryDict.id;
    comment_resourceType = resourceType;

    if (resourceType === "forum" && resourceCreatorId !== undefined && resourceCreatorId !== null) {
        comment_resourceCreatorId = parseInt(resourceCreatorId);
    }

    if (resourceClosed !== undefined && resourceClosed !== null && resourceClosed !== false && resourceClosed !== "false") {
        comment_resourceClosed = resourceClosed;
        if (resourceClosed && comment_resourceCreatorId !== null)
            getAnswer(parseInt(comment_resourceId));
    }
}

function runCommentsLibrary(resourceType, resourceCreatorId, resourceClosed) {
    $("#comments_notLogged").hide();
    $("#comments_alert").hide();

    prepareCommentsEnvironment(resourceType, resourceCreatorId, resourceClosed);
    fetchComments(0, true);

    $('#cancel').click(function () {
        comment_answerTo = null;
        $("#commentToAnswer").hide();
    });
    $('#send').click(function () {
        postComment(comment_resourceType, comment_resourceId);
    });
}

/*
    HELPER FUNCTIONS:
        - fetchComments: uses getComments to obatain comments' list and populate
            (if required) the comments'feed.
        - postComment: creates a new comment relying to a certain parent comment.
 */

function fetchComments(parentComment, show) {
    getComments(parseInt(comment_resourceId), comment_resourceType, parentComment, show);
}

function postComment(type, id) {
    $("#comments_alert").removeClass("d-block").addClass("d-none");

    var content = $("#comment_content");
    content.removeClass("is-invalid").removeClass("is-valid");

    if (content.val() === "") {
        content.removeClass("is-valid").addClass("is-invalid");
    } else {
        var token = localStorage.token;
        if (token === undefined || token === null)
            token = sessionStorage.token;

        var postJSON = {
            "comment": {
                "content": content.val()
            }
        };

        if (comment_answerTo !== undefined && comment_answerTo !== null) {
            postJSON.comment.parentComment = parseInt(comment_answerTo);
            //fetchComments(parseInt(postJSON.comment.parentComment), true);
        }

        showLoader();
        ajaxInProgress++;

        $.ajax({
            type: "POST", url: "../rest/" + type + "/" + id + "/comment",
            beforeSend: function (request) {
                request.setRequestHeader("X-TSO-Auth-Token", token);
            },
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify(postJSON),
        }).done(function (data) {
            mapUser(data.comment.userId);
            //comment_fromThread["parent-" + data.comment.parentComment] = parseInt(comment_fromThread["parent-" + data.comment.parentComment]) + 1;
            insertComment(data, true, false);
            parseUsers();
            comment_answerTo = null;
            content.val("");
            $("#commentToAnswer").hide();
            content.removeClass("is-invalid").removeClass("is-valid");
            ajaxInProgress--;
            hideLoader();

            $('html, body').animate({
                scrollTop: $("#msg-" + data.comment.id).offset().top - 550
            }, 500);
        }).fail(function (data) {
            content.removeClass("is-invalid").removeClass("is-valid");
            parseErrorMessage(data);
            ajaxInProgress--;
            hideLoader();
        });
    }
}