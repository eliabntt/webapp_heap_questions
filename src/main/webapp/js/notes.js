$(document).ready(function () {
    var token = localStorage.token;
    if (token === undefined || token === null)
        token = sessionStorage.token;
    if (token !== undefined && token !== null)
        $("#token").val(token);
});