var uid = localStorage.user;
if (uid === undefined || uid === null)
    uid = sessionStorage.user;

function loadUserJSON(data) {
    $("#helloUsername").html(data.user.username);
    $("#userSince").html(data.user.joinDate);
    $("#userMail").html(data.user.email);
    $("#userName").html(data.user.name);
    $("#userSurname").html(data.user.surname);
    $("#userLocation").html(data.user.location);

    let imageUrl = loadImage(data.user.profilePicture);
    $("#userPicture").attr("src", imageUrl);
}

function loadImage(picture) {
    if (picture === null || picture === undefined || picture === "null" || picture === "undefined")
        return "../img/default_avatar.png";
    else
        return "data:image/jpeg;charset=utf-8;base64," + picture;
}

function loadRecentSubmissions() {
    $.ajax({
        type: "GET",
        url: "../rest/user/" + uid + "/submission",
        data: {
            from: 1,
            howMany: 5
        },
        contentType: "application/json; charset=utf-8",
    }).done(function (data) {
        let container = $("#cardUserSubList");
        container.empty();

        // populate container from list
        let list = data["resource-list"];
        $.each(list, function (i, v) {
            let li = $("<li></li>").addClass("list-group-item");
            $("<a></a>")
                .attr("href", "submission.jsp?id=" + v.submission.id)
                .text(v.submission.title)
                .appendTo(li);
            li.appendTo(container);
        });

        // show or delete components
        $("#loaderUserS").remove();
        if (list.length === 0)
            $("#emptyUserSubList").removeClass("d-none").addClass("d-block");
        else
            $("#cardUserSubBody").remove();
    }).fail(function (data) {
        data = data.responseJSON;
        $("#emptyUserSublist")
            .removeClass("alert-info").addClass("alert-danger")
            .html("<strong>Error:</strong>" + data.message.errorCode + "<br/>" + data.message.message);
        $("#loaderUserS").remove();
    });
}

function loadRecentQuestions() {
    $.ajax({
        type: "GET",
        url: "../rest/user/" + uid + "/forum",
        data: {
            from: 1,
            howMany: 5
        },
        contentType: "application/json; charset=utf-8",
    }).done(function (data) {
        let container = $("#cardUserFQList");
        container.empty();

        // populate container from list
        let list = data["resource-list"];
        $.each(list, function (i, v) {
            let li = $("<li></li>").addClass("list-group-item");
            $("<a></a>")
                .attr("href", "./forum.jsp?id=" + v.forumQuestion.id)
                .text(v.forumQuestion.title)
                .appendTo(li);
            li.appendTo(container);
        });

        // show or delete components
        $("#loaderUserFQ").remove();
        if (list.length === 0)
            $("#emptyUserFQList").removeClass("d-none").addClass("d-block");
        else
            $("#cardUserFQBody").remove();
    }).fail(function (data) {
        data = data.responseJSON;
        $("#emptyUserFQList")
            .removeClass("alert-info").addClass("alert-danger")
            .html("<strong>Error:</strong>" + data.message.errorCode + "<br/>" + data.message.message);
        $("#loaderUserFQ").remove();
    });
}

function loadRecentComments() {
    $.ajax({
        type: "GET",
        url: "../rest/user/" + uid + "/comment",
        data: {
            from: 1,
            howMany: 5
        },
        contentType: "application/json; charset=utf-8",
    }).done(function (data) {
        let container = $("#cardUserCommList");
        container.empty();

        // populate container from list
        let list = data["resource-list"];
        $.each(list, function (i, v) {
            let resType = "submission";
            let resId = v.comment.submission;
            let icon = '<i class="fas fa-pencil-alt fa-fw fa-lg"></i>';

            if (parseInt(v.comment.submission) === 0) {
                resType = "forum";
                resId = v.comment.question;
                icon = '<i class="fas fa-question fa-fw fa-lg"></i>';
            }

            if (v.comment.state !== "Deleted" && parseInt(resId) > 0) {
                $("<li></li>")
                    .addClass("list-group-item")
                    .html(icon + "<a class='ml-2' href='./" + resType + ".jsp?id=" + resId + "'>" + v.comment.content + "</a>")
                    .appendTo(container);
            }
        });

        // show or delete components
        $("#loaderUserC").remove();
        if (list.length === 0)
            $("#emptyUserCommList").removeClass("d-none").addClass("d-block");
        else
            $("#cardUserCommBody").remove();
    }).fail(function (data) {
        data = data.responseJSON;
        $("#emptyUserCommList")
            .removeClass("alert-info").addClass("alert-danger")
            .html("<strong>Error:</strong>" + data.message.errorCode + "<br/>" + data.message.message);
        $("#loaderUserC").remove();
    });
}

$(document).ready(function () {
    var token = localStorage.token;
    if(token === null || token === undefined)
        token = sessionStorage.token;

    $.ajax({
        type: "GET",
        url: "../rest/user/" + uid,
        beforeSend: function (request) {
            request.setRequestHeader("X-TSO-Auth-Token", token);
        },
        contentType: "application/json; charset=utf-8",
    }).done(function (data) {
        loadUserJSON(data);
        loadRecentSubmissions();
        loadRecentQuestions();
        loadRecentComments();
        $("#btnNotes").attr('href', 'list-note?token=' + token);

        $("#loader").removeClass("d-block").addClass("d-none");
        $("#contentBody").removeClass("d-none").addClass("d-block");
    }).fail(function (data) {
        data = data.responseJSON;
        $("#alert")
            .html("<b>Error:</b>" + data.message.errorCode + "<br/>" + data.message.message)
            .removeClass("d-none")
            .addClass("d-block");
        $("#loader").removeClass("d-block").addClass("d-none");
        $("#contentBody").removeClass("d-none").addClass("d-block");
    });
});

