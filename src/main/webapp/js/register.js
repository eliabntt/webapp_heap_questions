$(document).ready(function () {
    $("#pictureDiv").hide();
    let image;

    function imageIsLoaded(e) {
        $('#pictureBox').attr('src', e.target.result);
        $("#pictureDiv").show();
    }

    function checkImage(str) {
        let img = document.createElement('img');
        img.onerror = function () {
            $("#pictureDiv").hide();
        };
        img.src = str;
    }

    $("#btnFormRegister").click(function (event) {
        $("#contentBody").removeClass("d-block").addClass("d-none");
        $("#loader").removeClass("d-none").addClass("d-block");

        let user = $.trim($("#inputUser").val());
        let mail = $.trim($("#inputEmail").val());
        let pas1 = $.trim($("#inputPass1").val());
        let pas2 = $.trim($("#inputPass2").val());
        let name = $.trim($("#inputName").val());
        let surn = $.trim($("#inputSurname").val());
        let dob = $.trim($("#inputBirth").val());
        let loc = $.trim($("#inputLoc").val());
        //encodeImageFileAsURL($("#inputPicture")[0]);

        let check = true;

        //previous validations cleaning
        $("#alertError").removeClass("d-block").addClass("d-none");
        $("#inputUser").removeClass("is-invalid").addClass("is-valid");
        $("#inputEmail").removeClass("is-invalid").addClass("is-valid");
        $("#inputPass1").removeClass("is-invalid").addClass("is-valid");
        $("#inputPass2").removeClass("is-invalid").addClass("is-valid");
        $("#inputName").removeClass("is-invalid").addClass("is-valid");
        $("#inputSurname").removeClass("is-invalid").addClass("is-valid");
        $("#inputBirth").removeClass("is-invalid").addClass("is-valid");
        $("#inputLoc").removeClass("is-invalid").addClass("is-valid");


        //validation part
        if (user === null || user === undefined || user === "") {
            $("#inputUser").removeClass("is-valid").addClass("is-invalid");
            check = false;
        }
        if (mail === null || mail === undefined || mail === "" || !(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))) {
            $("#inputEmail").removeClass("is-valid").addClass("is-invalid");
            check = false;
        }
        if (pas1 === null || pas1 === undefined || pas1 === "" || pas2 === null || pas2 === undefined || pas1 !== pas2) {
            $("#inputPass1").removeClass("is-valid").addClass("is-invalid");
            $("#inputPass2").removeClass("is-valid").addClass("is-invalid");
            check = false;
        }
        if (name === null || name === undefined || name === "") {
            $("#inputName").removeClass("is-valid").addClass("is-invalid");
            check = false;
        }
        if (surn === null || surn === undefined || surn === "") {
            $("#inputSurname").removeClass("is-valid").addClass("is-invalid");
            check = false;
        }
        if (dob === null || dob === undefined || dob === "") {
            $("#inputBirth").removeClass("is-valid").addClass("is-invalid");
            check = false;
        }
        if (loc === null || loc === undefined || loc === "") {
            $("#inputLoc").removeClass("is-valid").addClass("is-invalid");
            check = false;
        }

        //done only if validated
        if (check) {
            // mandatory data
            let toPass = {
                "user": {
                    "username": user,
                    "password": sha256(pas1),
                    "email": mail,
                    "name": name,
                    "surname": surn,
                    "birthDate": dob,
                    "location": loc
                }
            };
            // optional data
            if (image !== null && image !== undefined)
                toPass.user.profilePicture = image;

            $.ajax({
                type: "POST",
                url: "../rest/user",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify(toPass)
            }).done(function (data) {
                $("#loader").removeClass("d-block").addClass("d-none");
                $("#contentBody").removeClass("d-none").addClass("d-block");

                window.location.replace("login.jsp");
            }).fail(function (data) {
                data = data.responseJSON;
                $("#alertError span").text(data.message.errorCode + ": " + data.message.message);
                $("#alertError").removeClass("d-none").addClass("d-block");

                $("#loader").removeClass("d-block").addClass("d-none");
                $("#contentBody").removeClass("d-none").addClass("d-block");
            });
        }
    });

    $(":file").change(function () {
        if (this.files && this.files[0]) {
            let reader = new FileReader();

            reader.onload = imageIsLoaded;
            reader.onloadend = function () {
                image = reader.result.split(",")[1];
            };
            reader.readAsDataURL(this.files[0]);
        }
    });

    $("#pictureBoxLink").click(function () {
        $("#pictureBox").attr("src", "");
        $("#pictureDiv").hide();
        image = null;
    });
});