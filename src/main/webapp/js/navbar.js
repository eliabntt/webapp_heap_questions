$(document).ready(function () {
    setButtons();

    // logout button click
    $("#btnLogout").click(function () {
        $.ajax({
            type: "POST", url: "../rest/logout",
            beforeSend: function (request) {
                request.setRequestHeader("X-TSO-Auth-Token", token);
            },
            contentType: "application/json; charset=utf-8",
            dataType: "json"
        }).done(function () {
            localStorage.clear();
            sessionStorage.clear();
            setButtons();
            // redirect
            window.location.replace("home.jsp");
        });
    });

    function setButtons() {
        var token = localStorage.token;
        if (token === undefined || token === null)
            token = sessionStorage.token;
        var username = localStorage.username;
        if (username === undefined || username === null)
            username = sessionStorage.username;

        // set button and sidebar appearance
        if (token !== null && token !== undefined) {
            $("#spanUsername")
                .html("Hello, <strong>" + username + "</strong>")
                .addClass("pr-3");
            $("#btnLogin").hide();
            $("#btnRegister").hide();
            $("#btnProfile").show();
            $("#btnLogout").show();
            $("#cardNote").show();
            $("#btnAddFq").show();
            $("#btnAddSub").show();
        } else {
            $("#spanUsername")
                .html("") // auto-hide
                .removeClass("pr-2");
            $("#btnLogin").show();
            $("#btnRegister").show();
            $("#btnProfile").hide();
            $("#btnLogout").hide();
            $("#cardNote").hide();
            $("#btnAddFq").hide();
            $("#btnAddSub").hide();
        }
    }

    // search: enter key
    $("#inputSearch").keypress(function (e) {
        if (e.keyCode === 13) dosearch(e);
    });

    // search: button click
    $("#btnSearch").click(function (e) {
        dosearch(e);
    });

    // search from navbar
    function dosearch(e) {
        e.preventDefault();
        var selected = $("#selectSearchType option:selected").attr("value");
        var query = $("#inputSearch").val();
        switch (selected) {
            case "Submission":
                window.location.replace("submission.jsp?sub=" + query);
                break;
            case "Forum":
                window.location.replace("forum.jsp?que=" + query);
                break;
            case "SubmissionByTag":
                window.location.replace("submission.jsp?tag=" + query);
                break;
            case "ForumByTag":
                window.location.replace("forum.jsp?tag=" + query);
                break;
        }
    }
});