// retrieve token
var token = localStorage.token;
if (token === undefined || token === null)
    token = sessionStorage.token;

// retrieve token expire date
var expires = localStorage.expireDate;
if (expires === undefined || expires === null)
    expires = sessionStorage.expireDate;

// delete all user stored data, if token is expired
if (expires === undefined || expires === null || new Date(expires) < Date.now()) {
    localStorage.clear();
    sessionStorage.clear();
    token = null; // useful when calling below functions
}

// function for features accessible only after login
function checkLogged(url) {
    if (token === null || token === undefined)
        if (confirm("Feature for registered user only. Do you want to log-in/register?"))
            window.location.replace("login.jsp");
        else
            window.location.replace(url);
}

// function for features not available if logged in
function alreadyLogged() {
    if (token !== null && token !== undefined)
        window.location.replace("home.jsp");
}