let id = GetURLParameter('id');
let whichQue = GetURLParameter('que');
let whichTag = GetURLParameter('tag');

function GetURLParameter(param) {
    let url = window.location.search.substring(1);
    let urlVars = url.split('&');
    for (let i = 0; i < urlVars.length; i++) {
        let parName = urlVars[i].split('=');
        if (parName[0] === param)
            return parName[1];
    }
}

$(document).ready(function () {
    $("#alertError").removeClass("d-block").addClass("d-none");

    if (id === undefined || id === null) {
        // show the list
        $("#divShowQue").hide();
        show(1, 20);
        $("#divShowRecQue").show();
    }
    else {
        // single question
        $.ajax({
            url: 'create_forumquestion.jsp',
            type: 'GET'
        }).done(function (data) {
            $('#divEditQue').html(
                '<div id="divForm">' +
                $(data).find('#divForm').html() +
                '</div>' +
                '<div id="divPreview">' +
                $(data).find('#divPreview').html() +
                '</div>' +
                '<div class="form-row">' +
                '<button id="btnFormPreviewQue" class="btn btn-secondary mt-3">Preview</button>' +
                '<button id="btnFormUpdateQue" class="btn btn-secondary mt-3 ml-3">Update question</button>' +
                '</div>'
            );
        });

        // ajax call to populate categories' select
        $.ajax({
            type: "GET", url: "../rest/category",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
        }).done(function (data) {
            if (data !== undefined) {
                let values = data["resource-list"];
                let select = $("#inputCat")[0];
                $.each(values, function (i, v) {
                    $("<option></option>")
                        .attr("values", v.category.name)
                        .text(v.category.name)
                        .appendTo(select);
                });
                $("#inputCat option:first").attr("selected", "selected");
            }
        });

        // load the resource with the tags and activate buttons for edit/moderate/delete
        $("#divEditQue").hide();
        $("#divShowRecQue").hide();

        let isAdmin = localStorage.isAdmin;
        if (isAdmin === undefined || isAdmin === null)
            isAdmin = sessionStorage.isAdmin;

        let userId = localStorage.user;
        if (userId === undefined || userId === null)
            userId = sessionStorage.user;

        $.ajax({
            type: "GET", url: "../rest/forum/" + id,
        }).done(function (data) {
            $("#queMod").hide();
            $("#queDel").hide();
            $("#queEd").hide();
            if (data.forumQuestion.userId === parseInt(userId)) {
                $("#queDel").show();
                if (data.forumQuestion.closed === null || data.forumQuestion.closed === undefined ||
                    data.forumQuestion.closed === false || data.forumQuestion.closed === "false")
                    $("#queEd").show();
            }
            else if (isAdmin === true || isAdmin === "true")
                $("#queMod").show();

            $("#queTitle").text(data.forumQuestion.title);
            let date = data.forumQuestion.dateTime;
            date = date.substr(0, date.lastIndexOf("."));
            $("#queDate").text(date);
            $("#queCat").text(data.forumQuestion.category);
            $("#queCont").text(data.forumQuestion.content);

            if (data.forumQuestion.closed === null || data.forumQuestion.closed === undefined ||
                data.forumQuestion.closed === false || data.forumQuestion.closed === "false") {
                $("#queClo").text('Still open');
                $("#titleMySol").hide();
                $("#queSol").hide();
            } else {
                $("#queSol").text(data.forumQuestion.closed);
                $("#queClo").text('Closed');
            }

            $.ajax({
                type: "GET",
                url: "../rest/forum/" + id + "/tag"
            }).done(function (_data) {
                let values = _data.tag.values;
                if (values.length > 0)
                    $("#queTags").text(values.join());
                runCommentsLibrary("forum", data.forumQuestion.userId, data.forumQuestion.closed);
            });

        }).fail(function (data) {
            data = data.responseJSON;
            $("#alertError span").text(data.message.errorCode + ": " + data.message.message);
            $("#alertError").removeClass("d-none").addClass("d-block");
            window.location.replace("forum.jsp"); // maybe introduce delay
        });


        $("#divShowQue").show();
    }

    $("#newQue").click(function () {
        window.location.replace("create_forumquestion.jsp");
    });

    $("#nextPage").click(function () {
        let from = parseInt($('#recentQue tr:last th').text()) + 1;
        show(from, 20);
    });

    $("#prevPage").click(function () {
        let from = parseInt($('#recentQUe tr:last th').text()) + 1;
        if (from - 40 >= 1)
            show(from - 40, 20);
        else
            show(1, 20);
    });

    function show(from, howMany) {
        let url;
        // if both null go for standard list
        if ((whichTag === undefined || whichTag === null) &&
            (whichQue === undefined || whichQue === null))
            url = "../rest/forum";
        // if whichQue is null tag search
        else if (whichQue === undefined || whichQue === null) {
            url = "../rest/tag/" + whichTag + "/forum";
            if (whichTag !== "")
                $("#titleQue").text("Results for tag '" + decodeURIComponent(whichTag) + "'");
        }
        // if whichTag is null title search
        else if (whichTag === undefined || whichTag === null) {
            url = "../rest/forum/title/" + whichQue;
            if (whichQue !== "")
                $("#titleQue").text("Results for '" + decodeURIComponent(whichQue) + "'");
        } else {
            alert("Invalid search");
            url = "";
        }

        // button enable
        if (from === 1)
            $("#prevPageLi").addClass("disabled");
        else
            $("#prevPageLi").removeClass("disabled");

        $.ajax({
            type: "GET", url: url,
            data: {
                from: from,
                howMany: howMany + 1
            },
            contentType: "application/json; charset=utf-8"
        }).done(function (data) {
            let container = $("#recentQue tbody");
            container.empty();

            let list = data["resource-list"];
            let returned = list.length;

            $.each(list, function (i, v) {
                let tr = $("<tr></tr>");

                $("<th></th>")
                    .attr("scope", "row")
                    .text(from + i)
                    .appendTo(tr);

                let td = $("<td></td>");
                $("<a></a>")
                    .attr("href", "forum.jsp?id=" + v.forumQuestion.id)
                    .text(v.forumQuestion.title)
                    .appendTo(td);
                td.appendTo(tr);

                let date = v.forumQuestion.dateTime;
                date = date.substr(0, date.lastIndexOf("."));
                $("<td></td>")
                    .text(date)
                    .appendTo(tr);

                $("<td></td>")
                    .text(v.forumQuestion.category)
                    .appendTo(tr);

                $("<td></td>")
                    .text(v.forumQuestion.closed === true ? "closed" : "open")
                    .appendTo(tr);

                tr.appendTo(container);
            });

            // button enable
            if (returned === howMany + 1) {
                $('#recentQue tr:last').remove();
                $("#nextPageLi").removeClass("disabled");
            }
            else //if (returned <= howMany)
                $("#nextPageLi").addClass("disabled");
        });
    }

    $("#queMod").click(function () {
        $("#alertError").removeClass("d-block").addClass("d-none");
        // prepare modal with yes/no buttons
        $("#modalYNContent").text("Do you want to moderate?");
        $("#btnModalYes").click(function () {
            var token = localStorage.token;
            if (token === undefined || token === null)
                token = sessionStorage.token;

            $.ajax({
                type: "DELETE",
                beforeSend: function (request) {
                    request.setRequestHeader("X-TSO-Auth-Token", token);
                },
                url: "../rest/forum/" + id,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({
                    "forumQuestion": {
                        "state": "Moderated",
                    }
                })
            }).done(function (data) {
                // load correct redirect page and show modal with ok button
                $("#btnModalOk").click(function () {
                    window.location.replace("forum.jsp");
                });
                $("#modalYes").modal("show");
            }).fail(function (data) {
                data = data.responseJSON;
                $("#alertError span").text(data.message.errorCode + ": " + data.message.message);
                $("#alertError").removeClass("d-none").addClass("d-block");
            });
        });
        // show modal to confirm
        $("#modalYesNo").modal("show");
    });

    $("#queDel").click(function () {
        $("#alertError").removeClass("d-block").addClass("d-none");
        // prepare modal with yes/no buttons
        $("#modalYNContent").text("Do you want to delete?");
        $("#btnModalYes").click(function () {
            var token = localStorage.token;
            if (token === undefined || token === null)
                token = sessionStorage.token;

            $.ajax({
                type: "DELETE",
                beforeSend: function (request) {
                    request.setRequestHeader("X-TSO-Auth-Token", token);
                },
                url: "../rest/forum/" + id,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({
                    "forumQuestion": {
                        "state": "Deleted",
                    }
                })
            }).done(function (data) {
                // load correct redirect page and show modal with ok button
                $("#btnModalOk").click(function () {
                    window.location.replace("forum.jsp");
                });
                $("#modalYes").modal("show");
            }).fail(function (data) {
                data = data.responseJSON;
                $("#alertError span").text(data.message.errorCode + ": " + data.message.message);
                $("#alertError").removeClass("d-none").addClass("d-block");
            });
        });
        // show modal to confirm
        $("#modalYesNo").modal("show");
    });

    $("#queEd").click(function () {
        $("#divEditQue").show();
        $("#divPreview").hide();
        $("#divShowQue").hide();

        $("#inputTitle").val($("#queTitle").text());

        for (let i = 0; i < $("#inputCat")[0].options.length; i++)
            if ($("#inputCat")[0].options[i].value === $("#queCat").text())
                $("#inputCat")[0].options[i].setAttribute("selected", "selected");
            else
                $("#inputCat")[0].options[i].removeAttribute("selected");

        $("#inputContent").val($("#queCont").text());

        let tags = $("#queTags").text().split(",");
        $("#inputTagList").tagsinput('removeAll');
        //with each we have some problem so go with the for
        for (let i = 0; i < tags.length; i++)
            $("#inputTagList").tagsinput('add', tags[i]);


        $("#btnFormUpdateQue").click(function () {
            let title = $("#inputTitle").val();
            let cat = $("#inputCat").val();
            let cont = $("#inputContent").val();
            let sol = $("#inputSolution").val();

            let tags = $("#inputTagList").tagsinput('items');

            $("#alertError").removeClass("d-block").addClass("d-none");
            $("#inputTitle").removeClass("is-invalid").addClass("is-valid");
            $("#inputCat").removeClass("is-invalid").addClass("is-valid");
            $("#inputContent").removeClass("is-invalid").addClass("is-valid");

            let question;
            let check = true;
            if (title === null || title === undefined || !title) {
                check = false;
                $("#inputTitle").removeClass("is-valid").addClass("is-invalid");
            }
            if (cat === null || cat === undefined || !cat) {
                check = false;
                $("#inputCat").removeClass("is-valid").addClass("is-invalid");
            }
            if (cont === null || cont === undefined || !cont) {
                check = false;
                $("#inputContent").removeClass("is-valid").addClass("is-invalid");
            }

            if (check) {
                let toPass = {
                    "forumQuestion": {
                        "id": +id,
                        "title": title,
                        "category": cat,
                        "content": cont,
                    }
                };

                let toPassTags = {
                    "tag": {
                        values: tags
                    }
                };

                var token = localStorage.token;
                if (token === undefined || token === null)
                    token = sessionStorage.token;

                $.ajax({
                    type: "PUT",
                    url: "../rest/forum/" + id,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function (request) {
                        request.setRequestHeader("X-TSO-Auth-Token", token);
                    },
                    data: JSON.stringify(toPass)
                }).done(function (data) {
                    question = data;
                    $.ajax({
                        type: "PUT",
                        beforeSend: function (request) {
                            request.setRequestHeader("X-TSO-Auth-Token", token);
                        },
                        url: "../rest/forum/" + question.forumQuestion.id + "/tag",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        data: JSON.stringify(toPassTags)
                    }).done(function (dataTags) {
                        setTimeout(function () {
                            window.location.replace("forum.jsp?id=" + question.forumQuestion.id);
                        }, 500);
                    }).fail(function (data) {
                        data = data.responseJSON;
                        $("#alertError span").text(data.message.errorCode + ": " + data.message.message);
                        $("#alertError").removeClass("d-none").addClass("d-block");

                    });
                }).fail(function (data) {
                    data = data.responseJSON;
                    $("#alertError span").text(data.message.errorCode + ": " + data.message.message);
                    $("#alertError").removeClass("d-none").addClass("d-block");
                });
            }
        });

        //to the best of my knowledge works only if here
        $("#btnFormPreviewQue").click(function () {
            let me = $("#btnFormPreviewQue");
            if (me.text().includes("Exit")) {
                me.text("Preview");
                $("#divForm").show();
                $("#divPreview").hide();
            } else {
                let title = $("#inputTitle").val();
                let cat = $("#inputCat").val();
                let cont = $("#inputContent").val();

                let tags = $("#inputTagList").tagsinput('items');

                $("#divForm").hide();
                $("#prevTitle").text(title);
                $("#prevCat").text(cat);
                $("#prevCont").text(cont);
                $("#prevTags").text(tags);
                me.text("Exit preview");
                $("#divPreview").show();
            }
        });
    });
});
