let image;
let oldPassword;
let joinDate;

function loadUserJSON(data) {
    $("#inputUser").val(data.user.username);
    $("#inputEmail").val(data.user.email);
    $("#inputName").val(data.user.name);
    oldPassword = data.user.password;
    joinDate = data.user.joinDate;
    $("#inputSurname").val(data.user.surname);
    $("#inputBirth").val(data.user.birthDate);
    $("#inputLoc").val(data.user.location);
    image = data.user.profilePicture;

    $("#pictureDiv").hide();

    if (image !== undefined || image !== null) {
        let stringPic = "data:image/jpeg;charset=utf-8;base64," + image;
        $("#pictureBox").attr("src", stringPic);
        $("#pictureDiv").show();
        checkImage(stringPic);
    }
}

function imageIsLoaded(e) {
    $('#pictureBox').attr('src', e.target.result);
    $("#pictureDiv").show();
}

function checkImage(str) {
    let img = document.createElement('img');
    img.onerror = function () {
        $("#pictureDiv").hide();
    };
    img.src = str;
}

$(document).ready(function () {
    let uid = localStorage.user;
    if (uid === undefined || uid === null)
        uid = sessionStorage.user;

    var token = localStorage.token;
    if (token === undefined || token === null)
        token = sessionStorage.token;

    $.ajax({
        type: "GET",
        url: "../rest/user/" + uid,
        beforeSend: function (request) {
            request.setRequestHeader("X-TSO-Auth-Token", token);
        },
        contentType: "application/json; charset=utf-8",
    }).done(function (data) {
        loadUserJSON(data);
        $("#loader").removeClass("d-block").addClass("d-none");
        $("#contentBody").removeClass("d-none").addClass("d-block");
    }).fail(function (data) {
        data = data.responseJSON;
        $("#alertSuccess span").text("Settings saved!");
        $("#alertSuccess").removeClass("d-block").addClass("d-none");
        $("#alertError span").text(data.message.errorCode + ": " + data.message.message);
        $("#alertError").removeClass("d-none").addClass("d-block");

        $("#loader").removeClass("d-block").addClass("d-none");
        $("#contentBody").removeClass("d-none").addClass("d-block");
    });

    $("#btnFormRegister").click(function () {
        let user = $.trim($("#inputUser").val());
        let mail = $.trim($("#inputEmail").val());
        let pas1 = $.trim($("#inputPass1").val());
        let pas2 = $.trim($("#inputPass2").val());
        let name = $.trim($("#inputName").val());
        let surn = $.trim($("#inputSurname").val());
        let dob = $.trim($("#inputBirth").val());
        let loc = $.trim($("#inputLoc").val());

        let check = true;

        //previous validations cleaning
        $("#alertError").removeClass("d-block").addClass("d-none");
        $("#alertSuccess").removeClass("d-block").addClass("d-none");
        $("#inputUser").removeClass("is-invalid").addClass("is-valid");
        $("#inputEmail").removeClass("is-invalid").addClass("is-valid");
        $("#inputPass1").removeClass("is-invalid").addClass("is-valid");
        $("#inputPass2").removeClass("is-invalid").addClass("is-valid");
        $("#inputName").removeClass("is-invalid").addClass("is-valid");
        $("#inputSurname").removeClass("is-invalid").addClass("is-valid");
        $("#inputBirth").removeClass("is-invalid").addClass("is-valid");
        $("#inputLoc").removeClass("is-invalid").addClass("is-valid");

        //validation part
        if (user === null || user === undefined || user === "") {
            $("#inputUser").removeClass("is-valid").addClass("is-invalid");
            check = false;
        }
        if (mail === null || mail === undefined || mail === "" ||
            !(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))) {
            $("#inputEmail").removeClass("is-valid").addClass("is-invalid");
            check = false;
        }
        if (pas1 !== pas2) {
            $("#inputPass1").removeClass("is-valid").addClass("is-invalid");
            $("#inputPass2").removeClass("is-valid").addClass("is-invalid");
            check = false;
        } else if ((pas1 === null || pas1 === undefined || pas1 === "") &&
            (pas2 === null || pas2 === undefined || pas2 === ""))
            pas1 = oldPassword;
        if (name === null || name === undefined || name === "") {
            $("#inputName").removeClass("is-valid").addClass("is-invalid");
            check = false;
        }
        if (surn === null || surn === undefined || surn === "") {
            $("#inputSurname").removeClass("is-valid").addClass("is-invalid");
            check = false;
        }
        if (dob === null || dob === undefined || dob === "") {
            $("#inputBirth").removeClass("is-valid").addClass("is-invalid");
            check = false;
        }
        if (loc === null || loc === undefined || loc === "") {
            $("#inputLoc").removeClass("is-valid").addClass("is-invalid");
            check = false;
        }

        //done only if validated
        if (check) {
            $("#contentBody").removeClass("d-block").addClass("d-none");
            $("#loader").removeClass("d-none").addClass("d-block");

            let passwordToPass = pas1;
            if (pas1 !== oldPassword)
                passwordToPass = sha256(pas1);
            else
                passwordToPass = "";

            if (image === undefined)
                image = null;

            // mandatory data
            let toPass = {
                "user": {
                    "id": parseInt(uid),
                    "username": user,
                    "password": passwordToPass,
                    "email": mail,
                    "name": name,
                    "surname": surn,
                    "birthDate": dob,
                    "location": loc,
                    "profilePicture": image,
                    "joinDate": joinDate
                }
            };

            $.ajax({
                type: "PUT",
                url: "../rest/user/" + uid,
                beforeSend: function (request) {
                    request.setRequestHeader("X-TSO-Auth-Token", token);
                },
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify(toPass)
            }).done(function (data) {
                loadUserJSON(data);
                $("#alertSuccess span").text("Settings saved!");
                $("#alertSuccess").removeClass("d-block").addClass("d-none");
                $("#alertError").removeClass("d-block").addClass("d-none");
                $("#loader").removeClass("d-block").addClass("d-none");
                $("#contentBody").removeClass("d-none").addClass("d-block");
                setTimeout(function () {
                    window.location.replace("homeUser.jsp");
                }, 1000);
            }).fail(function (data) {
                $("#loader").removeClass("d-block").addClass("d-none");
                $("#contentBody").removeClass("d-none").addClass("d-block");
                data = data.responseJSON;

                $("#alertSuccess span").text("Settings saved!");
                $("#alertSuccess").removeClass("d-block").addClass("d-none");
                $("#alertError span").text(data.message.errorCode + ": " + data.message.message);
                $("#alertError").removeClass("d-none").addClass("d-block");
            });
        }
    });

    $(":file").change(function () {
        if (this.files && this.files[0]) {
            let reader = new FileReader();

            reader.onload = imageIsLoaded;
            reader.onloadend = function () {
                image = reader.result.split(",")[1];
            };
            reader.readAsDataURL(this.files[0]);
        }
    });

    $("#pictureDelete").click(function () {
        $("#pictureBox").attr("src", "");
        $("#pictureDiv").hide();
        image = null;
    });

    $("#btnFormDelete").click(function () {
        // prepare modal with yes/no buttons
        $("#modalYNContent").text("Are you sure you want to cancel your account?");
        $("#btnModalYes").click(function () {
            let uid = localStorage.user;
            if (uid === undefined || uid === null)
                uid = sessionStorage.user;

            var token = localStorage.token;
            if (token === undefined || token === null)
                token = sessionStorage.token;

            $.ajax({
                type: "DELETE",
                beforeSend: function (request) {
                    request.setRequestHeader("X-TSO-Auth-Token", token);
                },
                url: "../rest/user/" + uid,
            }).done(function (data) {
                localStorage.clear();
                sessionStorage.clear();
                // load correct redirect page and show modal with ok button
                $("#modalYContent").html($("#modalYContent").text() +
                    " We are very sorry to see that <i class='far fa-frown ml-1'></i>");
                $("#btnModalOk").click(function () {
                    window.location.replace("home.jsp");
                });
                $("#modalYes").modal("show");
            }).fail(function (data) {
                $("#modalYTitle").text("Error");
                $("#modalYContent").text("Error encountered while deleting the account.");
                $("#btnModalOk").attr("data-dismiss", "modal");
            });
        });
        // show modal to confirm
        $("#modalYesNo").modal("show");
    });
});