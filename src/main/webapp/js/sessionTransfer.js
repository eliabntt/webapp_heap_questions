/* opening a new tab, sessionStorage is not preserved, while localStorage is
 * the only way to have sessionStorage on all tabs is to pass it
 * using localStorage and sync events triggered by the browser */

let sessionStorageTransfer = function (event) {
    if (!event) event = window.event; // IE compatibility
    if (!event.newValue) return;      // do nothing if no value
    if (event.key === 'getSessionStorage') {
        /* another tab asked for sessionStorage:
         * send it using a temp var, then delete it */
        localStorage.setItem('currentSessionStorage', JSON.stringify(sessionStorage));
        localStorage.removeItem('currentSessionStorage');
    } else if (event.key === 'currentSessionStorage' && !sessionStorage.length) {
        // another tab sent data: retrieve it
        let data = JSON.parse(event.newValue);
        for (let key in data)
            sessionStorage.setItem(key, data[key]);
    }
};

// listen for changes to localStorage
if (window.addEventListener)
    window.addEventListener("storage", sessionStorageTransfer, false);
else
    window.attachEvent("onstorage", sessionStorageTransfer);

// trigger event asking other tabs for sessionStorage
if (!sessionStorage.length) {
    localStorage.setItem('getSessionStorage', 'justATrigger');
    localStorage.removeItem('getSessionStorage');
}