$(document).ready(function () {
    $("#btnFormLogin").click(function () {
        // hide error alert
        $("#alertError").removeClass("d-block").addClass("d-none");

        // try login ajax call to rest
        $.ajax({
            type: "POST", url: "../rest/login",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify({
                "LoginItem": {
                    "userData": $("#inputUserEmail").val(),
                    "password": sha256($("#inputPassword").val()),
                    "remember": $("#checkRemember").is(":checked")
                }
            }),
        }).done(function (data) {
            localStorage.clear();
            sessionStorage.clear();

            let userId = data.token.userId;
            var token = data.token.tokenValue;
            let username = data.token.username;
            let expireDate = data.token.expires;
            let isAdmin = data.token.isAdmin;

            if (token !== undefined && token !== null) {
                if ($("#checkRemember").is(":checked")) {
                    // save data to localStorage
                    localStorage.token = token;
                    localStorage.user = userId;
                    localStorage.username = username;
                    localStorage.expireDate = expireDate;
                    localStorage.isAdmin = isAdmin;
                } else {
                    // save data to sessionStorage
                    sessionStorage.token = token;
                    sessionStorage.user = userId;
                    sessionStorage.username = username;
                    sessionStorage.expireDate = expireDate;
                    sessionStorage.isAdmin = isAdmin;
                }

                // redirect
                window.location.replace("home.jsp");
            }
            else {
                // show error
                $("#alertError span").text("Credentials error.");
                $("#alertError").removeClass("d-none").addClass("d-block");
            }
        }).fail(function (data) {
            // show error
            data = data.responseJSON;
            $("#alertError span").text(" " + data.message.errorCode + ": " + data.message.message);
            $("#alertError").removeClass("d-none").addClass("d-block");
        });
    });
});