let id = GetURLParameter('id');
let whichSub = GetURLParameter('sub');
let whichTag = GetURLParameter('tag');

function GetURLParameter(param) {
    let url = window.location.search.substring(1);
    let urlVars = url.split('&');
    for (let i = 0; i < urlVars.length; i++) {
        let parName = urlVars[i].split('=');
        if (parName[0] === param)
            return parName[1];
    }
}

$(document).ready(function () {
    $("#alertError").removeClass("d-block").addClass("d-none");

    if (id === undefined || id === null) {
        // show the list
        $("#divShowSub").hide();
        show(1, 20);
        $("#divShowRecSub").show();
    }
    else {
        // single submission
        $.ajax({
            url: 'create_submission.jsp',
            type: 'GET'
        }).done(function (data) {
            $('#divEditSub').html(
                '<div id="divForm">' +
                $(data).find('#divForm').html() +
                '</div>' +
                '<div id="divPreview">' +
                $(data).find('#divPreview').html() +
                '</div>' +
                '<div class="form-row">' +
                '<button id="btnFormPreviewSub" class="btn btn-secondary mt-3">Preview</button>' +
                '<button id="btnFormUpdateSub" class="btn btn-secondary mt-3 ml-3">Update submission</button>' +
                '</div>'
            );
        });

        // ajax call to populate categories' select
        $.ajax({
            type: "GET", url: "../rest/category",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
        }).done(function (data) {
            if (data !== undefined) {
                let values = data["resource-list"];
                let select = $("#inputCat")[0];
                $.each(values, function (i, v) {
                    $("<option></option>")
                        .attr("values", v.category.name)
                        .text(v.category.name)
                        .appendTo(select);
                });
                $("#inputCat option:first").attr("selected", "selected");
            }
        });

        // load the resource with the tags and activate buttons for edit/moderate/delete
        $("#divEditSub").hide();
        $("#divShowRecSub").hide();

        let isAdmin = localStorage.isAdmin;
        if (isAdmin === undefined || isAdmin === null)
            isAdmin = sessionStorage.isAdmin;

        let userId = localStorage.user;
        if (userId === undefined || userId === null)
            userId = sessionStorage.user;

        $.ajax({
            type: "GET", url: "../rest/submission/" + id,
        }).done(function (data) {
            $("#subMod").hide();
            $("#subDel").hide();
            $("#subEd").hide();
            if (data.submission.userId === parseInt(userId)) {
                $("#subDel").show();
                $("#subEd").show();
            }
            else if (isAdmin === true || isAdmin === "true")
                $("#subMod").show();

            $("#subTitle").text(data.submission.title);
            let date = data.submission.dateTime;
            date = date.substr(0, date.lastIndexOf("."));
            $("#subDate").text(date);
            $("#subDiff").text(data.submission.difficulty);
            $("#subCat").text(data.submission.category);
            $("#subCont").text(data.submission.content);
            if (data.submission.solution === '')
                $("#subSol").text('Solution not present/necessary');
            else
                $("#subSol").text(data.submission.solution);

            $.ajax({
                type: "GET",
                url: "../rest/submission/" + id + "/tag"
            }).done(function (_data) {
                let values = _data.tag.values;
                if (values.length > 0)
                    $("#subTags").text(values.join());
                runCommentsLibrary("submission", null, false);
            });
        }).fail(function (data) {
            data = data.responseJSON;
            $("#alertError span").text(data.message.errorCode + ": " + data.message.message);
            $("#alertError").removeClass("d-none").addClass("d-block");
            window.location.replace("submission.jsp"); // maybe introduce delay
        });

        $("#divShowSub").show();
    }

    $("#newSub").click(function () {
        window.location.replace("create_submission.jsp");
    });

    $("#nextPage").click(function () {
        let from = parseInt($('#recentSub tr:last th').text()) + 1;
        show(from, 20);
    });

    $("#prevPage").click(function () {
        let from = parseInt($('#recentSub tr:last th').text()) + 1;
        if (from - 40 >= 1)
            show(from - 40, 20);
        else
            show(1, 20);
    });

    function show(from, howMany) {
        let url;
        // if both null go for standard list
        if ((whichTag === undefined || whichTag === null) &&
            (whichSub === undefined || whichSub === null))
            url = "../rest/submission";
        // if whichSub is null tag search
        else if (whichSub === undefined || whichSub === null) {
            url = "../rest/tag/" + whichTag + "/submission";
            if (whichTag !== "")
                $("#titleSub").text("Results for tag '" + decodeURIComponent(whichTag) + "'");
        }
        // if whichTag is null title search
        else if (whichTag === undefined || whichTag === null) {
            url = "../rest/submission/title/" + whichSub;
            if (whichSub !== "")
                $("#titleSub").text("Results for '" + decodeURIComponent(whichSub) + "'");
        } else {
            alert("Invalid search");
            url = "";
        }

        // button enable
        if (from === 1)
            $("#prevPageLi").addClass("disabled");
        else
            $("#prevPageLi").removeClass("disabled");

        $.ajax({
            type: "GET", url: url,
            data: {
                from: from,
                howMany: howMany + 1
            },
            contentType: "application/json; charset=utf-8"
        }).done(function (data) {
            let container = $("#recentSub tbody");
            container.empty();

            let list = data["resource-list"];
            let returned = list.length;

            $.each(list, function (i, v) {
                let tr = $("<tr></tr>");

                $("<th></th>")
                    .attr("scope", "row")
                    .text(from + i)
                    .appendTo(tr);

                let td = $("<td></td>");
                $("<a></a>")
                    .attr("href", "submission.jsp?id=" + v.submission.id)
                    .text(v.submission.title)
                    .appendTo(td);
                td.appendTo(tr);

                let date = v.submission.dateTime;
                date = date.substr(0, date.lastIndexOf("."));
                $("<td></td>")
                    .text(date)
                    .appendTo(tr);

                $("<td></td>")
                    .text(v.submission.category)
                    .appendTo(tr);

                $("<td></td>")
                    .text(v.submission.difficulty)
                    .appendTo(tr);

                tr.appendTo(container);
            });

            // button enable
            if (returned === howMany + 1) {
                $('#recentSub tr:last').remove();
                $("#nextPageLi").removeClass("disabled");
            }
            else //if (returned <= howMany)
                $("#nextPageLi").addClass("disabled");
        });
    }

    $("#subMod").click(function () {
        $("#alertError").removeClass("d-block").addClass("d-none");
        // prepare modal with yes/no buttons
        $("#modalYNContent").text("Do you want to moderate?");
        $("#btnModalYes").click(function () {
            var token = localStorage.token;
            if (token === undefined || token === null)
                token = sessionStorage.token;

            $.ajax({
                type: "DELETE",
                beforeSend: function (request) {
                    request.setRequestHeader("X-TSO-Auth-Token", token);
                },
                url: "../rest/submission/" + id,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({
                    "submission": {
                        "state": "Moderated",
                    }
                })
            }).done(function (data) {
                // load correct redirect page and show modal with ok button
                $("#btnModalOk").click(function () {
                    window.location.replace("forum.jsp");
                });
                $("#modalYes").modal("show");
            }).fail(function (data) {
                data = data.responseJSON;
                $("#alertError span").text(data.message.errorCode + ": " + data.message.message);
                $("#alertError").removeClass("d-none").addClass("d-block");
            });
        });
        // show modal to confirm
        $("#modalYesNo").modal("show");
    });

    $("#subDel").click(function () {
        $("#alertError").removeClass("d-block").addClass("d-none");
        // prepare modal with yes/no buttons
        $("#modalYNContent").text("Do you want to delete?");
        $("#btnModalYes").click(function () {
            var token = localStorage.token;
            if (token === undefined || token === null)
                token = sessionStorage.token;

            $.ajax({
                type: "DELETE",
                beforeSend: function (request) {
                    request.setRequestHeader("X-TSO-Auth-Token", token);
                },
                url: "../rest/submission/" + id,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({
                    "submission": {
                        "state": "Deleted",
                    }
                })
            }).done(function (data) {
                // load correct redirect page and show modal with ok button
                $("#btnModalOk").click(function () {
                    window.location.replace("forum.jsp");
                });
                $("#modalYes").modal("show");
            }).fail(function (data) {
                data = data.responseJSON;
                $("#alertError span").text(data.message.errorCode + ": " + data.message.message);
                $("#alertError").removeClass("d-none").addClass("d-block");
            });
        });
        // show modal to confirm
        $("#modalYesNo").modal("show");
    });

    $("#subEd").click(function () {
        $("#divEditSub").show();
        $("#divPreview").hide();
        $("#divShowSub").hide();

        $("#inputTitle").val($("#subTitle").text());

        for (let i = 0; i < $("#inputDiff")[0].options.length; i++)
            if ($("#inputDiff")[0].options[i].value === $("#subDiff").text())
                $("#inputDiff")[0].options[i].setAttribute("selected", "selected");
            else
                $("#inputDiff")[0].options[i].removeAttribute("selected");

        for (let i = 0; i < $("#inputCat")[0].options.length; i++)
            if ($("#inputCat")[0].options[i].value === $("#subCat").text())
                $("#inputCat")[0].options[i].setAttribute("selected", "selected");
            else
                $("#inputCat")[0].options[i].removeAttribute("selected");

        $("#inputContent").val($("#subCont").text());

        if ($("#subSol").text() !== "Solution not present/necessary")
            $("#inputSolution").val($("#subSol").text());

        let tags = $("#subTags").text().split(",");
        $("#inputTagList").tagsinput('removeAll');
        //with each we have some problem so go with the for
        for (let i = 0; i < tags.length; i++)
            $("#inputTagList").tagsinput('add', tags[i]);


        $("#btnFormUpdateSub").click(function () {
            let title = $("#inputTitle").val();
            let diff = $("#inputDiff").val();
            let cat = $("#inputCat").val();
            let cont = $("#inputContent").val();
            let sol = $("#inputSolution").val();

            let tags = $("#inputTagList").tagsinput('items');

            $("#alertError").removeClass("d-block").addClass("d-none");
            $("#inputTitle").removeClass("is-invalid").addClass("is-valid");
            $("#inputDiff").removeClass("is-invalid").addClass("is-valid");
            $("#inputCat").removeClass("is-invalid").addClass("is-valid");
            $("#inputContent").removeClass("is-invalid").addClass("is-valid");

            let submission;
            let check = true;
            if (title === null || title === undefined || !title) {
                check = false;
                $("#inputTitle").removeClass("is-valid").addClass("is-invalid");
            }
            if (diff === null || diff === undefined || !diff) {
                check = false;
                $("#inputDiff").removeClass("is-valid").addClass("is-invalid");
            }
            if (cat === null || cat === undefined || !cat) {
                check = false;
                $("#inputCat").removeClass("is-valid").addClass("is-invalid");
            }
            if (cont === null || cont === undefined || !cont) {
                check = false;
                $("#inputContent").removeClass("is-valid").addClass("is-invalid");
            }

            if (check) {
                let toPass = {
                    "submission": {
                        "id": +id,
                        "title": title,
                        "category": cat,
                        "minPlanToVis": "Free",
                        "difficulty": diff,
                        "content": cont,
                        "solution": sol
                    }
                };

                let toPassTags = {
                    "tag": {
                        values: tags
                    }
                };

                var token = localStorage.token;
                if (token === undefined || token === null)
                    token = sessionStorage.token;

                $.ajax({
                    type: "PUT",
                    url: "../rest/submission/" + id,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function (request) {
                        request.setRequestHeader("X-TSO-Auth-Token", token);
                    },
                    data: JSON.stringify(toPass)
                }).done(function (data) {
                    submission = data;
                    $.ajax({
                        type: "PUT",
                        beforeSend: function (request) {
                            request.setRequestHeader("X-TSO-Auth-Token", token);
                        },
                        url: "../rest/submission/" + submission.submission.id + "/tag",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        data: JSON.stringify(toPassTags)
                    }).done(function (dataTags) {
                        setTimeout(function () {
                            window.location.replace("submission.jsp?id=" + submission.submission.id);
                        }, 500);
                    }).fail(function (data) {
                        data = data.responseJSON;
                        $("#alertError span").text(data.message.errorCode + ": " + data.message.message);
                        $("#alertError").removeClass("d-none").addClass("d-block");

                    });
                }).fail(function (data) {
                    data = data.responseJSON;
                    $("#alertError span").text(data.message.errorCode + ": " + data.message.message);
                    $("#alertError").removeClass("d-none").addClass("d-block");
                });
            }
        });

        //to the best of my knowledge works only if here
        $("#btnFormPreviewSub").click(function () {
            let me = $("#btnFormPreviewSub");
            if (me.text().includes("Exit")) {
                me.text("Preview");
                $("#divForm").show();
                $("#divPreview").hide();
            } else {
                let title = $("#inputTitle").val();
                let diff = $("#inputDiff").val();
                let cat = $("#inputCat").val();
                let cont = $("#inputContent").val();
                let sol = $("#inputSolution").val();
                if (sol === '') sol = 'Solution not present/necessary';

                let tags = $("#inputTagList").tagsinput('items');

                $("#divForm").hide();
                $("#prevTitle").text(title);
                $("#prevDiff").text(diff);
                $("#prevCat").text(cat);
                $("#prevCont").text(cont);
                $("#prevSol").text(sol);
                $("#prevTags").text(tags);
                me.text("Exit preview");
                $("#divPreview").show();
            }
        });
    });
});
