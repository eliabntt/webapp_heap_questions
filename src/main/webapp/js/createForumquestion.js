$(document).ready(function () {
    $("#divPreview").hide();

    // ajax call to populate categories' select
    $.ajax({
        type: "GET", url: "../rest/category",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
    }).done(function (data) {
        if (data !== undefined) {
            let values = data["resource-list"];
            let select = $("#inputCat")[0];
            $.each(values, function (i, v) {
                $("<option></option>")
                    .attr("values", v.category.name)
                    .text(v.category.name)
                    .appendTo(select);
            });
            $("#inputCat option:first").attr("selected", "selected");
        }
    });
});

$("#btnFormCreateQue").click(function () {
    let title = $("#inputTitle").val();
    let cat = $("#inputCat").val();
    let cont = $("#inputContent").val();

    let tags = $("#inputTagList").tagsinput('items');

    $("#alertError").removeClass("d-block").addClass("d-none");
    $("#inputTitle").removeClass("is-invalid").addClass("is-valid");
    $("#inputCat").removeClass("is-invalid").addClass("is-valid");
    $("#inputContent").removeClass("is-invalid").addClass("is-valid");

    let forumquestion;
    let check = true;
    if (title === null || title === undefined || !title) {
        check = false;
        $("#inputTitle").removeClass("is-valid").addClass("is-invalid");
    }
    if (cat === null || cat === undefined || !cat) {
        check = false;
        $("#inputCat").removeClass("is-valid").addClass("is-invalid");
    }
    if (cont === null || cont === undefined || !cont) {
        check = false;
        $("#inputContent").removeClass("is-valid").addClass("is-invalid");
    }

    if (check) {
        let toPass = {
            "forumQuestion": {
                "title": title,
                "category": cat,
                "content": cont,
            }
        };

        let toPassTags = {
            "tag": {
                values: tags
            }
        };

        var token = localStorage.token;
        if (token === undefined || token === null)
            token = sessionStorage.token;

        $.ajax({
            type: "POST",
            url: "../rest/forum",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function (request) {
                request.setRequestHeader("X-TSO-Auth-Token", token);
            },
            data: JSON.stringify(toPass)
        }).done(function (data) {
            forumquestion = data;
            $.ajax({
                type: "POST",
                beforeSend: function (request) {
                    request.setRequestHeader("X-TSO-Auth-Token", token);
                },
                url: "../rest/forum/" + forumquestion.forumQuestion.id + "/tag",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify(toPassTags)
            }).done(function (data) {
                window.location.replace("forum.jsp?id=" + forumquestion.forumQuestion.id);
            }).fail(function (data) {
                $("#alertError span").text(data.message.errorCode + ": " + data.message.message);
                $("#alertError").removeClass("d-none").addClass("d-block");
            });
        }).fail(function (data) {
            $("#alertError span").text(data.message.errorCode + ": " + data.message.message);
            $("#alertError").removeClass("d-none").addClass("d-block");
        });
    }
});

$("#btnFormPreviewQue").click(function () {
    let me = $("#btnFormPreviewQue");
    if (me.text().includes("Exit")) {
        me.text("Preview");
        $("#divForm").show();
        $("#divPreview").hide();
    } else {
        let title = $("#inputTitle").val();
        let cat = $("#inputCat").val();
        let cont = $("#inputContent").val();

        let tags = $("#inputTagList").tagsinput('items');

        $("#divForm").hide();
        $("#prevTitle").text(title);
        $("#prevCat").text(cat);
        $("#prevCont").text(cont);
        $("#prevTags").text(tags);
        me.text("Exit preview");
        $("#divPreview").show();
    }
});