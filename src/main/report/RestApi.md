# REST API for HeapQuestions
## Merged to report, now _outdated_.

The server accepts only request and response bodies with MIME type `application/json` or `application/json; charset=utf-8`.

Default message body, valid for the communication of errors or information, contains message, error-code, error-details fields (of which message is the only used if there are no errors, of course).

Note: unsupported methods always return code `405 METHOD NOT ALLOWED` with the standard error message body:

```json
{
    'message':
    {
        'message' : 'Unsupported operation for URI {uri}.',
        'error-code' : 405,
        'error-details' : 'Requested operation {operation}.'
    }
}
```

A method of an endpoint marked with `(pr)` is considered private, accessible only after login and passing into the request the header

```http
X-TSO-Auth-Token = {token}
```

A parameter marked with `(r)` is required.

## JSON Objects

login object
    userData    string  Username or email of the user.
    password    string  Password encoded in SHA-256.
    remember    bool    If the login has to be remembered for longer time.

token object
    user        long        User ID on the platform.
    tokenValue  string      Authorization token for private endpoints.
    expires     timestamp   Expiration date/time.

user object
    id              long    User ID.
    username        string  Username.
    password        string  Encoded password.
    email           string  User email.
    isAdmin         bool    Whether the user has admin rights.
    name            string  First name.
    surname         string  Last name.
    birthDate       string  Birth date as timestamp.
    joinDate        string  Platform's join date as timestamp.
    location        string  User (generic) location.
    profilePicture  string  Base64-encoded picture.

submission object
    id              long    ID.
    userId          long    Author ID.
    title           string  Title of the submission.
    dateTime        string  Timestamp.
    difficulty      string  One of "Easy","Medium","Hard","Extreme".
    state           string  One of "Visible","Edited","Moderated","Deleted".
    content         string  Text of the submission.
    solution        string  Text of the provided solution.
    category        string  Category name the submission belongs to.
    minPlanToVis    string  Minimum plan to visualize the submission.

forumQuestion object
    id          long    ID.
    userId      long    Author ID.
    category    string  Category name the question belongs to.
    dateTime    string  Timestamp.
    closed      boolean Whether the question is closed.
    title       string  Title of the question.
    content     string  Text of the question.
    state       string  One of "Visible","Edited","Moderated","Deleted".

comment object
    id              long    ID.
    userId          long    Author ID.
    question        long    Question ID or empty.
    submission      long    Submission ID or empty.
    parentComment   long    Parent comment ID or empty.
    dateTime        string  Timestamp.
    downVote        int     Number of downvotes.
    upVote          int     Number of upvotes.
    notify          boolean Whether to notify the user about the comment.
    isAnswer        boolean If comment is marked as answer for the question.
    content         string  Text of the comment.
    state           string  One of "Visible","Edited","Moderated","Deleted".

tag object
    value   string  Value of the tag.

company object
    id              int     ID.
    name            string  Name of the company.
    description     string  Brief description.
    website         string  Website of the company.
    logo            string  Logo of the company.
    operativeArea   string  One operative area from the preconfigured list.

category object
    name            string  Name of the category.
    description     string  Brief description.

## Endpoints

/login
    POST
        Login to the platform.
        REQUEST parameters:
            login object
        RESPONSE:
            200 OK: Successful login.
                token object
            401 UNAUTHORIZED: Login failed.
                Default error message structure.
            500 SERVER ERROR: Errors occurred during elaborations.
                Default error message structure.

/logout
    POST (pr)
        User logout from the platform.
        REQUEST parameters:
            Only auth. header.
        RESPONSE:
            200 OK: Successful logout
                Default message structure.
            401 UNAUTHORIZED: Unauthorized user, cannot perform elaboration.
                Default error message structure.
            500 SERVER ERROR: Errors occurred during elaborations.
                Default error message structure.

/user
    POST
        Creates a new user.
        REQUEST parameters:
            Auth header.
            user object
        RESPONSE:
            201 CREATED: User created successfully.
                Same structure as input, information from the database.
            403 FORBIDDEN: User is already logged in, so it can not register.
                Default error message structure.
            409 CONFLICT: Cannot create the user because it already exists.
                Default error message structure.
            500 SERVER ERROR: Errors occurred during elaborations.
                Default error message structure.
    GET
        Returns a list of users.
        REQUEST parameters:
            None.
        RESPONSE:
            200 OK: List retrieved successfully.
                resource-list   Array of users (same structures as in POST request).
            500 SERVER ERROR: Errors occurred during elaborations.
                Default error message structure.
/user/{id}
    GET (pr)
        Returns the user that has the given ID.
        REQUEST parameters:
            None.
        RESPONSE:
            200 OK: Retrieved successfully.
                user object
            404 NOT FOUND: No such user on the platform.
                Default error message structure.
            401 UNAUTHORIZED: Unauthorized user, cannot perform elaboration.
                Default error message structure.
            403 FORBIDDEN: User not logged in, cannot perform elaboration.
                Default error message structure.
            500 SERVER ERROR: Errors occurred during elaborations.
                Default error message structure.
    PUT (pr)
        Updates the user that has the given ID.
        REQUEST parameters:
            Auth header.
            user object
        RESPONSE:
            200 OK: User retrieved successfully.
                Same structure as input, information from the database.
            400 BAD REQUEST: JSON's User ID does not match requested {id}.
                Default error message structure.
            401 UNAUTHORIZED: Unauthorized user, cannot perform elaboration.
                Default error message structure.
            403 FORBIDDEN: User not logged in, cannot perform elaboration.
                Default error message structure.
            404 NOT FOUND: No such user on the platform.
                Default error message structure.
            409 CONFLICT: Cannot update the user because other resources depend on it.
                Default error message structure.
            500 SERVER ERROR: Errors occurred during elaborations.
                Default error message structure.
    DELETE (pr)
        Deletes the user that has the given ID.
        REQUEST parameters:
            Auth header.
        RESPONSE:
            200 OK: User deleted successfully.
                Same structure for the user, information from the database.
            401 UNAUTHORIZED: Unauthorized user, cannot perform elaboration.
                Default error message structure.
            403 FORBIDDEN: User not logged in, cannot perform elaboration.
                Default error message structure.
            404 NOT FOUND: No such user on the platform.
                Default error message structure.
            409 CONFLICT: Cannot delete the user because other resources depend on it.
                Default error message structure.
            500 SERVER ERROR: Errors occurred during elaborations.
                Default error message structure.

/user/{id}/comment
    GET
        Returns a list of comments authored by the user.
        REQUEST parameters:
            None.
        RESPONSE:
            200 OK: List retrieved successfully.
                resource-list   Array of comments (see structure in dedicated endpoint).
            500 SERVER ERROR: Errors occurred during elaborations.
                Default error message structure.

/user/{id}/submission
    GET
        Returns a list of submissions authored by the user.
        REQUEST parameters:
            None.
        RESPONSE:
            200 OK: List retrieved successfully.
                resource-list   Array of submissions (see structure in dedicated endpoint).
            500 SERVER ERROR: Errors occurred during elaborations.
                Default error message structure.

/user/{id}/forum
    GET
        Returns a list of forum questions authored by the user.
        REQUEST parameters:
            None.
        RESPONSE:
            200 OK: List retrieved successfully.
                resource-list   Array of questions (see structure in dedicated endpoint).
            500 SERVER ERROR: Errors occurred during elaborations.
                Default error message structure.

/submission
    POST (pr)
        Creates a new submission.
        REQUEST parameters:
            Auth header.
            submission object
        RESPONSE:
            201 CREATED: Submission created successfully.
                Same structure as input, information from the database.
            401 UNAUTHORIZED: Unauthorized user, cannot perform elaboration.
                Default error message structure.
            403 FORBIDDEN: User not logged in, cannot perform elaboration.
                Default error message structure.
            409 CONFLICT: Cannot create the submission because it already exists.
                Default error message structure.
            500 SERVER ERROR: Errors occurred during elaborations.
                Default error message structure.
    GET
        Returns a list of submissions.
        REQUEST parameters:
            None.
        RESPONSE:
            200 OK: List retrieved successfully.
                resource-list   Array of submissions (same structures as in POST request).
            500 SERVER ERROR: Errors occurred during elaborations.
                Default error message structure.
/submission/{id}
    GET
        Returns the submission that has the given ID.
        REQUEST parameters:
            None.
        RESPONSE:
            200 OK: Retrieved successfully.
                submission object
            404 NOT FOUND: No such submission on the platform.
                Default error message structure.
            500 SERVER ERROR: Errors occurred during elaborations.
                Default error message structure.
    PUT (pr)
        Updates the submission that has the given ID.
        REQUEST parameters:
            Auth header.
            submission object
        RESPONSE:
            200 OK: Submission updated successfully.
                Same structure as input, information from the database.
            400 BAD REQUEST: JSON's Submission ID does not match requested {id}.
                Default error message structure.
            401 UNAUTHORIZED: Unauthorized user, cannot perform elaboration.
                Default error message structure.
            403 FORBIDDEN: User not logged in, cannot perform elaboration.
                Default error message structure.
            404 NOT FOUND: No such submission on the platform.
                Default error message structure.
            409 CONFLICT: Cannot update the submission because other resources depend on it.
                Default error message structure.
            500 SERVER ERROR: Errors occurred during elaborations.
                Default error message structure.
    DELETE (pr)
        Deletes the submission that has the given ID.
        REQUEST parameters:
            Auth header.
        RESPONSE:
            200 OK: Submission deleted successfully.
                Same structure for the submission, information from the database.
            401 UNAUTHORIZED: Unauthorized user, cannot perform elaboration.
                Default error message structure.
            403 FORBIDDEN: User not logged in, cannot perform elaboration.
                Default error message structure.
            404 NOT FOUND: No such submission on the platform.
                Default error message structure.
            409 CONFLICT: Cannot delete the submission because other resources depend on it.
                Default error message structure.
            500 SERVER ERROR: Errors occurred during elaborations.
                Default error message structure.

/submission/{id}/comment
    POST (pr)
        Creates a comment on the submission.
        REQUEST parameters:
            Auth header.
            comment object
        RESPONSE:
            201 CREATED: Comment created successfully.
                Same structure as input, information from the database.
            401 UNAUTHORIZED: Unauthorized user, cannot perform elaboration.
                Default error message structure.
            403 FORBIDDEN: User not logged in, cannot perform elaboration.
                Default error message structure.
            409 CONFLICT: Cannot create the comment because it already exists.
                Default error message structure.
            500 SERVER ERROR: Errors occurred during elaborations.
                Default error message structure.
    GET
        Returns a list of comment on the submission.
        REQUEST parameters:
            from, howMany: as uri parameters, to indicate a subset of comments
        RESPONSE:
            200 OK: List retrieved successfully.
                resource-list   Array of comments.
            500 SERVER ERROR: Errors occurred during elaborations.
                Default error message structure.

/submission/{id}/tag
    POST (pr)
        Adds a list of tags to the submission with given ID.
        REQUEST parameters:
            Auth header.
            "list of tags" object
        RESPONSE:
            200 OK: List added and retrieved successfully.
                resource-list   Array of tags (same structures as in input).
            401 UNAUTHORIZED: Unauthorized user, cannot perform elaboration.
                Default error message structure.
            403 FORBIDDEN: User not logged in, cannot perform elaboration.
                Default error message structure.
            500 SERVER ERROR: Errors occurred during elaborations.
                Default error message structure.
    PUT (pr)
        Updates the tag list of the submission with given ID.
        REQUEST parameters:
            Auth header.
            "list of tags" object
        RESPONSE:
            200 OK: List retrieved successfully.
                resource-list   Array of tags (same structures as in input).
            401 UNAUTHORIZED: Unauthorized user, cannot perform elaboration.
                Default error message structure.
            403 FORBIDDEN: User not logged in, cannot perform elaboration.
                Default error message structure.
            500 SERVER ERROR: Errors occurred during elaborations.
                Default error message structure.

/forum
    POST (pr)
        Creates a new forum question.
        REQUEST parameters:
            Auth header.
            forumQuestion object
        RESPONSE:
            201 CREATED: Question created successfully.
                Same structure as input, information from the database.
            401 UNAUTHORIZED: Unauthorized user, cannot perform elaboration.
                Default error message structure.
            403 FORBIDDEN: User not logged in, cannot perform elaboration.
                Default error message structure.
            409 CONFLICT: Cannot create the question because it already exists.
                Default error message structure.
            500 SERVER ERROR: Errors occurred during elaborations.
                Default error message structure.
    GET
        Returns a list of forum questions.
        REQUEST parameters:
            None.
        RESPONSE:
            200 OK: List retrieved successfully.
                resource-list   Array of forum questions (same structures as in POST request).
            500 SERVER ERROR: Errors occurred during elaborations.
                Default error message structure.
/forum/{id}
    GET
        Returns the forum question that has the given ID.
        REQUEST parameters:
            None.
        RESPONSE:
            200 OK: Retrieved successfully.
                forumQuestion object
            404 NOT FOUND: No such question on the platform.
                Default error message structure.
            500 SERVER ERROR: Errors occurred during elaborations.
                Default error message structure.
    PUT (pr)
        Updates the forum question that has the given ID.
        REQUEST parameters:
            Auth header.
            forumQuestion object
        RESPONSE:
            200 OK: Question updated successfully.
                Same structure as input, information from the database.
            400 BAD REQUEST: JSON's question ID does not match requested {id}.
                Default error message structure.
            401 UNAUTHORIZED: Unauthorized user, cannot perform elaboration.
                Default error message structure.
            403 FORBIDDEN: User not logged in, cannot perform elaboration.
                Default error message structure.
            404 NOT FOUND: No such question on the platform.
                Default error message structure.
            409 CONFLICT: Cannot update the question because other resources depend on it.
                Default error message structure.
            500 SERVER ERROR: Errors occurred during elaborations.
                Default error message structure.
    DELETE (pr)
        Deletes the forum question that has the given ID.
        REQUEST parameters:
            Auth header.
        RESPONSE:
            200 OK: Question deleted successfully.
                Same structure for the forumQuestion, information from the database.
            401 UNAUTHORIZED: Unauthorized user, cannot perform elaboration.
                Default error message structure.
            403 FORBIDDEN: User not logged in, cannot perform elaboration.
                Default error message structure.
            404 NOT FOUND: No such question on the platform.
                Default error message structure.
            409 CONFLICT: Cannot delete the forum question because other resources depend on it.
                Default error message structure.
            500 SERVER ERROR: Errors occurred during elaborations.
                Default error message structure.

/forum/{id}/comment
    POST (pr)
        Creates a comment on the forum question.
        REQUEST parameters:
            Auth header.
            comment object
        RESPONSE:
            201 CREATED: Comment created successfully.
                Same structure as input, information from the database.
            401 UNAUTHORIZED: Unauthorized user, cannot perform elaboration.
                Default error message structure.
            403 FORBIDDEN: User not logged in, cannot perform elaboration.
                Default error message structure.
            409 CONFLICT: Cannot create the comment because it already exists.
                Default error message structure.
            500 SERVER ERROR: Errors occurred during elaborations.
                Default error message structure.
    GET
        Returns a list of comment on the forum question.
        REQUEST parameters:
            from, howMany: as uri parameters, to indicate a subset of comments
        RESPONSE:
            200 OK: List retrieved successfully.
                resource-list   Array of comments.
            500 SERVER ERROR: Errors occurred during elaborations.
                Default error message structure.

/forum/{id}/tag
    POST (pr)
        Adds a list of tags to the question with given ID.
        REQUEST parameters:
            Auth header.
            "list of tags" object
        RESPONSE:
            200 OK: List added and retrieved successfully.
                resource-list   Array of tags (same structures as in input).
            401 UNAUTHORIZED: Unauthorized user, cannot perform elaboration.
                Default error message structure.
            403 FORBIDDEN: User not logged in, cannot perform elaboration.
                Default error message structure.
            500 SERVER ERROR: Errors occurred during elaborations.
                Default error message structure.
    PUT (pr)
        Updates the tag list of the question with given ID.
        REQUEST parameters:
            Auth header.
            "list of tags" object
        RESPONSE:
            200 OK: List retrieved successfully.
                resource-list   Array of tags (same structures as in input).
            401 UNAUTHORIZED: Unauthorized user, cannot perform elaboration.
                Default error message structure.
            403 FORBIDDEN: User not logged in, cannot perform elaboration.
                Default error message structure.
            500 SERVER ERROR: Errors occurred during elaborations.
                Default error message structure.

/comment/{id}
    GET
        Returns the comment that has the given ID.
        REQUEST parameters:
            None.
        RESPONSE:
            200 OK: Comment retrieved successfully.
                comment object
            404 NOT FOUND: No such comment on the platform.
                Default error message structure.
            500 SERVER ERROR: Errors occurred during elaborations.
                Default error message structure.
    PUT (pr)
        Updates the comment that has the given ID.
        REQUEST parameters:
            Auth header.
            comment object
        RESPONSE:
            200 OK: comment updated successfully.
                Same structure as input, information from the database.
            400 BAD REQUEST: JSON's question ID does not match requested {id}.
                Default error message structure.
            401 UNAUTHORIZED: Unauthorized user, cannot perform elaboration.
                Default error message structure.
            403 FORBIDDEN: User not logged in, cannot perform elaboration.
                Default error message structure.
            404 NOT FOUND: No such comment on the platform.
                Default error message structure.
            409 CONFLICT: Cannot update the comment because other resources depend on it.
                Default error message structure.
            500 SERVER ERROR: Errors occurred during elaborations.
                Default error message structure.
    DELETE (pr)
        Deletes the comment that has the given ID.
        REQUEST parameters:
            Auth header.
        RESPONSE:
            200 OK: Comment deleted successfully.
                Same structure for the comment, information from the database.
            401 UNAUTHORIZED: Unauthorized user, cannot perform elaboration.
                Default error message structure.
            403 FORBIDDEN: User not logged in, cannot perform elaboration.
                Default error message structure.
            404 NOT FOUND: No such comment on the platform.
                Default error message structure.
            409 CONFLICT: Cannot delete the comment because other resources depend on it.
                Default error message structure.
            500 SERVER ERROR: Errors occurred during elaborations.
                Default error message structure.

/comment/{id}/isAnswer
    PUT (pr)
        Updates the comment setting isAnswer to the given value.
        REQUEST parameters:
            Auth header.
            comment object (only isAnswer is relevant)
        RESPONSE:
            200 OK: comment updated successfully.
                Same structure as input, information from the database.
            400 BAD REQUEST: JSON's question ID does not match requested {id}.
                Default error message structure.
            401 UNAUTHORIZED: Unauthorized user, cannot perform elaboration.
                Default error message structure.
            404 NOT FOUND: No such comment on the platform.
                Default error message structure.
            409 CONFLICT: Cannot update the comment because other resources depend on it.
                Default error message structure.
            500 SERVER ERROR: Errors occurred during elaborations.
                Default error message structure.

/company
    POST (pr)
        Creates a new company.
        REQUEST parameters:
            Auth header.
            company object
        RESPONSE:
            201 CREATED: Company created successfully.
                Same structure as input, information from the database.
            401 UNAUTHORIZED: Unauthorized user, cannot perform elaboration.
                Default error message structure.
            403 FORBIDDEN: User not logged in, cannot perform elaboration.
                Default error message structure.
            409 CONFLICT: Cannot create the company because it already exists.
                Default error message structure.
            500 SERVER ERROR: Errors occurred during elaborations.
                Default error message structure.
    GET
        Returns a list of companies.
        REQUEST parameters:
            None.
        RESPONSE:
            200 OK: List retrieved successfully.
                resource-list   Array of companies (same structures as in POST request).
            500 SERVER ERROR: Errors occurred during elaborations.
                Default error message structure.
/company/{id}
    GET
        Returns the company that has the given ID.
        REQUEST parameters:
            None.
        RESPONSE:
            200 OK: Retrieved successfully.
                forumQuestion object
            404 NOT FOUND: No such company on the platform.
                Default error message structure.
            500 SERVER ERROR: Errors occurred during elaborations.
                Default error message structure.
    PUT (pr)
        Updates the company that has the given ID.
        REQUEST parameters:
            Auth header.
            company object
        RESPONSE:
            200 OK: Company updated successfully.
                Same structure as input, information from the database.
            400 BAD REQUEST: JSON's question ID does not match requested {id}.
                Default error message structure.
            401 UNAUTHORIZED: Unauthorized user, cannot perform elaboration.
                Default error message structure.
            403 FORBIDDEN: User not logged in, cannot perform elaboration.
                Default error message structure.
            404 NOT FOUND: No such company on the platform.
                Default error message structure.
            409 CONFLICT: Cannot update the company because other resources depend on it.
                Default error message structure.
            500 SERVER ERROR: Errors occurred during elaborations.
                Default error message structure.
    DELETE (pr)
        Deletes the company that has the given ID.
        REQUEST parameters:
            Auth header.
        RESPONSE:
            200 OK: Company deleted successfully.
                Same structure for the company, information from the database.
            401 UNAUTHORIZED: Unauthorized user, cannot perform elaboration.
                Default error message structure.
            403 FORBIDDEN: User not logged in, cannot perform elaboration.
                Default error message structure.
            404 NOT FOUND: No such company on the platform.
                Default error message structure.
            409 CONFLICT: Cannot delete the company because other resources depend on it.
                Default error message structure.
            500 SERVER ERROR: Errors occurred during elaborations.
                Default error message structure.

/category
    GET
        Retrieves a list of categories.
        REQUEST parameters:
            None.
        RESPONSE:
            200 OK: List retrieved successfully.
                resource-list   Array of categories (category objects).
            405 METHOD NOT ALLOWED: No parameters are needed.
                Default error message
            500 SERVER ERROR: Errors occurred during elaborations.
                Default error message structure.

/tag/{value}/submission
    GET
        Retrieves a list of submissions having a specific tag value.
        REQUEST parameters:
            None.
        RESPONSE:
            200 OK: List retrieved successfully.
                resource-list   Array of submissions (submission objects).
            500 SERVER ERROR: Errors occurred during elaborations.
                Default error message structure.

/tag/{value}/forum
    GET
        Retrieves a list of forum question having a specific tag value.
        REQUEST parameters:
            None.
        RESPONSE:
            200 OK: List retrieved successfully.
                resource-list   Array of forum questions (questions objects).
            500 SERVER ERROR: Errors occurred during elaborations.
                Default error message structure.
