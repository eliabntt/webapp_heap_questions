function parseJSON(inputJSON) {
    var resource = inputJSON.responseJSON;
    if (resource == undefined || resource == null)
        resource = inputJSON;

    var text = "";
    if (resource["resource-list"] != undefined && resource["resource-list"] != null) {
        text += "<h3>Items list:</h3><br/>";
        text += "<table>";
        resource = resource["resource-list"];
        for (var i = 0; i < resource.length; i++) {
            var classObject = Object.values(resource[i])[0];

            text += "<thead><tr><th>Object:</th><th>" + Object.getOwnPropertyNames(resource[i])[0] + "</th></tr></thead>";

            var entries = Object.entries(classObject);

            for (var j = 0; j < entries.length; j++) {
                if (entries[j][0] == "profilePicture" && entries[j][1] != null)
                    text += '<tr><td>' + entries[j][0] + '</td><td><img src="data:image/jpeg;charset=utf-8;base64,' + entries[j][1] + '"width="200" height="200"/></td></tr>';
                else
                    text += '<tr><td>' + entries[j][0] + '</td><td>' + entries[j][1] + '</td></tr>';
            }
        }
        text += "</table>";
    }
    else {
        var classObject = Object.values(resource)[0];
        text += "<table>";
        text += "<thead><tr><th>Object:</th><th>" + Object.getOwnPropertyNames(resource)[0] + "</th></tr></thead>";

        var entries = Object.entries(classObject);

        for (var j = 0; j < entries.length; j++) {
            if (entries[j][0] == "profilePicture" && entries[j][1] != null)
                text += '<tr><td>' + entries[j][0] + '</td><td><img src="data:image/jpeg;charset=utf-8;base64,' + entries[j][1] + '"width="200" height="200"/></td></tr>';
            else
                text += '<tr><td>' + entries[j][0] + '</td><td>' + entries[j][1] + '</td></tr>';
        }
        text += "</table>";
    }
    return text;
}

function deleteExpiredToken() {
    // delete token, username and expire date if token is expired
    var expires = localStorage.expireDate;
    if (new Date(expires) < Date.now() || expires === undefined) {
        localStorage.removeItem("token");
        localStorage.removeItem("user");
        localStorage.removeItem("expireDate");
        $(document.body).prepend('<button id="login" type="button">Login</button>');
        $("#login").click(function () {
            window.location = "login.html";
        });
    }
    else {
        $(document.body).prepend('<button id="logout_official" type="button">Logout</button>');
        $("#logout_official").click(function () {
            $.ajax({
                type: "POST", url: "../rest/logout",
                beforeSend: function (request) {
                    request.setRequestHeader("X-TSO-Auth-Token", localStorage.token);
                },
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                complete: function (data) {
                    $('#resultDiv').html(parseJSON(data));
                    localStorage.removeItem("token");
                    localStorage.removeItem("user");
                    localStorage.removeItem("expireDate");
                    location.reload(true);
                }
            });
        });
    }
}

function ObjectName(inputObject) {
    return Object.getOwnPropertyNames(inputObject)[0];
}

function getParameter(inputObject, parameterName) {
    var classObject = Object.values(inputObject)[0];
    var entries = Object.entries(classObject);
    for (var j = 0; j < entries.length; j++)
        if (entries[j][0] === parameterName)
            return entries[j][1];
    return null;
}