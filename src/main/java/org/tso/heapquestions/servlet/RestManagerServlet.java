// Copyright 2018 The StackOverflowers
package org.tso.heapquestions.servlet;

import org.tso.heapquestions.database.AuthDatabase;
import org.tso.heapquestions.resource.Message;
import org.tso.heapquestions.resource.OperativeAreas;
import org.tso.heapquestions.rest.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Manages the REST API for the different REST resources.
 */
public final class RestManagerServlet extends AbstractDatabaseServlet {
    /**
     * The JSON MIME media type
     */
    private static final String JSON_MEDIA_TYPE = "application/json";

    /**
     * The JSON UTF-8 MIME media type
     */
    private static final String JSON_UTF_8_MEDIA_TYPE = "application/json; charset=utf-8";

    /**
     * The any MIME media type
     */
    private static final String ALL_MEDIA_TYPE = "*/*";

    /**
     * Service the REST request.
     *
     * @param req The request object.
     * @param res The response object.
     * @throws IOException If any error occurs in the client/server communication.
     */
    @Override
    protected final void service(final HttpServletRequest req, final HttpServletResponse res) throws IOException {
        res.setContentType(JSON_UTF_8_MEDIA_TYPE);
        final OutputStream out = res.getOutputStream();
        Connection con = null;
        try {
            con = getConnection();

            // if the request method and/or the MIME media type are not allowed, return.
            // Appropriate error message sent by {@code checkMethodMediaType}
            if (!checkMethodMediaType(req, res)) return;

            // if the requested resource involves login, delegate its processing and return
            // note: login is always public
            if (processLogin(req, res, con)) return;

            // check for valid token in the request, for privacy reasons
            long userIdFromToken = checkTokenGetUser(req, res, con);

            // fully private calls
            if (userIdFromToken != AuthDatabase.INVALID_TOKEN) {

                // if the requested resource involves logout, delegate its processing and return
                if (processLogout(req, res, con)) return;

                // if the requested resource involves schema creation, delegate its processing and return
                // used for test purposes only
                //if (processSetup(req, res, con)) return;
            }

            // mixed-privacy calls (e.g. GETs are public, other methods are private, or similar)
            // if the requested resource involves users, delegate its processing and return
            if (processUser(req, res, con, userIdFromToken)) return;

            // if the requested resource involves submission, delegate its processing and return
            if (processSubmission(req, res, con, userIdFromToken)) return;

            // if the requested resource involves forumQuestion, delegate its processing and return
            if (processForumQuestion(req, res, con, userIdFromToken)) return;

            // if the requested resource involves a precise comment, delegate its processing.
            if (processComment(req, res, con, userIdFromToken)) return;

            // if the requested resource involves forumQuestion, delegate its processing and return
            if (processCompany(req, res, con, userIdFromToken)) return;

            // if the requested resource involves tag, delegate its processing and return
            if (processTag(req, res, con, userIdFromToken)) return;

            // if the requested resource involves category, delegate its processing and return
            if (processCategory(req, res, con)) return;

            // if none of the above process methods succeeds, it means an unknown resource has been requested
            final Message m = new Message("Unknown resource requested.", "404",
                    String.format("Requested resource is %s.", req.getRequestURI()));
            res.setStatus(HttpServletResponse.SC_NOT_FOUND);
            m.toJSON(out);
        } catch (SQLException e) {
            try {
                con.close();
                final Message m = new Message("Cannot open DB connection.", "500",
                        null);
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(out);
            } catch (SQLException ex) {
                final Message m = new Message("Cannot close DB connection", "500",
                        null);
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(out);
            }

        } finally {
            // ensure to always flush and close the output stream
            out.flush();
            out.close();
            try {
                con.close();
            } catch (SQLException e) {
                //Calling the method close on a Connection object that is already closed is a no-op.
                final Message m = new Message("There is a problem with DB connection", "500",
                        null);
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(out);
            }
        }
    }

    /**
     * Checks that the request method and MIME media type are allowed.
     *
     * @param req The HTTP request.
     * @param res The HTTP response.
     * @return {@code true} if the request method and the MIME type are allowed; {@code false} otherwise.
     * @throws IOException If any error occurs in the client/server communication.
     */
    private boolean checkMethodMediaType(final HttpServletRequest req, final HttpServletResponse res) throws IOException {
        final String method = req.getMethod();
        final String contentType = req.getHeader("Content-Type");
        final String accept = req.getHeader("Accept");
        final OutputStream out = res.getOutputStream();

        Message m;

        if (accept == null) {
            m = new Message("Output media type not specified.", "400", "Accept request header missing.");
            res.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            m.toJSON(out);
            return false;
        }

        if (!accept.contains(JSON_MEDIA_TYPE) && !accept.contains(ALL_MEDIA_TYPE)) {
            m = new Message("Unsupported output media type. Resources are represented only in application/json.",
                    "406", String.format("Requested representation is %s.", accept));
            res.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
            m.toJSON(out);
            return false;
        }

        switch (method) {
            case "GET":
            case "DELETE":
                // nothing to do
                break;

            case "POST":
            case "PUT":
                if (contentType == null) {
                    m = new Message("Input media type not specified.", "400", "Content-Type request header missing.");
                    res.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    m.toJSON(out);
                    return false;
                }

                if (!contentType.contains(JSON_MEDIA_TYPE)) {
                    m = new Message("Unsupported input media type. Resources are represented only in application/json.",
                            "415", String.format("Submitted representation is %s.", contentType));
                    res.setStatus(HttpServletResponse.SC_UNSUPPORTED_MEDIA_TYPE);
                    m.toJSON(out);
                    return false;
                }
                break;
            default:
                m = new Message("Unsupported operation.",
                        "405", String.format("Requested operation %s.", method));
                res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                m.toJSON(out);
                return false;
        }
        return true;
    }

    /**
     * Checks whether there is a submitted token for private RESTs requests.
     *
     * @param req The HTTP request.
     * @param res The HTTP response.
     * @return {@code true} if the submitted token is valid; {@code false} otherwise.
     */
    private long checkTokenGetUser(final HttpServletRequest req, final HttpServletResponse res, final Connection con) {
        // simply delegate user extraction from submitted token and return output
        try {
            return new AuthRestResource(req, res, con).checkTokenGetUser();
        } catch (SQLException e) {
            return AuthDatabase.INVALID_TOKEN;
        }
    }

    /**
     * Checks for and processes Database schema creation.
     *
     * @param req The HTTP request.
     * @param res The HTTP response.
     * @return {@code true} If processed, {@code false} otherwise.
     * @throws IOException If any error occurs in the client/server communication.
     * @deprecated Shall not be used in production environments.
     */
    private boolean processSetup(final HttpServletRequest req, final HttpServletResponse res, final Connection con) throws IOException {
        final String method = req.getMethod();
        final OutputStream out = res.getOutputStream();
        Message m;

        // check if the requested resource is invalid for this function
        String path;
        try {
            path = new URI(req.getRequestURI()).getPath();
        } catch (URISyntaxException e) {
            // this should NEVER be called!
            path = req.getRequestURI();
        }
        if (path.lastIndexOf("rest/setup") <= 0)
            return false;

        if (method.equals("GET"))
            new SetupDatabaseRestResource(req, res, con).setupDatabase();
        else {
            m = new Message("Unsupported operation for URI /setup.",
                    "405", String.format("Requested operation %s.", method));
            res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
            m.toJSON(out);
        }

        return true;
    }

    /**
     * Checks for and processes login operations.
     *
     * @param req The HTTP request.
     * @param res The HTTP response.
     * @return {@code true} If processed, {@code false} otherwise.
     * @throws IOException If any error occurs in the client/server communication.
     */
    private boolean processLogin(final HttpServletRequest req, final HttpServletResponse res, final Connection con) throws IOException {
        final String method = req.getMethod();
        final OutputStream out = res.getOutputStream();
        Message m;

        // check if the requested resource is invalid for this function
        String path;
        try {
            path = new URI(req.getRequestURI()).getPath();
        } catch (URISyntaxException e) {
            // this should NEVER be called!
            path = req.getRequestURI();
        }
        if (path.lastIndexOf("rest/login") <= 0)
            return false;

        if (method.equals("POST"))
            new AuthRestResource(req, res, con).login();
        else {
            m = new Message("Unsupported operation for URI /login.",
                    "405", String.format("Requested operation %s.", method));
            res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
            m.toJSON(out);
        }

        return true;
    }

    /**
     * Checks for and processes login operations.
     *
     * @param req The HTTP request.
     * @param res The HTTP response.
     * @return {@code true} If processed, {@code false} otherwise.
     * @throws IOException If any error occurs in the client/server communication.
     */
    private boolean processLogout(final HttpServletRequest req, final HttpServletResponse res, final Connection con) throws IOException {
        final String method = req.getMethod();
        final OutputStream out = res.getOutputStream();
        Message m;

        // check if the requested resource is invalid for this function
        String path;
        try {
            path = new URI(req.getRequestURI()).getPath();
        } catch (URISyntaxException e) {
            // this should NEVER be called!
            path = req.getRequestURI();
        }
        if (path.lastIndexOf("rest/logout") <= 0)
            return false;

        if (method.equals("POST"))
            new AuthRestResource(req, res, con).logout();
        else {
            m = new Message("Unsupported operation for URI /logout.",
                    "405", String.format("Requested operation %s.", method));
            res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
            m.toJSON(out);
        }

        return true;
    }

    /**
     * Checks for and processes user operations.
     *
     * @param req             The HTTP request.
     * @param res             The HTTP response.
     * @param userIdFromToken The Id of the authenticated user (avoids spoofing).
     * @return {@code true} If processed, {@code false} otherwise.
     * @throws IOException If any error occurs in the client/server communication.
     */
    private boolean processUser(final HttpServletRequest req, final HttpServletResponse res, final Connection con,
                                final long userIdFromToken) throws IOException {
        final String method = req.getMethod();
        final OutputStream out = res.getOutputStream();
        Message m;

        // check if the requested resource is invalid for this function
        String path;
        try {
            path = new URI(req.getRequestURI()).getPath();
        } catch (URISyntaxException e) {
            // this should NEVER be called!
            path = req.getRequestURI();
        }
        if (!path.contains("rest/user"))
            return false;

        if (path.lastIndexOf("rest/user/") > 0) {
            // id is present
            if (path.lastIndexOf("comment") >= 0) {
                // requested comment list for an user
                switch (method) {
                    case "GET": // public
                    {
                        long from;
                        long howMany;
                        try {
                            from = Long.parseLong(req.getParameter("from"));
                            howMany = Long.parseLong(req.getParameter("howMany"));
                            new CommentRestResource(req, res, con).listFromHowMany(from, howMany, null);
                        } catch (NumberFormatException e) {
                            new CommentRestResource(req, res, con).list();
                        }
                        break;
                    }
                    default:
                        m = new Message("Unsupported operation for URI /user/{id}/comment.",
                                "405", String.format("Requested operation %s.", method));
                        res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                        m.toJSON(out);
                        break;
                }
            } else if (path.lastIndexOf("submission") >= 0) {
                // requested submission list for an user
                switch (method) {
                    case "GET": // public
                        new SubmissionRestResource(req, res, con).list();
                        break;
                    default:
                        m = new Message("Unsupported operation for URI /user/{id}/submission.",
                                "405", String.format("Requested operation %s.", method));
                        res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                        m.toJSON(out);
                        break;
                }
            } else if (path.lastIndexOf("forum") >= 0) {
                // requested question list for an user
                switch (method) {
                    case "GET": // public
                        new ForumQuestionRestResource(req, res, con).list();
                        break;
                    default:
                        m = new Message("Unsupported operation for URI /user/{id}/forum.",
                                "405", String.format("Requested operation %s.", method));
                        res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                        m.toJSON(out);
                        break;
                }
            } else {
                // no comment or submission or forum present, going for the detail of the user
                // if not logged in cannot do anything here
                if (userIdFromToken != AuthDatabase.INVALID_TOKEN) {
                    switch (method) {
                        case "GET": // private
                            new UserRestResource(req, res, con).read(userIdFromToken);
                            break;
                        case "PUT": // private
                            new UserRestResource(req, res, con).update(userIdFromToken);
                            break;
                        case "DELETE": // private
                            new UserRestResource(req, res, con).delete(userIdFromToken);
                            break;
                        default:
                            m = new Message("Unsupported operation for URI /user/{id}.",
                                    "405", String.format("Requested operation %s.", method));
                            res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                            m.toJSON(out);
                            break;
                    }
                } else {
                    if (method.equals("GET"))
                        new UserRestResource(req, res, con).readBasicInfo();
                    else {
                        m = new Message("User not logged in. Operation not permitted.",
                                "403", String.format("Requested operation %s.", method));
                        res.setStatus(HttpServletResponse.SC_FORBIDDEN);
                        m.toJSON(out);
                    }
                }
            }
        } else {
            // no id, general call
            switch (method) {
                case "POST":// public: everyone can register!
                    if (userIdFromToken == AuthDatabase.INVALID_TOKEN)
                        //but only if it's not already logged in
                        new UserRestResource(req, res, con).create();
                    else {
                        m = new Message("User already logged in. Operation not permitted.",
                                "405", String.format("Requested operation %s.", method));
                        res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                        m.toJSON(out);
                    }
                    break;
                case "GET": // public
                    new UserRestResource(req, res, con).list();
                    break;
                default:
                    m = new Message("Unsupported operation for URI /user.",
                            "405", String.format("Requested operation %s.", method));
                    res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                    m.toJSON(out);
                    break;
            }
        }
        return true;
    }

    /**
     * Checks for and processes submission operations.
     *
     * @param req             The HTTP request.
     * @param res             The HTTP response.
     * @param userIdFromToken Id of the authenticated user.
     * @return {@code true} If processed, {@code false} otherwise.
     * @throws IOException If any error occurs in the client/server communication.
     */
    private boolean processSubmission(final HttpServletRequest req, final HttpServletResponse res, final Connection con,
                                      final long userIdFromToken) throws IOException {
        final String method = req.getMethod();
        final OutputStream out = res.getOutputStream();
        Message m;

        // check if the requested resource is invalid for this function
        String path;
        try {
            path = new URI(req.getRequestURI()).getPath();
        } catch (URISyntaxException e) {
            // this should NEVER be called!
            path = req.getRequestURI();
        }
        int lastIndex = path.lastIndexOf("rest/submission");
        if (lastIndex <= 0)
            return false;

        if (path.lastIndexOf("rest/submission/") > 0) {
            // id is present
            if (path.lastIndexOf("tag") >= 0)
                // requested tags for a submission
                processTagForResource(req, res, con, userIdFromToken);

            else if (path.lastIndexOf("comment") >= 0)
                // requested comments for a submission
                processCommentForResource(req, res, con, userIdFromToken);

            else if (path.lastIndexOf("title") >= 0) {
                switch (method) {
                    case ("GET"):
                        // public
                        long from;
                        long howMany;
                        try {
                            // these are passed as url parameters, not from json
                            String _from = req.getParameter("from");
                            String _howMany = req.getParameter("howMany");
                            if (_from != null && _howMany != null) {
                                from = Long.parseLong(_from);
                                howMany = Long.parseLong(_howMany);
                                new SubmissionRestResource(req, res, con).listFromHowMany(from, howMany);
                            } else {
                                m = new Message("Wrong parameters for URI /submission/title/{title}.",
                                        "405", String.format("Requested operation %s, but passed only one between from and howMany parameters.", method));
                                res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                                m.toJSON(res.getOutputStream());
                            }
                        } catch (NumberFormatException e) {
                            m = new Message("Wrong parameters for URI /submission/title/{title}.",
                                    "405", String.format("Requested operation %s.", method));
                            res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                            m.toJSON(res.getOutputStream());
                        }
                        break;
                    default:
                        m = new Message("Unsupported operation for URI /submission/title/{title}.",
                                "405", String.format("Requested operation %s.", method));
                        res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                        m.toJSON(out);
                        break;
                }
            } else {
                // no tag or comment present
                switch (method) {
                    case "GET": // public
                        new SubmissionRestResource(req, res, con).read();
                        break;
                    case "PUT": // private
                        if (userIdFromToken != AuthDatabase.INVALID_TOKEN)
                            new SubmissionRestResource(req, res, con).update(userIdFromToken);
                        else {
                            m = new Message("User not logged in. Operation not permitted.",
                                    "403", String.format("Requested operation %s.", method));
                            res.setStatus(HttpServletResponse.SC_FORBIDDEN);
                            m.toJSON(out);
                        }
                        break;
                    case "DELETE": // private
                        if (userIdFromToken != AuthDatabase.INVALID_TOKEN)
                            new SubmissionRestResource(req, res, con).delete(userIdFromToken);
                        else {
                            m = new Message("User not logged in. Operation not permitted.",
                                    "403", String.format("Requested operation %s.", method));
                            res.setStatus(HttpServletResponse.SC_FORBIDDEN);
                            m.toJSON(out);
                        }
                        break;
                    default:
                        m = new Message("Unsupported operation for URI /submission/{id}.",
                                "405", String.format("Requested operation %s.", method));
                        res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                        m.toJSON(out);
                        break;
                }
            }
        } else {
            // no id, general call
            switch (method) {
                case "POST":
                    // private
                    if (userIdFromToken != AuthDatabase.INVALID_TOKEN)
                        new SubmissionRestResource(req, res, con).create(userIdFromToken);
                    else {
                        m = new Message("User not logged in. Operation not permitted.",
                                "403", String.format("Requested operation %s.", method));
                        res.setStatus(HttpServletResponse.SC_FORBIDDEN);
                        m.toJSON(out);
                    }
                    break;
                case "GET": // public
                    new SubmissionRestResource(req, res, con).list();
                    break;
                default:
                    m = new Message("Unsupported operation for URI /submission.",
                            "405", String.format("Requested operation %s.", method));
                    res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                    m.toJSON(out);
                    break;
            }
        }
        return true;
    }

    /**
     * Checks for and processes forum question operations.
     *
     * @param req             The HTTP request.
     * @param res             The HTTP response.
     * @param userIdFromToken The authenticated user id.
     * @return {@code true} If processed, {@code false} otherwise.
     * @throws IOException If any error occurs in the client/server communication.
     */
    private boolean processForumQuestion(final HttpServletRequest req, final HttpServletResponse res, final Connection con,
                                         final long userIdFromToken) throws IOException {
        final String method = req.getMethod();
        final OutputStream out = res.getOutputStream();
        Message m;

        // check if the requested resource is invalid for this function
        String path;
        try {
            path = new URI(req.getRequestURI()).getPath();
        } catch (URISyntaxException e) {
            // this should NEVER be called!
            path = req.getRequestURI();
        }

        int lastIndex = path.lastIndexOf("rest/forum");
        if (lastIndex <= 0)
            return false;

        if (path.lastIndexOf("rest/forum/") > 0) {
            // id is present
            if (path.lastIndexOf("tag") >= 0)
                // requested tags for a question
                processTagForResource(req, res, con, userIdFromToken);

            else if (path.lastIndexOf("comment") >= 0)
                // requested comments of a question
                processCommentForResource(req, res, con, userIdFromToken);

            else if (path.lastIndexOf("answer") >= 0) {
                if (method.equals("GET"))
                    new ForumQuestionRestResource(req, res, con).readAnswer();
                else {
                    m = new Message("Unsupported operation for URI /forum/{id}/answer.",
                            "405", String.format("Requested operation %s.", method));
                    res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                    m.toJSON(out);
                }
            }

            else if (path.lastIndexOf("title") >= 0) {
                switch (method) {
                    case ("GET"):
                        // public
                        long from;
                        long howMany;
                        try {
                            // these are passed as url parameters, not from json
                            String _from = req.getParameter("from");
                            String _howMany = req.getParameter("howMany");
                            if (_from != null && _howMany != null) {
                                from = Long.parseLong(_from);
                                howMany = Long.parseLong(_howMany);
                                new ForumQuestionRestResource(req, res, con).listFromHowMany(from, howMany);
                            } else {
                                m = new Message("Wrong parameters for URI /forum/title/{title}.",
                                        "405", String.format("Requested operation %s, but passed only one between from and howMany parameters.", method));
                                res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                                m.toJSON(res.getOutputStream());
                            }
                        } catch (NumberFormatException e) {
                            m = new Message("Wrong parameters for URI /forum/title/{title}.",
                                    "405", String.format("Requested operation %s.", method));
                            res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                            m.toJSON(res.getOutputStream());
                        }
                        break;
                    default:
                        m = new Message("Unsupported operation for URI /forum/title/{title}.",
                                "405", String.format("Requested operation %s.", method));
                        res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                        m.toJSON(out);
                        break;
                }
            } else {
                // no tag or comment present
                switch (method) {
                    case "GET": // public
                        new ForumQuestionRestResource(req, res, con).read();
                        break;
                    case "PUT": // private
                        if (userIdFromToken != AuthDatabase.INVALID_TOKEN)
                            new ForumQuestionRestResource(req, res, con).update(userIdFromToken);
                        else {
                            m = new Message("User not logged in. Operation not permitted.",
                                    "403", String.format("Requested operation %s.", method));
                            res.setStatus(HttpServletResponse.SC_FORBIDDEN);
                            m.toJSON(out);
                        }
                        break;
                    case "DELETE": // private
                        if (userIdFromToken != AuthDatabase.INVALID_TOKEN)
                            new ForumQuestionRestResource(req, res, con).delete(userIdFromToken);
                        else {
                            m = new Message("User not logged in. Operation not permitted.",
                                    "403", String.format("Requested operation %s.", method));
                            res.setStatus(HttpServletResponse.SC_FORBIDDEN);
                            m.toJSON(out);
                        }
                        break;
                    default:
                        m = new Message("Unsupported operation for URI /forum/{id}.",
                                "405", String.format("Requested operation %s.", method));
                        res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                        m.toJSON(out);
                        break;
                }
            }
        } else {
            // no id, general call
            switch (method) {
                case "POST":// private
                    if (userIdFromToken != AuthDatabase.INVALID_TOKEN)
                        new ForumQuestionRestResource(req, res, con).create(userIdFromToken);
                    else {
                        m = new Message("User not logged in. Operation not permitted.",
                                "403", String.format("Requested operation %s.", method));
                        res.setStatus(HttpServletResponse.SC_FORBIDDEN);
                        m.toJSON(out);
                    }
                    break;
                case "GET": // public
                    new ForumQuestionRestResource(req, res, con).list();
                    break;
                default:
                    m = new Message("Unsupported operation for URI /forum.",
                            "405", String.format("Requested operation %s.", method));
                    res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                    m.toJSON(out);
                    break;
            }
        }
        return true;
    }

    /**
     * Checks for and processes company operations.
     *
     * @param req             The HTTP request.
     * @param res             The HTTP response.
     * @param userIdFromToken The token-authenticated user.
     * @return {@code true} If processed, {@code false} otherwise.
     * @throws IOException If any error occurs in the client/server communication.
     */
    private boolean processCompany(final HttpServletRequest req, final HttpServletResponse res, final Connection con,
                                   final long userIdFromToken) throws IOException {
        final String method = req.getMethod();
        final OutputStream out = res.getOutputStream();
        Message m;

        String path;
        try {
            path = new URI(req.getRequestURI()).getPath();
        } catch (URISyntaxException e) {
            // this should NEVER be called!
            path = req.getRequestURI();
        }

        // check if the requested resource is invalid for this function
        int lastIndex = path.lastIndexOf("rest/company");
        if (lastIndex <= 0)
            return false;

        if (path.lastIndexOf("rest/company/") > 0) {
            // id is present
            switch (method) {
                case "GET": {
                    String value = path.substring(path.lastIndexOf("company") + "company".length());
                    value = value.substring(1);
                    if (isInteger(value))
                        new CompanyRestResource(req, res, con).read();
                    else {
                        OperativeAreas area = OperativeAreas.fromString(value);
                        if (area == null)
                            new CompanyRestResource(req, res, con).searchByName(value);
                        else
                            new CompanyRestResource(req, res, con).searchByOperativeArea(area);
                    }
                    break;
                }
                case "PUT": // private
                    if (userIdFromToken != AuthDatabase.INVALID_TOKEN)
                        new CompanyRestResource(req, res, con).update(userIdFromToken);
                    else {
                        m = new Message("User not logged in. Operation not permitted.",
                                "403", String.format("Requested operation %s.", method));
                        res.setStatus(HttpServletResponse.SC_FORBIDDEN);
                        m.toJSON(out);
                    }
                    break;
                case "DELETE": // private
                    if (userIdFromToken != AuthDatabase.INVALID_TOKEN)
                        new CompanyRestResource(req, res, con).delete(userIdFromToken);
                    else {
                        m = new Message("User not logged in. Operation not permitted.",
                                "403", String.format("Requested operation %s.", method));
                        res.setStatus(HttpServletResponse.SC_FORBIDDEN);
                        m.toJSON(out);
                    }
                    break;
                default:
                    m = new Message("Unsupported operation for URI /company/{id}.",
                            "405", String.format("Requested operation %s.", method));
                    res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                    m.toJSON(out);
                    break;
            }
        } else {
            // no id, general call
            switch (method) {
                case "POST": // public
                    if (userIdFromToken != AuthDatabase.INVALID_TOKEN)
                        new CompanyRestResource(req, res, con).create();
                    else {
                        m = new Message("User not logged in. Operation not permitted.",
                                "403", String.format("Requested operation %s.", method));
                        res.setStatus(HttpServletResponse.SC_FORBIDDEN);
                        m.toJSON(out);
                    }
                    break;
                case "GET": //public
                    new CompanyRestResource(req, res, con).list();
                    break;
                default:
                    m = new Message("Unsupported operation for URI /company.",
                            "405", String.format("Requested operation %s.", method));
                    res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                    m.toJSON(out);
                    break;
            }
        }
        return true;
    }

    /**
     * Checks for and processes operations regarding tags.
     *
     * @param req The HTTP request.
     * @param res The HTTP response.
     * @return {@code true} If processed, {@code false} otherwise.
     * @throws IOException If any error occurs in the client/server communication.
     */
    private boolean processTag(final HttpServletRequest req, final HttpServletResponse res, final Connection con, final long userIdFromToken) throws IOException {
        final String method = req.getMethod();
        final OutputStream out = res.getOutputStream();
        Message m;

        // check if the requested resource is invalid for this function
        String path;
        try {
            path = new URI(req.getRequestURI()).getPath();
        } catch (URISyntaxException e) {
            // this should NEVER be called!
            path = req.getRequestURI();
        }
        if (path.lastIndexOf("rest/tag") <= 0)
            return false;

        if (path.lastIndexOf("submission") > 0 | path.lastIndexOf("forum") > 0)
            switch (method) {
                case "GET":
                    new TagRestResource(req, res, con).getResourcesHavingTag();
                    break;
                default:
                    m = new Message("Unsupported operation for URI /tag/{value}/{resource}.",
                            "405", String.format("Requested operation %s.", method));
                    res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                    m.toJSON(out);
                    break;
            }
        else switch (method) {
            case "GET":
                new TagRestResource(req, res, con).listSimilarTags();
                break;
            default:
                m = new Message("Unsupported operation for URI /tag.",
                        "405", String.format("Requested operation %s.", method));
                res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                m.toJSON(out);
                break;
        }
        return true;
    }

    /**
     * Checks for and process precise comments.
     *
     * @param req             The HTTP request.
     * @param res             The HTTP response.
     * @param userIdFromToken The id of the authenticated user.
     * @return {@code true} If processed, {@code false} otherwise.
     * @throws IOException If any error occurs in the client/server communication.
     */
    private boolean processComment(final HttpServletRequest req, final HttpServletResponse res, final Connection con,
                                   final long userIdFromToken) throws IOException {
        final String method = req.getMethod();
        final OutputStream out = res.getOutputStream();
        Message m;

        // check if the requested resource is invalid for this function
        String path;
        try {
            path = new URI(req.getRequestURI()).getPath();
        } catch (URISyntaxException e) {
            // this should NEVER be called!
            path = req.getRequestURI();
        }
        if (path.lastIndexOf("rest/comment") <= 0)
            return false;

        if (path.lastIndexOf("comment/") > 0) {
            // id is present
            if (path.lastIndexOf("isAnswer") > 0) {
                if (method.equals("PUT"))
                    new CommentRestResource(req, res, con).setAsAnswer(userIdFromToken);
                else {
                    m = new Message("Unsupported operation for URI /comment/{id}/isAnswer.",
                            "405", String.format("Requested operation %s.", method));
                    res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                    m.toJSON(out);
                }
            } else switch (method) {
                case "GET": // public
                    if ((req.getParameter("upVote").equals("true") || req.getParameter("upVote").equals("false")))
                        new CommentRestResource(req, res, con).updateUpDownVote();
                    else
                        new CommentRestResource(req, res, con).read();
                    break;
                case "PUT": // private
                {
                    if (userIdFromToken != AuthDatabase.INVALID_TOKEN)
                        new CommentRestResource(req, res, con).update(userIdFromToken);
                    else {
                        m = new Message("User not logged in. Operation not permitted.",
                                "403", String.format("Requested operation %s.", method));
                        res.setStatus(HttpServletResponse.SC_FORBIDDEN);
                        m.toJSON(out);
                    }
                    break;
                }
                case "DELETE": {// private
                    if (userIdFromToken != AuthDatabase.INVALID_TOKEN) {
                        new CommentRestResource(req, res, con).delete(userIdFromToken);
                    } else {
                        m = new Message("User not logged in. Operation not permitted.",
                                "403", String.format("Requested operation %s.", method));
                        res.setStatus(HttpServletResponse.SC_FORBIDDEN);
                        m.toJSON(out);
                    }
                    break;
                }
                default:
                    m = new Message("Unsupported operation for URI /comment/{id}.",
                            "405", String.format("Requested operation %s.", method));
                    res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                    m.toJSON(out);
                    break;
            }
        } else {
            m = new Message("Unsupported operation for URI /comment.",
                    "405", String.format("Requested operation %s.", method));
            res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
            m.toJSON(out);
        }
        return true;
    }

    /**
     * Checks for and process cateories.
     *
     * @param req The HTTP request.
     * @param res The HTTP response.
     * @return {@code true} If processed, {@code false} otherwise.
     * @throws IOException If any error occurs in the client/server communication.
     */
    private boolean processCategory(final HttpServletRequest req, final HttpServletResponse res, final Connection con) throws
            IOException {
        final String method = req.getMethod();
        final OutputStream out = res.getOutputStream();
        Message m;

        // check if the requested resource is invalid for this function
        String path = req.getRequestURI();
        if (path.lastIndexOf("rest/category") <= 0)
            return false;

        if (path.lastIndexOf("rest/category/") > 0) {
            // id is present
            m = new Message("Category REST can't be called with a parameter",
                    "405", null);
            res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
            m.toJSON(res.getOutputStream());
        } else {
            switch (method) {
                case "GET":
                    new CategoryRestResource(req, res, con).list();
                    break;
                default:
                    m = new Message("Unsupported operation for URI /category",
                            "405", String.format("Requested operation %s.", method));
                    res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                    m.toJSON(res.getOutputStream());
                    break;
            }
        }
        return true;
    }

    /**
     * Tests String to Integer conversion.
     *
     * @param s The string to convert.
     * @return Whether the String represents an Integer.
     */
    private static boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch (NumberFormatException | NullPointerException e) {
            return false;
        }
        // only got here if we didn't return false
        return true;
    }

    /**
     * Process the tag method either for Submission or ForumQuestion resources
     *
     * @param req             The HTTP request.
     * @param res             The HTTP response.
     * @param userIdFromToken The id of the authenticated user.
     * @throws IOException If any error occurs in the client/server communication.
     */
    private void processTagForResource(final HttpServletRequest req, final HttpServletResponse res, final Connection con,
                                       final long userIdFromToken) throws IOException {
        Message m;

        String method = req.getMethod();
        switch (method) {
            case "POST":
                if (userIdFromToken != AuthDatabase.INVALID_TOKEN)
                    new TagRestResource(req, res, con).addList(userIdFromToken);
                else {
                    m = new Message("User not logged in. Operation not permitted.",
                            "403", String.format("Requested operation %s.", method));
                    res.setStatus(HttpServletResponse.SC_FORBIDDEN);
                    m.toJSON(res.getOutputStream());
                }
                break;
            case "PUT":
                if (userIdFromToken != AuthDatabase.INVALID_TOKEN)
                    new TagRestResource(req, res, con).updateList(userIdFromToken);
                else {
                    m = new Message("User not logged in. Operation not permitted.",
                            "403", String.format("Requested operation %s.", method));
                    res.setStatus(HttpServletResponse.SC_FORBIDDEN);
                    m.toJSON(res.getOutputStream());
                }
                break;
            case "GET":
                new TagRestResource(req, res, con).getList();
                break;
            case "DELETE":
                if (userIdFromToken != AuthDatabase.INVALID_TOKEN)
                    new TagRestResource(req, res, con).deleteList(userIdFromToken);
                else {
                    m = new Message("User not logged in. Operation not permitted.",
                            "403", String.format("Requested operation %s.", method));
                    res.setStatus(HttpServletResponse.SC_FORBIDDEN);
                    m.toJSON(res.getOutputStream());
                }
                break;
            default:
                m = new Message("Unsupported operation for URI /{resourceType}/{id}/tag.",
                        "405", String.format("Requested operation %s.", method));
                res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                m.toJSON(res.getOutputStream());
                break;
        }
    }

    /**
     * Process the comment method either for Submission or ForumQuestion resources
     *
     * @param req             The HTTP request.
     * @param res             The HTTP response.
     * @param userIdFromToken The id of the authenticated user.
     * @throws IOException If any error occurs in the client/server communication.
     */
    private void processCommentForResource(final HttpServletRequest req, final HttpServletResponse res, final Connection con,
                                           final long userIdFromToken) throws IOException {
        Message m;

        String method = req.getMethod();
        switch (method) {
            case "GET": { // public
                long from;
                long howMany;
                long parentComment;
                try {
                    // these are passed as url parameters, not from json
                    String _from = req.getParameter("from");
                    String _howMany = req.getParameter("howMany");
                    String _parentComment = req.getParameter("parentComment");
                    if (_from != null && _howMany != null && _parentComment != null) {
                        from = Long.parseLong(_from);
                        howMany = Long.parseLong(_howMany);
                        parentComment = Long.parseLong(_parentComment);
                        if (parentComment == 0)
                            new CommentRestResource(req, res, con).listFromHowMany(from, howMany, null);
                        else
                            new CommentRestResource(req, res, con).listFromHowMany(from, howMany, parentComment);

                    } else {
                        if (_from == null && _howMany == null)
                            new CommentRestResource(req, res, con).list();
                        else {
                            m = new Message("Wrong parameters for URI /submission/{id}/comment.",
                                    "405", String.format("Requested operation %s, but passed only one between from and howMany parameters.", method));
                            res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                            m.toJSON(res.getOutputStream());
                        }
                    }
                } catch (NumberFormatException e) {
                    m = new Message("Wrong parameters for URI /submission/{id}/comment.",
                            "405", String.format("Requested operation %s.", method));
                    res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                    m.toJSON(res.getOutputStream());
                }
                break;
            }
            case "POST": // private
                if (userIdFromToken != AuthDatabase.INVALID_TOKEN)
                    new CommentRestResource(req, res, con).create(userIdFromToken);
                else {
                    m = new Message("User not logged in. Operation not permitted.",
                            "403", String.format("Requested operation %s.", method));
                    res.setStatus(HttpServletResponse.SC_FORBIDDEN);
                    m.toJSON(res.getOutputStream());
                }
                break;
            default: {
                m = new Message("Unsupported operation for URI /forum/{id}/comment.",
                        "405", String.format("Requested operation %s.", method));
                res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                m.toJSON(res.getOutputStream());
                break;
            }
        }
    }
}
