package org.tso.heapquestions.servlet;

import org.tso.heapquestions.database.AuthDatabase;
import org.tso.heapquestions.database.NotesDatabase;
import org.tso.heapquestions.resource.Message;
import org.tso.heapquestions.resource.Note;
import org.tso.heapquestions.rest.AuthRestResource;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Reads a note.
 */
public final class ReadNoteServlet extends AbstractDatabaseServlet {
    /**
     * Read a note from the database.
     *
     * @param req the HTTP request from the client.
     * @param res the HTTP response from the server.
     * @throws IOException if any error occurs in the client/server communication.
     */
    public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        Connection con = null;
        long id = -1;
        String token = null;

        Note n = null;
        Message m = null;

        try {
            // retrieves the request parameters
            id = Long.parseLong(req.getParameter("id"));
            token = req.getParameter("token");

            // extracts user from submitted token
            con = getDataSource().getConnection();
            long userIdFromToken = checkTokenGetUser(req, res, con, token);

            // delegates reading the note
            n = new NotesDatabase(con, id).read(userIdFromToken);
            m = new Message("Note successfully retrieved.");
        } catch (NumberFormatException ex) {
            m = new Message("Cannot read the note. Invalid input parameters: the id must be long.",
                    "400", ex.getMessage());
        } catch (SQLException ex) {
            m = new Message("Cannot read the note.", "500", ex.getMessage());
        } finally {
            try {
                if (con != null && !con.isClosed())
                    con.close();
            } catch (SQLException ex) {
                m = new Message("Cannot close the connection.", "500", ex.getMessage());
            }
        }

        // delegate showing to other page
        req.setAttribute("note", n);
        req.setAttribute("message", m);
        req.getRequestDispatcher("/jsp/show_note.jsp").forward(req, res);

        /* ALTERNATIVE method: pure Java to build the page (difficult to style)

        // set the MIME media type of the response
        res.setContentType("text/html; charset=utf-8");

        // get a stream to write the response
        PrintWriter out = res.getWriter();

        // write the HTML page
        out.printf("<!DOCTYPE html>%n");

        out.printf("<html lang='en'>%n");
        out.printf("<head>%n");
        out.printf("<meta charset='utf-8'>%n");
        out.printf("<title>Read Note</title>%n");
        out.printf("</head>%n");

        out.printf("<body>%n");
        out.printf("<h1>Read Note</h1>%n");
        out.printf("<hr/>%n");

        if (m.isError()) {
            out.printf("<ul>%n");
            out.printf("<li>error code: %s</li>%n", m.getErrorCode());
            out.printf("<li>message: %s</li>%n", m.getMessage());
            out.printf("<li>details: %s</li>%n", m.getErrorDetails());
            out.printf("</ul>%n");
        } else if (n != null) {
            out.printf("<p>%s</p>%n", m.getMessage());
            out.printf("<ul>%n");
            out.printf("<li>id: %s</li>%n", n.getId());
            out.printf("<li>content: %s</li>%n", n.getContent());
            out.printf("<li>date: %s</li>%n", n.getDateTime());
            out.printf("<li>user: %s</li>%n", n.getUserId());
            out.printf("</ul>%n");
        } else
            out.printf("<p>%s</p>", "Something went wrong");

        out.printf("</body>%n");
        out.printf("</html>%n");
        out.flush();
        out.close(); */
    }

    /**
     * Checks whether there is a submitted token for private RESTs requests.
     *
     * @param req The HTTP request.
     * @param res The HTTP response.
     * @return {@code true} if the submitted token is valid; {@code false} otherwise.
     */
    private long checkTokenGetUser(final HttpServletRequest req, final HttpServletResponse res, final Connection con, final String token) {
        // simply delegate user extraction from submitted token and return output
        try {
            return new AuthRestResource(req, res, con).checkTokenGetUser(token);
        } catch (SQLException e) {
            return AuthDatabase.INVALID_TOKEN;
        }
    }
}
