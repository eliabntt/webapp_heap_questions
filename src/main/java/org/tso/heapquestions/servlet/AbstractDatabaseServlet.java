// Copyright 2018 The StackOverflowers
package org.tso.heapquestions.servlet;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.http.HttpServlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Gets the {@link DataSource} for managing the connection pool to the database.
 */
public abstract class AbstractDatabaseServlet extends HttpServlet {
    /**
     * The connection pool to the database.
     */
    private DataSource ds;

    /**
     * Gets the {@link DataSource} for managing the connection pool to the database.
     *
     * @param config a {@code ServletConfig} object containing the servlet's configuration and initialization parameters.
     * @throws ServletException if an exception has occurred that interferes with the servlet's normal operation
     */
    public void init(ServletConfig config) throws ServletException {
        // the JNDI lookup context
        InitialContext cxt;

        try {
            cxt = new InitialContext();
            ds = (DataSource) cxt.lookup("java:/comp/env/jdbc/tsoDBstud");
        } catch (NamingException e) {
            ds = null;
            throw new ServletException(String.format("Impossible to access the connection pool to the database: %s", e.getMessage()));
        }
    }

    /**
     * Releases the {@link DataSource} for managing the connection pool to the database.
     */
    public void destroy() {
        ds = null;
    }

    /**
     * Returns the {@link DataSource} for managing the connection pool to the database.
     *
     * @return the {@code DataSource} for managing the connection pool to the database
     */
    protected final DataSource getDataSource() {
        return ds;
    }

    /**
     * Returns the {@link Connection} from the connection pool to the database.
     *
     * @return the {@code Connection} to the database.
     * @throws SQLException If error occurred during the connection retrieval.
     */
    protected final Connection getConnection() throws SQLException {
        return ds.getConnection();
    }
}
