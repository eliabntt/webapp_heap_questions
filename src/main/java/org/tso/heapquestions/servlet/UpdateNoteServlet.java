package org.tso.heapquestions.servlet;

import org.tso.heapquestions.database.AuthDatabase;
import org.tso.heapquestions.database.NotesDatabase;
import org.tso.heapquestions.resource.Message;
import org.tso.heapquestions.resource.Note;
import org.tso.heapquestions.rest.AuthRestResource;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Updates a note.
 */
public final class UpdateNoteServlet extends AbstractDatabaseServlet {
    /**
     * Update the note in the database.
     *
     * @param req the HTTP request from the client.
     * @param res the HTTP response from the server.
     * @throws ServletException if any error occurs while executing the servlet.
     * @throws IOException      if any error occurs in the client/server communication.
     */
    public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        Connection con = null;
        String content = null;
        String token = null;
        long id = -1;

        Note n = null;
        Message m = null;

        try {
            // retrieves the request parameters
            req.setCharacterEncoding("UTF-8");
            content = req.getParameter("content");
            token = req.getParameter("token");
            id = Long.parseLong(req.getParameter("id"));

            // extracts user from submitted token
            con = getDataSource().getConnection();
            long userIdFromToken = checkTokenGetUser(req, res, con, token);

            // creates the object and delegates update
            n = new Note(id, userIdFromToken, null, content);
            n = new NotesDatabase(con, n).update();

            if (n != null)
                m = new Message("Note successfully updated.");
            else
                m = new Message("Cannot update the note for a reason that is not an error");
        } catch (NumberFormatException ex) {
            m = new Message("Cannot update the note. Invalid input parameters: the id must be long.",
                    "400", ex.getMessage());
        } catch (SQLException ex) {
            m = new Message("Cannot update the note.", "500", ex.getMessage());
        } finally {
            try {
                if (con != null && !con.isClosed())
                    con.close();
            } catch (SQLException ex) {
                m = new Message("Cannot close the connection.", "500", ex.getMessage());
            }
        }

        // load data into the request and forward
        req.setAttribute("note", n);
        req.setAttribute("message", m);
        req.getRequestDispatcher("/jsp/show_note.jsp").forward(req, res);
    }

    /**
     * Checks whether there is a submitted token for private RESTs requests.
     *
     * @param req The HTTP request.
     * @param res The HTTP response.
     * @return {@code true} if the submitted token is valid; {@code false} otherwise.
     */
    private long checkTokenGetUser(final HttpServletRequest req, final HttpServletResponse res, final Connection con, final String token) {
        // simply delegate user extraction from submitted token and return output
        try {
            return new AuthRestResource(req, res, con).checkTokenGetUser(token);
        } catch (SQLException e) {
            return AuthDatabase.INVALID_TOKEN;
        }
    }
}
