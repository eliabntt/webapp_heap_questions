// Copyright 2018 The StackOverflowers

/**
 * Manages the Web application (controller and view).
 */
package org.tso.heapquestions.servlet;