// Copyright 2018 The StackOverflowers
package org.tso.heapquestions.rest;

import org.tso.heapquestions.database.CategoryDatabase;
import org.tso.heapquestions.resource.Category;
import org.tso.heapquestions.resource.Message;
import org.tso.heapquestions.resource.ResourceList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.util.List;

/**
 * Represents a REST resource for {@link Category} resources.
 */
public class CategoryRestResource extends RestResource {
    /**
     * Creates a new REST resource for categories.
     *
     * @param req The HTTP request.
     * @param res The HTTP response.
     * @param con The connection to the database.
     */
    public CategoryRestResource(HttpServletRequest req, HttpServletResponse res, Connection con) {
        super(req, res, con);
    }

    /**
     * Lists all categories.
     *
     * @throws IOException If errors occurred during operations.
     */
    public void list() throws IOException {
        List<Category> cl;
        Message m;

        try {
            // creates a new object for accessing the database and lists all the categories
            cl = new CategoryDatabase(con).list();
            if (cl != null) {
                res.setStatus(HttpServletResponse.SC_OK);
                new ResourceList(cl).toJSON(res.getOutputStream());
            } else {
                // it should not happen
                m = new Message("Cannot list categories, unexpected error.", "500", null);
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        } catch (Throwable t) {
            m = new Message("Cannot list categories, unexpected error.", "500", t.getMessage());
            res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            m.toJSON(res.getOutputStream());
        }
    }
}
