// Copyright 2018 The StackOverflowers

/**
 * Implements the REST resources API.
 */
package org.tso.heapquestions.rest;