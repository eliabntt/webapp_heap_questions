// Copyright 2018 The StackOverflowers
package org.tso.heapquestions.rest;

import org.tso.heapquestions.database.AuthDatabase;
import org.tso.heapquestions.database.CommentDatabase;
import org.tso.heapquestions.database.ForumDatabase;
import org.tso.heapquestions.resource.*;

import javax.naming.AuthenticationException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * Represents a REST resource for {@link ForumQuestion} resources.
 */
public class ForumQuestionRestResource extends RestResource {
    /**
     * Creates a new REST resource for forum questions.
     *
     * @param req The HTTP request.
     * @param res The HTTP response.
     * @param con The connection to the database.
     */
    public ForumQuestionRestResource(HttpServletRequest req, HttpServletResponse res, Connection con) {
        super(req, res, con);
    }

    /**
     * Creates a forum question.
     *
     * @param userId The ID of the user that requested the operation.
     * @throws IOException If errors occurred during operations.
     */
    public void create(final long userId) throws IOException {
        ForumQuestion fq;
        Message m;
        try {
            final ForumQuestion question = ForumQuestion.fromJSON(req.getInputStream());
            fq = new ForumDatabase(con, question).create(userId);

            if (fq != null) {
                res.setStatus(HttpServletResponse.SC_CREATED);
                fq.toJSON(res.getOutputStream());
            } else {
                m = new Message("Cannot create the forum question, unexpected error.", "500", null);
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        } catch (Throwable t) {
            if (t instanceof SQLException && ((SQLException) t).getSQLState().equals("23505")) {
                m = new Message("Cannot create the forum question, it already exists.", "409", t.getMessage());
                res.setStatus(HttpServletResponse.SC_CONFLICT);
                m.toJSON(res.getOutputStream());
            } else {
                m = new Message("Cannot create the forum question, unexpected error.", "500", t.getMessage());
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        }
    }

    /**
     * Returns a forum question.
     *
     * @throws IOException If errors occurred during operations.
     */
    public void read() throws IOException {
        ForumQuestion fq;
        Message m;

        try {
            // parse the URI path to extract the id
            String path = req.getRequestURI();
            path = path.substring(path.lastIndexOf("forum") + "forum".length());
            final long id = Long.parseLong(path.substring(1));

            // creates a new object for accessing the database and reads the forum question
            fq = new ForumDatabase(con, id).read();
            if (fq != null) {
                res.setStatus(HttpServletResponse.SC_OK);
                fq.toJSON(res.getOutputStream());
            } else {
                m = new Message(String.format("Question %d not found.", id), "404", null);
                res.setStatus(HttpServletResponse.SC_NOT_FOUND);
                m.toJSON(res.getOutputStream());
            }
        } catch (Throwable t) {
            m = new Message("Cannot read forum question, unexpected error.", "500", t.getMessage());
            res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            m.toJSON(res.getOutputStream());
        }
    }


    /**
     * Returns the answer for a forum question.
     *
     * @throws IOException If errors occurred during operations.
     */
    public void readAnswer() throws IOException {
        Comment c;
        Message m;

        try {
            // parse the URI path to extract the id
            String path = req.getRequestURI();
            path = path.substring(path.lastIndexOf("forum") + "forum".length());
            final long id = Long.parseLong(path.substring(1, path.indexOf("answer") - 1));

            // creates a new object for accessing the database and reads the forum question's answer
            c = new CommentDatabase(con, id).getAnswer();
            if (c != null) {
                res.setStatus(HttpServletResponse.SC_OK);
                c.toJSON(res.getOutputStream());
            } else {
                m = new Message(String.format("Answer to the question %d not found.", id), "404", null);
                res.setStatus(HttpServletResponse.SC_NOT_FOUND);
                m.toJSON(res.getOutputStream());
            }
        } catch (Throwable t) {
            m = new Message("Cannot read the answer of the forum question, unexpected error.", "500", t.getMessage());
            res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            m.toJSON(res.getOutputStream());
        }
    }


    /**
     * Updates a forum question.
     *
     * @param userIdFromToken User ID extracted from authorization token.
     * @throws IOException If errors occurred during operations.
     */
    public void update(final long userIdFromToken) throws IOException {
        ForumQuestion fq;
        Message m;

        try {
            // parse the URI path to extract the id
            String path = req.getRequestURI();
            path = path.substring(path.lastIndexOf("forum") + "forum".length());
            final long resourceId = Long.parseLong(path.substring(1));

            final ForumQuestion question = ForumQuestion.fromJSON(req.getInputStream());
            if (resourceId != question.getId()) {
                m = new Message("Wrong request for URI /forumQuestion/{id}: URI request and question resource id differ.",
                        "400", String.format("Request URI id %d; user resource id %d.", resourceId, question.getId()));
                res.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                m.toJSON(res.getOutputStream());
            }

            // check if user is authorized to manage resource
            // throws exception if unauthorized else does nothing
            new AuthDatabase(con).isAuthForResource(userIdFromToken, resourceId, ResourceSelector.FORUM_QUESTION);

            // creates a new object for accessing the database and updates the forum question
            fq = new ForumDatabase(con, question).update();
            if (fq != null) {
                res.setStatus(HttpServletResponse.SC_OK);
                fq.toJSON(res.getOutputStream());
            } else {
                m = new Message(String.format("Question %d cannot be updated.", resourceId), "406", null);
                res.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
                m.toJSON(res.getOutputStream());
            }
        } catch (Throwable t) {
            if (t instanceof SQLException && ((SQLException) t).getSQLState().equals("23503")) {
                m = new Message("Cannot update the forum question: other resources depend on it.", "409", t.getMessage());
                res.setStatus(HttpServletResponse.SC_CONFLICT);
                m.toJSON(res.getOutputStream());
            } else if (t instanceof AuthenticationException) {
                m = new Message("Cannot update the forum question: current user is not authorized.", "401", t.getMessage());
                res.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                m.toJSON(res.getOutputStream());
            } else {
                m = new Message("Cannot update the forum question: unexpected error.", "500", t.getMessage());
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        }
    }

    /**
     * Deletes a forum question (actually updates its state).
     *
     * @param userIdFromToken User ID extracted from authorization token.
     * @throws IOException If errors occurred during operations.
     */
    public void delete(final long userIdFromToken) throws IOException {
        ForumQuestion fq;
        Message m;

        try {
            // parse the URI path to extract the id
            String path = req.getRequestURI();
            path = path.substring(path.lastIndexOf("forum") + "forum".length());
            final long resourceId = Long.parseLong(path.substring(1));

            // read the question from the JSON representation
            ForumQuestion forumQuestion = ForumQuestion.fromJSON(req.getInputStream());

            // check if user is authorized to manage resource
            // throws exception if unauthorized else does nothing
            switch (forumQuestion.getState()) {
                case Deleted: {
                    // checks for author or admin
                    new AuthDatabase(con).isAuthForResource(userIdFromToken, resourceId, ResourceSelector.FORUM_QUESTION);
                    break;
                }
                case Moderated: {
                    // checks for admin
                    new AuthDatabase(con).isAuthForResource(userIdFromToken);
                    break;
                }
                default: {
                    // error message: invalid usage
                    m = new Message("Forum question state not valid for this call.", "403", null);
                    res.setStatus(HttpServletResponse.SC_FORBIDDEN);
                    m.toJSON(res.getOutputStream());
                    break;
                }
            }

            // creates a new object for accessing the database and deletes the forum question
            fq = new ForumDatabase(con, forumQuestion, resourceId).deleteChangeState();

            if (fq != null) {
                res.setStatus(HttpServletResponse.SC_OK);
                fq.toJSON(res.getOutputStream());
            } else {
                m = new Message(String.format("Question %d cannot be deleted.", resourceId), "406", null);
                res.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
                m.toJSON(res.getOutputStream());
            }
        } catch (Throwable t) {
            if (t instanceof SQLException && ((SQLException) t).getSQLState().equals("23503")) {
                m = new Message("Cannot change state of the forum question: other resources depend on it.", "409", t.getMessage());
                res.setStatus(HttpServletResponse.SC_CONFLICT);
                m.toJSON(res.getOutputStream());
            } else if (t instanceof AuthenticationException) {
                m = new Message("Cannot change state of the forum question: current user is not authorized.", "401", t.getMessage());
                res.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                m.toJSON(res.getOutputStream());
            } else {
                m = new Message("Cannot change state of the forum question: unexpected error.", "500", t.getMessage());
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        }
    }

    /**
     * Lists all or some forum questions.
     *
     * @throws IOException If errors occurred during operations.
     */
    public void list() throws IOException {
        List<ForumQuestion> fql;
        Message m;

        try {
            String path;
            try {
                path = new URI(req.getRequestURI()).getPath();
            } catch (URISyntaxException e) {
                // this should NEVER be called!
                path = req.getRequestURI();
            }

            if (path.lastIndexOf("user") > 0) {
                path = path.substring(path.lastIndexOf("user") + "user".length());
                final long userId = Long.parseLong(path.substring(1, path.indexOf("forum") - 1));
                try {
                    long from = Long.parseLong(req.getParameter("from"));
                    long howMany = Long.parseLong(req.getParameter("howMany"));
                    fql = new ForumDatabase(con, userId).listByUserFromHowMany(from, howMany);
                } catch (NumberFormatException e) {
                    fql = new ForumDatabase(con, userId).list();
                }
            } else
                try {
                    long from = Long.parseLong(req.getParameter("from"));
                    long howMany = Long.parseLong(req.getParameter("howMany"));
                    fql = new ForumDatabase(con).listFromHowMany(from, howMany);
                } catch (NumberFormatException e) {
                    fql = new ForumDatabase(con).list();
                }

            if (fql != null) {
                res.setStatus(HttpServletResponse.SC_OK);
                new ResourceList(fql).toJSON(res.getOutputStream());
            } else {
                // it should not happen
                m = new Message("Cannot list forum questions: unexpected error.", "500", null);
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        } catch (Throwable t) {
            m = new Message("Cannot search forum questions: unexpected error.", "500", t.getMessage());
            res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            m.toJSON(res.getOutputStream());
        }
    }

    /**
     * Lists some forum questions for a searched term.
     *
     * @param from    Search starting point: the {@code end - from}-th question.
     * @param howMany How many question to return from the starting point.
     * @throws IOException If errors occurred during operations.
     */
    public void listFromHowMany(final long from, final long howMany) throws IOException {
        List<ForumQuestion> fq;
        Message m;

        try {
            final String searchingTitle;

            String path;
            try {
                path = new URI(req.getRequestURI()).getPath();
            } catch (URISyntaxException e) {
                // this should NEVER be called!
                path = req.getRequestURI();
            }

            if (path.lastIndexOf("title") > 0) {
                path = path.substring(path.lastIndexOf("title") + "title".length());
                searchingTitle = path.substring(1);
            } else {
                // should not happen
                throw new Exception("Wrong URL");
            }

            fq = new ForumDatabase(con, searchingTitle).getSearchList(from, howMany);

            // check results
            if (fq != null) {
                res.setStatus(HttpServletResponse.SC_OK);
                new ResourceList(fq).toJSON(res.getOutputStream());
            } else {
                // it should not happen
                m = new Message("Cannot list questions, unexpected error.", "500", null);
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        } catch (Throwable t) {
            // manage errors
            m = new Message("Cannot find questions, unexpected error.", "500", t.getMessage());
            res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            m.toJSON(res.getOutputStream());
        }
    }
}
