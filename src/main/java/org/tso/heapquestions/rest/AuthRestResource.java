// Copyright 2018 The StackOverflowers
package org.tso.heapquestions.rest;

import org.tso.heapquestions.database.AuthDatabase;
import org.tso.heapquestions.resource.*;

import javax.naming.AuthenticationException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Represents a REST resource for user's login.
 */
public class AuthRestResource extends RestResource {
    /**
     * Creates a new REST resource for user's login.
     *
     * @param req The HTTP request.
     * @param res The HTTP response.
     * @param con The connection to the database.
     */
    public AuthRestResource(HttpServletRequest req, HttpServletResponse res, Connection con) {
        super(req, res, con);
    }

    /**
     * The main login function.
     *
     * @throws IOException If errors occurred during operations.
     */
    public void login() throws IOException {
        try {
            final LoginItem item = LoginItem.fromJSON(req.getInputStream());

            // try to login and get response
            Token credentials = new AuthDatabase(con, item.getUserData(), item.getPassword(), item.doRemember()).doLogin();
            if (credentials == null) {
                // login not valid, show appropriate message
                Message m = new Message("Login failed", "401", "Please check username and password.");
                res.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                m.toJSON(res.getOutputStream());
            } else {
                // login succeeded, send back JSON with authorization info
                credentials.toJSON(res.getOutputStream());
            }
        } catch (Throwable t) {
            // this should not be called, if so, some server problems occurred.
            Message m = new Message("Login failed", "500", t.getMessage());
            res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            m.toJSON(res.getOutputStream());
        }
    }

    /**
     * The main logout function.
     *
     * @throws IOException If errors occurred during operations.
     */
    public void logout() throws IOException {
        Message m;
        try {
            // no need to pass userIdFromToken since it's retrieved from request's header
            final String token = req.getHeader("X-TSO-Auth-Token");
            // try to logout and get response
            boolean done = new AuthDatabase(con, token).doLogout();
            if (done) {
                // logout completed
                m = new Message("Logout completed successfully");
                res.setStatus(HttpServletResponse.SC_OK);
                m.toJSON(res.getOutputStream());
            } else {
                // error on logout
                m = new Message("Errors during logout", "500", null);
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        } catch (Throwable t) {
            // this should not be called, if so, some server problems occur.
            m = new Message("Errors during logout", "500", t.getMessage());
            res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            m.toJSON(res.getOutputStream());
        }
    }

    /**
     * The token validity checking function.
     *
     * @return Whether the token is a valid one.
     * @throws SQLException If errors occurred during database operations.
     */
    public long checkTokenGetUser() throws SQLException {
        final String token = req.getHeader("X-TSO-Auth-Token");
        return new AuthDatabase(con, token).checkTokenGetUser();
    }

    /**
     * The token validity checking function.
     *
     * @param token The token to check.
     * @return Whether the token is a valid one.
     * @throws SQLException If errors occurred during database operations.
     */
    public long checkTokenGetUser(String token) throws SQLException {
        return new AuthDatabase(con, token).checkTokenGetUser();
    }

    /**
     * Checks if an user is authorized to manage a Resource (of type {@link Submission} or {@link ForumQuestion} or {@link Comment}).
     *
     * @param authId       User ID to test.
     * @param resourceId   Resource ID.
     * @param resourceType Resource type.
     * @throws SQLException            If error occurs during operations.
     * @throws AuthenticationException If the user is not authorized to continue.
     * @deprecated Should never be used, in favor of calling directly {@link AuthDatabase}.
     */
    public void checkAuthorization(final long authId, final long resourceId, final ResourceSelector resourceType)
            throws SQLException, AuthenticationException {
        new AuthDatabase(con).isAuthForResource(authId, resourceId, resourceType);
    }


    /**
     * Checks if an user is authorized to manage a Resource (of type {@link Submission} or {@link ForumQuestion} or {@link Comment}).
     *
     * @param authId User ID to test.
     * @throws SQLException            If error occurs during operations.
     * @throws AuthenticationException If the user is not authorized to continue.
     * @deprecated Should never be used, in favor of calling directly {@link AuthDatabase}.
     */
    public void checkAuthorization(final long authId)
            throws SQLException, AuthenticationException {
        new AuthDatabase(con).isAuthForResource(authId);
    }
}
