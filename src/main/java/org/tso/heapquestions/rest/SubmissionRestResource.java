// Copyright 2018 The StackOverflowers
package org.tso.heapquestions.rest;

import org.tso.heapquestions.database.AuthDatabase;
import org.tso.heapquestions.database.SubmissionDatabase;
import org.tso.heapquestions.resource.Message;
import org.tso.heapquestions.resource.ResourceList;
import org.tso.heapquestions.resource.ResourceSelector;
import org.tso.heapquestions.resource.Submission;

import javax.naming.AuthenticationException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * Represents a REST resource for {@link Submission} resources.
 */
public class SubmissionRestResource extends RestResource {
    /**
     * Creates a new REST resource for submissions.
     *
     * @param req The HTTP request.
     * @param res The HTTP response.
     * @param con The connection to the database.
     */
    public SubmissionRestResource(HttpServletRequest req, HttpServletResponse res, Connection con) {
        super(req, res, con);
    }

    /**
     * Creates a submission.
     *
     * @param userId The ID of the user that requested the operation.
     * @throws IOException If errors occurred during operations.
     */
    public void create(final long userId) throws IOException {
        Submission s;
        Message m;
        try {
            final Submission submission = Submission.fromJSON(req.getInputStream());
            s = new SubmissionDatabase(con, submission).create(userId);

            if (s != null) {
                res.setStatus(HttpServletResponse.SC_CREATED);
                s.toJSON(res.getOutputStream());
            } else {
                m = new Message("Cannot create the submission, unexpected error.", "500", null);
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        } catch (Throwable t) {
            if (t instanceof SQLException && ((SQLException) t).getSQLState().equals("23505")) {
                m = new Message("Cannot create the submission, it already exists.", "409", t.getMessage());
                res.setStatus(HttpServletResponse.SC_CONFLICT);
                m.toJSON(res.getOutputStream());
            } else {
                m = new Message("Cannot create the submission, unexpected error.", "500", t.getMessage());
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        }
    }

    /**
     * Returns a submission.
     *
     * @throws IOException If errors occurred during operations.
     */
    public void read() throws IOException {
        Submission s;
        Message m;

        try {
            // parse the URI path to extract the id
            String path = req.getRequestURI();
            path = path.substring(path.lastIndexOf("submission") + "submission".length());
            final long id = Long.parseLong(path.substring(1));

            // creates a new object for accessing the database and reads the submission
            s = new SubmissionDatabase(con, id).read();
            if (s != null) {
                res.setStatus(HttpServletResponse.SC_OK);
                s.toJSON(res.getOutputStream());
            } else {
                m = new Message(String.format("Submission %d not found.", id), "404", null);
                res.setStatus(HttpServletResponse.SC_NOT_FOUND);
                m.toJSON(res.getOutputStream());
            }
        } catch (Throwable t) {
            m = new Message("Cannot read submission, unexpected error.", "500", t.getMessage());
            res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            m.toJSON(res.getOutputStream());
        }
    }

    /**
     * Updates a submission.
     *
     * @param userIdFromToken User ID extracted from authorization token.
     * @throws IOException If errors occurred during operations.
     */
    public void update(final long userIdFromToken) throws IOException {
        Submission s;
        Message m;

        try {
            // parse the URI path to extract the id
            String path = req.getRequestURI();
            path = path.substring(path.lastIndexOf("submission") + "submission".length());
            final long resourceId = Long.parseLong(path.substring(1));

            final Submission submission = Submission.fromJSON(req.getInputStream());

            if (resourceId != submission.getId()) {
                m = new Message("Wrong request for URI /submission/{id}: URI request and submission resource id differ.",
                        "400", String.format("Request URI id %d; submission resource id %d.", resourceId, submission.getId()));
                res.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                m.toJSON(res.getOutputStream());
            }

            // check if user is authorized to manage resource
            // throws exception if unauthorized else does nothing
            new AuthDatabase(con).isAuthForResource(userIdFromToken, resourceId, ResourceSelector.SUBMISSION);

            // creates a new object for accessing the database and updates the submission
            s = new SubmissionDatabase(con, submission).update();
            if (s != null) {
                res.setStatus(HttpServletResponse.SC_OK);
                s.toJSON(res.getOutputStream());
            } else {
                m = new Message(String.format("Submission %d cannot be updated.", resourceId), "406", null);
                res.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
                m.toJSON(res.getOutputStream());
            }
        } catch (Throwable t) {
            if (t instanceof SQLException && ((SQLException) t).getSQLState().equals("23503")) {
                m = new Message("Cannot update the submission: other resources depend on it.", "409", t.getMessage());
                res.setStatus(HttpServletResponse.SC_CONFLICT);
                m.toJSON(res.getOutputStream());
            } else if (t instanceof AuthenticationException) {
                m = new Message("Cannot update the submission: current user is not authorized.", "401", t.getMessage());
                res.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                m.toJSON(res.getOutputStream());
            } else {
                m = new Message("Cannot update the submission, unexpected error.", "500", t.getMessage());
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        }
    }

    /**
     * Deletes a submission (actually updates its state).
     *
     * @param userIdFromToken User ID extracted from authorization token.
     * @throws IOException If errors occurred during operations.
     */
    public void delete(final long userIdFromToken) throws IOException {
        Submission s;
        Message m;

        try {
            // parse the URI path to extract the id
            String path = req.getRequestURI();
            path = path.substring(path.lastIndexOf("submission") + "submission".length());
            final long resourceId = Long.parseLong(path.substring(1));

            // read the submission from the JSON representation
            Submission submission = Submission.fromJSON(req.getInputStream());

            // check if user is authorized to manage resource
            // throws exception if unauthorized else does nothing
            switch (submission.getState()) {
                case Deleted: {
                    // checks for author or admin
                    new AuthDatabase(con).isAuthForResource(userIdFromToken, resourceId, ResourceSelector.SUBMISSION);
                    break;
                }
                case Moderated: {
                    // checks for admin
                    new AuthDatabase(con).isAuthForResource(userIdFromToken);
                    break;
                }
                default: {
                    // error message: invalid usage
                    m = new Message("Submission state not valid for this call.", "403", null);
                    res.setStatus(HttpServletResponse.SC_FORBIDDEN);
                    m.toJSON(res.getOutputStream());
                    break;
                }
            }

            // creates a new object for accessing the database and deletes the submission
            s = new SubmissionDatabase(con, submission, resourceId).deleteChangeState();
            if (s != null) {
                res.setStatus(HttpServletResponse.SC_OK);
                s.toJSON(res.getOutputStream());
            } else {
                m = new Message(String.format("Submission %d cannot be deleted.", resourceId), "406", null);
                res.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
                m.toJSON(res.getOutputStream());
            }
        } catch (Throwable t) {
            if (t instanceof SQLException && ((SQLException) t).getSQLState().equals("23503")) {
                m = new Message("Cannot change state of the submission: other resources depend on it.", "409", t.getMessage());
                res.setStatus(HttpServletResponse.SC_CONFLICT);
                m.toJSON(res.getOutputStream());
            } else if (t instanceof AuthenticationException) {
                m = new Message("Cannot change state of the submission: current user is not authorized.", "401", t.getMessage());
                res.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                m.toJSON(res.getOutputStream());
            } else {
                m = new Message("Cannot change state of the submission: unexpected error.", "500", t.getMessage());
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        }
    }

    /**
     * Lists all or some submissions.
     *
     * @throws IOException If errors occurred during operations.
     */
    public void list() throws IOException {
        List<Submission> sl;
        Message m;

        try {
            String path;
            try {
                path = new URI(req.getRequestURI()).getPath();
            } catch (URISyntaxException e) {
                // this should NEVER be called!
                path = req.getRequestURI();
            }

            if (path.lastIndexOf("user") > 0) {
                path = path.substring(path.lastIndexOf("user") + "user".length());
                final long userId = Long.parseLong(path.substring(1, path.indexOf("submission") - 1));
                try {
                    long from = Long.parseLong(req.getParameter("from"));
                    long howMany = Long.parseLong(req.getParameter("howMany"));
                    sl = new SubmissionDatabase(con, userId).listByUserFromHowMany(from, howMany);
                } catch (NumberFormatException e) {
                    sl = new SubmissionDatabase(con, userId).list();
                }
            } else
                try {
                    long from = Long.parseLong(req.getParameter("from"));
                    long howMany = Long.parseLong(req.getParameter("howMany"));
                    sl = new SubmissionDatabase(con).listFromHowMany(from, howMany);
                } catch (NumberFormatException e) {
                    sl = new SubmissionDatabase(con).list();
                }

            if (sl != null) {
                res.setStatus(HttpServletResponse.SC_OK);
                new ResourceList(sl).toJSON(res.getOutputStream());
            } else {
                // it should not happen
                m = new Message("Cannot list submissions, unexpected error.", "500", null);
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        } catch (Throwable t) {
            m = new Message("Cannot search submissions, unexpected error.", "500", t.getMessage());
            res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            m.toJSON(res.getOutputStream());
        }
    }

    /**
     * Lists some submissions for a searched term.
     *
     * @param from    Search starting point: the {@code end - from}-th submission.
     * @param howMany How many submission to return from the starting point.
     * @throws IOException If errors occurred during operations.
     */
    public void listFromHowMany(final long from, final long howMany) throws IOException {
        List<Submission> subs;
        Message m;

        try {
            final String searchingTitle;

            String path;
            try {
                path = new URI(req.getRequestURI()).getPath();
            } catch (URISyntaxException e) {
                // this should NEVER be called!
                path = req.getRequestURI();
            }

            if (path.lastIndexOf("title") > 0) {
                path = path.substring(path.lastIndexOf("title") + "title".length());
                searchingTitle = path.substring(1);
            } else {
                // should not happen
                throw new Exception("Wrong URL");
            }

            subs = new SubmissionDatabase(con, searchingTitle).getSearchList(from, howMany);

            // check results
            if (subs != null) {
                res.setStatus(HttpServletResponse.SC_OK);
                new ResourceList(subs).toJSON(res.getOutputStream());
            } else {
                // it should not happen
                m = new Message("Cannot list submissions, unexpected error.", "500", null);
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        } catch (Throwable t) {
            // manage errors
            m = new Message("Cannot find submissions, unexpected error.", "500", t.getMessage());
            res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            m.toJSON(res.getOutputStream());
        }
    }
}
