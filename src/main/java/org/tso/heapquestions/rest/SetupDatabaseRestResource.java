// Copyright 2018 The StackOverflowers
package org.tso.heapquestions.rest;

import org.tso.heapquestions.database.SetupDatabase;
import org.tso.heapquestions.resource.Message;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * @deprecated Creates a REST resource to setup a database. Only for private tests.
 */
public class SetupDatabaseRestResource extends RestResource {
    /**
     * Creates a new REST resource for database setup.
     *
     * @param req the HTTP request.
     * @param res the HTTP response.
     * @param con the connection to the database.
     */
    public SetupDatabaseRestResource(HttpServletRequest req, HttpServletResponse res, Connection con) {
        super(req, res, con);
    }

    /**
     * Gets the database operative.
     *
     * @throws IOException If errors occurred during operations.
     */
    public void setupDatabase() throws IOException {
        try {
            boolean ok = new SetupDatabase(con).setup();
            if (ok) {
                res.setStatus(HttpServletResponse.SC_CREATED);
            } else {
                Message m = new Message("Cannot create schema: unexpected error.", "500", "Setup failed.");
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        } catch (SQLException e) {
            Message m = new Message("Cannot create schema: unexpected error.", "500", e.getMessage());
            res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            m.toJSON(res.getOutputStream());
        }
    }
}
