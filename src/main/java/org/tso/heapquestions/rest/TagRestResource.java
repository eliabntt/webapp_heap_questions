// Copyright 2018 The StackOverflowers
package org.tso.heapquestions.rest;

import org.tso.heapquestions.database.AuthDatabase;
import org.tso.heapquestions.database.TagDatabase;
import org.tso.heapquestions.resource.*;

import javax.naming.AuthenticationException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.util.List;

/**
 * Represents a REST resource for {@link Tag} resources.
 */
public class TagRestResource extends RestResource {
    /**
     * Creates a new REST resource for tags.
     *
     * @param req The HTTP request.
     * @param res The HTTP response.
     * @param con The connection to the database.
     */
    public TagRestResource(HttpServletRequest req, HttpServletResponse res, Connection con) {
        super(req, res, con);
    }

    /**
     * Adds a list of tags to a Resource (of type {@link Submission} or {@link ForumQuestion}).
     *
     * @param userId The ID of the user that requested the operation.
     * @throws IOException If errors occurred during operations.
     */
    public void addList(final long userId) throws IOException {
        Tag tags;
        Message m;

        try {
            String path = req.getRequestURI();
            final long resourceId;
            final ResourceSelector resourceType;

            if (path.lastIndexOf("submission") > 0) {
                // list tags of a submission
                path = path.substring(path.lastIndexOf("submission") + "submission".length());
                resourceId = Long.parseLong(path.substring(1, path.indexOf("tag") - 1));
                resourceType = ResourceSelector.SUBMISSION;
            } else if (path.lastIndexOf("forum") > 0) {
                // list tags of a forum question
                path = path.substring(path.lastIndexOf("forum") + "forum".length());
                resourceId = Long.parseLong(path.substring(1, path.indexOf("tag") - 1));
                resourceType = ResourceSelector.FORUM_QUESTION;
            } else {
                // should not happen
                throw new Exception("Wrong URL");
            }

            // check if user can update the company (it's an admin)
            new AuthDatabase(con).isAuthForResource(userId, resourceId, resourceType);

            // access the json object and the database to get the list
            final List<String> values = Tag.fromJSON(req.getInputStream());
            Tag tag = new Tag(values);
            tags = new TagDatabase(con, tag, resourceId, resourceType).addTagsToResource();

            // check results
            if (tags != null) {
                res.setStatus(HttpServletResponse.SC_OK);
                tags.toJSON(res.getOutputStream());
            } else {
                // it should not happen
                m = new Message("Cannot add tags, unexpected error.", "500", null);
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        } catch (Throwable t) {
            // manage errors
            if (t instanceof AuthenticationException) {
                m = new Message("Cannot add the tags: current user is not authorized.", "401", t.getMessage());
                res.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                m.toJSON(res.getOutputStream());
            } else {
                m = new Message("Cannot add tags, unexpected error.", "500", t.getMessage());
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        }
    }

    /**
     * Gets a list of tags associated to a Resource (of type {@link Submission} or {@link ForumQuestion}).
     *
     * @throws IOException If errors occurred during operations.
     */
    public void getList() throws IOException {
        Tag tags;
        Message m;

        try {
            String path = req.getRequestURI();
            final long resourceId;
            final ResourceSelector resourceType;

            if (path.lastIndexOf("submission") > 0) {
                // list tags of a submission
                path = path.substring(path.lastIndexOf("submission") + "submission".length());
                resourceId = Long.parseLong(path.substring(1, path.indexOf("tag") - 1));
                resourceType = ResourceSelector.SUBMISSION;
            } else if (path.lastIndexOf("forum") > 0) {
                // list tags of a forum question
                path = path.substring(path.lastIndexOf("forum") + "forum".length());
                resourceId = Long.parseLong(path.substring(1, path.indexOf("tag") - 1));
                resourceType = ResourceSelector.FORUM_QUESTION;
            } else {
                // should not happen
                throw new Exception("Wrong URL");
            }

            // access the database to get the list
            tags = new TagDatabase(con, resourceId, resourceType).readTagsForResource();

            // check results
            if (tags != null) {
                res.setStatus(HttpServletResponse.SC_OK);
                tags.toJSON(res.getOutputStream());
            } else {
                // it should not happen
                m = new Message("Cannot list tags, unexpected error.", "500", null);
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        } catch (Throwable t) {
            // manage errors
            m = new Message("Cannot list tags, unexpected error.", "500", t.getMessage());
            res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            m.toJSON(res.getOutputStream());
        }
    }

    /**
     * Updates a list of tags associated to a Resource (of type {@link Submission} or {@link ForumQuestion}).
     *
     * @param userId The ID of the user that requested the operation.
     * @throws IOException If errors occurred during operations.
     */
    public void updateList(final long userId) throws IOException {
        Tag tags;
        Message m;

        try {
            String path = req.getRequestURI();
            final long resourceId;
            final ResourceSelector resourceType;

            if (path.lastIndexOf("submission") > 0) {
                // list tags of a submission
                path = path.substring(path.lastIndexOf("submission") + "submission".length());
                resourceId = Long.parseLong(path.substring(1, path.indexOf("tag") - 1));
                resourceType = ResourceSelector.SUBMISSION;
            } else if (path.lastIndexOf("forum") > 0) {
                // list tags of a forum question
                path = path.substring(path.lastIndexOf("forum") + "forum".length());
                resourceId = Long.parseLong(path.substring(1, path.indexOf("tag") - 1));
                resourceType = ResourceSelector.FORUM_QUESTION;
            } else {
                // should not happen
                throw new Exception("Wrong URL");
            }

            // check if user can update the company (it's an admin)
            new AuthDatabase(con).isAuthForResource(userId, resourceId, resourceType);

            // access the json object and the database to get the list
            final List<String> values = Tag.fromJSON(req.getInputStream());
            Tag tag = new Tag(values);
            tags = new TagDatabase(con, tag, resourceId, resourceType).updateTagsForResource();

            // check results
            if (tags != null) {
                res.setStatus(HttpServletResponse.SC_OK);
                tags.toJSON(res.getOutputStream());
            } else {
                // it should not happen
                m = new Message("Cannot update tags, unexpected error.", "500", null);
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        } catch (Throwable t) {
            // manage errors
            if (t instanceof AuthenticationException) {
                m = new Message("Cannot update the tag: current user is not authorized.", "401", t.getMessage());
                res.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                m.toJSON(res.getOutputStream());
            } else {
                m = new Message("Cannot update tags, unexpected error.", "500", t.getMessage());
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        }
    }

    /**
     * Deletes a list of tags associated to a Resource (of type {@link Submission} or {@link ForumQuestion}).
     *
     * @param userId The ID of the user that requested the operation.
     * @throws IOException If errors occurred during operations.
     */
    public void deleteList(final long userId) throws IOException {
        Tag tags;
        Message m;

        try {
            String path = req.getRequestURI();
            final long resourceId;
            final ResourceSelector resourceType;

            if (path.lastIndexOf("submission") > 0) {
                // list tags of a submission
                path = path.substring(path.lastIndexOf("submission") + "submission".length());
                resourceId = Long.parseLong(path.substring(1, path.indexOf("tag") - 1));
                resourceType = ResourceSelector.SUBMISSION;
            } else if (path.lastIndexOf("forum") > 0) {
                // list tags of a forum question
                path = path.substring(path.lastIndexOf("forum") + "forum".length());
                resourceId = Long.parseLong(path.substring(1, path.indexOf("tag") - 1));
                resourceType = ResourceSelector.FORUM_QUESTION;
            } else {
                // should not happen
                throw new Exception("Wrong URL");
            }

            // check if user can update the company (it's an admin)
            new AuthDatabase(con).isAuthForResource(userId, resourceId, resourceType);

            // access the database to get the list
            tags = new TagDatabase(con, resourceId, resourceType).deleteTagsForResource();

            // check results
            if (tags != null) {
                res.setStatus(HttpServletResponse.SC_OK);
                tags.toJSON(res.getOutputStream());
            } else {
                // it should not happen
                m = new Message("Cannot delete tags, unexpected error.", "500", null);
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        } catch (Throwable t) {
            // manage errors
            if (t instanceof AuthenticationException) {
                m = new Message("Cannot delete the tags: current user is not authorized.", "401", t.getMessage());
                res.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                m.toJSON(res.getOutputStream());
            } else {
                m = new Message("Cannot delete the tags, unexpected error.", "500", t.getMessage());
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        }
    }

    /**
     * Gets a list of tags similar to a specific one.
     *
     * @throws IOException If errors occurred during operations.
     */
    public void listSimilarTags() throws IOException {
        Tag tags;
        Message m;
        try {
            String path = req.getRequestURI();
            final String tagToQuery;

            if (path.lastIndexOf("tag") > 0) {
                // list tags of a submission
                path = path.substring(path.lastIndexOf("tag") + "tag".length());
                tagToQuery = path.substring(1);
            } else {
                // should not happen
                throw new Exception("Wrong URL");
            }

            // access the database to get the list
            tags = new TagDatabase(con).getSimilarTag(tagToQuery);

            // check results
            if (tags != null) {
                res.setStatus(HttpServletResponse.SC_OK);
                tags.toJSON(res.getOutputStream());
            } else {
                // it should not happen
                m = new Message("Cannot list similar tags, unexpected error.", "500", null);
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        } catch (Throwable t) {
            // manage errors
            m = new Message("Cannot list tags, unexpected error.", "500", t.getMessage());
            res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            m.toJSON(res.getOutputStream());
        }
    }

    /**
     * Gets a list of Resources (of type {@link Submission} or {@link ForumQuestion}) having a determinate tag.
     *
     * @throws IOException If errors occurred during operations.
     */
    public void getResourcesHavingTag() throws IOException {
        Message m;
        List<?> result;
        try {
            String path = req.getRequestURI();
            final String tagToQuery;
            final ResourceSelector resourceType;

            if (path.lastIndexOf("tag") > 0) {
                // list tags of a submission
                path = path.substring(path.lastIndexOf("tag") + "tag".length());
                path = path.substring(1);

                if (path.lastIndexOf("submission") > 0) {
                    // list tags of a submission
                    tagToQuery = path.substring(0, path.indexOf("submission") - 1);
                    resourceType = ResourceSelector.SUBMISSION;
                } else if (path.lastIndexOf("forum") > 0) {
                    // list tags of a forum question
                    tagToQuery = path.substring(0, path.indexOf("forum") - 1);
                    resourceType = ResourceSelector.FORUM_QUESTION;
                } else {
                    // should not happen
                    throw new Exception("Wrong URL");
                }
            } else {
                // should not happen
                throw new Exception("Wrong URL");
            }

            // access the database to get the list
            try {
                long from = Long.parseLong(req.getParameter("from"));
                long howMany = Long.parseLong(req.getParameter("howMany"));
                result = new TagDatabase(con).getResourcesHavingTag(tagToQuery, resourceType, from, howMany);

                if (result != null) {
                    res.setStatus(HttpServletResponse.SC_OK);
                    new ResourceList(result).toJSON(res.getOutputStream());
                } else {
                    // it should not happen
                    m = new Message("Cannot list resource by tag, unexpected error.", "500", "Number format excepiton");
                    res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                    m.toJSON(res.getOutputStream());
                }
            } catch (NumberFormatException e) {
                m = new Message("Cannot list tag by resource, unexpected error.", "500", e.getMessage());
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        } catch (Throwable t) {
            // manage errors
            m = new Message("Cannot list tag by resource, unexpected error.", "500", t.getMessage());
            res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            m.toJSON(res.getOutputStream());
        }
    }
}
