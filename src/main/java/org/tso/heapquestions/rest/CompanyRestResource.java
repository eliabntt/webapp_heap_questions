// Copyright 2018 The StackOverflowers
package org.tso.heapquestions.rest;

import org.tso.heapquestions.database.AuthDatabase;
import org.tso.heapquestions.database.CompanyDatabase;
import org.tso.heapquestions.resource.Company;
import org.tso.heapquestions.resource.Message;
import org.tso.heapquestions.resource.OperativeAreas;
import org.tso.heapquestions.resource.ResourceList;

import javax.naming.AuthenticationException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * Represents a REST resource for {@link Company} resources.
 */
public class CompanyRestResource extends RestResource {

    /**
     * Creates a new REST resource for companies.
     *
     * @param req The HTTP request.
     * @param res The HTTP response.
     * @param con The connection to the database.
     */
    public CompanyRestResource(HttpServletRequest req, HttpServletResponse res, Connection con) {
        super(req, res, con);
    }

    /**
     * Creates a company.
     *
     * @throws IOException If errors occurred during operations.
     */
    public void create() throws IOException {
        Company c;
        Message m;

        try {
            final Company company = Company.fromJSON(req.getInputStream());
            c = new CompanyDatabase(con, company).create();

            if (c != null) {
                res.setStatus(HttpServletResponse.SC_CREATED);
                c.toJSON(res.getOutputStream());
            } else {
                m = new Message("Cannot create the company, unexpected error.", "500", null);
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        } catch (Throwable t) {
            if (t instanceof SQLException && ((SQLException) t).getSQLState().equals("23505")) {
                m = new Message("Cannot create the company, it already exists.", "409", t.getMessage());
                res.setStatus(HttpServletResponse.SC_CONFLICT);
                m.toJSON(res.getOutputStream());
            } else {
                m = new Message("Cannot create the company, unexpected error.", "500", t.getMessage());
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        }
    }

    /**
     * Returns a company.
     *
     * @throws IOException If errors occurred during operations.
     */
    public void read() throws IOException {
        Company c;
        Message m;

        try {
            // parse the URI path to extract the id
            String path = req.getRequestURI();
            path = path.substring(path.lastIndexOf("company") + "company".length());
            final int id = Integer.parseInt(path.substring(1));

            // creates a new object for accessing the database and reads the company
            c = new CompanyDatabase(con, id).read();
            if (c != null) {
                res.setStatus(HttpServletResponse.SC_OK);
                c.toJSON(res.getOutputStream());
            } else {
                m = new Message(String.format("Company %d not found.", id), "404", null);
                res.setStatus(HttpServletResponse.SC_NOT_FOUND);
                m.toJSON(res.getOutputStream());
            }
        } catch (Throwable t) {
            m = new Message("Cannot read company, unexpected error.", "500", t.getMessage());
            res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            m.toJSON(res.getOutputStream());
        }
    }

    /**
     * Updates a company.
     *
     * @param userId The ID of the user that requested the operation.
     * @throws IOException If errors occurred during operations.
     */
    public void update(final long userId) throws IOException {
        Company c;
        Message m;

        try {
            // check if user can update the company (it's an admin)
            new AuthDatabase(con).isAuthForResource(userId);

            // parse the URI path to extract the id
            String path = req.getRequestURI();
            path = path.substring(path.lastIndexOf("company") + "company".length());
            final int id = Integer.parseInt(path.substring(1));

            final Company company = Company.fromJSON(req.getInputStream());
            if (id != company.getId()) {
                m = new Message("Wrong request for URI /company/{id}: URI request and company resource id differ.",
                        "400", String.format("Request URI id %d; user resource id %d.", id, company.getId()));
                res.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                m.toJSON(res.getOutputStream());
            }

            // creates a new object for accessing the database and updates the submission
            c = new CompanyDatabase(con, company).update();
            if (c != null) {
                res.setStatus(HttpServletResponse.SC_OK);
                c.toJSON(res.getOutputStream());
            } else {
                m = new Message(String.format("Company %d cannot be updated.", id), "406", null);
                res.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
                m.toJSON(res.getOutputStream());
            }
        } catch (Throwable t) {
            if (t instanceof SQLException && ((SQLException) t).getSQLState().equals("23503")) {
                m = new Message("Cannot update the company: other resources depend on it.", "409", t.getMessage());
                res.setStatus(HttpServletResponse.SC_CONFLICT);
                m.toJSON(res.getOutputStream());
            } else if (t instanceof AuthenticationException) {
                m = new Message("Cannot update the company: current user is not authorized.", "401", t.getMessage());
                res.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                m.toJSON(res.getOutputStream());
            } else {
                m = new Message("Cannot update the company, unexpected error.", "500", t.getMessage());
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        }
    }

    /**
     * Deletes a company.
     *
     * @param userId The ID of the user that requested the operation.
     * @throws IOException If errors occurred during operations.
     */
    public void delete(final long userId) throws IOException {
        Company c;
        Message m;

        try {
            // check if user can update the company (it's an admin)
            new AuthDatabase(con).isAuthForResource(userId);

            // parse the URI path to extract the id
            String path = req.getRequestURI();
            path = path.substring(path.lastIndexOf("company") + "company".length());
            final int id = Integer.parseInt(path.substring(1));

            // creates a new object for accessing the database and deletes the company
            c = new CompanyDatabase(con, id).delete();
            if (c != null) {
                res.setStatus(HttpServletResponse.SC_OK);
                c.toJSON(res.getOutputStream());
            } else {
                m = new Message(String.format("Company %d cannot be deleted.", id), "406", null);
                res.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
                m.toJSON(res.getOutputStream());
            }
        } catch (Throwable t) {
            if (t instanceof SQLException && ((SQLException) t).getSQLState().equals("23503")) {
                m = new Message("Cannot delete the company: other resources depend on it.", "409", t.getMessage());
                res.setStatus(HttpServletResponse.SC_CONFLICT);
                m.toJSON(res.getOutputStream());
            } else if (t instanceof AuthenticationException) {
                m = new Message("Cannot update the company: current user is not authorized.", "401", t.getMessage());
                res.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                m.toJSON(res.getOutputStream());
            } else {
                m = new Message("Cannot delete the company: unexpected error.", "500", t.getMessage());
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        }
    }

    /**
     * Lists all companies.
     *
     * @throws IOException If errors occurred during operations.
     */
    public void list() throws IOException {
        List<Company> cl;
        Message m;

        try {
            cl = new CompanyDatabase(con).list();
            if (cl != null) {
                res.setStatus(HttpServletResponse.SC_OK);
                new ResourceList(cl).toJSON(res.getOutputStream());
            } else {
                // it should not happen
                m = new Message("Cannot list companies, unexpected error.", "500", null);
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        } catch (Throwable t) {
            m = new Message("Cannot list companies, unexpected error.", "500", t.getMessage());
            res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            m.toJSON(res.getOutputStream());
        }
    }

    /**
     * Searches companies by name.
     *
     * @param name Company's name.
     * @throws IOException If errors occurred during operations.
     */
    public void searchByName(final String name) throws IOException {
        List<Company> cl;
        Message m;

        try {
            cl = new CompanyDatabase(con).searchByName(name);
            if (cl != null) {
                res.setStatus(HttpServletResponse.SC_OK);
                new ResourceList(cl).toJSON(res.getOutputStream());
            } else {
                // it should not happen
                m = new Message("Cannot search companies, unexpected error.", "500", null);
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        } catch (Throwable t) {
            m = new Message("Cannot search companies, unexpected error.", "500", t.getMessage());
            res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            m.toJSON(res.getOutputStream());
        }
    }

    /**
     * Searches companies by operative area.
     *
     * @param area Company's operative area.
     * @throws IOException If errors occurred during operations.
     */
    public void searchByOperativeArea(final OperativeAreas area) throws IOException {
        List<Company> cl;
        Message m;

        try {
            cl = new CompanyDatabase(con).searchByOperativeArea(area);
            if (cl != null) {
                res.setStatus(HttpServletResponse.SC_OK);
                new ResourceList(cl).toJSON(res.getOutputStream());
            } else {
                // it should not happen
                m = new Message("Cannot search companies, unexpected error.", "500", null);
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        } catch (Throwable t) {
            m = new Message("Cannot search companies, unexpected error.", "500", t.getMessage());
            res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            m.toJSON(res.getOutputStream());
        }
    }
}
