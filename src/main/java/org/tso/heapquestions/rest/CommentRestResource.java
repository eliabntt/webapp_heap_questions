// Copyright 2018 The StackOverflowers
package org.tso.heapquestions.rest;

import org.tso.heapquestions.database.AuthDatabase;
import org.tso.heapquestions.database.CommentDatabase;
import org.tso.heapquestions.resource.Comment;
import org.tso.heapquestions.resource.Message;
import org.tso.heapquestions.resource.ResourceList;
import org.tso.heapquestions.resource.ResourceSelector;

import javax.naming.AuthenticationException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * Represents a REST resource for {@link Comment} resources.
 */
public class CommentRestResource extends RestResource {
    /**
     * Creates a new REST resource for comments.
     *
     * @param req The HTTP request.
     * @param res The HTTP response.
     * @param con The connection to the database.
     */
    public CommentRestResource(HttpServletRequest req, HttpServletResponse res, Connection con) {
        super(req, res, con);
    }

    /**
     * Creates a comment.
     *
     * @param userId The ID of the user that requested the operation.
     * @throws IOException If errors occurred during operations.
     */
    public void create(long userId) throws IOException {
        Comment c;
        Message m;
        try {
            final long resourceId;
            String path = req.getRequestURI();
            final ResourceSelector parentType; // note: 'user' value for this enum is not valid

            // check if the comment refers to a submission or a question
            if (path.lastIndexOf("submission") > 0) {
                // comment is to a submission
                path = path.substring(path.lastIndexOf("submission") + "submission".length());
                resourceId = Long.parseLong(path.substring(1, path.indexOf("comment") - 1));
                parentType = ResourceSelector.SUBMISSION;
            } else if (path.lastIndexOf("forum") > 0) {
                // comment is to a forum question
                path = path.substring(path.lastIndexOf("forum") + "forum".length());
                resourceId = Long.parseLong(path.substring(1, path.indexOf("comment") - 1));
                parentType = ResourceSelector.FORUM_QUESTION;
            } else {
                throw new Exception("Wrong URL");
            }

            // get data to create it, via delegation
            final Comment comment = Comment.fromJSON(req.getInputStream());
            c = new CommentDatabase(con, comment, resourceId, parentType).create(userId);

            // check results
            if (c != null) {
                res.setStatus(HttpServletResponse.SC_CREATED);
                c.toJSON(res.getOutputStream());
            } else {
                m = new Message("Cannot create the comment, unexpected error.", "500", null);
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        } catch (Throwable t) {
            // manage errors
            if (t instanceof SQLException && ((SQLException) t).getSQLState().equals("23505")) {
                m = new Message("Cannot create the comment, it already exists.", "409", t.getMessage());
                res.setStatus(HttpServletResponse.SC_CONFLICT);
                m.toJSON(res.getOutputStream());
            } else {
                m = new Message("Cannot create the comment, unexpected error.", "500", t.getMessage());
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        }
    }

    /**
     * Returns a comment.
     *
     * @throws IOException If errors occurred during operations.
     */
    public void read() throws IOException {
        Comment c;
        Message m;

        try {
            // parse the URI path to extract the id
            String path = req.getRequestURI();
            if (path.lastIndexOf("comment") > 0)
                path = path.substring(path.lastIndexOf("comment") + "comment".length());
            final long commentId = Long.parseLong(path.substring(1));

            // access the database to read the comment's data
            c = new CommentDatabase(con, commentId).read();

            // check results
            if (c != null) {
                res.setStatus(HttpServletResponse.SC_OK);
                c.toJSON(res.getOutputStream());
            } else {
                m = new Message(String.format("Comment %d not found.", commentId), "404", null);
                res.setStatus(HttpServletResponse.SC_NOT_FOUND);
                m.toJSON(res.getOutputStream());
            }
        } catch (Throwable t) {
            // manage errors
            m = new Message("Cannot read comment, unexpected error.", "500", t.getMessage());
            res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            m.toJSON(res.getOutputStream());
        }
    }

    /**
     * Returns a comment which is an answer for a question.
     *
     * @throws IOException If errors occurred during operations.
     */
    public void getAnswer() throws IOException {
        Comment c;
        Message m;

        try {
            // parse the URI path to extract the id
            String path = req.getRequestURI();
            if (path.lastIndexOf("forum") > 0)
                path = path.substring(path.lastIndexOf("forum") + "forum".length());
            final long questionId = Long.parseLong(path.substring(1, path.indexOf("isAnswer") - 1));
            // -1 excludes the / after the id

            // access the database to read the comment's data
            c = new CommentDatabase(con, questionId).getAnswer();

            // check results
            if (c != null) {
                res.setStatus(HttpServletResponse.SC_OK);
                c.toJSON(res.getOutputStream());
            } else {
                m = new Message(String.format("Answer of question %d not found.", questionId), "404", null);
                res.setStatus(HttpServletResponse.SC_NOT_FOUND);
                m.toJSON(res.getOutputStream());
            }
        } catch (Throwable t) {
            // manage errors
            m = new Message("Cannot get the answer, unexpected error.", "500", t.getMessage());
            res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            m.toJSON(res.getOutputStream());
        }
    }

    /**
     * Updates a comment.
     *
     * @param userIdFromToken User ID extracted from authorization token.
     * @throws IOException If errors occurred during operations.
     */
    public void update(final long userIdFromToken) throws IOException {
        Comment c;
        Message m;

        try {
            // parse the URI path to extract the id
            String path = req.getRequestURI();
            if (path.lastIndexOf("comment") > 0)
                path = path.substring(path.lastIndexOf("comment") + "comment".length());
            final long commentId = Long.parseLong(path.substring(1));

            // check if user is authorized to manage resource
            // throws exception if unauthorized else does nothing
            new AuthDatabase(con).isAuthForResource(userIdFromToken, commentId, ResourceSelector.COMMENT);

            // create the object from the JSON representation
            final Comment comment = Comment.fromJSON(req.getInputStream());

            // access the database to update the comment
            c = new CommentDatabase(con, comment, commentId).update();

            // check results
            if (c != null) {
                res.setStatus(HttpServletResponse.SC_OK);
                c.toJSON(res.getOutputStream());
            } else {
                m = new Message(String.format("Comment %d cannot be updated.", commentId), "406", null);
                res.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
                m.toJSON(res.getOutputStream());
            }
        } catch (Throwable t) {
            // manage errors
            if (t instanceof SQLException && ((SQLException) t).getSQLState().equals("23503")) {
                m = new Message("Cannot update the comment: other resources depend on it.", "409", t.getMessage());
                res.setStatus(HttpServletResponse.SC_CONFLICT);
                m.toJSON(res.getOutputStream());
            } else if (t instanceof AuthenticationException) {
                m = new Message("Cannot update the comment: current user is not authorized.", "401", t.getMessage());
                res.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                m.toJSON(res.getOutputStream());
            } else {
                m = new Message("Cannot update the comment: unexpected error.", "500", t.getMessage());
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        }
    }


    /**
     * Updates votes of a comment.
     *
     * @throws IOException If errors occurred during operations.
     */
    public void updateUpDownVote() throws IOException {
        Comment c = null;
        Message m;

        try {
            // parse the URI path to extract the id
            String path = req.getRequestURI();
            if (path.lastIndexOf("comment") > 0)
                path = path.substring(path.lastIndexOf("comment") + "comment".length());
            final long commentId = Long.parseLong(path.substring(1));

            // check whether the request is for an upvote or a downvote
            boolean isUpvote = req.getParameter("upVote").equals("true");

            // access the database to update the comment
            c = new CommentDatabase(con, null, commentId).updateVote(isUpvote);

            // check results
            if (c != null) {
                res.setStatus(HttpServletResponse.SC_OK);
                c.toJSON(res.getOutputStream());
            } else {
                m = new Message(String.format("Comment %d cannot be updated.", commentId), "406", null);
                res.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
                m.toJSON(res.getOutputStream());
            }
        } catch (Throwable t) {
            // manage errors
            if (t instanceof SQLException && ((SQLException) t).getSQLState().equals("23503")) {
                m = new Message("Cannot update the comment: other resources depend on it.", "409", t.getMessage());
                res.setStatus(HttpServletResponse.SC_CONFLICT);
                m.toJSON(res.getOutputStream());
            } else if (t instanceof AuthenticationException) {
                m = new Message("Cannot update the comment: current user is not authorized.", "401", t.getMessage());
                res.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                m.toJSON(res.getOutputStream());
            } else {
                m = new Message("Cannot update the comment: unexpected error.", "500", t.getMessage());
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        }
    }

    /**
     * Deletes a comment (actually updates its state).
     *
     * @param userIdFromToken User ID extracted from authorization token.
     * @throws IOException If errors occurred during operations.
     */
    public void delete(final long userIdFromToken) throws IOException {
        Comment c;
        Message m;

        try {
            // parse the URI path to extract the id
            String path = req.getRequestURI();
            path = path.substring(path.lastIndexOf("comment") + "comment".length());
            final long resourceId = Long.parseLong(path.substring(1));

            // create the object from the JSON representation
            Comment com = Comment.fromJSON(req.getInputStream());

            // check if user is authorized to manage resource
            // throws exception if unauthorized else does nothing
            switch (com.getState()) {
                case Deleted: {
                    // checks for author or admin
                    new AuthDatabase(con).isAuthForResource(userIdFromToken, resourceId, ResourceSelector.COMMENT);
                    break;
                }
                case Moderated: {
                    // checks for admin
                    new AuthDatabase(con).isAuthForResource(userIdFromToken);
                    break;
                }
                default: {
                    // error message: invalid usage
                    m = new Message("Comment state not valid for this call.", "403", null);
                    res.setStatus(HttpServletResponse.SC_FORBIDDEN);
                    m.toJSON(res.getOutputStream());
                    break;
                }
            }

            // access the database to change state to deleted
            c = new CommentDatabase(con, com, resourceId).deleteChangeState();

            // check results
            if (c != null) {
                res.setStatus(HttpServletResponse.SC_OK);
                c.toJSON(res.getOutputStream());
            } else {
                m = new Message(String.format("Comment %d cannot be deleted/moderated.", resourceId), "406", null);
                res.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
                m.toJSON(res.getOutputStream());
            }
        } catch (Throwable t) {
            // manage errors
            if (t instanceof SQLException && ((SQLException) t).getSQLState().equals("23503")) {
                m = new Message("Cannot change state of the comment: other resources depend on it.", "409", t.getMessage());
                res.setStatus(HttpServletResponse.SC_CONFLICT);
                m.toJSON(res.getOutputStream());
            } else if (t instanceof AuthenticationException) {
                m = new Message("Cannot change state of the comment: current user is not authorized.", "401", t.getMessage());
                res.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                m.toJSON(res.getOutputStream());
            } else {
                m = new Message("Cannot change state of the comment: unexpected error.", "500", t.getMessage());
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        }
    }

    /**
     * Lists all comments.
     *
     * @throws IOException If errors occurred during operations.
     */
    public void list() throws IOException {
        List<Comment> cl;
        Message m;

        try {
            String path = req.getRequestURI();

            final long resourceId;
            final ResourceSelector parentType;
            if (path.lastIndexOf("submission") > 0) {
                // list comments of a submission
                path = path.substring(path.lastIndexOf("submission") + "submission".length());
                resourceId = Long.parseLong(path.substring(1, path.indexOf("comment") - 1));
                parentType = ResourceSelector.SUBMISSION;
            } else if (path.lastIndexOf("forum") > 0) {
                // list comments of a forum question
                path = path.substring(path.lastIndexOf("forum") + "forum".length());
                resourceId = Long.parseLong(path.substring(1, path.indexOf("comment") - 1));
                parentType = ResourceSelector.FORUM_QUESTION;
            } else if (path.lastIndexOf("user") > 0) {
                // list comments created by an user
                path = path.substring(path.lastIndexOf("user") + "user".length());
                resourceId = Long.parseLong(path.substring(1, path.indexOf("comment") - 1));
                parentType = ResourceSelector.USER;
            } else {
                // should not happen
                throw new Exception("Wrong URL");
            }

            // access the database to get the list
            cl = new CommentDatabase(con, resourceId, parentType).getSubComments();

            // check results
            if (cl != null) {
                res.setStatus(HttpServletResponse.SC_OK);
                new ResourceList(cl).toJSON(res.getOutputStream());
            } else {
                // it should not happen
                m = new Message("Cannot list comment, unexpected error.", "500", null);
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        } catch (Throwable t) {
            // manage errors
            m = new Message("Cannot search comment, unexpected error.", "500", t.getMessage());
            res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            m.toJSON(res.getOutputStream());
        }
    }

    /**
     * Lists some comments from a starting point.
     *
     * @param from          Search starting point: the {@code end - from}-th comment.
     * @param howMany       How many comments to return from the starting point.
     * @param parentComment Parent comment, if sub-comments (reply) are wanted.
     * @throws IOException If errors occurred during operations.
     */
    public void listFromHowMany(final long from, final long howMany, final Long parentComment) throws IOException {
        List<Comment> cl;
        Message m;

        try {
            String path = req.getRequestURI();

            final long resourceId;
            final ResourceSelector parentType;
            if (path.lastIndexOf("submission") > 0) {
                // list comments of a submission
                path = path.substring(path.lastIndexOf("submission") + "submission".length());
                resourceId = Long.parseLong(path.substring(1, path.indexOf("comment") - 1));
                parentType = ResourceSelector.SUBMISSION;
            } else if (path.lastIndexOf("forum") > 0) {
                // list comments of a forum question
                path = path.substring(path.lastIndexOf("forum") + "forum".length());
                resourceId = Long.parseLong(path.substring(1, path.indexOf("comment") - 1));
                parentType = ResourceSelector.FORUM_QUESTION;
            } else if (path.lastIndexOf("user") > 0) {
                // list comments created by an user
                path = path.substring(path.lastIndexOf("user") + "user".length());
                resourceId = Long.parseLong(path.substring(1, path.indexOf("comment") - 1));
                parentType = ResourceSelector.USER;
            } else {
                // should not happen
                throw new Exception("Wrong URL");
            }

            // access the database to get the list
            cl = new CommentDatabase(con, resourceId, parentType).getBlockOfSubComments(from, howMany, parentComment);

            // check results
            if (cl != null) {
                res.setStatus(HttpServletResponse.SC_OK);
                new ResourceList(cl).toJSON(res.getOutputStream());
            } else {
                // it should not happen
                m = new Message("Cannot list comments, unexpected error.", "500", null);
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        } catch (Throwable t) {
            // manage errors
            m = new Message("Cannot find comments, unexpected error.", "500", t.getMessage());
            res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            m.toJSON(res.getOutputStream());
        }
    }

    /**
     * Set the comment as answer for the question.
     *
     * @param userIdFromToken User ID extracted from authorization token.
     * @throws IOException If errors occurred during operations.
     */
    public void setAsAnswer(final long userIdFromToken) throws IOException {
        Comment c;
        Message m;

        try {
            // parse the URI path to extract the id
            String path = req.getRequestURI();
            if (path.lastIndexOf("comment") > 0)
                path = path.substring(path.lastIndexOf("comment") + "comment".length());
            else {
                // should not happen
                throw new Exception("Wrong URL");
            }

            final long commentId = Long.parseLong(path.substring(1, path.indexOf("isAnswer") - 1));

            // access the database to update the comment
            c = new CommentDatabase(con, commentId).selectAsAnswer(userIdFromToken);

            // check results
            if (c != null) {
                res.setStatus(HttpServletResponse.SC_OK);
                c.toJSON(res.getOutputStream());
            } else {
                m = new Message(String.format("Comment %d not set as answer. Operation not permitted.", commentId), "404", null);
                res.setStatus(HttpServletResponse.SC_NOT_FOUND);
                m.toJSON(res.getOutputStream());
            }
        } catch (Throwable t) {
            // manage errors
            if (t instanceof SQLException && ((SQLException) t).getSQLState().equals("23503")) {
                m = new Message("Cannot set the comment as answer.", "409", t.getMessage());
                res.setStatus(HttpServletResponse.SC_CONFLICT);
                m.toJSON(res.getOutputStream());
            } else if (t instanceof AuthenticationException) {
                m = new Message("Cannot set the comment as answer: current user is not authorized", "401", t.getMessage());
                res.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                m.toJSON(res.getOutputStream());
            } else {
                m = new Message("Cannot set the comment as answer: unexpected error.", "500", t.getMessage());
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        }
    }
}
