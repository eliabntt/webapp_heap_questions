// Copyright 2018 The StackOverflowers
package org.tso.heapquestions.rest;

import org.tso.heapquestions.database.AuthDatabase;
import org.tso.heapquestions.database.UserDatabase;
import org.tso.heapquestions.resource.Message;
import org.tso.heapquestions.resource.ResourceList;
import org.tso.heapquestions.resource.ResourceSelector;
import org.tso.heapquestions.resource.User;

import javax.naming.AuthenticationException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * Represents a REST resource for {@link User} resources.
 */
public class UserRestResource extends RestResource {
    /**
     * Creates a new REST resource for users.
     *
     * @param req The HTTP request.
     * @param res The HTTP response.
     * @param con The connection to the database.
     */
    public UserRestResource(HttpServletRequest req, HttpServletResponse res, Connection con) {
        super(req, res, con);
    }

    /**
     * Creates an user.
     *
     * @throws IOException If errors occurred during operations.
     */
    public void create() throws IOException {
        User u;
        Message m;

        try {
            final User user = User.fromJSON(req.getInputStream());
            u = new UserDatabase(con, user).create();

            if (u != null) {
                res.setStatus(HttpServletResponse.SC_CREATED);
                u.toJSON(res.getOutputStream());
            } else {
                m = new Message("Cannot create the user, unexpected error.", "500", null);
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        } catch (Throwable t) {
            if (t instanceof SQLException && ((SQLException) t).getSQLState().equals("23505")) {
                m = new Message("Cannot create the user, it already exists.", "E5A2", t.getMessage());
                res.setStatus(HttpServletResponse.SC_CONFLICT);
                m.toJSON(res.getOutputStream());
            } else {
                m = new Message("Cannot create the user, unexpected error.", "500", t.getMessage());
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        }
    }

    /**
     * Returns an user.
     *
     * @param userIdFromToken User ID extracted from authorization token.
     * @throws IOException If errors occurred during operations.
     */
    public void read(final long userIdFromToken) throws IOException {
        User u;
        Message m;

        try {
            // parse the URI path to extract the id
            String path = req.getRequestURI();
            path = path.substring(path.lastIndexOf("user") + "user".length());
            final long id = Long.parseLong(path.substring(1));

            // check if user is authorized to manage resource
            // throws exception if unauthorized else does nothing
            new AuthDatabase(con).isAuthForResource(userIdFromToken, id, ResourceSelector.USER);

            // creates a new object for accessing the database and reads the user
            u = new UserDatabase(con, id).read(true);
            if (u != null) {
                res.setStatus(HttpServletResponse.SC_OK);
                u.toJSON(res.getOutputStream());
            } else {
                m = new Message(String.format("User %d not found.", id), "404", null);
                res.setStatus(HttpServletResponse.SC_NOT_FOUND);
                m.toJSON(res.getOutputStream());
            }
        } catch (Throwable t) {
            if (t instanceof AuthenticationException) {
                m = new Message("Cannot read the user detail: current user is not authorized.", "401", t.getMessage());
                res.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                m.toJSON(res.getOutputStream());
            } else {
                m = new Message("Cannot read user, unexpected error.", "500", t.getMessage());
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        }
    }

    /**
     * Returns basic info about an user.
     *
     * @throws IOException If errors occurred during operations.
     */
    public void readBasicInfo() throws IOException {
        User u;
        Message m;

        try {
            // parse the URI path to extract the id
            String path = req.getRequestURI();
            path = path.substring(path.lastIndexOf("user") + "user".length());
            final long id = Long.parseLong(path.substring(1));

            // creates a new object for accessing the database and reads the user
            u = new UserDatabase(con, id).read(false);

            if (u != null) {
                res.setStatus(HttpServletResponse.SC_OK);
                u.toJSON(res.getOutputStream());
            } else {
                m = new Message(String.format("User %d not found.", id), "404", null);
                res.setStatus(HttpServletResponse.SC_NOT_FOUND);
                m.toJSON(res.getOutputStream());
            }
        } catch (Throwable t) {
            if (t instanceof AuthenticationException) {
                m = new Message("Cannot read the user detail: current user is not authorized.", "401", t.getMessage());
                res.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                m.toJSON(res.getOutputStream());
            } else {
                m = new Message("Cannot read user, unexpected error.", "500", t.getMessage());
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        }
    }


    /**
     * Updates an user.
     *
     * @param userIdFromToken User ID extracted from authorization token.
     * @throws IOException If errors occurred during operations.
     */
    public void update(final long userIdFromToken) throws IOException {
        User u;
        Message m;

        try {
            // parse the URI path to extract the id
            String path = req.getRequestURI();
            path = path.substring(path.lastIndexOf("user") + "user".length());
            final long id = Long.parseLong(path.substring(1));

            // check if user is authorized to manage resource
            // throws exception if unauthorized else does nothing
            new AuthDatabase(con).isAuthForResource(userIdFromToken, id, ResourceSelector.USER);

            final User user = User.fromJSON(req.getInputStream());

            if (id != user.getId()) {
                m = new Message("Wrong request for URI /user/{id}: URI request and user resource id differ.",
                        "400", String.format("Request URI id %d; user resource id %d.", id, user.getId()));
                res.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                m.toJSON(res.getOutputStream());
            }

            // creates a new object for accessing the database and updates the user
            u = new UserDatabase(con, user).update();
            if (u != null) {
                res.setStatus(HttpServletResponse.SC_OK);
                u.toJSON(res.getOutputStream());
            } else {
                m = new Message(String.format("User %d cannot be updated.", id), "406", null);
                res.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
                m.toJSON(res.getOutputStream());
            }
        } catch (Throwable t) {
            if (t instanceof SQLException && ((SQLException) t).getSQLState().equals("23503")) {
                m = new Message("Cannot update the user: other resources depend on it.", "409", t.getMessage());
                res.setStatus(HttpServletResponse.SC_CONFLICT);
                m.toJSON(res.getOutputStream());
            } else if (t instanceof AuthenticationException) {
                m = new Message("Cannot update the user: current user is not authorized.", "401", t.getMessage());
                res.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                m.toJSON(res.getOutputStream());
            } else {
                m = new Message("Cannot update the user: unexpected error.", "500", t.getMessage());
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        }
    }

    /**
     * Deletes an user.
     *
     * @param userIdFromToken User ID extracted from authorization token.
     * @throws IOException If errors occurred during operations.
     */
    public void delete(final long userIdFromToken) throws IOException {
        User u;
        Message m;

        try {
            // parse the URI path to extract the id
            String path = req.getRequestURI();
            path = path.substring(path.lastIndexOf("user") + "user".length());
            final long id = Long.parseLong(path.substring(1));

            // check if user is authorized to manage resource
            // throws exception if unauthorized else does nothing
            new AuthDatabase(con).isAuthForResource(userIdFromToken, id, ResourceSelector.USER);

            // creates a new object for accessing the database and deletes the user
            u = new UserDatabase(con, id).delete();
            if (u != null) {
                res.setStatus(HttpServletResponse.SC_OK);
                u.toJSON(res.getOutputStream());
            } else {
                m = new Message(String.format("User %d cannot be deleted.", id), "406", null);
                res.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
                m.toJSON(res.getOutputStream());
            }
        } catch (Throwable t) {
            if (t instanceof SQLException && ((SQLException) t).getSQLState().equals("23503")) {
                m = new Message("Cannot delete the user: other resources depend on it.", "409", t.getMessage());
                res.setStatus(HttpServletResponse.SC_CONFLICT);
                m.toJSON(res.getOutputStream());
            } else if (t instanceof AuthenticationException) {
                m = new Message("Cannot delete the user: current user is not authorized.", "401", t.getMessage());
                res.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                m.toJSON(res.getOutputStream());
            } else {
                m = new Message("Cannot delete the user: unexpected error.", "500", t.getMessage());
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        }
    }

    /**
     * Lists all users.
     *
     * @throws IOException If errors occurred during operations.
     */
    public void list() throws IOException {
        List<User> ul;
        Message m;

        try {
            // creates a new object for accessing the database and lists all the employees
            ul = new UserDatabase(con).list();
            if (ul != null) {
                res.setStatus(HttpServletResponse.SC_OK);
                new ResourceList(ul).toJSON(res.getOutputStream());
            } else {
                // it should not happen
                m = new Message("Cannot list users, unexpected error.", "500", null);
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        } catch (Throwable t) {
            m = new Message("Cannot search users, unexpected error.", "500", t.getMessage());
            res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            m.toJSON(res.getOutputStream());
        }
    }
}
