// Copyright 2018 The StackOverflowers
package org.tso.heapquestions.rest;

import org.tso.heapquestions.database.PrivateChatMessageDatabase;
import org.tso.heapquestions.resource.Message;
import org.tso.heapquestions.resource.PrivateChatMessage;
import org.tso.heapquestions.resource.ResourceList;
import org.tso.heapquestions.resource.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * Represents a REST resource for {@link PrivateChatMessage} resources.
 */
public class PrivateChatMessageRestResource extends RestResource {
    //todo needs tests, deployed in an apposite separate branch

    /**
     * Creates a new REST resource for private chat messages.
     *
     * @param req The HTTP request.
     * @param res The HTTP response.
     * @param con The connection to the database.
     */
    public PrivateChatMessageRestResource(HttpServletRequest req, HttpServletResponse res, Connection con) {
        super(req, res, con);
    }

    /**
     * Creates a message.
     *
     * @throws IOException If errors occurred during operations.
     */
    public void create() throws IOException {
        PrivateChatMessage pcm;
        Message m;

        try {
            final PrivateChatMessage chat = PrivateChatMessage.fromJSON(req.getInputStream());
            pcm = new PrivateChatMessageDatabase(con, chat).create();

            if (pcm != null) {
                res.setStatus(HttpServletResponse.SC_CREATED);
                pcm.toJSON(res.getOutputStream());
            } else {
                m = new Message("Cannot create the chat message, unexpected error.", "500", null);
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        } catch (Throwable t) {
            if (t instanceof SQLException && ((SQLException) t).getSQLState().equals("23505")) {
                m = new Message("Cannot create the chat message, it already exists.", "409", t.getMessage());
                res.setStatus(HttpServletResponse.SC_CONFLICT);
                m.toJSON(res.getOutputStream());
            } else {
                m = new Message("Cannot create the chat message, unexpected error.", "500", t.getMessage());
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        }
    }

    /**
     * Reads a message.
     *
     * @throws IOException If errors occurred during operations.
     */
    public void read() throws IOException {
        PrivateChatMessage pcm;
        Message m;

        try {
            // parse the URI path to extract the id
            String path = req.getRequestURI();
            path = path.substring(path.lastIndexOf("privateChat") + "privateChat".length());
            final int id = Integer.parseInt(path.substring(1));

            // creates a new object for accessing the database and reads the message
            pcm = new PrivateChatMessageDatabase(con, id).read();
            if (pcm != null) {
                res.setStatus(HttpServletResponse.SC_OK);
                pcm.toJSON(res.getOutputStream());
            } else {
                m = new Message(String.format("Chat message %d not found.", id), "404", null);
                res.setStatus(HttpServletResponse.SC_NOT_FOUND);
                m.toJSON(res.getOutputStream());
            }
        } catch (Throwable t) {
            m = new Message("Cannot read chat message, unexpected error.", "500", t.getMessage());
            res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            m.toJSON(res.getOutputStream());
        }
    }

    /**
     * Updates a message.
     *
     * @throws IOException If errors occurred during operations.
     */
    public void update() throws IOException {
        PrivateChatMessage pcm;
        Message m;

        try {
            // parse the URI path to extract the id
            String path = req.getRequestURI();
            path = path.substring(path.lastIndexOf("privateChat") + "privateChat".length());
            final int id = Integer.parseInt(path.substring(1));

            final PrivateChatMessage chat = PrivateChatMessage.fromJSON(req.getInputStream());
            if (id != chat.getId()) {
                m = new Message("Wrong request for URI /privateChat/{id}: URI request and company resource id differ.",
                        "400", String.format("Request URI id %d; message resource id %d.", id, chat.getId()));
                res.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                m.toJSON(res.getOutputStream());
            }

            // creates a new object for accessing the database and updates the message
            pcm = new PrivateChatMessageDatabase(con, chat).update();
            if (pcm != null) {
                res.setStatus(HttpServletResponse.SC_OK);
                pcm.toJSON(res.getOutputStream());
            } else {
                m = new Message(String.format("Chat message %d cannot be updated.", id), "406", null);
                res.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
                m.toJSON(res.getOutputStream());
            }
        } catch (Throwable t) {
            if (t instanceof SQLException && ((SQLException) t).getSQLState().equals("23503")) {
                m = new Message("Cannot update the chat message: other resources depend on it.", "409", t.getMessage());
                res.setStatus(HttpServletResponse.SC_CONFLICT);
                m.toJSON(res.getOutputStream());
            } else {
                m = new Message("Cannot update the chat message, unexpected error.", "500", t.getMessage());
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        }
    }

    /**
     * Deletes a message.
     *
     * @throws IOException If errors occurred during operations.
     */
    public void delete() throws IOException {
        PrivateChatMessage pcm;
        Message m;

        try {
            // parse the URI path to extract the id
            String path = req.getRequestURI();
            path = path.substring(path.lastIndexOf("privateChat") + "privateChat".length());
            final int id = Integer.parseInt(path.substring(1));

            // creates a new object for accessing the database and deletes the message
            pcm = new PrivateChatMessageDatabase(con, id).delete();
            if (pcm != null) {
                res.setStatus(HttpServletResponse.SC_OK);
                pcm.toJSON(res.getOutputStream());
            } else {
                m = new Message(String.format("Chat message %d cannot be deleted.", id), "406", null);
                res.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
                m.toJSON(res.getOutputStream());
            }
        } catch (Throwable t) {
            if (t instanceof SQLException && ((SQLException) t).getSQLState().equals("23503")) {
                m = new Message("Cannot delete the chat message: other resources depend on it.", "409", t.getMessage());
                res.setStatus(HttpServletResponse.SC_CONFLICT);
                m.toJSON(res.getOutputStream());
            } else {
                m = new Message("Cannot delete the company: unexpected error.", "500", t.getMessage());
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        }
    }

    /**
     * Lists chat messages between two users.
     *
     * @param senderId   The sender ID.
     * @param receiverId The receiver ID.
     * @param from       Search starting point: the {@code end - from}-th message.
     * @param howMany    How many messages to return from the starting point.
     * @throws IOException If errors occurred during operations.
     */
    public void list(final long senderId, final long receiverId, final long from, final long howMany) throws IOException {
        List<PrivateChatMessage> cl;
        Message m;

        try {
            cl = new PrivateChatMessageDatabase(con).list(senderId, receiverId, from, howMany);
            if (cl != null) {
                res.setStatus(HttpServletResponse.SC_OK); 
                new ResourceList(cl).toJSON(res.getOutputStream());
            } else {
                // it should not happen
                m = new Message("Cannot list chat messages, unexpected error.", "500", null);
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        } catch (Throwable t) {
            m = new Message("Cannot list chat messages, unexpected error.", "500", t.getMessage());
            res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            m.toJSON(res.getOutputStream());
        }
    }

    /**
     * Lists all the users an user has messages with.
     *
     * @param currentUserId The user ID.
     * @throws IOException If errors occurred during operations.
     */
    public void userChatList(final long currentUserId) throws IOException {
        List<User> cl;
        Message m;

        try {
            cl = new PrivateChatMessageDatabase(con).listChatUsers(currentUserId);
            if (cl != null) {
                res.setStatus(HttpServletResponse.SC_OK);
                new ResourceList(cl).toJSON(res.getOutputStream());
            } else {
                // it should not happen
                m = new Message("Cannot search users, unexpected error.", "500", null);
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        } catch (Throwable t) {
            m = new Message("Cannot search users, unexpected error.", "500", t.getMessage());
            res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            m.toJSON(res.getOutputStream());
        }
    }
}
