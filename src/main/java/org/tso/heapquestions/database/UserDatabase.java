// Copyright 2018 The StackOverflowers
package org.tso.heapquestions.database;

import org.tso.heapquestions.resource.Tools;
import org.tso.heapquestions.resource.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Manages an {@link User} in the database.
 */
public class UserDatabase {
    private static final String STATEMENT_CREATE_BASE = "INSERT INTO tso.user(mail, username, password) VALUES(?,?,?) RETURNING *;";
    private static final String STATEMENT_CREATE_EXTENDED = "INSERT INTO tso.userExtended(id, isAdmin, name, surname, birthDate, joinDate, location, profilePic) VALUES(?,?,?,?,?,DEFAULT,?,?) RETURNING *;";
    private static final String STATEMENT_READ = "SELECT * FROM tso.user INNER JOIN tso.userExtended ON (tso.user.id = tso.userExtended.id) WHERE tso.user.id = ?;";
    private static final String STATEMENT_UPDATE_BASE_PASSWORD = "UPDATE tso.user SET (mail, username, password) = (?,?,?) WHERE id = ? RETURNING *;";
    private static final String STATEMENT_UPDATE_BASE = "UPDATE tso.user SET (mail, username) = (?,?) WHERE id = ? RETURNING *;";
    private static final String STATEMENT_UPDATE_EXTENDED = "UPDATE tso.userExtended SET (name, surname, birthDate, location, profilePic) = (?,?,?,?,?) WHERE id = ? RETURNING *;";
    private static final String STATEMENT_DELETE_PRESAVE = "SELECT * FROM tso.userExtended WHERE id = ?;";
    private static final String STATEMENT_DELETE_ACTION = "DELETE FROM tso.user WHERE id = ? RETURNING *;";
    private static final String STATEMENT_LIST = "SELECT username, profilepic FROM tso.user INNER JOIN tso.userextended ON tso.user.id = tso.userextended.id ORDER BY username ASC;";

    private final Connection con;
    private final User user;
    private final Long id;

    /**
     * Constructor for creating and updating an user.
     *
     * @param con  Connection to the database.
     * @param user User to be managed.
     */
    public UserDatabase(final Connection con, final User user) {
        this.con = con;
        this.user = user;
        this.id = null;
    }

    /**
     * Constructor for reading and deleting an user.
     *
     * @param con Connection to the database.
     * @param id  ID of the user to be managed.
     */
    public UserDatabase(final Connection con, final long id) {
        this.con = con;
        this.user = null;
        this.id = id;
    }

    /**
     * Constructor to list all users.
     *
     * @param con Connection to the database.
     */
    public UserDatabase(final Connection con) {
        this(con, null);
    }

    /**
     * Creates a new user in the database.
     *
     * @return The new inserted user.
     * @throws SQLException       If any error occurs while inserting the user.
     * @throws InstantiationError If the wrong constructor was used.
     */
    public User create() throws SQLException, InstantiationError {
        PreparedStatement ps = null;
        ResultSet rs = null;
        User readUser; // the result

        // check if wrong constructor was used
        if (user == null)
            throw new InstantiationError("Wrong constructor, use that with User");

        try {
            // disable autocommit so to have an unique global transaction
            con.setAutoCommit(false);

            // 1 - create base user
            // prepare and execute statement
            ps = con.prepareStatement(STATEMENT_CREATE_BASE);
            ps.setString(1, user.getEmail());
            ps.setString(2, user.getUsername());
            ps.setString(3, Tools.sha256(user.getEncodedPassword()));

            // execute and self-notify about errors
            rs = ps.executeQuery();
            if (!rs.next())
                throw new SQLException("Base user not created");

            // read values
            long id = rs.getLong("id");
            String mail = rs.getString("mail");
            String username = rs.getString("username");
            String password = rs.getString("password");

            // 2 - add extended informations
            // prepare and execute statements
            ps = con.prepareStatement(STATEMENT_CREATE_EXTENDED);
            ps.setLong(1, id);
            ps.setBoolean(2, user.isAdmin());
            ps.setString(3, user.getName());
            ps.setString(4, user.getSurname());
            ps.setDate(5, user.getBirthDate());
            ps.setString(6, user.getLocation());
            if (user.getProfilePicture() != null)
                ps.setBytes(7, user.getProfilePicture());
            else
                ps.setNull(7, Types.OTHER);

            // execute and self-notify about errors
            rs = ps.executeQuery();
            con.commit();

            if (!rs.next())
                throw new SQLException("Extended user not created");

            // read values
            assert (id == rs.getLong("id"));
            boolean isAdmin = rs.getBoolean("isAdmin");
            String name = rs.getString("name");
            String surname = rs.getString("surname");
            Date birthDate = rs.getDate("birthDate");
            Date joinDate = rs.getDate("joinDate");
            String location = rs.getString("location");
            byte[] profilePicture = null;
            if (user.getProfilePicture() != null)
                profilePicture = rs.getBytes("profilePic");

            /*
             * New users does not have a period nor a plan since Free
             * is the default and it is not tracked: subscription to
             * higher plans is not managed here.
             * Further steps would be:
             * - insert a period (for active plan, starting from now)
             * - insert a free plan for the user to start
             */

            readUser = new User(id, username, password, mail, isAdmin,
                    name, surname, birthDate, joinDate, location, profilePicture);
        } catch (SQLException exception) {
            SQLException external = exception;
            try {
                con.rollback();
            } catch (SQLException ex) {
                //this should never be called
                external = new SQLException(exception.getMessage() + "; Error when rollbacking commit", ex.getSQLState(), ex.getErrorCode());
            }
            // throw again the exception for external management
            throw external;
        } finally {
            if (rs != null)
                rs.close();
            if (ps != null)
                ps.close();
            con.setAutoCommit(true);
            con.close();
        }
        return readUser;
    }

    /**
     * Reads an user from the database.
     *
     * @param getAllInfo Whether to return all user information or only public data.
     * @return The user.
     * @throws SQLException If any error occurs while searching for the user.
     */
    public User read(final boolean getAllInfo) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        User readUser; // the result

        // check if wrong constructor was used
        if (id == null)
            throw new InstantiationError("Wrong constructor, use that with id");

        try {
            // read base and extended user information
            ps = con.prepareStatement(STATEMENT_READ);
            ps.setLong(1, id);

            rs = ps.executeQuery();
            if (!rs.next())
                throw new SQLException("User does not exists");

            // read values
            Long readId = rs.getLong("id");
            assert (id.equals(readId));

            // public data
            String username = rs.getString("username");
            byte[] profilePicture = rs.getBytes("profilePic");
            String mail = null, password = null, name = null, surname = null, location = null;
            Date birthDate = null, joinDate = null;
            boolean isAdmin = false;

            if (getAllInfo) {
                // read also private data
                mail = rs.getString("mail");
                password = rs.getString("password");
                isAdmin = rs.getBoolean("isAdmin");
                name = rs.getString("name");
                surname = rs.getString("surname");
                birthDate = rs.getDate("birthDate");
                joinDate = rs.getDate("joinDate");
                location = rs.getString("location");
            } else
                readId = null; // todo maybe senseless

            readUser = new User(readId, username, password, mail, isAdmin,
                    name, surname, birthDate, joinDate, location, profilePicture);
        } finally {
            if (rs != null)
                rs.close();
            if (ps != null)
                ps.close();
            con.close();
        }
        return readUser;
    }

    /**
     * Updates an user in the database.
     *
     * @return The updated user.
     * @throws SQLException       If any error occurs while updating the user.
     * @throws InstantiationError If the wrong constructor was used.
     */
    public User update() throws SQLException, InstantiationError {
        PreparedStatement ps = null;
        ResultSet rs = null;
        User readUser; // the result

        // check if wrong constructor was used
        if (user == null)
            throw new InstantiationError("Wrong constructor, use that with User");

        try {
            // disable autocommit so to have an unique global transaction
            con.setAutoCommit(false);

            // 1 - update base user information
            if (user.getEncodedPassword().equals("")) {
                ps = con.prepareStatement(STATEMENT_UPDATE_BASE);
                ps.setLong(3, user.getId());
            } else {
                ps = con.prepareStatement(STATEMENT_UPDATE_BASE_PASSWORD);
                ps.setString(3, Tools.sha256(user.getEncodedPassword()));
                ps.setLong(4, user.getId());
            }

            ps.setString(1, user.getEmail());
            ps.setString(2, user.getUsername());

            rs = ps.executeQuery();
            if (!rs.next())
                throw new SQLException("Cannot update base user");

            // read values
            long id = rs.getLong("id");
            assert (id == user.getId());
            String mail = rs.getString("mail");
            String username = rs.getString("username");
            String password = rs.getString("password");

            // 2 - update user extra information
            ps = con.prepareStatement(STATEMENT_UPDATE_EXTENDED);
            ps.setString(1, user.getName());
            ps.setString(2, user.getSurname());
            ps.setDate(3, user.getBirthDate());
            ps.setString(4, user.getLocation());
            ps.setBytes(5, user.getProfilePicture());
            ps.setLong(6, user.getId());

            rs = ps.executeQuery();
            con.commit();
            if (!rs.next())
                throw new SQLException("Cannot update extended user");

            // read values
            id = rs.getLong("id");
            assert (id == user.getId());
            boolean isAdmin = rs.getBoolean("isAdmin");
            String name = rs.getString("name");
            String surname = rs.getString("surname");
            Date birthDate = rs.getDate("birthDate");
            Date joinDate = rs.getDate("joinDate");
            String location = rs.getString("location");
            byte[] profilePicture = rs.getBytes("profilePic");

            readUser = new User(id, username, password, mail, isAdmin,
                    name, surname, birthDate, joinDate, location, profilePicture);
        } catch (SQLException exception) {
            SQLException external = exception;
            try {
                con.rollback();
            } catch (SQLException ex) {
                //this should never be called
                external = new SQLException(exception.getMessage() + "Error when rollbacking commit", ex.getSQLState(), ex.getErrorCode());
            }
            // throw again the exception for external management
            throw external;
        } finally {
            if (rs != null)
                rs.close();
            if (ps != null)
                ps.close();
            con.setAutoCommit(true);
            con.close();
        }
        return readUser;
    }

    /**
     * Deletes an user from the database.
     *
     * @return The deleted user.
     * @throws SQLException If any error occurs while deleting the user.
     */
    public User delete() throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        User readUser; // the result

        try {
            // disable autocommit so to have an unique global transaction
            con.setAutoCommit(false);

            // 1 - read extra information before deleting user (because of cascade option)
            ps = con.prepareStatement(STATEMENT_DELETE_PRESAVE);
            ps.setLong(1, id);

            rs = ps.executeQuery();
            if (!rs.next())
                throw new SQLException("Cannot read extended parameters before deleting user");

            // read values
            long readId = rs.getLong("id");
            assert (readId == id);
            boolean isAdmin = rs.getBoolean("isAdmin");
            String name = rs.getString("name");
            String surname = rs.getString("surname");
            Date birthDate = rs.getDate("birthDate");
            Date joinDate = rs.getDate("joinDate");
            String location = rs.getString("location");
            byte[] profilePicture = rs.getBytes("profilePic");

            // 2 - delete user, and by cascade effect its extended info,
            // and information about plans and periods and other linked objects
            ps = con.prepareStatement(STATEMENT_DELETE_ACTION);
            ps.setLong(1, id);

            rs = ps.executeQuery();
            con.commit();
            if (!rs.next())
                throw new SQLException("Cannot remove user");

            // read values
            readId = rs.getLong("id");
            assert (readId == id);
            String mail = rs.getString("mail");
            String username = rs.getString("username");
            String password = rs.getString("password");

            readUser = new User(id, username, password, mail, isAdmin,
                    name, surname, birthDate, joinDate, location, profilePicture);
        } catch (SQLException exception) {
            SQLException external = exception;
            try {
                con.rollback();
            } catch (SQLException ex) {
                //this should never be called
                external = new SQLException(exception.getMessage() + "Error when rollbacking commit", ex.getSQLState(), ex.getErrorCode());
            }
            // throw again the exception for external management
            throw external;
        } finally {
            if (rs != null)
                rs.close();
            if (ps != null)
                ps.close();
            con.setAutoCommit(true);
            con.close();
        }
        return readUser;
    }

    /**
     * Lists all the users in the database.
     *
     * @return The list of users.
     * @throws SQLException If any error occurs while searching for users.
     */
    public List<User> list() throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        final List<User> users = new ArrayList<>(); // the result

        try {
            ps = con.prepareStatement(STATEMENT_LIST);

            rs = ps.executeQuery();
            while (rs.next())
                users.add(new User(rs.getString("username"), rs.getBytes("profilePic")));

            /* full object: users.add(new User(rs.getLong("id"),
                 rs.getString("username"), rs.getString("password"),
                 rs.getString("mail"), rs.getBoolean("isAdmin"),
                 rs.getString("name"), rs.getString("surname"),
                 rs.getDate("birthDate"), rs.getDate("joinDate"),
                 rs.getString("location"), rs.getBytes("profilePic")
            ); */
        } finally {
            if (rs != null)
                rs.close();
            if (ps != null)
                ps.close();
            con.close();
        }
        return users;
    }
}
