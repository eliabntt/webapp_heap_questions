// Copyright 2018 The StackOverflowers
package org.tso.heapquestions.database;

import org.tso.heapquestions.resource.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Manages a {@link Comment} in the database.
 */
public class CommentDatabase {
    private static final String STATEMENT_CREATE_SUBMISSION_COMMENT = "INSERT INTO tso.comment VALUES (DEFAULT, ?, NULL, ?, ?, DEFAULT, DEFAULT, DEFAULT, ?, DEFAULT , ?, DEFAULT) RETURNING *;";
    private static final String STATEMENT_CREATE_FORUM_COMMENT = "INSERT INTO tso.comment VALUES (DEFAULT, ?, ?, NULL, ?, DEFAULT, DEFAULT, DEFAULT, ?, DEFAULT , ?, DEFAULT) RETURNING *;";
    private static final String STATEMENT_READ_COMMENT = "SELECT * FROM tso.comment WHERE id = ? AND (state = 'Visible' OR state = 'Edited');";
    private static final String STATEMENT_UPDATE_COMMENT = "UPDATE tso.comment SET (dateTime, downVote, upVote, notify, content, state)=(?,?,?,?,?,?::tso.itemstate) WHERE id = ? AND isAnswer = false RETURNING *;";

    private static final String STATEMENT_UPDATE_COMMENT_UPVOTE = "UPDATE tso.comment SET upVote = upVote + 1 WHERE id = ? AND (state = 'Visible' OR state = 'Edited') RETURNING *;";
    private static final String STATEMENT_UPDATE_COMMENT_DOWNVOTE = "UPDATE tso.comment SET downVote = downVote + 1 WHERE id = ? AND (state = 'Visible' OR state = 'Edited') RETURNING *;";

    private static final String STATEMENT_CHANGE_COMMENT_STATE = "UPDATE tso.comment SET (state)=(?::tso.itemState) WHERE id = ? RETURNING *;";
    private static final String STATEMENT_GET_USER_COMMENTS = "SELECT * FROM tso.comment WHERE userId = ? AND (state = 'Visible' OR state = 'Edited') ORDER BY dateTime DESC;";
    private static final String STATEMENT_GET_SUBMISSION_COMMENTS = "SELECT * FROM tso.comment WHERE submission = ? AND (state = 'Visible' OR state = 'Edited') ORDER BY dateTime DESC;";
    private static final String STATEMENT_GET_FORUM_COMMENTS = "SELECT * FROM tso.comment WHERE question = ? AND (state = 'Visible' OR state = 'Edited') ORDER BY dateTime DESC;";

    private static final String STATEMENT_GET_USER_COMMENTS_FROM_HOWMANY = "SELECT * FROM tso.comment WHERE userId = ? AND (state = 'Visible' OR state = 'Edited') ORDER BY dateTime DESC OFFSET ? LIMIT ?;";
    private static final String STATEMENT_GET_SUBMISSION_COMMENTS_FROM_HOWMANY_PARENT = "SELECT * FROM tso.comment WHERE submission = ?  AND parentComment = ? ORDER BY dateTime OFFSET ? LIMIT ?;";
    private static final String STATEMENT_GET_SUBMISSION_COMMENTS_FROM_HOWMANY_NULL_PARENT = "SELECT * FROM tso.comment WHERE submission = ?  AND parentComment IS NULL ORDER BY dateTime OFFSET ? LIMIT ?;";
    private static final String STATEMENT_GET_FORUM_COMMENTS_FROM_HOWMANY_PARENT = "SELECT * FROM tso.comment WHERE question = ?  AND parentComment = ? ORDER BY dateTime OFFSET ? LIMIT ?;";
    private static final String STATEMENT_GET_FORUM_COMMENTS_FROM_HOWMANY_NULL_PARENT = "SELECT * FROM tso.comment WHERE question = ?  AND parentComment IS NULL ORDER BY dateTime OFFSET ? LIMIT ?;";

    private static final String STATEMENT_SELECT_AS_ANSWER = "UPDATE tso.comment SET isAnswer = true WHERE id = ?  AND (state = 'Visible' OR state = 'Edited') AND userid = " +
            "(SELECT userId FROM tso.forumquestion WHERE id = (SELECT question FROM tso.comment WHERE id = ?) AND userId = ? AND (state = 'Visible' OR state = 'Edited')) RETURNING *";
    private static final String STATEMENT_CLOSE_FORUM = "UPDATE tso.forumquestion SET closed = ? WHERE id = (SELECT question FROM tso.comment WHERE id = ?);";
    private static final String STATEMENT_SELECT_FORUM_ANSWER = "SELECT * FROM tso.comment WHERE question = ? AND isAnswer = true LIMIT 1;";

    private final Connection con;
    private final Comment com;
    private final Long id;
    private ResourceSelector parentType;

    /**
     * Constructor for creating a comment.
     *
     * @param con        Connection to the database.
     * @param com        Comment to be managed.
     * @param resourceId ID of the Resource the comment refers to.
     * @param parentType Whether the Resource is a {@link Submission} or a {@link ForumQuestion}.
     */
    public CommentDatabase(final Connection con, final Comment com, final long resourceId, final ResourceSelector parentType) {
        this.con = con;
        this.com = com;
        this.id = resourceId;
        this.parentType = parentType;
    }

    /**
     * Constructor for reading a comment.
     *
     * @param con Connection to the database.
     * @param id  {@link Comment} ID.
     */
    public CommentDatabase(final Connection con, final long id) {
        this.con = con;
        this.id = id;
        this.com = null;
    }

    /**
     * Constructor for updating or changing state to a comment.
     *
     * @param con   Connection to the database.
     * @param com   Comment new data.
     * @param comId Comment ID.
     */
    public CommentDatabase(final Connection con, final Comment com, final long comId) {
        this.con = con;
        this.id = comId;
        this.com = com;
    }

    /**
     * Constructor to list all comments.
     *
     * @param con        Connection to the database.
     * @param resourceId ID of the Resource the comment refers to.
     * @param parentType Whether the Resource is a {@link Submission} or a {@link ForumQuestion} or a {@link User}.
     */
    public CommentDatabase(final Connection con, final long resourceId, final ResourceSelector parentType) {
        this.con = con;
        this.com = null;
        this.id = resourceId;
        this.parentType = parentType;
    }

    /**
     * Creates a new comment to a {@link Submission} or a {@link ForumQuestion} in the database.
     *
     * @param userId The ID of the user that requested the operation.
     * @return The new inserted comment.
     * @throws SQLException       If any error occurs while inserting the comment.
     * @throws InstantiationError If the wrong constructor was used or if the statement cannot be created.
     */
    public Comment create(long userId) throws SQLException, InstantiationError {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Comment comment = null;

        // check if wrong constructor was used
        if (com == null)
            throw new InstantiationError("Wrong constructor, use that with Comment");

        // cannot accept an user here, so return null
        // in any case, this should not happen.
        if (parentType == ResourceSelector.USER)
            return null;

        try {
            con.setAutoCommit(false);

            // select which query to perform
            if (parentType == ResourceSelector.SUBMISSION)
                ps = con.prepareStatement(STATEMENT_CREATE_SUBMISSION_COMMENT);
            else if (parentType == ResourceSelector.FORUM_QUESTION)
                ps = con.prepareStatement(STATEMENT_CREATE_FORUM_COMMENT);
            else
                throw new InstantiationError("Cannot create the statement, unknown error.");

            // load comment's data accordingly
            ps.setLong(1, userId);
            ps.setLong(2, id);
            if (com.getParentComment() == null)
                ps.setNull(3, Types.BIGINT);
            else
                ps.setLong(3, com.getParentComment());
            ps.setBoolean(4, com.isNotify());
            ps.setString(5, com.getContent());

            rs = ps.executeQuery();
            if (rs.next())
                comment = extractFrom(rs);
            con.commit();
        } catch (SQLException exception) {
            SQLException external = exception;
            try {
                con.rollback();
            } catch (SQLException ex) {
                //this should never be called
                external = new SQLException(exception.getMessage() + "Error when rollbacking commit", ex.getSQLState(), ex.getErrorCode());
            }
            // throw again the exception for external management
            throw external;
        } finally {
            if (rs != null)
                rs.close();
            if (ps != null)
                ps.close();
            con.setAutoCommit(true);
            con.close();
        }
        return comment;
    }

    /**
     * Reads a comment from the database.
     *
     * @return The comment.
     * @throws SQLException If any error occurs while searching for the comment.
     */
    public Comment read() throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Comment comment = null;

        try {
            ps = con.prepareStatement(STATEMENT_READ_COMMENT);
            ps.setLong(1, id);

            rs = ps.executeQuery();
            if (rs.next())
                comment = extractFrom(rs);
        } finally {
            if (rs != null)
                rs.close();
            if (ps != null)
                ps.close();
            con.close();
        }
        return comment;
    }

    /**
     * Updates a comment in the database.
     *
     * @return The updated comment.
     * @throws SQLException       If any error occurs while updating the comment.
     * @throws InstantiationError If the wrong constructor was used.
     */
    public Comment update() throws SQLException, InstantiationError {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Comment comment = null;

        // check if wrong constructor was used
        if (com == null)
            throw new InstantiationError("Wrong constructor, use that with Comment");

        try {
            con.setAutoCommit(false);

            ps = con.prepareStatement(STATEMENT_UPDATE_COMMENT);
            ps.setTimestamp(1, com.getDateTime());
            ps.setInt(2, com.getDownVote());
            ps.setInt(3, com.getUpVote());
            ps.setBoolean(4, com.isNotify());
            ps.setString(5, com.getContent());
            ps.setString(6, ItemState.Edited.toString());
            ps.setLong(7, id);

            rs = ps.executeQuery();
            if (rs.next())
                comment = extractFrom(rs);
            con.commit();
        } catch (SQLException exception) {
            SQLException external = exception;
            try {
                con.rollback();
            } catch (SQLException ex) {
                //this should never be called
                external = new SQLException(exception.getMessage() + "Error when rollbacking commit", ex.getSQLState(), ex.getErrorCode());
            }
            // throw again the exception for external management
            throw external;
        } finally {
            if (rs != null)
                rs.close();
            if (ps != null)
                ps.close();
            con.setAutoCommit(true);
            con.close();
        }
        return comment;
    }

    /**
     * Deletes a comment by changing its state to 'deleted'.
     *
     * @return The updated comment.
     * @throws SQLException       If any error occurs while updating the comment.
     * @throws InstantiationError If the wrong constructor was used.
     */
    public Comment deleteChangeState() throws SQLException, InstantiationError {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Comment comment = null;

        // check if wrong constructor was used
        if (com == null)
            throw new InstantiationError("Wrong constructor, use that with Comment");

        try {
            con.setAutoCommit(false);

            ps = con.prepareStatement(STATEMENT_CLOSE_FORUM);
            ps.setBoolean(1, false);
            ps.setLong(2, id);
            ps.execute();
            ps = con.prepareStatement(STATEMENT_CHANGE_COMMENT_STATE);
            ps.setString(1, com.getState().toString());
            ps.setLong(2, id);

            rs = ps.executeQuery();
            if (rs.next())
                comment = extractFrom(rs);
            else {
                ps = con.prepareStatement(STATEMENT_CLOSE_FORUM);
                ps.setBoolean(1, true);
                ps.setLong(2, id);
                ps.execute();
            }
            con.commit();
        } catch (SQLException exception) {
            SQLException external = exception;
            try {
                con.rollback();
            } catch (SQLException ex) {
                //this should never be called
                external = new SQLException(exception.getMessage() + "Error when rollbacking commit", exception.getSQLState(), exception.getErrorCode());
            }
            // throw again the exception for external management
            throw external;
        } finally {
            if (rs != null)
                rs.close();
            if (ps != null)
                ps.close();
            con.setAutoCommit(true);
            con.close();
        }
        return comment;
    }

    /**
     * Gets comments for a {@link Submission} or a {@link ForumQuestion} from the database.
     *
     * @return A list of comments.
     * @throws SQLException       If any error occurs during operations.
     * @throws InstantiationError If the statement cannot be created.
     */
    public List<Comment> getSubComments() throws SQLException, InstantiationError {
        PreparedStatement ps = null;
        ResultSet rs = null;
        final List<Comment> comment = new ArrayList<>();

        try {
            // select which query to perform
            switch (parentType) {
                case USER:
                    ps = con.prepareStatement(STATEMENT_GET_USER_COMMENTS);
                    break;
                case FORUM_QUESTION:
                    ps = con.prepareStatement(STATEMENT_GET_FORUM_COMMENTS);
                    break;
                case SUBMISSION:
                    ps = con.prepareStatement(STATEMENT_GET_SUBMISSION_COMMENTS);
                    break;
                default:
                    throw new InstantiationError("Cannot create the statement, unknown error.");
            }
            ps.setLong(1, id);

            rs = ps.executeQuery();
            while (rs.next())
                comment.add(extractFrom(rs));
        } finally {
            if (rs != null)
                rs.close();
            if (ps != null)
                ps.close();
            if (con != null)
                con.close();
        }
        return comment;
    }

    /**
     * Gets a block of comments starting from a specific one, for a {@link Submission} or a {@link ForumQuestion}, from the database.
     *
     * @param from          Search starting point: the {@code end - from}-th message.
     * @param howMany       How many messages to return from the starting point.
     * @param parentComment Parent comment, if sub-comments (reply) are wanted.
     * @return A list of comments.
     * @throws SQLException             If any error occurs during operations.
     * @throws IllegalArgumentException If wrong parameters were passed.
     * @throws InstantiationError       If the statement cannot be created.
     */
    // Useful for querying some comments at a time without getting them all in a row (can be quite time-consuming).
    public List<Comment> getBlockOfSubComments(final long from, final long howMany, final Long parentComment) throws SQLException, IllegalArgumentException, InstantiationError {
        if (from < 1 || howMany < 1)
            throw new IllegalArgumentException("Function parameters must be all positive");

        PreparedStatement ps = null;
        ResultSet rs = null;
        final List<Comment> comment = new ArrayList<>();

        try {
            // select which query to perform
            switch (parentType) {
                case USER: {
                    ps = con.prepareStatement(STATEMENT_GET_USER_COMMENTS_FROM_HOWMANY);
                    ps.setLong(1, id);
                    ps.setLong(2, from - 1);

                    /* It is an offset, so if I specify from 1, the first comment, the table offset will be 0.
                     * If I want the comment #10, I skip the first 9 comments, so offset = 9. */
                    ps.setLong(3, howMany);
                    break;
                }
                case FORUM_QUESTION: {
                    if (parentComment == null) {
                        ps = con.prepareStatement(STATEMENT_GET_FORUM_COMMENTS_FROM_HOWMANY_NULL_PARENT);
                        ps.setLong(1, id);
                        ps.setLong(2, from - 1);

                        /* It is an offset, so if I specify from 1, the first comment, the table offset will be 0.
                         * If I want the comment #10, I skip the first 9 comments, so offset = 9. */
                        ps.setLong(3, howMany);
                    } else {
                        ps = con.prepareStatement(STATEMENT_GET_FORUM_COMMENTS_FROM_HOWMANY_PARENT);
                        ps.setLong(1, id);
                        ps.setLong(3, from - 1);
                        ps.setLong(4, howMany);
                        ps.setLong(2, parentComment);
                    }
                    break;
                }
                case SUBMISSION: {
                    if (parentComment == null) {
                        ps = con.prepareStatement(STATEMENT_GET_SUBMISSION_COMMENTS_FROM_HOWMANY_NULL_PARENT);
                        ps.setLong(1, id);
                        ps.setLong(2, from - 1);
                        ps.setLong(3, howMany);
                    } else {
                        ps = con.prepareStatement(STATEMENT_GET_SUBMISSION_COMMENTS_FROM_HOWMANY_PARENT);
                        ps.setLong(1, id);
                        ps.setLong(3, from - 1);
                        ps.setLong(4, howMany);
                        ps.setLong(2, parentComment);
                    }
                    break;
                }
                default:
                    throw new InstantiationError("Cannot create the statement, unknown error");
            }

            rs = ps.executeQuery();
            while (rs.next())
                comment.add(extractFrom(rs));
        } finally {
            if (rs != null)
                rs.close();
            if (ps != null)
                ps.close();
            if (con != null)
                con.close();
        }
        return comment;
    }

    /**
     * Select a comment as answer for a question.
     *
     * @param userId The ID of the user that requested the operation.
     * @return The comment accepted as answer.
     * @throws SQLException             If any error occurs during operations.
     * @throws IllegalArgumentException If wrong parameters were passed.
     */
    public Comment selectAsAnswer(final long userId) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Comment comment = null;

        try {
            con.setAutoCommit(false);

            ps = con.prepareStatement(STATEMENT_CLOSE_FORUM);
            ps.setBoolean(1, true);
            ps.setLong(2, id);
            ps.execute();

            ps = con.prepareStatement(STATEMENT_SELECT_AS_ANSWER);
            ps.setLong(1, id);
            ps.setLong(2, id);
            ps.setLong(3, userId);

            rs = ps.executeQuery();
            if (rs.next())
                comment = extractFrom(rs);
            else {
                ps = con.prepareStatement(STATEMENT_CLOSE_FORUM);
                ps.setBoolean(1, false);
                ps.setLong(2, id);
                ps.execute();
            }

            con.commit();
        } catch (SQLException exception) {
            SQLException external = exception;
            try {
                con.rollback();
            } catch (SQLException ex) {
                //this should never be called
                external = new SQLException(exception.getMessage() + "Error when rollbacking commit");
            }
            // throw again the exception for external management
            throw external;
        } finally {
            if (rs != null)
                rs.close();
            if (ps != null)
                ps.close();
            con.setAutoCommit(true);
            con.close();
        }
        return comment;
    }

    /**
     * Selects the answer for a question.
     *
     * @return The comment accepted as answer.
     * @throws SQLException             If any error occurs during operations.
     * @throws IllegalArgumentException If wrong parameters were passed.
     */
    public Comment getAnswer() throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Comment comment = null;

        try {
            con.setAutoCommit(false);

            ps = con.prepareStatement(STATEMENT_SELECT_FORUM_ANSWER);
            ps.setLong(1, id);
            ps.execute();

            rs = ps.executeQuery();
            if (rs.next())
                comment = extractFrom(rs);
            con.commit();
        } catch (SQLException exception) {
            SQLException external = exception;
            try {
                con.rollback();
            } catch (SQLException ex) {
                //this should never be called
                external = new SQLException(exception.getMessage() + "Error when rollbacking commit");
            }
            // throw again the exception for external management
            throw external;
        } finally {
            if (rs != null)
                rs.close();
            if (ps != null)
                ps.close();
            con.setAutoCommit(true);
            con.close();
        }
        return comment;
    }

    /**
     * Updates the vote on a comment in the database.
     *
     * @param upVote Whether an update to upVote or downVote counter is needed.
     * @return The updated comment.
     * @throws SQLException       If any error occurs while updating the comment.
     * @throws InstantiationError If the wrong constructor was used.
     */
    public Comment updateVote(boolean upVote) throws SQLException, InstantiationError {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Comment comment = null;

        // check if wrong constructor was used
        if (com != null)
            throw new InstantiationError("Wrong constructor, use that with Id");

        try {
            con.setAutoCommit(false);

            if (upVote)
                ps = con.prepareStatement(STATEMENT_UPDATE_COMMENT_UPVOTE);
            else
                ps = con.prepareStatement(STATEMENT_UPDATE_COMMENT_DOWNVOTE);
            ps.setLong(1, id);

            rs = ps.executeQuery();
            if (rs.next())
                comment = extractFrom(rs);
            con.commit();
        } catch (SQLException exception) {
            SQLException external = exception;
            try {
                con.rollback();
            } catch (SQLException ex) {
                //this should never be called
                external = new SQLException(exception.getMessage() + "Error when rollbacking commit", ex.getSQLState(), ex.getErrorCode());
            }
            // throw again the exception for external management
            throw external;
        } finally {
            if (rs != null)
                rs.close();
            if (ps != null)
                ps.close();
            con.setAutoCommit(true);
            con.close();
        }
        return comment;
    }

    /**
     * Gets a comment object from a query's result.
     *
     * @param rs Result of a query.
     * @return The corresponding comment.
     * @throws SQLException If any error occurs during operations.
     */
    private Comment extractFrom(ResultSet rs) throws SQLException {
        return new Comment(rs.getLong("id"), rs.getLong("userId"),
                rs.getLong("question"), rs.getLong("submission"),
                rs.getLong("parentComment"), rs.getTimestamp("dateTime"),
                rs.getInt("downvote"), rs.getInt("upvote"),
                rs.getBoolean("notify"), rs.getBoolean("isAnswer"), rs.getString("content"),
                ItemState.fromString(rs.getString("state")));
    }
}
