// Copyright 2018 The StackOverflowers
package org.tso.heapquestions.database;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * @deprecated Loads the app schema into the database.
 */
public class SetupDatabase {
    private final Connection con;
    private final ArrayList<String> queries;

    /**
     * @param con The connection to the database.
     * @throws IOException If error occurs during operations.
     */
    public SetupDatabase(final Connection con) throws IOException {
        this.con = con;

        // read from local resources
        String[] filenames = {
                "org/tso/heapquestions/database/table-schema.sql",
                "org/tso/heapquestions/database/functions.sql",
                "org/tso/heapquestions/database/insert.sql"
        };
        queries = new ArrayList<>();
        for (String file : filenames) {
            Object queryFile = this.getClass().getClassLoader().getResource(file).getContent();
            Scanner reader = new Scanner((InputStream) queryFile);
            String temp;
            StringBuilder q = new StringBuilder();

            while (reader.hasNextLine()) {
                temp = reader.nextLine();
                // strip comment lines
                if (!temp.startsWith("-- ")) {
                    q.append(temp.trim());

                    // end of single query reached
                    if (temp.contains(";")) {
                        queries.add(q.toString());
                        q = new StringBuilder();
                    }
                }
            }
            reader.close();
        }
    }

    /**
     * Performs database setup.
     *
     * @return Whether the operation was successfully completed.
     * @throws SQLException If errors occurred during setup.
     */
    public boolean setup() throws SQLException {
        if (queries.size() == 0)
            return false;

        Statement st = con.createStatement();
        for (String q : queries)
            st.execute(q);
        st.close();
        con.close();
        return true;
    }
}
