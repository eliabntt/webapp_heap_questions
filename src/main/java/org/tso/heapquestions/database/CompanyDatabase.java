// Copyright 2018 The StackOverflowers
package org.tso.heapquestions.database;

import org.tso.heapquestions.resource.Company;
import org.tso.heapquestions.resource.OperativeAreas;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Manages a {@link Company} in the database.
 */
public class CompanyDatabase {
    private static final String STATEMENT_CREATE = "INSERT INTO tso.company(id, name, description, website, logo, operativeArea) VALUES (DEFAULT, ?, ?, ?, ?, ?::tso.operativeAreas) RETURNING *;";
    private static final String STATEMENT_READ = "SELECT * FROM tso.company WHERE id = ?;";
    private static final String STATEMENT_UPDATE = "UPDATE tso.company SET(name, description, website, logo, operativeArea) = (?, ?, ?, ?, ?::tso.operativeAreas) WHERE id = ? RETURNING *;";
    private static final String STATEMENT_DELETE = "DELETE FROM tso.company WHERE id = ? RETURNING *;";
    private static final String STATEMENT_LIST = "SELECT * FROM tso.company ORDER BY name;";

    private static final String STATEMENT_SEARCH_BY_NAME = "SELECT * FROM tso.company WHERE name ILIKE ? ORDER BY name;";
    private static final String STATEMENT_SEARCH_BY_OPERATIVE_AREA = "SELECT * FROM tso.company WHERE operativeArea = ?::tso.operativeAreas ORDER BY name;";

    private final Connection con;
    private final Company company;
    private final Integer id;

    /**
     * Constructor for creating and updating a company.
     *
     * @param con     Connection to the database.
     * @param company Company to be managed.
     */
    public CompanyDatabase(final Connection con, final Company company) {
        this.con = con;
        this.company = company;
        this.id = null;
    }

    /**
     * Constructor for reading and deleting a company.
     *
     * @param con Connection to the database.
     * @param id  ID of the company to be managed.
     */
    public CompanyDatabase(final Connection con, final int id) {
        this.con = con;
        this.company = null;
        this.id = id;
    }

    /**
     * Constructor to list all companies.
     *
     * @param con Connection to the database.
     */
    public CompanyDatabase(final Connection con) {
        this.con = con;
        this.company = null;
        this.id = null;
    }

    /**
     * Creates a new company in the database.
     *
     * @return The new inserted company.
     * @throws SQLException       If any error occurs while inserting the company.
     * @throws InstantiationError If the wrong constructor was used.
     */
    public Company create() throws SQLException, InstantiationError {
        // check if wrong constructor was used
        if (company == null)
            throw new InstantiationError("Wrong constructor, use that with Company");

        PreparedStatement ps = null;
        ResultSet rs = null;
        Company readCompany = null;

        try {
            con.setAutoCommit(false);

            ps = con.prepareStatement(STATEMENT_CREATE);
            ps.setString(1, company.getName().toLowerCase());
            ps.setString(2, company.getDescription());
            ps.setString(3, company.getWebsite());
            if (company.getLogo() != null)
                ps.setBytes(4, company.getLogo());
            else
                ps.setNull(4, Types.OTHER);
            ps.setString(5, company.getOperativeArea().toString());

            rs = ps.executeQuery();
            if (rs.next())
                readCompany = extractFrom(rs);
            con.commit();
        } catch (SQLException exception) {
            SQLException external = exception;
            try {
                con.rollback();
            } catch (SQLException ex) {
                //this should never be called
                external = new SQLException(exception.getMessage() + "Error when rollbacking commit", ex.getSQLState(), ex.getErrorCode());
            }
            // throw again the exception for external management
            throw external;
        } finally {
            if (rs != null)
                rs.close();
            if (ps != null)
                ps.close();
            con.setAutoCommit(true);
            con.close();
        }
        return readCompany;
    }

    /**
     * Reads a company from the database.
     *
     * @return The company.
     * @throws SQLException       If any error occurs while inserting the company.
     * @throws InstantiationError If the wrong constructor was used.
     */
    public Company read() throws SQLException, InstantiationError {
        // check if wrong constructor was used
        if (id == null)
            throw new InstantiationError("Wrong constructor, use that with id");

        PreparedStatement ps = null;
        ResultSet rs = null;
        Company readCompany = null;

        try {
            ps = con.prepareStatement(STATEMENT_READ);
            ps.setInt(1, id);

            rs = ps.executeQuery();
            if (rs.next())
                readCompany = extractFrom(rs);
        } finally {
            if (rs != null)
                rs.close();
            if (ps != null)
                ps.close();
            con.close();
        }
        return readCompany;
    }

    /**
     * Updates a company in the database.
     *
     * @return The updated company.
     * @throws SQLException       If any error occurs while inserting the company.
     * @throws InstantiationError If the wrong constructor was used.
     */
    public Company update() throws SQLException, InstantiationError {
        // check if wrong constructor was used
        if (company == null)
            throw new InstantiationError("Wrong constructor, use that with Company");

        PreparedStatement ps = null;
        ResultSet rs = null;
        Company readCompany = null;

        try {
            con.setAutoCommit(false);

            ps = con.prepareStatement(STATEMENT_UPDATE);
            ps.setString(1, company.getName().toLowerCase());
            ps.setString(2, company.getDescription());
            ps.setString(3, company.getWebsite());
            if (company.getLogo() != null)
                ps.setBytes(4, company.getLogo());
            else
                ps.setNull(4, Types.OTHER);
            ps.setString(5, company.getOperativeArea().toString());
            ps.setInt(6, company.getId());

            rs = ps.executeQuery();
            if (rs.next())
                readCompany = extractFrom(rs);
            con.commit();
        } catch (SQLException exception) {
            SQLException external = exception;
            try {
                con.rollback();
            } catch (SQLException ex) {
                //this should never be called
                external = new SQLException(exception.getMessage() + "Error when rollbacking commit", ex.getSQLState(), ex.getErrorCode());
            }
            // throw again the exception for external management
            throw external;
        } finally {
            if (rs != null)
                rs.close();
            if (ps != null)
                ps.close();
            con.setAutoCommit(true);
            con.close();
        }
        return readCompany;
    }

    /**
     * Deletes a company in the database.
     *
     * @return The deleted company.
     * @throws SQLException       If any error occurs while inserting the company.
     * @throws InstantiationError If the wrong constructor was used.
     */
    public Company delete() throws SQLException, InstantiationError {
        // check if wrong constructor was used
        if (id == null)
            throw new InstantiationError("Wrong constructor, use that with id");

        PreparedStatement ps = null;
        ResultSet rs = null;
        Company readCompany = null;

        try {
            con.setAutoCommit(false);

            ps = con.prepareStatement(STATEMENT_DELETE);
            ps.setInt(1, id);

            rs = ps.executeQuery();
            if (rs.next())
                readCompany = extractFrom(rs);
            con.commit();
        } catch (SQLException exception) {
            SQLException external = exception;
            try {
                con.rollback();
            } catch (SQLException ex) {
                //this should never be called
                external = new SQLException(exception.getMessage() + "Error when rollbacking commit", ex.getSQLState(), ex.getErrorCode());
            }
            // throw again the exception for external management
            throw external;
        } finally {
            if (rs != null)
                rs.close();
            if (ps != null)
                ps.close();
            con.setAutoCommit(true);
            con.close();
        }
        return readCompany;
    }

    /**
     * Lists all companies in the database.
     *
     * @return The list of companies.
     * @throws SQLException       If any error occurs while inserting the company.
     * @throws InstantiationError If the wrong constructor was used.
     */
    public List<Company> list() throws SQLException, InstantiationError {
        // check if wrong constructor was used
        if (id != null || company != null)
            throw new InstantiationError("Wrong constructor, use that with only connection");

        PreparedStatement ps = null;
        ResultSet rs = null;
        final List<Company> companies = new ArrayList<>();

        try {
            ps = con.prepareStatement(STATEMENT_LIST);

            rs = ps.executeQuery();
            while (rs.next())
                companies.add(extractFrom(rs));
        } finally {
            if (rs != null)
                rs.close();
            if (ps != null)
                ps.close();
            con.close();
        }
        return companies;
    }

    /**
     * Searches companies by name.
     *
     * @param name Company's name.
     * @return The list of companies.
     * @throws SQLException             If any error occurs while inserting the company.
     * @throws InstantiationError       If the wrong constructor was used.
     * @throws IllegalArgumentException If the method was called for getting the full list (use {@code CompanyDatabase.list()}).
     */
    public List<Company> searchByName(final String name) throws SQLException, InstantiationError, IllegalArgumentException {
        // check if wrong constructor was used
        if (id != null || company != null)
            throw new InstantiationError("Wrong constructor, use that with only connection");
        if (name == null)
            throw new IllegalArgumentException("This method is intended for searching companies by name. To get a full list, please use list().");

        PreparedStatement ps = null;
        ResultSet rs = null;
        final List<Company> companies = new ArrayList<>();

        try {
            ps = con.prepareStatement(STATEMENT_SEARCH_BY_NAME);
            ps.setString(1, "%" + name.toLowerCase() + "%"); // % is for LIKE clause

            rs = ps.executeQuery();
            while (rs.next())
                companies.add(extractFrom(rs));
        } finally {
            if (rs != null)
                rs.close();
            if (ps != null)
                ps.close();
            con.close();
        }
        return companies;
    }

    /**
     * Searches companies by operative area.
     *
     * @param area Company's operative area.
     * @return The list of companies.
     * @throws SQLException             If any error occurs while inserting the company.
     * @throws InstantiationError       If the wrong constructor was used.
     * @throws IllegalArgumentException If the method was called for getting the full list (use {@code CompanyDatabase.list()}).
     */
    public List<Company> searchByOperativeArea(final OperativeAreas area) throws SQLException, InstantiationError, IllegalArgumentException {
        // check if wrong constructor was used
        if (id != null || company != null)
            throw new InstantiationError("Wrong constructor, use that with only connection");
        if (area == null)
            throw new IllegalArgumentException("This method is intended for searching companies by operative areas. To get a full list, please use list().");

        PreparedStatement ps = null;
        ResultSet rs = null;
        final List<Company> companies = new ArrayList<>();

        try {
            ps = con.prepareStatement(STATEMENT_SEARCH_BY_OPERATIVE_AREA);
            ps.setString(1, area.toString());

            rs = ps.executeQuery();
            while (rs.next())
                companies.add(extractFrom(rs));
        } finally {
            if (rs != null)
                rs.close();
            if (ps != null)
                ps.close();
            con.close();
        }
        return companies;
    }

    /**
     * Gets a company object from a query's result.
     *
     * @param rs Result of a query.
     * @return The corresponding company.
     * @throws SQLException If any error occurs during operations.
     */
    private Company extractFrom(ResultSet rs) throws SQLException {
        return new Company(rs.getInt("id"), rs.getString("name").toLowerCase(),
                rs.getString("description"), rs.getString("website"),
                rs.getBytes("logo"), OperativeAreas.fromString(rs.getString("operativeArea")));
    }
}
