// Copyright 2018 The StackOverflowers
package org.tso.heapquestions.database;

import org.tso.heapquestions.resource.ForumQuestion;
import org.tso.heapquestions.resource.ItemState;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Manages a {@link ForumQuestion} in the database.
 */
public class ForumDatabase {
    private static final String STATEMENT_CREATE = "INSERT INTO tso.forumQuestion VALUES(DEFAULT, ?, ?, DEFAULT, DEFAULT, ?, ?, DEFAULT) RETURNING *;";
    private static final String STATEMENT_READ = "SELECT * FROM tso.forumQuestion WHERE id = ? AND (state = 'Visible' OR state = 'Edited');";
    private static final String STATEMENT_UPDATE = "UPDATE tso.forumQuestion SET (title, content, category, state) = (?, ?, ?, ?::tso.itemstate) WHERE id = ? AND closed = false AND ( state = 'Edited' OR state = 'Visible' ) RETURNING *;";
    private static final String STATEMENT_CHANGE_STATE = "UPDATE tso.forumQuestion SET (state) = (?::tso.itemstate) WHERE id = ? RETURNING *;";
    private static final String STATEMENT_LIST = "SELECT * FROM tso.forumQuestion WHERE (state = 'Visible' OR state = 'Edited') ORDER BY dateTime DESC;";
    private static final String STATEMENT_LIST_TERM = "SELECT * FROM tso.forumQuestion WHERE (state = 'Visible' OR state = 'Edited') AND title ILIKE ? ORDER BY dateTime DESC OFFSET ? LIMIT ?;";
    private static final String STATEMENT_LIST_FROM_HOWMANY = "SELECT * FROM tso.forumQuestion WHERE (state = 'Visible' OR state = 'Edited') ORDER BY dateTime DESC OFFSET ? LIMIT ?;";
    private static final String STATEMENT_LIST_BY_USER = "SELECT * FROM tso.forumQuestion WHERE userId = ? AND (state = 'Visible' OR state = 'Edited') ORDER BY dateTime DESC;";
    private static final String STATEMENT_LIST_BY_USER_FROM_HOWMANY = "SELECT * FROM tso.forumQuestion WHERE userId = ? AND (state = 'Visible' OR state = 'Edited') ORDER BY dateTime DESC OFFSET ? LIMIT ?;";

    private final Connection con;
    private final ForumQuestion question;
    private final Long id;
    private final String searchTerm;

    /**
     * Constructor for creating and updating a forum question.
     *
     * @param con      Connection to the database.
     * @param question Question to be managed.
     */
    public ForumDatabase(final Connection con, final ForumQuestion question) {
        this.con = con;
        this.question = question;
        this.id = null;
        this.searchTerm = null;
    }

    /**
     * Constructor for reading a forum question.
     *
     * @param con Connection to the database.
     * @param id  ID of the question to be managed.
     */
    public ForumDatabase(final Connection con, final long id) {
        this.con = con;
        this.question = null;
        this.id = id;
        this.searchTerm = null;
    }

    /**
     * Constructor for searching a set of forum questions given the search term.
     *
     * @param con Connection to the database.
     * @param st  (part of the)Title to be searched.
     */
    public ForumDatabase(final Connection con, final String st) {
        this.con = con;
        this.question = null;
        this.id = null;
        this.searchTerm = st;
    }

    /**
     * Constructor for changing the state of a forum question.
     *
     * @param con      Connection to the database.
     * @param question The question to be managed
     * @param id       ID of the question to be managed.
     */
    public ForumDatabase(final Connection con, final ForumQuestion question, final long id) {
        this.con = con;
        this.question = question;
        this.id = id;
        this.searchTerm = null;
    }

    /**
     * Constructor to list all questions.
     *
     * @param con Connection to the database.
     */
    public ForumDatabase(Connection con) {
        this.con = con;
        this.question = null;
        this.id = null;
        this.searchTerm = null;
    }

    /**
     * Creates a new forum question in the database.
     *
     * @param userId The ID of the user that requested the operation.
     * @return The new inserted forum question.
     * @throws SQLException       If any error occurs while inserting the forum question.
     * @throws InstantiationError If the wrong constructor was used.
     */
    public ForumQuestion create(long userId) throws SQLException, InstantiationError {
        PreparedStatement ps = null;
        ResultSet rs = null;
        ForumQuestion readQue = null;

        // check if wrong constructor was used
        if (question == null)
            throw new InstantiationError("Wrong constructor, use that with Question");

        try {
            con.setAutoCommit(false);

            ps = con.prepareStatement(STATEMENT_CREATE);
            ps.setLong(1, userId);
            ps.setString(2, question.getCategory());
            ps.setString(3, question.getTitle());
            ps.setString(4, question.getContent());

            rs = ps.executeQuery();
            if (rs.next())
                readQue = extractFrom(rs);
            con.commit();
        } catch (SQLException exception) {
            SQLException external = exception;
            try {
                con.rollback();
            } catch (SQLException ex) {
                //this should never be called
                external = new SQLException(exception.getMessage() + "Error when rollbacking commit", ex.getSQLState(), ex.getErrorCode());
            }
            // throw again the exception for external management
            throw external;
        } finally {
            if (rs != null)
                rs.close();
            if (ps != null)
                ps.close();
            con.setAutoCommit(true);
            con.close();
        }
        return readQue;
    }

    /**
     * Reads a forum question from the database.
     *
     * @return The forum question.
     * @throws SQLException If any error occurs while searching for the forum question.
     */
    public ForumQuestion read() throws SQLException, InstantiationError {
        PreparedStatement ps = null;
        ResultSet rs = null;
        ForumQuestion readQue = null;

        // check if wrong constructor was used
        if (id == null)
            throw new InstantiationError("Wrong constructor, use that with id");

        try {
            ps = con.prepareStatement(STATEMENT_READ);
            ps.setLong(1, id);

            rs = ps.executeQuery();
            if (rs.next())
                readQue = extractFrom(rs);
        } finally {
            if (rs != null)
                rs.close();
            if (ps != null)
                ps.close();
            con.close();
        }
        return readQue;
    }

    /**
     * Updates a forum question in the database.
     *
     * @return The updated forum question.
     * @throws SQLException If any error occurs while updating the forum question.
     */
    public ForumQuestion update() throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        ForumQuestion readQue = null;

        try {
            con.setAutoCommit(false);

            ps = con.prepareStatement(STATEMENT_UPDATE);
            ps.setString(1, question.getTitle());
            ps.setString(2, question.getContent());
            ps.setString(3, question.getCategory());
            ps.setString(4, ItemState.Edited.toString());
            ps.setLong(5, question.getId());

            rs = ps.executeQuery();
            if (rs.next())
                readQue = extractFrom(rs);
            con.commit();
        } catch (SQLException exception) {
            SQLException external = exception;
            try {
                con.rollback();
            } catch (SQLException ex) {
                //this should never be called
                external = new SQLException(exception.getMessage() + "Error when rollbacking commit", ex.getSQLState(), ex.getErrorCode());
            }
            // throw again the exception for external management
            throw external;
        } finally {
            if (rs != null)
                rs.close();
            if (ps != null)
                ps.close();
            con.setAutoCommit(true);
            con.close();
        }
        return readQue;
    }

    /**
     * Deletes a forum question from the database.
     *
     * @return The deleted forum question.
     * @throws SQLException If any error occurs while deleting the forum question.
     */
    public ForumQuestion deleteChangeState() throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        ForumQuestion readQue = null;

        // check if wrong constructor was used
        if (id == null)
            throw new InstantiationError("Wrong constructor, use that with id");

        try {
            con.setAutoCommit(false);

            ps = con.prepareStatement(STATEMENT_CHANGE_STATE);
            ps.setString(1, question.getState().toString());
            ps.setLong(2, id);

            rs = ps.executeQuery();
            if (rs.next())
                readQue = extractFrom(rs);
            con.commit();
        } catch (SQLException exception) {
            SQLException external = exception;
            try {
                con.rollback();
            } catch (SQLException ex) {
                //this should never be called
                external = new SQLException(exception.getMessage() + "Error when rollbacking commit", ex.getSQLState(), ex.getErrorCode());
            }
            // throw again the exception for external management
            throw external;
        } finally {
            if (rs != null)
                rs.close();
            if (ps != null)
                ps.close();
            con.setAutoCommit(true);
            con.close();
        }
        return readQue;
    }

    /**
     * Lists all the question in the database.
     *
     * @return The list of forum questions.
     * @throws SQLException If any error occurs while searching for forum questions.
     */
    public List<ForumQuestion> list() throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        final List<ForumQuestion> forumQuestions = new ArrayList<>();

        try {
            if (id != null) {
                ps = con.prepareStatement(STATEMENT_LIST_BY_USER);
                ps.setLong(1, id);
            } else
                ps = con.prepareStatement(STATEMENT_LIST);

            rs = ps.executeQuery();
            while (rs.next())
                forumQuestions.add(extractFrom(rs));
        } finally {
            if (rs != null)
                rs.close();
            if (ps != null)
                ps.close();
            con.close();
        }
        return forumQuestions;
    }


    /**
     * Lists a certain number of submissions in the database for a user starting from a submission.
     *
     * @param from    The starting submission in the order.
     * @param howMany How many submissions to fetch.
     * @return The list of submissions.
     * @throws SQLException If any error occurs while searching for submissions.
     */
    public List<ForumQuestion> listByUserFromHowMany(final long from, final long howMany) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        final List<ForumQuestion> questions = new ArrayList<>();

        try {
            if (id == null)
                throw new IllegalArgumentException("Pass the user id when calling the method");
            if (from < 1 || howMany < 1)
                throw new IllegalArgumentException("Pass positive values of 'from' and 'howMany'");

            ps = con.prepareStatement(STATEMENT_LIST_BY_USER_FROM_HOWMANY);
            ps.setLong(1, id);
            ps.setLong(2, from - 1);
            ps.setLong(3, howMany);

            rs = ps.executeQuery();
            while (rs.next())
                questions.add(extractFrom(rs));
        } finally {
            if (rs != null)
                rs.close();
            if (ps != null)
                ps.close();
            con.close();
        }
        return questions;
    }


    /**
     * Retrieves some forum questions starting from a specific question.
     *
     * @param from    The starting question.
     * @param howMany How many questions.
     * @return A list of questions.
     * @throws SQLException If any error occurs while searching for forum questions.
     */
    public List<ForumQuestion> listFromHowMany(long from, long howMany) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        final List<ForumQuestion> forumQuestions = new ArrayList<>();

        if (from < 1L && howMany < 1L)
            throw new IllegalArgumentException("Parameters must be all positive");

        try {
            ps = con.prepareStatement(STATEMENT_LIST_FROM_HOWMANY);
            ps.setLong(1, from - 1L);
            ps.setLong(2, howMany);
            rs = ps.executeQuery();
            while (rs.next())
                forumQuestions.add(this.extractFrom(rs));
            return forumQuestions;
        } finally {
            if (rs != null)
                rs.close();
            if (ps != null)
                ps.close();
            con.close();
        }
    }

    /**
     * Gets a block of forum questions starting from a specific one from the database with the searched title.
     *
     * @param from    Search starting point: the {@code end - from}-th question.
     * @param howMany How many questions to return from the starting point.
     * @return A list of forum questions.
     * @throws SQLException             If any error occurs during operations.
     * @throws IllegalArgumentException If wrong parameters were passed.
     * @throws InstantiationError       If the statement cannot be created.
     */
    public List<ForumQuestion> getSearchList(final long from, final long howMany) throws SQLException, IllegalArgumentException, InstantiationError {
        if (from < 1 || howMany < 1)
            throw new IllegalArgumentException("Function parameters must be all positive");

        PreparedStatement ps = null;
        ResultSet rs = null;
        final List<ForumQuestion> fqs = new ArrayList<>();

        try {
            ps = con.prepareStatement(STATEMENT_LIST_TERM);
            ps.setString(1, searchTerm + "%");
            ps.setLong(2, from - 1);

            /* It is an offset, so if I specify from 1, the first comment, the table offset will be 0.
             * If I want the comment #10, I skip the first 9 comments, so offset = 9. */
            ps.setLong(3, howMany);
            rs = ps.executeQuery();

            while (rs.next())
                fqs.add(extractFrom(rs));
        } finally {
            if (rs != null)
                rs.close();
            if (ps != null)
                ps.close();
            if (con != null)
                con.close();
        }
        return fqs;
    }

    /**
     * Gets a forum question object from a query's result.
     *
     * @param rs Result of a query.
     * @return The corresponding question.
     * @throws SQLException If any error occurs during operations.
     */
    ForumQuestion extractFrom(ResultSet rs) throws SQLException {
        return new ForumQuestion(rs.getLong("id"), rs.getLong("userId"),
                rs.getString("category"), rs.getTimestamp("dateTime"),
                rs.getBoolean("closed"), rs.getString("title"),
                rs.getString("content"), ItemState.fromString(rs.getString("state")));
    }
}
