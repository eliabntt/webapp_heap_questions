// Copyright 2018 The StackOverflowers
package org.tso.heapquestions.database;

import org.tso.heapquestions.resource.PrivateChatMessage;
import org.tso.heapquestions.resource.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Manages a {@link PrivateChatMessage} in the database.
 */
public class PrivateChatMessageDatabase {
    private static final String STATEMENT_CREATE = "INSERT INTO tso.privateChat(senderId, receiverId, dateTime, content) VALUES(?, ?, ?, ?) RETURNING *;";
    private static final String STATEMENT_READ = "SELECT * FROM tso.privateChat WHERE msgId = ?;";
    private static final String STATEMENT_UPDATE = "UPDATE tso.privateChat SET (dateTime, content) = (?, ?) WHERE msgId = ? RETURNING *;";
    private static final String STATEMENT_DELETE = "DELETE FROM tso.privateChat WHERE msgId = ? RETURNING *;";
    private static final String STATEMENT_LIST = "SELECT * FROM tso.privateChat WHERE senderId = ? AND receiverId = ? ORDER BY dateTime DESC LIMIT ? OFFSET ?;";

    private static final String STATEMENT_LIST_USERS = "SELECT DISTINCT mail, username, password, tso.userExtended.* FROM tso.user INNER JOIN tso.userExtended ON (tso.user.id = tso.userExtended.id) WHERE " +
            "tso.user.id IN(SELECT DISTINCT senderId AS id FROM tso.privateChat WHERE receiverId = ?) AND " +
            "tso.user.id IN(SELECT DISTINCT receiverId AS id FROM tso.privateChat WHERE senderId = ?);";

    private final Connection con;
    private final PrivateChatMessage chat;
    private final Long messageId;

    /**
     * Constructor for creating and updating a message.
     *
     * @param con         Connection to the database.
     * @param chatMessage Message to be managed.
     */
    public PrivateChatMessageDatabase(final Connection con, final PrivateChatMessage chatMessage) {
        this.con = con;
        this.chat = chatMessage;
        this.messageId = null;
    }

    /**
     * Constructor for reading and deleting a message.
     *
     * @param con       Connection to the database.
     * @param messageId ID of the message to be managed.
     */
    public PrivateChatMessageDatabase(final Connection con, final long messageId) {
        this.con = con;
        this.chat = null;
        this.messageId = messageId;
    }

    /**
     * Constructor to list all messages.
     *
     * @param con Connection to the database.
     */
    public PrivateChatMessageDatabase(final Connection con) {
        this.con = con;
        this.chat = null;
        this.messageId = null;
    }

    /**
     * Creates a new message in the database.
     *
     * @return The new inserted message.
     * @throws SQLException       If any error occurs while inserting the message.
     * @throws InstantiationError If the wrong constructor was used.
     */
    public PrivateChatMessage create() throws SQLException, InstantiationError {
        if (chat == null)
            throw new InstantiationError("Wrong constructor, use that with Chat");

        PreparedStatement ps = null;
        ResultSet rs = null;
        PrivateChatMessage readChat = null;

        try {
            con.setAutoCommit(false);

            ps = con.prepareStatement(STATEMENT_CREATE);
            ps.setLong(1, chat.getSender());
            ps.setLong(2, chat.getReceiver());
            ps.setTimestamp(3, chat.getDateTime());
            ps.setString(4, chat.getContent());

            rs = ps.executeQuery();
            if (rs.next())
                readChat = extractMessageFrom(rs);
            con.commit();
        } catch (SQLException exception) {
            SQLException external = exception;
            try {
                con.rollback();
            } catch (SQLException ex) {
                //this should never be called
                external = new SQLException(exception.getMessage() + "Error when rollbacking commit", ex.getSQLState(), ex.getErrorCode());
            }
            // throw again the exception for external management
            throw external;
        } finally {
            if (rs != null)
                rs.close();
            if (ps != null)
                ps.close();
            con.setAutoCommit(true);
            con.close();
        }
        return readChat;
    }

    /**
     * Reads a message from the database.
     *
     * @return The message.
     * @throws SQLException       If any error occurs while searching for the message.
     * @throws InstantiationError If the wrong constructor was used.
     */
    public PrivateChatMessage read() throws SQLException, InstantiationError {
        if (messageId == null)
            throw new InstantiationError("Wrong constructor, use that with id");

        PreparedStatement ps = null;
        ResultSet rs = null;
        PrivateChatMessage readChat = null;

        try {
            ps = con.prepareStatement(STATEMENT_READ);
            ps.setLong(1, messageId);

            rs = ps.executeQuery();
            if (rs.next())
                readChat = extractMessageFrom(rs);
        } finally {
            if (rs != null)
                rs.close();
            if (ps != null)
                ps.close();
            con.close();
        }
        return readChat;
    }

    /**
     * Updates a message in the database.
     *
     * @return The updated message.
     * @throws SQLException       If any error occurs while updating the message.
     * @throws InstantiationError If the wrong constructor was used.
     */
    public PrivateChatMessage update() throws SQLException, InstantiationError {
        if (chat == null)
            throw new InstantiationError("Wrong constructor, use that with Chat");

        PreparedStatement ps = null;
        ResultSet rs = null;
        PrivateChatMessage readChat = null;

        try {
            con.setAutoCommit(false);

            ps = con.prepareStatement(STATEMENT_UPDATE);
            ps.setTimestamp(1, chat.getDateTime());
            ps.setString(2, chat.getContent());

            rs = ps.executeQuery();
            if (rs.next())
                readChat = extractMessageFrom(rs);
            con.commit();
        } catch (SQLException exception) {
            SQLException external = exception;
            try {
                con.rollback();
            } catch (SQLException ex) {
                //this should never be called
                external = new SQLException(exception.getMessage() + "Error when rollbacking commit", exception.getSQLState(), exception.getErrorCode());
            }
            // throw again the exception for external management
            throw external;
        } finally {
            if (rs != null)
                rs.close();
            if (ps != null)
                ps.close();
            con.setAutoCommit(true);
            con.close();
        }
        return readChat;
    }

    /**
     * Deletes a message from the database.
     *
     * @return The deleted message.
     * @throws SQLException       If any error occurs while deleting the message.
     * @throws InstantiationError If the wrong constructor was used.
     */
    public PrivateChatMessage delete() throws SQLException, InstantiationError {
        if (messageId == null)
            throw new InstantiationError("Wrong constructor, use that with id");

        PreparedStatement ps = null;
        ResultSet rs = null;
        PrivateChatMessage readChat = null;

        try {
            con.setAutoCommit(false);

            ps = con.prepareStatement(STATEMENT_DELETE);
            ps.setLong(1, messageId);

            rs = ps.executeQuery();
            if (rs.next())
                readChat = extractMessageFrom(rs);
            con.commit();
        } catch (SQLException exception) {
            SQLException external = exception;
            try {
                con.rollback();
            } catch (SQLException ex) {
                //this should never be called
                external = new SQLException(exception.getMessage() + "Error when rollbacking commit", exception.getSQLState(), exception.getErrorCode());
            }
            // throw again the exception for external management
            throw external;
        } finally {
            if (rs != null)
                rs.close();
            if (ps != null)
                ps.close();
            con.setAutoCommit(true);
            con.close();
        }
        return readChat;
    }

    /**
     * Lists chat messages between two users.
     *
     * @param senderId   The sender ID.
     * @param receiverId The receiver ID.
     * @param from       Search starting point: the {@code end - from}-th message.
     * @param howMany    How many messages to return from the starting point.
     * @return A list of messages.
     * @throws SQLException             If an error occurs while searching for messages.
     * @throws InstantiationError       If the wrong constructor was used.
     * @throws IllegalArgumentException If wrong parameters were passed.
     */
    public List<PrivateChatMessage> list(final long senderId, final long receiverId, final long from, final long howMany) throws SQLException, InstantiationError, IllegalArgumentException {
        if (messageId != null || chat != null)
            throw new InstantiationError("Wrong constructor, use that with only connection");

        if (senderId < 1 || receiverId < 1 || from < 1 || howMany < 1)
            throw new IllegalArgumentException("Parameters MUST be all positive");

        PreparedStatement ps = null;
        ResultSet rs = null;
        final List<PrivateChatMessage> chats = new ArrayList<>();

        try {
            ps = con.prepareStatement(STATEMENT_LIST);
            ps.setLong(1, senderId);
            ps.setLong(2, receiverId);
            ps.setLong(3, howMany);
            ps.setLong(4, from - 1);

            rs = ps.executeQuery();
            while (rs.next())
                chats.add(extractMessageFrom(rs));
        } finally {
            if (rs != null)
                rs.close();
            if (ps != null)
                ps.close();
            con.close();
        }
        return chats;
    }

    /**
     * Lists all the users an user has messages with.
     *
     * @param userId The user ID.
     * @return A list of users.
     * @throws SQLException             If an error occurs while searching for messages and users.
     * @throws InstantiationError       If the wrong constructor was used.
     * @throws IllegalArgumentException If wrong parameters were passed.
     */
    public List<User> listChatUsers(final long userId) throws SQLException, InstantiationError, IllegalArgumentException {
        if (messageId != null || chat != null)
            throw new InstantiationError("Wrong constructor, use that with only connection");

        if (userId < 1)
            throw new IllegalArgumentException("User id MUST be positive");

        PreparedStatement ps = null;
        ResultSet rs = null;
        final List<User> users = new ArrayList<>();

        try {
            ps = con.prepareStatement(STATEMENT_LIST_USERS);
            ps.setLong(1, userId);
            ps.setLong(2, userId);

            rs = ps.executeQuery();
            while (rs.next())
                users.add(extractUserFrom(rs));
        } finally {
            if (rs != null)
                rs.close();
            if (ps != null)
                ps.close();
            con.close();
        }
        return users;
    }

    /**
     * Gets a message object from a query's result.
     *
     * @param rs Result of a query.
     * @return The corresponding submission.
     * @throws SQLException If any error occurs during operations.
     */
    private PrivateChatMessage extractMessageFrom(ResultSet rs) throws SQLException {
        return new PrivateChatMessage(rs.getLong("msgId"),
                rs.getLong("senderId"),
                rs.getLong("receiverId"),
                Timestamp.valueOf(rs.getString("dateTime")),
                rs.getString("content"));
    }

    /**
     * Gets a user object from a query's result.
     *
     * @param rs Result of a query.
     * @return The corresponding submission.
     * @throws SQLException If any error occurs during operations.
     */
    private User extractUserFrom(ResultSet rs) throws SQLException {
        return new User(rs.getLong("id"),
                rs.getString("username"), rs.getString("password"),
                rs.getString("mail"), rs.getBoolean("isAdmin"),
                rs.getString("name"), rs.getString("surname"),
                rs.getDate("birthDate"), rs.getDate("joinDate"),
                rs.getString("location"), rs.getBytes("profilePic"));
    }
}
