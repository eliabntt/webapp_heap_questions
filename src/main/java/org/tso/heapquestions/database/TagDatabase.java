// Copyright 2018 The StackOverflowers
package org.tso.heapquestions.database;

import org.tso.heapquestions.resource.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Manages a {@link Comment} in the database.
 */
public class TagDatabase {
    private static final String STATEMENT_CREATE_SUBMISSION_TAG = "INSERT INTO tso.submissiontags VALUES (?, ?) RETURNING *;";
    private static final String STATEMENT_CREATE_FORUM_TAG = "INSERT INTO tso.forumtags VALUES (?, ?) RETURNING *;";
    private static final String STATEMENT_CREATE_TAG = "INSERT INTO tso.tag (value) SELECT ? WHERE NOT EXISTS (SELECT value FROM tso.tag WHERE value = ?) RETURNING *;";
    // this last query inserts the tag value only if it does not yet exist in the database

    private static final String STATEMENT_DELETE_SUBMISSION_TAG = "DELETE FROM tso.submissiontags WHERE submission = ? RETURNING *;";
    private static final String STATEMENT_DELETE_FORUM_TAG = "DELETE FROM tso.forumtags WHERE question = ? RETURNING *;";

    private static final String STATEMENT_GET_SUBMISSION_TAG = "SELECT * FROM tso.submissiontags WHERE submission = ?;";
    private static final String STATEMENT_GET_FORUM_TAG = "SELECT * FROM tso.forumtags WHERE question = ?;";

    // "" are not mandatory
    private static final String STATEMENT_GET_SUBMISSION_BY_TAG =
            "SELECT sub.* FROM tso.submissiontags AS st JOIN tso.submission AS sub ON (st.submission = sub.id)" +
            "WHERE tag ILIKE ? AND id IN (SELECT id FROM tso.submission WHERE state = 'Edited' OR state = 'Visible' ORDER BY dateTime DESC)" +
            "GROUP BY id ORDER BY dateTime DESC OFFSET ? LIMIT ?;";

    private static final String STATEMENT_GET_FORUM_BY_TAG =
            "SELECT fq.* FROM tso.forumtags AS ft JOIN tso.forumquestion AS fq ON (ft.question = fq.id)" +
            "WHERE tag ILIKE ? AND id IN (SELECT id FROM tso.forumquestion WHERE state = 'Edited' OR state = 'Visible' ORDER BY dateTime DESC)" +
            "GROUP BY id ORDER BY dateTime DESC OFFSET ? LIMIT ?;";

    private static final String STATEMENT_GET_TAGS = "SELECT value as tag FROM tso.tag WHERE value ILIKE ?;";

    private final Connection con;
    private final Tag tags;
    private final Long resourceId;
    private final ResourceSelector resourceType;

    /**
     * Constructor for creating a comment.
     *
     * @param con          Connection to the database.
     * @param tags         List of tags to be managed.
     * @param resourceId   ID of the Resource the comment refers to.
     * @param resourceType Whether the Resource (identified by {@code resourceID}) is a {@link Submission} or a {@link ForumQuestion}.
     */
    public TagDatabase(final Connection con, final Tag tags, final long resourceId, final ResourceSelector resourceType) {
        this.con = con;
        this.tags = tags;
        this.resourceId = resourceId;
        this.resourceType = resourceType;
    }

    /**
     * Constructor for having tags linked to a Resource (a {@link Submission} or a {@link ForumQuestion}).
     *
     * @param con          Connection to the database.
     * @param resourceId   ID of the Resource the tag refers to.
     * @param resourceType Whether the Resource (identified by {@code resourceID}) is a {@link Submission} or a {@link ForumQuestion}.
     */
    public TagDatabase(final Connection con, final long resourceId, final ResourceSelector resourceType) {
        this.con = con;
        this.tags = null;
        this.resourceId = resourceId;
        this.resourceType = resourceType;
    }

    /**
     * Constructor to search both Resources sharing a tag and list of similar tags.
     *
     * @param con Connection to the database.
     */
    public TagDatabase(final Connection con) {
        this.con = con;
        this.tags = null;
        this.resourceId = null;
        this.resourceType = null;
    }

    /**
     * Creates a tag in the database, if it doesn't exist.
     *
     * @param value Tag value.
     * @return The created tag.
     * @throws SQLException If any error occurs while inserting the tag.
     */
    private boolean createTag(String value) throws SQLException {
        PreparedStatement ps = null;
        try {
            con.setAutoCommit(false);

            ps = con.prepareStatement(STATEMENT_CREATE_TAG);
            ps.setString(1, value.toLowerCase());
            ps.setString(2, value.toLowerCase());

            ps.execute();
            con.commit();
        } catch (SQLException exception) {
            SQLException external = exception;
            try {
                con.rollback();
            } catch (SQLException ex) {
                //this should never be called
                external = new SQLException(exception.getMessage() + "Error when rollbacking commit", ex.getSQLState(), ex.getErrorCode());
            }
            // throw again the exception for external management
            throw external;
        } finally {
            if (ps != null)
                ps.close();
        }
        return true;
    }

    /**
     * Adds tags to a Resource ({@link Submission} or {@link ForumQuestion}).
     *
     * @return The new list of tags.
     * @throws SQLException       If any error occurs while linking tags.
     * @throws InstantiationError If the wrong constructor was used or if the statement cannot be created.
     */
    public Tag addTagsToResource() throws SQLException, InstantiationError {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Tag tag;

        // check if wrong constructor was used
        if (tags == null | resourceId == null)
            throw new InstantiationError("Wrong constructor, use that with Tags and resourceId");

        try {
            con.setAutoCommit(false);

            // create tags if the aren't in the database
            // select right query for submission or forum question
            if (resourceType == ResourceSelector.SUBMISSION)
                ps = con.prepareStatement(STATEMENT_CREATE_SUBMISSION_TAG);
            else if (resourceType == ResourceSelector.FORUM_QUESTION)
                ps = con.prepareStatement(STATEMENT_CREATE_FORUM_TAG);
            else
                throw new InstantiationError("Cannot create the statement, unknown error");

            // process each tag as a unique batch (and transaction)
            for (String tagValue : tags.getValue()) {
                String value = tagValue.toLowerCase();
                if (createTag(value)) {
                    ps.setLong(1, resourceId);
                    ps.setString(2, value);
                    ps.addBatch();
                } else
                    throw new SQLException("Error while creating Tag");
            }
            ps.executeBatch();

            // get current tags to return
            if (resourceType == ResourceSelector.SUBMISSION)
                ps = con.prepareStatement(STATEMENT_GET_SUBMISSION_TAG);
            else
                ps = con.prepareStatement(STATEMENT_GET_FORUM_TAG);
            ps.setLong(1, resourceId);

            rs = ps.executeQuery();
            con.commit();

            // read result
            List<String> tagList = new ArrayList<>();
            while (rs.next())
                tagList.add(rs.getString("tag").toLowerCase());
            tag = extractTagFrom(tagList);
        } catch (SQLException exception) {
            SQLException external = exception;
            try {
                con.rollback();
            } catch (SQLException ex) {
                //this should never be called
                external = new SQLException(exception.getMessage() + "Error when rollbacking commit", ex.getSQLState(), ex.getErrorCode());
            }
            // throw again the exception for external management
            throw external;
        } finally {
            if (rs != null)
                rs.close();
            if (ps != null)
                ps.close();
            con.setAutoCommit(true);
            con.close();
        }
        return tag;
    }

    /**
     * Get tags for a Resource ({@link Submission} or {@link ForumQuestion}).
     *
     * @return The list of tags of the resource.
     * @throws SQLException       If any error occurs while linking tags.
     * @throws InstantiationError If the wrong constructor was used.
     */
    public Tag readTagsForResource() throws SQLException, InstantiationError {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Tag tag;

        // check if wrong constructor was used
        if (resourceId == null)
            throw new InstantiationError("Wrong constructor, use that with resourceId");

        try {
            // select right query for submission or forum question
            if (resourceType == ResourceSelector.SUBMISSION)
                ps = con.prepareStatement(STATEMENT_GET_SUBMISSION_TAG);
            else
                ps = con.prepareStatement(STATEMENT_GET_FORUM_TAG);
            ps.setLong(1, resourceId);

            // read result
            rs = ps.executeQuery();
            List<String> tagList = new ArrayList<>();
            while (rs.next())
                tagList.add(rs.getString("tag").toLowerCase());
            tag = extractTagFrom(tagList);
        } catch (SQLException exception) {
            SQLException external = exception;
            try {
                // cancel both queries' effect in case of errors
                con.rollback();
            } catch (SQLException ex) {
                //this should never be called
                external = new SQLException(exception.getMessage() + "Error when rollbacking commit", exception.getSQLState(), exception.getErrorCode());
            }
            // throw again the exception for external management
            throw external;
        } finally {
            if (rs != null)
                rs.close();
            if (ps != null)
                ps.close();
            con.close();
        }
        return tag;
    }

    /**
     * Update tags for a Resource ({@link Submission} or {@link ForumQuestion}).
     *
     * @return The updated list of tags of the resource.
     * @throws SQLException       If any error occurs while linking tags.
     * @throws InstantiationError If the wrong constructor was used.
     */
    public Tag updateTagsForResource() throws SQLException, InstantiationError {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Tag tag;

        // check if wrong constructor was used
        if (tags == null | resourceId == null)
            throw new InstantiationError("Wrong constructor, use that with Tags");

        try {
            // process each tag as a unique batch (and transaction)
            con.setAutoCommit(false);

            if (resourceType == ResourceSelector.SUBMISSION)
                ps = con.prepareStatement(STATEMENT_DELETE_SUBMISSION_TAG);
            else
                ps = con.prepareStatement(STATEMENT_DELETE_FORUM_TAG);
            ps.setLong(1, resourceId);
            ps.execute();

            if (resourceType == ResourceSelector.SUBMISSION)
                ps = con.prepareStatement(STATEMENT_CREATE_SUBMISSION_TAG);
            else
                ps = con.prepareStatement(STATEMENT_CREATE_FORUM_TAG);

            for (String tagValue : tags.getValue()) {
                String value = tagValue.toLowerCase();
                if (createTag(value)) {
                    ps.setLong(1, resourceId);
                    ps.setString(2, value);
                    ps.addBatch();
                } else
                    throw new SQLException("Error while creating Tag");
            }
            ps.executeBatch();

            // get current tags to return
            if (resourceType == ResourceSelector.SUBMISSION)
                ps = con.prepareStatement(STATEMENT_GET_SUBMISSION_TAG);
            else
                ps = con.prepareStatement(STATEMENT_GET_FORUM_TAG);
            ps.setLong(1, resourceId);

            rs = ps.executeQuery();
            con.commit();

            // read result
            List<String> tagList = new ArrayList<>();
            while (rs.next())
                tagList.add(rs.getString("tag").toLowerCase());
            tag = extractTagFrom(tagList);
        } catch (SQLException exception) {
            SQLException external = exception;
            try {
                con.rollback();
            } catch (SQLException ex) {
                //this should never be called
                external = new SQLException(exception.getMessage() + "Error when rollbacking commit", ex.getSQLState(), ex.getErrorCode());
            }
            // throw again the exception for external management
            throw external;
        } finally {
            if (rs != null)
                rs.close();
            if (ps != null)
                ps.close();
            con.setAutoCommit(true);
            con.close();
        }
        return tag;
    }

    /**
     * Delete tags of a Resource ({@link Submission} or {@link ForumQuestion}).
     *
     * @return The list of deleted tags.
     * @throws SQLException If any error occurs while linking tags.
     */
    public Tag deleteTagsForResource() throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Tag tag;

        // check if wrong constructor was used
        if (resourceId == null)
            throw new InstantiationError("Wrong constructor, use that with resourceId");

        try {
            con.setAutoCommit(false);

            // select right query for submission or forum question
            if (resourceType == ResourceSelector.SUBMISSION)
                ps = con.prepareStatement(STATEMENT_DELETE_SUBMISSION_TAG);
            else
                ps = con.prepareStatement(STATEMENT_DELETE_FORUM_TAG);
            ps.setLong(1, resourceId);

            rs = ps.executeQuery();
            con.commit();

            // read result
            List<String> tagList = new ArrayList<>();
            while (rs.next())
                tagList.add(rs.getString("tag").toLowerCase());
            tag = extractTagFrom(tagList);
        } catch (SQLException exception) {
            SQLException external = exception;
            try {
                con.rollback();
            } catch (SQLException ex) {
                //this should never be called
                external = new SQLException(exception.getMessage() + "Error when rollbacking commit", ex.getSQLState(), ex.getErrorCode());
            }
            // throw again the exception for external management
            throw external;
        } finally {
            if (rs != null)
                rs.close();
            if (ps != null)
                ps.close();
            con.setAutoCommit(true);
            con.close();
        }
        return tag;
    }

    /**
     * Gets a list of Resources ({@link Submission}s or {@link ForumQuestion}s) that share the same tag.
     *
     * @param <T>             The return type: Submission or ForumQuestion.
     * @param tag             The common tag.
     * @param relToSubmission Whether to search only among submissions or forum questions.
     * @param from            Search starting point: the {@code end - from}-th resource.
     * @param howMany         How many resources to return from the starting point.
     * @return The list of resources.
     * @throws SQLException If any error occurs while linking tags.
     */
    public <T extends Resource> List<T> getResourcesHavingTag(final String tag, final ResourceSelector relToSubmission, long from, long howMany) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<T> results = new ArrayList<>();

        try {
            // select right query for submission or forum question
            if (relToSubmission == ResourceSelector.SUBMISSION)
                ps = con.prepareStatement(STATEMENT_GET_SUBMISSION_BY_TAG);
            else
                ps = con.prepareStatement(STATEMENT_GET_FORUM_BY_TAG);

            ps.setString(1, tag.toLowerCase() + "%");
            ps.setLong(2, from - 1);
            ps.setLong(3, howMany);

            rs = ps.executeQuery();
            if (relToSubmission == ResourceSelector.SUBMISSION) {
                while (rs.next())
                    results.add((T) new SubmissionDatabase(null).extractFrom(rs));
            } else {
                while (rs.next())
                    results.add((T) new ForumDatabase(null).extractFrom(rs));
            }
        } finally {
            if (rs != null)
                rs.close();
            if (ps != null)
                ps.close();
            con.close();
        }
        return results;
    }

    /**
     * Gets tags similar to a given one.
     *
     * @param tagToCompare The tag to compare.
     * @return A list of tags.
     * @throws SQLException If any error occurs while linking tags.
     */
    public Tag getSimilarTag(final String tagToCompare) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Tag tag;

        try {
            ps = con.prepareStatement(STATEMENT_GET_TAGS);
            ps.setString(1, tagToCompare.toLowerCase() + "%"); // % is for LIKE clause
            rs = ps.executeQuery();

            // read result
            List<String> tagList = new ArrayList<>();
            while (rs.next())
                tagList.add(rs.getString("tag").toLowerCase());
            tag = extractTagFrom(tagList);
        } finally {
            if (rs != null)
                rs.close();
            if (ps != null)
                ps.close();
            con.close();
        }
        return tag;
    }

    /**
     * Gets a {@link Tag} object from a query's result.
     *
     * @param tags The list of tags resulting from a query.
     * @return The corresponding tag.
     */
    private Tag extractTagFrom(List<String> tags) {
        return new Tag(tags);
    }

    /**
     * Gets a {@link ResourceTag} object from a query's result.
     *
     * @param rs Result of a query.
     * @return The corresponding id.
     * @throws SQLException If any error occurs during operations.
     */
    private ResourceTag extractResourceTagFrom(ResultSet rs) throws SQLException {
        return new ResourceTag(rs.getLong("id"), rs.getString("tag").toLowerCase());
    }
}
