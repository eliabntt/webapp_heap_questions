// Copyright 2018 The StackOverflowers
package org.tso.heapquestions.database;

import org.tso.heapquestions.resource.Category;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Manages a {@link Category} in the database.
 */
public class CategoryDatabase {
    private static final String STATEMENT_READ = "SELECT * FROM tso.category;";

    private final Connection con;

    /**
     * Constructor to list all categories.
     *
     * @param con Connection to the database.
     */
    public CategoryDatabase(final Connection con) {
        this.con = con;
    }

    /**
     * Lists all the categories in the database.
     *
     * @return The list of categories.
     * @throws SQLException If any error occurs while searching for categories.
     */
    public ArrayList<Category> list() throws SQLException {
        ArrayList<Category> categories = new ArrayList<>();
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            ps = con.prepareStatement(STATEMENT_READ);

            rs = ps.executeQuery();
            while (rs.next()) {
                String name = rs.getString("name");
                String description = rs.getString("description");
                categories.add(new Category(name, description));
            }
        } finally {
            if (rs != null)
                rs.close();
            if (ps != null)
                ps.close();
            con.close();
        }
        return categories;
    }
}
