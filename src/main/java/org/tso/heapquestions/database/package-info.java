// Copyright 2018 The StackOverflowers

/**
 * Provides access to the underlying database.
 */
package org.tso.heapquestions.database;