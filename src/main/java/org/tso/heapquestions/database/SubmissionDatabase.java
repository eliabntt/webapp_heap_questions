// Copyright 2018 The StackOverflowers
package org.tso.heapquestions.database;

import org.tso.heapquestions.resource.Difficulties;
import org.tso.heapquestions.resource.ItemState;
import org.tso.heapquestions.resource.PlanLevel;
import org.tso.heapquestions.resource.Submission;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Manages a {@link Submission} in the database.
 */
public class SubmissionDatabase {
    private static final String STATEMENT_CREATE = "INSERT INTO tso.submission VALUES(DEFAULT, ?, ?, DEFAULT, ?::tso.difficulties, DEFAULT, ?, ?, ?, ?::tso.planlevel) RETURNING *;";
    private static final String STATEMENT_READ = "SELECT * FROM tso.submission WHERE id = ? AND (state = 'Visible' OR state = 'Edited');";
    private static final String STATEMENT_UPDATE = "UPDATE tso.submission SET (title, difficulty, state, content, solution, category, minPlanToVis) = (?, ?::tso.difficulties, ?::tso.itemstate, ?, ?, ?, ?::tso.planlevel) WHERE id = ? AND (state = 'Edited' OR state = 'Visible') RETURNING *;";
    private static final String STATEMENT_CHANGE_STATE = "UPDATE tso.submission SET (state) = (?::tso.itemstate) WHERE id = ? RETURNING *;";
    private static final String STATEMENT_LIST = "SELECT * FROM tso.submission WHERE  (state = 'Visible' OR state = 'Edited') ORDER BY dateTime DESC;";
    private static final String STATEMENT_LIST_FROM_HOWMANY = "SELECT * FROM tso.submission WHERE  (state = 'Visible' OR state = 'Edited')  ORDER BY dateTime DESC OFFSET ? LIMIT ?;";
    private static final String STATEMENT_LIST_BY_USER = "SELECT * FROM tso.submission WHERE userId = ? AND (state = 'Visible' OR state = 'Edited') ORDER BY dateTime DESC;";
    private static final String STATEMENT_LIST_BY_USER_FROM_HOWMANY = "SELECT * FROM tso.submission WHERE userId = ? AND (state = 'Visible' OR state = 'Edited')  ORDER BY dateTime DESC OFFSET ? LIMIT ?;";
    private static final String STATEMENT_LIST_TERM = "SELECT * FROM tso.submission WHERE (state = 'Visible' OR state = 'Edited') AND title ILIKE ? ORDER BY dateTime DESC OFFSET ? LIMIT ?;";


    private final Connection con;
    private final Submission submission;
    private final Long id;
    private final String searchTerm;

    /**
     * Constructor for creating and updating a submission.
     *
     * @param con        Connection to the database.
     * @param submission Submission to be managed.
     */
    public SubmissionDatabase(final Connection con, final Submission submission) {
        this.con = con;
        this.submission = submission;
        this.id = null;
        this.searchTerm = null;
    }

    /**
     * Constructor for reading a submission.
     *
     * @param con Connection to the database.
     * @param id  ID of the Submission to be managed.
     */
    public SubmissionDatabase(final Connection con, final long id) {
        this.con = con;
        this.submission = null;
        this.id = id;
        this.searchTerm = null;
    }

    /**
     * Constructor for changing the state of a submission.
     *
     * @param con        Connection to the database.
     * @param submission Submission to be managed.
     * @param id         ID of the Submission to be managed.
     */
    public SubmissionDatabase(final Connection con, final Submission submission, final long id) {
        this.con = con;
        this.submission = submission;
        this.id = id;
        this.searchTerm = null;
    }

    /**
     * Constructor for searching a set of submissions given the search term.
     *
     * @param con Connection to the database.
     * @param st  (part of the)Title to be searched.
     */
    public SubmissionDatabase(final Connection con, final String st) {
        this.con = con;
        this.submission = null;
        this.id = null;
        this.searchTerm = st;
    }

    /**
     * Constructor to list all submissions.
     *
     * @param con Connection to the database.
     */
    public SubmissionDatabase(Connection con) {
        this.con = con;
        this.submission = null;
        this.id = null;
        this.searchTerm = null;
    }

    /**
     * Creates a new submission in the database.
     *
     * @param userId The ID of the user that requested the operation.
     * @return The new inserted submission.
     * @throws SQLException       If any error occurs while inserting the submission.
     * @throws InstantiationError If the wrong constructor was used.
     */
    public Submission create(long userId) throws SQLException, InstantiationError {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Submission readSub = null;

        // check if wrong constructor was used
        if (submission == null)
            throw new InstantiationError("Wrong constructor, use that with Submission");

        try {
            con.setAutoCommit(false);

            ps = con.prepareStatement(STATEMENT_CREATE);
            ps.setLong(1, userId);
            ps.setString(2, submission.getTitle());
            ps.setString(3, submission.getDifficulty().toString());
            ps.setString(4, submission.getContent());
            ps.setString(5, submission.getSolution());
            ps.setString(6, submission.getCategory());
            ps.setString(7, submission.getMinPlanToVis().toString());

            rs = ps.executeQuery();
            if (rs.next())
                readSub = extractFrom(rs);
            con.commit();
        } catch (SQLException exception) {
            SQLException external = exception;
            try {
                con.rollback();
            } catch (SQLException ex) {
                //this should never be called
                external = new SQLException(exception.getMessage() + "Error when rollbacking commit", ex.getSQLState(), ex.getErrorCode());
            }
            // throw again the exception for external management
            throw external;
        } finally {
            if (rs != null)
                rs.close();
            if (ps != null)
                ps.close();
            con.setAutoCommit(true);
            con.close();
        }
        return readSub;
    }

    /**
     * Reads a submission from the database.
     *
     * @return The submission.
     * @throws SQLException If any error occurs while searching for the submission.
     */
    public Submission read() throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Submission readSub = null;

        // check if wrong constructor was used
        if (id == null)
            throw new InstantiationError("Wrong constructor, use that with id");

        try {
            ps = con.prepareStatement(STATEMENT_READ);
            ps.setLong(1, id);

            rs = ps.executeQuery();
            if (rs.next())
                readSub = extractFrom(rs);
        } finally {
            if (rs != null)
                rs.close();
            if (ps != null)
                ps.close();
            con.close();
        }
        return readSub;
    }

    /**
     * Updates a submission in the database.
     *
     * @return The updated submission.
     * @throws SQLException       If any error occurs while updating the submission.
     * @throws InstantiationError If the wrong constructor was used.
     */
    public Submission update() throws SQLException, InstantiationError {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Submission readSub = null;

        // check if wrong constructor was used
        if (submission == null)
            throw new InstantiationError("Wrong constructor, use that with Submission");

        try {
            con.setAutoCommit(false);

            ps = con.prepareStatement(STATEMENT_UPDATE);
            ps.setString(1, submission.getTitle());
            ps.setString(2, submission.getDifficulty().toString());
            ps.setString(3, ItemState.Edited.toString()); //Edited state if updated
            ps.setString(4, submission.getContent());
            ps.setString(5, submission.getSolution());
            ps.setString(6, submission.getCategory());
            ps.setString(7, submission.getMinPlanToVis().toString());
            ps.setLong(8, submission.getId());

            rs = ps.executeQuery();
            if (rs.next())
                readSub = extractFrom(rs);
            con.commit();
        } catch (SQLException exception) {
            SQLException external = exception;
            try {
                con.rollback();
            } catch (SQLException ex) {
                //this should never be called
                external = new SQLException(exception.getMessage() + "Error when rollbacking commit", ex.getSQLState(), ex.getErrorCode());
            }
            // throw again the exception for external management
            throw external;
        } finally {
            if (rs != null)
                rs.close();
            if (ps != null)
                ps.close();
            con.setAutoCommit(true);
            con.close();
        }
        return readSub;
    }

    /**
     * Deletes a submission from the database.
     *
     * @return The deleted submission.
     * @throws SQLException       If any error occurs while deleting the submission.
     * @throws InstantiationError If the wrong constructor was used.
     */
    public Submission deleteChangeState() throws SQLException, InstantiationError {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Submission readSub = null;

        // check if wrong constructor was used
        if (id == null)
            throw new InstantiationError("Wrong constructor, use that with id.");

        try {
            con.setAutoCommit(false);

            ps = con.prepareStatement(STATEMENT_CHANGE_STATE);
            ps.setString(1, submission.getState().toString()); //deleted or moderated
            ps.setLong(2, id);

            rs = ps.executeQuery();
            if (rs.next())
                readSub = extractFrom(rs);
            con.commit();
        } catch (SQLException exception) {
            SQLException external = exception;
            try {
                con.rollback();
            } catch (SQLException ex) {
                //this should never be called
                external = new SQLException(exception.getMessage() + "Error when rollbacking commit", exception.getSQLState(), exception.getErrorCode());
            }
            // throw again the exception for external management
            throw external;
        } finally {
            if (rs != null)
                rs.close();
            if (ps != null)
                ps.close();
            con.setAutoCommit(true);
            con.close();
        }
        return readSub;
    }

    /**
     * Lists all the submissions in the database.
     *
     * @return The list of submissions.
     * @throws SQLException If any error occurs while searching for submissions.
     */
    public List<Submission> list() throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        final List<Submission> submissions = new ArrayList<>();

        try {
            if (id != null) {
                ps = con.prepareStatement(STATEMENT_LIST_BY_USER);
                ps.setLong(1, id);
            } else
                ps = con.prepareStatement(STATEMENT_LIST);

            rs = ps.executeQuery();
            while (rs.next())
                submissions.add(extractFrom(rs));
        } finally {
            if (rs != null)
                rs.close();
            if (ps != null)
                ps.close();
            con.close();
        }
        return submissions;
    }

    /**
     * Lists a certain number of submissions in the database for a user starting from a submission.
     *
     * @param from    The starting submission in the order.
     * @param howMany How many submissions to fetch.
     * @return The list of submissions.
     * @throws SQLException If any error occurs while searching for submissions.
     */
    public List<Submission> listByUserFromHowMany(final long from, final long howMany) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        final List<Submission> submissions = new ArrayList<>();

        try {
            if (id == null)
                throw new IllegalArgumentException("Pass the user id when calling the method");
            if (from < 1 || howMany < 1)
                throw new IllegalArgumentException("To use this pass 'from' and 'howMany' positives");

            ps = con.prepareStatement(STATEMENT_LIST_BY_USER_FROM_HOWMANY);
            ps.setLong(1, id);
            ps.setLong(2, from - 1);
            ps.setLong(3, howMany);

            rs = ps.executeQuery();
            while (rs.next())
                submissions.add(extractFrom(rs));
        } finally {
            if (rs != null)
                rs.close();
            if (ps != null)
                ps.close();
            con.close();
        }
        return submissions;
    }

    /**
     * Lists some submissions starting from a specific submission.
     *
     * @param from    The first submission to consider.
     * @param howMany How many submissions.
     * @return A list of submissions.
     * @throws SQLException If any error occurs while searching for submissions.
     */
    public List<Submission> listFromHowMany(long from, long howMany) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        final List<Submission> submissions = new ArrayList<>();

        if (from < 1 && howMany < 1)
            throw new IllegalArgumentException("Parameters must be all positive");

        try {
            ps = con.prepareStatement(STATEMENT_LIST_FROM_HOWMANY);
            ps.setLong(1, from - 1);
            ps.setLong(2, howMany);
            rs = ps.executeQuery();
            while (rs.next())
                submissions.add(extractFrom(rs));
        } finally {
            if (rs != null)
                rs.close();
            if (ps != null)
                ps.close();
            this.con.close();
        }
        return submissions;
    }

    /**
     * Gets a block of submissions starting from a specific one from the database with the searched title.
     *
     * @param from    Search starting point: the {@code end - from}-th submission.
     * @param howMany How many submissions to return from the starting point.
     * @return A list of submissions.
     * @throws SQLException             If any error occurs during operations.
     * @throws IllegalArgumentException If wrong parameters were passed.
     * @throws InstantiationError       If the statement cannot be created.
     */
    public List<Submission> getSearchList(final long from, final long howMany) throws SQLException, IllegalArgumentException, InstantiationError {
        if (from < 1 || howMany < 1)
            throw new IllegalArgumentException("Function parameters must be all positive");

        PreparedStatement ps = null;
        ResultSet rs = null;
        final List<Submission> subs = new ArrayList<>();

        try {
            ps = con.prepareStatement(STATEMENT_LIST_TERM);
            ps.setString(1, searchTerm + "%");
            ps.setLong(2, from - 1);

            /* It is an offset, so if I specify from 1, the first comment, the table offset will be 0.
             * If I want the comment #10, I skip the first 9 comments, so offset = 9. */
            ps.setLong(3, howMany);
            rs = ps.executeQuery();

            while (rs.next())
                subs.add(extractFrom(rs));
        } finally {
            if (rs != null)
                rs.close();
            if (ps != null)
                ps.close();
            if (con != null)
                con.close();
        }
        return subs;
    }

    /**
     * Gets a submission object from a query's result.
     *
     * @param rs Result of a query.
     * @return The corresponding submission.
     * @throws SQLException If any error occurs during operations.
     */
    Submission extractFrom(ResultSet rs) throws SQLException {
        return new Submission(rs.getLong("id"), rs.getLong("userId"),
                rs.getString("title"), rs.getTimestamp("dateTime"),
                Difficulties.fromString(rs.getString("difficulty")),
                ItemState.fromString(rs.getString("state")), rs.getString("content"),
                rs.getString("solution"), rs.getString("category"),
                PlanLevel.fromString(rs.getString("minPlanToVis")));
    }
}
