package org.tso.heapquestions.database;

import org.tso.heapquestions.resource.Note;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Manages a {@link Note} in the database.
 */
public class NotesDatabase {
    private static final String STATEMENT_CREATE = "INSERT INTO tso.note VALUES (DEFAULT, ?, DEFAULT, ?) RETURNING *;";
    private static final String STATEMENT_READ = "SELECT * FROM tso.note WHERE id = ? AND userId = ?;";
    private static final String STATEMENT_UPDATE = "UPDATE tso.note SET (content, dateTime)=(?, DEFAULT) WHERE id = ? AND userid = ? RETURNING *;";
    private static final String STATEMENT_DELETE = "DELETE FROM tso.note WHERE id = ? AND userId = ? RETURNING *;";
    private static final String STATEMENT_LIST = "SELECT * FROM tso.note WHERE userid = ? ORDER BY dateTime DESC;";

    private final Connection con;
    private final Long id;
    private final Note note;

    /**
     * Constructor to delete, list or read notes.
     *
     * @param con Connection to the database.
     * @param id  The note's ID.
     */
    public NotesDatabase(final Connection con, final Long id) {
        this.con = con;
        this.id = id;
        this.note = null;
    }

    /**
     * Constructor to create or update a note.
     *
     * @param con  Connection to the database.
     * @param note The note object.
     */
    public NotesDatabase(final Connection con, final Note note) {
        this.con = con;
        this.note = note;
        this.id = null;
    }

    /**
     * Create a new note for a user.
     *
     * @param userId The id of the user creating the note
     * @return The note created.
     * @throws SQLException If any error occurs while inserting the comment.
     */
    public Note create(long userId) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Note result = null;

        if (note == null)
            throw new InstantiationError("Wrong constructor, use that with Note");

        try {
            con.setAutoCommit(false);

            ps = con.prepareStatement(STATEMENT_CREATE);
            ps.setLong(1, userId);
            ps.setString(2, note.getContent());

            rs = ps.executeQuery();
            if (rs.next())
                result = extractFrom(rs);
            con.commit();
        } catch (SQLException exception) {
            SQLException external = exception;
            try {
                con.rollback();
            } catch (SQLException ex) {
                //this should never be called
                external = new SQLException(exception.getMessage() + "Error when rollbacking commit", ex.getSQLState(), ex.getErrorCode());
            }
            // throw again the exception for external management
            throw external;
        } finally {
            if (rs != null)
                rs.close();
            if (ps != null)
                ps.close();
            con.setAutoCommit(true);
            con.close();
        }
        return result;
    }

    /**
     * Reads a note from the database.
     *
     * @param userId The current user ID.
     * @return The note.
     * @throws SQLException If any error occurs while searching for the note.
     */
    public Note read(long userId) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Note result = null;

        try {
            ps = con.prepareStatement(STATEMENT_READ);
            ps.setLong(1, id);
            ps.setLong(2, userId);

            rs = ps.executeQuery();
            if (rs.next())
                result = extractFrom(rs);
        } finally {
            if (rs != null)
                rs.close();
            if (ps != null)
                ps.close();
            con.close();
        }
        return result;
    }

    /**
     * Updates a note in the database.
     *
     * @return The updated note.
     * @throws SQLException       If any error occurs while updating the comment.
     * @throws InstantiationError If the wrong constructor was used.
     */
    public Note update() throws SQLException, InstantiationError {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Note result = null;

        // check if wrong constructor was used
        if (note == null)
            throw new InstantiationError("Wrong constructor, use that with Note");

        try {
            con.setAutoCommit(false);

            ps = con.prepareStatement(STATEMENT_UPDATE);
            ps.setString(1, note.getContent());
            ps.setLong(2, note.getId());
            ps.setLong(3, note.getUserId());

            rs = ps.executeQuery();
            if (rs.next())
                result = extractFrom(rs);
            con.commit();
        } catch (SQLException exception) {
            SQLException external = exception;
            try {
                con.rollback();
            } catch (SQLException ex) {
                //this should never be called
                external = new SQLException(exception.getMessage() + "Error when rollbacking commit", ex.getSQLState(), ex.getErrorCode());
            }
            // throw again the exception for external management
            throw external;
        } finally {
            if (rs != null)
                rs.close();
            if (ps != null)
                ps.close();
            con.setAutoCommit(true);
            con.close();
        }
        return result;
    }

    /**
     * Delete a note in the database.
     *
     * @param userId The current user ID.
     * @return The deleted note.
     * @throws SQLException       If any error occurs while deleting the note.
     * @throws InstantiationError If the wrong constructor was used.
     */
    public Note delete(long userId) throws SQLException, InstantiationError {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Note result = null;

        // check if wrong constructor was used
        if (id == null)
            throw new InstantiationError("Wrong constructor, use that with id");

        try {
            con.setAutoCommit(false);
            ps = con.prepareStatement(STATEMENT_DELETE);
            ps.setLong(1, id);
            ps.setLong(2, userId);

            rs = ps.executeQuery();
            if (rs.next())
                result = extractFrom(rs);
            con.commit();
        } catch (SQLException exception) {
            SQLException external = exception;
            try {
                con.rollback();
            } catch (SQLException ex) {
                //this should never be called
                external = new SQLException(exception.getMessage() + "Error when rollbacking commit", ex.getSQLState(), ex.getErrorCode());
            }
            // throw again the exception for external management
            throw external;
        } finally {
            if (rs != null)
                rs.close();
            if (ps != null)
                ps.close();
            con.setAutoCommit(true);
            con.close();
        }
        return result;
    }

    /**
     * Lists all the notes for a user.
     *
     * @return The list of notes.
     * @throws SQLException If any error occurs while searching for notes.
     */
    public List<Note> list() throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        final List<Note> notes = new ArrayList<>(); // the result

        try {
            ps = con.prepareStatement(STATEMENT_LIST);
            ps.setLong(1, id);

            rs = ps.executeQuery();
            while (rs.next())
                notes.add(extractFrom(rs));
        } finally {
            if (rs != null)
                rs.close();
            if (ps != null)
                ps.close();
            con.close();
        }
        return notes;
    }

    /**
     * Gets a Note object from a query's result.
     *
     * @param rs Result of a query.
     * @return The corresponding note.
     * @throws SQLException If any error occurs during operations.
     */
    private Note extractFrom(ResultSet rs) throws SQLException {
        return new Note(rs.getLong("id"), rs.getLong("userid"),
                rs.getTimestamp("dateTime"), rs.getString("content"));
    }
}
