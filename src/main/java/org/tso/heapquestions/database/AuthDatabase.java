// Copyright 2018 The StackOverflowers
package org.tso.heapquestions.database;

import org.tso.heapquestions.resource.*;

import javax.naming.AuthenticationException;
import java.security.SecureRandom;
import java.sql.*;
import java.util.Calendar;

/**
 * Manages login/logout operations on the database.
 */
public final class AuthDatabase {
    public static final long INVALID_TOKEN = -1;

    private static final String STATEMENT_LOGIN = "SELECT u.id, username, isadmin FROM tso.userextended AS ue INNER JOIN" +
            "(SELECT id, username from tso.user WHERE (username = ? OR mail = ?) AND password = ?) AS u ON ue.id = u.id";
    private static final String STATEMENT_TOKEN_CREATE = "INSERT INTO tso.token(token, userId, expireDate) VALUES (?,?,?) RETURNING *";
    private static final String STATEMENT_TOKEN_DELETE_EXPIRED = "DELETE FROM tso.token WHERE userId = ? AND expireDate < current_timestamp;";
    private static final String STATEMENT_TOKEN_DELETE_LOGOUT = "DELETE FROM tso.token WHERE token = ? RETURNING token;";
    private static final String STATEMENT_TOKEN_CHECK = "SELECT * FROM tso.token WHERE token = ? AND expireDate > current_timestamp;";
    private static final String STATEMENT_ADMIN_CHECK = "SELECT isAdmin FROM tso.userExtended WHERE id = ? AND isAdmin = TRUE;";
    private static final String STATEMENT_AUTH_CHECK_COMMENT = "SELECT userId FROM tso.comment WHERE id = ? AND userId = ?;";
    private static final String STATEMENT_AUTH_CHECK_QUESTION = "SELECT userId FROM tso.forumQuestion WHERE id = ? AND userId = ?;";
    private static final String STATEMENT_AUTH_CHECK_SUBMISSION = "SELECT userId FROM tso.submission WHERE id = ? AND userId = ?;";

    private final Connection con;
    private final String userOrEmail, password, token;
    private final boolean remember;

    /**
     * Constructor used for login procedure.
     *
     * @param con         The connection to the database.
     * @param userOrEmail The username or the e-mail of the logging user.
     * @param password    The password of the logging user.
     * @param remember    Whether the login has to have a longer duration.
     */
    public AuthDatabase(final Connection con, final String userOrEmail,
                        final String password, final boolean remember) {
        this.con = con;
        this.userOrEmail = userOrEmail;
        this.password = Tools.sha256(password);
        this.remember = remember;
        this.token = null;
    }

    /**
     * Constructor used for token checking and logout procedure.
     *
     * @param con   The connection to the database.
     * @param token Access token to be invalidated (deleted).
     */
    public AuthDatabase(final Connection con, final String token) {
        this.con = con;
        this.token = token;
        userOrEmail = password = null;
        remember = false;
    }

    /**
     * Constructor for user authorization checking.
     *
     * @param con The connection to the database.
     */
    public AuthDatabase(final Connection con) {
        this.con = con;
        token = null;
        userOrEmail = password = null;
        remember = false;
    }

    /**
     * The login procedure.
     *
     * @return A {@link Token} object containing the token for accessing the platform's content.
     * @throws SQLException If error occurs during operations.
     */
    public Token doLogin() throws SQLException {
        /*
         * Description of the procedure:
         *
         * Check for good user credentials
         * If they are not valid
         *   Return a null object.
         * If they are valid
         *   Delete old expired user tokens;
         *   Create a new token and return it.
         */

        PreparedStatement ps = null;
        ResultSet rs = null;
        Token credentials = null; // output object

        try {
            con.setAutoCommit(false);

            // check username/mail and password
            ps = con.prepareStatement(STATEMENT_LOGIN);
            ps.setString(1, userOrEmail);
            ps.setString(2, userOrEmail);
            ps.setString(3, password);
            // assume password has already been encoded when reading the LoginItem obj.

            Long queriedId = null;
            String username = null;
            boolean isAdmin = false;

            rs = ps.executeQuery();
            if (rs.next()) {
                queriedId = rs.getLong("id");
                username = rs.getString("username");
                isAdmin = rs.getBoolean("isadmin");
            }
            // credentials present
            if (queriedId != null && username != null) {
                rs.close();
                ps.close();

                // delete old expired tokens
                ps = con.prepareStatement(STATEMENT_TOKEN_DELETE_EXPIRED);
                ps.setLong(1, queriedId);
                ps.executeUpdate();
                ps.close();

                // no token found; create a new one
                ps = con.prepareStatement(STATEMENT_TOKEN_CREATE);
                ps.setString(1, getToken(queriedId));
                ps.setLong(2, queriedId);
                ps.setDate(3, getDate(remember));

                rs = ps.executeQuery();
                con.commit();
                if (rs.next())
                    credentials = new Token(rs.getLong("userId"),
                            username,
                            rs.getString("token"),
                            rs.getTimestamp("expireDate"),
                            isAdmin);
            }
        } catch (SQLException exception) {
            SQLException external = exception;
            try {
                con.rollback();
            } catch (SQLException ex) {
                //this should never be called
                external = new SQLException(exception.getMessage() + "Error when rollbacking commit", ex.getSQLState(), ex.getErrorCode());
            }
            // throw again the exception for external management
            throw external;
        } finally {
            if (ps != null)
                ps.close();
            if (rs != null)
                rs.close();
            con.setAutoCommit(true);
            con.close();
        }
        return credentials;
    }

    /**
     * Creates a String token from the user ID, date and some randomness.
     *
     * @param id The user ID.
     * @return A SHA256-encoded String token.
     */
    private String getToken(long id) {
        /*
         * Tokens are generated by hashing some information with sha256:
         * - secure random long number
         * - user ID
         * - generation time
         */

        SecureRandom rnd = new SecureRandom();
        String newToken = Long.toString(rnd.nextLong()) +
                Long.toString(id) +
                Long.toString(System.currentTimeMillis());
        return Tools.sha256(newToken);
    }

    /**
     * Creates an expire date for tokens.
     *
     * @param longDuration Whether the user has select to remember him/her.
     * @return The expire date.
     */
    private Date getDate(boolean longDuration) {
        // "Persistent" tokens will expire in a week, standard ones in 1 day
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_MONTH, (longDuration ? 7 : 1));
        return new Date(cal.getTimeInMillis());
    }

    /**
     * The logout procedure.
     *
     * @return Whether the procedure was successfully completed.
     * @throws SQLException If error occurs during operations.
     */
    public boolean doLogout() throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean done = false; // output object

        try {
            con.setAutoCommit(false);

            // remove token from table
            ps = con.prepareStatement(STATEMENT_TOKEN_DELETE_LOGOUT);
            ps.setString(1, token);

            rs = ps.executeQuery();
            con.commit();
            if (rs.next()) {
                String delToken = rs.getString("token");
                done = delToken.equals(token);
            }
            con.commit();
        } catch (SQLException exception) {
            SQLException external = exception;
            try {
                con.rollback();
            } catch (SQLException ex) {
                //this should never be called
                external = new SQLException(exception.getMessage() + "Error when rollbacking commit", ex.getSQLState(), ex.getErrorCode());
            }
            // throw again the exception for external management
            throw external;
        } finally {
            if (ps != null)
                ps.close();
            if (rs != null)
                rs.close();
            con.setAutoCommit(true);
            con.close();
        }
        return done;
    }

    /**
     * The token validity checking procedure.
     *
     * @return The User ID associated with the token, or {@code INVALID_TOKEN}.
     * @throws SQLException If error occurs during operations.
     */
    public long checkTokenGetUser() throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        long authId = INVALID_TOKEN;

        try {
            // get token data, if present
            ps = con.prepareStatement(STATEMENT_TOKEN_CHECK);
            ps.setString(1, token);

            rs = ps.executeQuery();
            if (rs.next())
                authId = rs.getLong("userId");
        } finally {
            if (ps != null)
                ps.close();
            if (rs != null)
                rs.close();
        }
        return authId;
    }

    /**
     * Checks if an user is an admin authorized to manage a Resource (of type {@link Submission}, {@link ForumQuestion} or {@link Comment}).
     *
     * @param authId User ID to test.
     * @throws SQLException            If error occurs during operations.
     * @throws AuthenticationException If the user is not authorized to continue.
     */
    public void isAuthForResource(final long authId) throws SQLException, AuthenticationException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = con.prepareStatement(STATEMENT_ADMIN_CHECK);
            ps.setLong(1, authId);

            rs = ps.executeQuery();
            if (rs.next()) {
                rs.close();
                ps.close();
                return; // admin detected
            }
            // execution stops above if the user is an admin
            con.close();
            throw new AuthenticationException("Unauthorized operations on the resource");
        } finally {
            if (ps != null)
                ps.close();
            if (rs != null)
                rs.close();
            // close connection only if unauthorized
        }
    }

    /**
     * Checks if an user is authorized to manage a Resource (of type {@link User}, {@link Submission}, {@link ForumQuestion} or {@link Comment}).
     * Authorized means it's an admin or the author of the resource, except for User type, where only the author/itself is allowed, for privacy reasons.
     *
     * @param authId       User ID to test.
     * @param resourceId   Resource ID.
     * @param resourceType Resource type.
     * @throws SQLException            If error occurs during operations.
     * @throws AuthenticationException If the user is not authorized to continue.
     */
    public void isAuthForResource(final long authId, final long resourceId, final ResourceSelector resourceType) throws SQLException, AuthenticationException {
        PreparedStatement ps = null;
        ResultSet rs = null;

        if (resourceType == ResourceSelector.USER) {
            // admin cannot change other users info
            // only check if it's the same user that changes its info
            if (authId != resourceId)
                throw new AuthenticationException("Unauthorized operations on the resource");
        } else {
            // admin position check
            try {
                ps = con.prepareStatement(STATEMENT_ADMIN_CHECK);
                ps.setLong(1, authId);
                rs = ps.executeQuery();
                if (rs.next()) {
                    rs.close();
                    ps.close();
                    return; // admin detected
                }

                // author of resource check
                switch (resourceType) {
                    case COMMENT:
                        ps = con.prepareStatement(STATEMENT_AUTH_CHECK_COMMENT);
                        break;
                    case FORUM_QUESTION:
                        ps = con.prepareStatement(STATEMENT_AUTH_CHECK_QUESTION);
                        break;
                    case SUBMISSION:
                        ps = con.prepareStatement(STATEMENT_AUTH_CHECK_SUBMISSION);
                        break;
                }
                ps.setLong(1, resourceId);
                ps.setLong(2, authId);

                rs = ps.executeQuery();
                if (!rs.next()) {
                    // empty set: unauthorized user. Close the connection and throw the exception.
                    con.close();
                    throw new AuthenticationException("Unauthorized operations on the resource");
                }
            } finally {
                if (ps != null)
                    ps.close();
                if (rs != null)
                    rs.close();
            }
        }
    }
}
