// Copyright 2018 The StackOverflowers
package org.tso.heapquestions.resource;

/**
 * Represents subscription {@link PricingPlan} levels.
 */
public enum PlanLevel {
    Free("Free"),
    Advanced("Advanced"),
    Pro("Pro"),
    Annual("Annual"),
    Lifetime("Lifetime");

    private final String text;

    PlanLevel(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public static PlanLevel fromString(String text) throws IllegalArgumentException {
        for (PlanLevel l : PlanLevel.values())
            if (l.text.equalsIgnoreCase(text))
                return l;
        throw new IllegalArgumentException("Plan not valid! Passed: " + text);
    }

    @Override
    public String toString() {
        return text;
    }
}
