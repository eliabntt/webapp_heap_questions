// Copyright 2018 The StackOverflowers
package org.tso.heapquestions.resource;

/**
 * Distinguishes {@link Comment}s' parent and allows for specific query selection in the REST interface.
 */
public enum ResourceSelector {
    COMMENT, USER, FORUM_QUESTION, SUBMISSION
}
