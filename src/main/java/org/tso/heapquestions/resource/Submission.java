// Copyright 2018 The StackOverflowers
package org.tso.heapquestions.resource;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Timestamp;

/**
 * A submission of an interview question to the portal.
 */
public class Submission extends Resource {
    private final long id;
    private final long user;
    private final String title;
    private final Timestamp dateTime;
    private final Difficulties difficulty;
    private final ItemState state;
    private final String content;
    private final String solution;
    private final String category;
    private final PlanLevel minPlanToVis;

    /**
     * @param id           Identification number.
     * @param user         The creator ({@link User}) ID.
     * @param title        Title.
     * @param dateTime     Timestamp.
     * @param difficulty   Difficulty of the answer.
     * @param state        State of the submission.
     * @param content      Content of the question.
     * @param solution     Provided solution to the question.
     * @param category     {@link Category} (name) the question belongs to.
     * @param minPlanToVis Minimum {@link PricingPlan} level needed to visualize the submission.
     */
    public Submission(final long id, final long user, final String title, final Timestamp dateTime,
                      final Difficulties difficulty, final ItemState state, final String content,
                      final String solution, final String category, final PlanLevel minPlanToVis) {
        this.id = id;
        this.user = user;
        this.title = title;
        this.dateTime = dateTime;
        this.difficulty = difficulty;
        this.state = state;
        this.content = content;
        this.solution = solution;
        this.category = category;
        this.minPlanToVis = minPlanToVis;
    }

    /**
     * Gets the ID.
     *
     * @return The ID.
     */
    public final long getId() {
        return id;
    }

    /**
     * Gets the user ID.
     *
     * @return The user ID.
     */
    public final long getUser() {
        return user;
    }

    /**
     * Gets the title.
     *
     * @return The title.
     */
    public final String getTitle() {
        return title;
    }

    /**
     * Gets the timestamp.
     *
     * @return The timestamp.
     */
    public final Timestamp getDateTime() {
        return dateTime;
    }

    /**
     * Gets the difficulty.
     *
     * @return The difficulty.
     */
    public final Difficulties getDifficulty() {
        return difficulty;
    }

    /**
     * Gets the state.
     *
     * @return The state.
     */
    public final ItemState getState() {
        return state;
    }

    /**
     * Gets the content of the question.
     *
     * @return The content.
     */
    public final String getContent() {
        return content;
    }

    /**
     * Gets the solution.
     *
     * @return The solution.
     */
    public final String getSolution() {
        return solution;
    }

    /**
     * Gets the category name.
     *
     * @return The category name.
     */
    public final String getCategory() {
        return category;
    }

    /**
     * Gets the minimum plan (type) to visualize.
     *
     * @return The plan type.
     */
    public final PlanLevel getMinPlanToVis() {
        return minPlanToVis;
    }

    /**
     * A JSON representation of the object.
     *
     * @param out The stream to which the JSON representation has to be written.
     * @throws IOException If a JSON object cannot be generated.
     */
    @Override
    public final void toJSON(OutputStream out) throws IOException {
        final JsonGenerator gen = JSON_FACTORY.createGenerator(out);

        gen.writeStartObject();
        gen.writeFieldName("submission");
        gen.writeStartObject();

        gen.writeNumberField("id", id);
        gen.writeNumberField("userId", user);
        gen.writeStringField("title", title);
        gen.writeStringField("dateTime", dateTime.toString());
        gen.writeStringField("difficulty", difficulty.toString());
        gen.writeStringField("state", state.toString());
        gen.writeStringField("content", content);
        gen.writeStringField("solution", solution);
        gen.writeStringField("category", category);
        gen.writeStringField("minPlanToVis", minPlanToVis.toString());

        gen.writeEndObject();
        gen.writeEndObject();
        gen.flush();
    }

    /**
     * Creates a submission from its JSON representation.
     *
     * @param in The input stream containing the JSON document.
     * @return The created submission.
     * @throws IOException If an error occurs while parsing.
     */
    public static Submission fromJSON(final InputStream in) throws IOException {
        long id = -1, user = -1;
        Timestamp dateTime = null;
        String title = null, content = null, solution = null, category = null;
        Difficulties difficulty = null;
        ItemState state = null;
        PlanLevel minPlanToVis = null;

        final JsonParser jp = JSON_FACTORY.createParser(in);

        // advance to the next element (if any) while we are not on the start
        // of an element or the element is not a token element
        while (jp.getCurrentToken() != JsonToken.FIELD_NAME || !("submission".equals(jp.getCurrentName())))
            if (jp.nextToken() == null)
                throw new IOException("Unable to parse JSON, no submission object found.");

        while (jp.nextToken() != JsonToken.END_OBJECT)
            if (jp.getCurrentToken() == JsonToken.FIELD_NAME)
                switch (jp.getCurrentName()) {
                    case "id":
                        jp.nextToken();
                        id = jp.getLongValue();
                        break;
                    case "userId":
                        jp.nextToken();
                        user = jp.getLongValue();
                        break;
                    case "title":
                        jp.nextToken();
                        title = jp.getText();
                        break;
                    case "dateTime":
                        jp.nextToken();
                        dateTime = Timestamp.valueOf(jp.getText());
                        break;
                    case "difficulty":
                        jp.nextToken();
                        difficulty = Difficulties.fromString(jp.getText());
                        break;
                    case "state":
                        jp.nextToken();
                        state = ItemState.fromString(jp.getText());
                        break;
                    case "content":
                        jp.nextToken();
                        content = jp.getText();
                        break;
                    case "solution":
                        jp.nextToken();
                        solution = jp.getText();
                        break;
                    case "category":
                        jp.nextToken();
                        category = jp.getText();
                        break;
                    case "minPlanToVis":
                        jp.nextToken();
                        minPlanToVis = PlanLevel.fromString(jp.getText());
                        break;
                }
        return new Submission(id, user, title, dateTime, difficulty, state, content, solution, category, minPlanToVis);
    }
}