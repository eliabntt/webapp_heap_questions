// Copyright 2018 The StackOverflowers
package org.tso.heapquestions.resource;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Timestamp;

/**
 * Represents a private chat message between two {@link User}.
 */
public class PrivateChatMessage extends Resource {
    private final long id;
    private final long sender;
    private final long receiver;
    private final Timestamp dateTime;
    private final String content;

    /**
     * @param id       Identification number.
     * @param sender   The sender {@link User} ID.
     * @param receiver The receiver {@link User} ID.
     * @param dateTime Timestamp.
     * @param content  Content of the message.
     */
    public PrivateChatMessage(final long id, final long sender, final long receiver, final Timestamp dateTime, final String content) {
        this.id = id;
        this.sender = sender;
        this.receiver = receiver;
        this.dateTime = dateTime;
        this.content = content;
    }

    /**
     * Gets the ID.
     *
     * @return The ID.
     */
    public final long getId() {
        return id;
    }

    /**
     * Gets the sender user ID.
     *
     * @return The sender ID.
     */
    public final long getSender() {
        return sender;
    }

    /**
     * Gets the receiver user ID.
     *
     * @return The receiver ID.
     */
    public final long getReceiver() {
        return receiver;
    }

    /**
     * Gets the timestamp.
     *
     * @return The timestamp.
     */
    public final Timestamp getDateTime() {
        return dateTime;
    }

    /**
     * Gets the content.
     *
     * @return The content.
     */
    public final String getContent() {
        return content;
    }

    /**
     * A JSON representation of the object.
     *
     * @param out The stream to which the JSON representation has to be written.
     * @throws IOException If a JSON object cannot be generated.
     */
    @Override
    public final void toJSON(OutputStream out) throws IOException {
        final JsonGenerator gen = JSON_FACTORY.createGenerator(out);

        gen.writeStartObject();
        gen.writeFieldName("privateChat");
        gen.writeStartObject();

        gen.writeNumberField("id", id);
        gen.writeNumberField("senderId", sender);
        gen.writeNumberField("receiverId", receiver);
        gen.writeStringField("dateTime", dateTime.toString());
        gen.writeStringField("content", content);

        gen.writeEndObject();
        gen.writeEndObject();
        gen.flush();
    }

    /**
     * Creates a private chat message from its JSON representation.
     *
     * @param in The input stream containing the JSON document.
     * @return The created chat message.
     * @throws IOException If an error occurs while parsing.
     */
    public static PrivateChatMessage fromJSON(InputStream in) throws IOException {
        long messageId = -1;
        long senderId = -1;
        long receiverId = -1;
        Timestamp dateTime = null;
        String content = null;

        final JsonParser jp = JSON_FACTORY.createParser(in);

        // advance to the next element (if any) while we are not on the start
        // of an element or the element is not a token element
        while (jp.getCurrentToken() != JsonToken.FIELD_NAME || !("privateChat".equals(jp.getCurrentName())))
            if (jp.nextToken() == null)
                throw new IOException("Unable to parse JSON: no company object found.");

        while (jp.nextToken() != JsonToken.END_OBJECT)
            if (jp.getCurrentToken() == JsonToken.FIELD_NAME)
                switch (jp.getCurrentName()) {
                    case "id":
                        jp.nextToken();
                        messageId = jp.getLongValue();
                        break;
                    case "senderId":
                        jp.nextToken();
                        senderId = jp.getLongValue();
                        break;
                    case "receiverId":
                        jp.nextToken();
                        receiverId = jp.getLongValue();
                        break;
                    case "dateTime":
                        jp.nextToken();
                        dateTime = Timestamp.valueOf(jp.getText());
                        break;
                    case "content":
                        jp.nextToken();
                        content = jp.getText();
                        break;
                }
        return new PrivateChatMessage(messageId, senderId, receiverId, dateTime, content);
    }
}
