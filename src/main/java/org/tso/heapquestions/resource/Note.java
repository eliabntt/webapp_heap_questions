// Copyright 2018 The StackOverflowers
package org.tso.heapquestions.resource;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Timestamp;

/**
 * Represents a note, similar to a Gist.
 */
public class Note extends Resource {
    private final Long id;
    private final Long userId;
    private final Timestamp dateTime;
    private final String content;

    /**
     * @param id       Identification number.
     * @param user     The creator ({@link User}) ID.
     * @param dateTime Timestamp.
     * @param content  Content of the note.
     */
    public Note(final Long id, final Long user, final Timestamp dateTime, final String content) {
        this.id = id;
        this.userId = user;
        this.dateTime = dateTime;
        this.content = content;
    }

    /**
     * Gets the ID.
     *
     * @return The ID.
     */
    public final Long getId() {
        return id;
    }

    /**
     * Gets the user ID.
     *
     * @return The user ID.
     */
    public final Long getUserId() {
        return userId;
    }

    /**
     * Gets the timestamp.
     *
     * @return The timestamp.
     */
    public final Timestamp getDateTime() {
        return dateTime;
    }

    /**
     * Gets the content.
     *
     * @return The content.
     */
    public final String getContent() {
        return content;
    }

    /**
     * A JSON representation of the object.
     *
     * @param out The stream to which the JSON representation has to be written.
     * @throws IOException If a JSON object cannot be generated.
     */
    @Override
    public final void toJSON(OutputStream out) throws IOException {
        final JsonGenerator gen = JSON_FACTORY.createGenerator(out);

        gen.writeStartObject();
        gen.writeFieldName("note");
        gen.writeStartObject();

        gen.writeNumberField("id", id);
        gen.writeNumberField("userId", userId);
        gen.writeStringField("dateTime", dateTime.toString());
        gen.writeStringField("content", content);

        gen.writeEndObject();
        gen.writeEndObject();
        gen.flush();
    }

    /**
     * Creates a note from its JSON representation.
     *
     * @param in The input stream containing the JSON document.
     * @return The created note.
     * @throws IOException If an error occurs while parsing.
     */
    public static Note fromJSON(final InputStream in) throws IOException {
        Long id = null;
        Long user = null;
        Timestamp dateTime = null;
        String content = null;

        final JsonParser jp = JSON_FACTORY.createParser(in);

        // advance to the next element (if any) while we are not on the start
        // of an element or the element is not a token element
        while (jp.getCurrentToken() != JsonToken.FIELD_NAME || !("note".equals(jp.getCurrentName())))
            if (jp.nextToken() == null)
                throw new IOException("Unable to parse JSON, no note object found.");

        while (jp.nextToken() != JsonToken.END_OBJECT)
            if (jp.getCurrentToken() == JsonToken.FIELD_NAME)
                switch (jp.getCurrentName()) {
                    case "id":
                        jp.nextToken();
                        id = jp.getLongValue();
                        break;
                    case "userId":
                        jp.nextToken();
                        user = jp.getLongValue();
                        break;
                    case "dateTime":
                        jp.nextToken();
                        dateTime = Timestamp.valueOf(jp.getText());
                        break;
                    case "content":
                        jp.nextToken();
                        content = jp.getText();
                        break;
                }
        return new Note(id, user, dateTime, content);
    }
}
