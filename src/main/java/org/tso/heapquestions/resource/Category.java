// Copyright 2018 The StackOverflowers
package org.tso.heapquestions.resource;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Represents a category for posts ({@link ForumQuestion} and/or {@link Submission}).
 */
public class Category extends Resource {
    private final String name;
    private final String description;

    /**
     * @param name        Category's name.
     * @param description A brief description.
     */
    public Category(final String name, final String description) {
        this.name = name;
        this.description = description;
    }

    /**
     * Gets the category's name.
     *
     * @return The name.
     */
    public final String getName() {
        return name;
    }

    /**
     * Gets the category's description.
     *
     * @return The description.
     */
    public final String getDescription() {
        return description;
    }

    /**
     * A JSON representation of the object.
     *
     * @param out The stream to which the JSON representation has to be written.
     * @throws IOException If a JSON object cannot be generated.
     */
    @Override
    public final void toJSON(OutputStream out) throws IOException {
        final JsonGenerator gen = JSON_FACTORY.createGenerator(out);

        gen.writeStartObject();
        gen.writeFieldName("category");
        gen.writeStartObject();

        gen.writeStringField("name", name);
        gen.writeStringField("description", description);

        gen.writeEndObject();
        gen.writeEndObject();
        gen.flush();
    }

    /**
     * Creates a category from its JSON representation.
     *
     * @param in The input stream containing the JSON document.
     * @return The created category.
     * @throws IOException If an error occurs while parsing.
     */
    public static Category fromJSON(final InputStream in) throws IOException {
        String name = null;
        String description = null;

        final JsonParser jp = JSON_FACTORY.createParser(in);

        // advance to the next element (if any) while we are not on the start
        // of an element or the element is not a token element
        while (jp.getCurrentToken() != JsonToken.FIELD_NAME || !("category".equals(jp.getCurrentName())))
            if (jp.nextToken() == null)
                throw new IOException("Unable to parse JSON, no category object found.");

        while (jp.nextToken() != JsonToken.END_OBJECT)
            if (jp.getCurrentToken() == JsonToken.FIELD_NAME)
                switch (jp.getCurrentName()) {
                    case "name":
                        jp.nextToken();
                        name = jp.getText();
                        break;
                    case "description":
                        jp.nextToken();
                        description = jp.getText();
                        break;
                }
        return new Category(name, description);
    }
}
