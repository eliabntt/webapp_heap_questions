// Copyright 2018 The StackOverflowers
package org.tso.heapquestions.resource;

/**
 * Represents scholar/academical qualification types for {@link Qualification}.
 */
public enum QualificationType {
    HighSchool("High School"),
    BachelorDegree("Bachelors Degree"),
    MasterDegree("Masters Degree"),
    PhD("PhD"),
    Other("Other");

    private final String text;

    QualificationType(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public static QualificationType fromString(String text) throws IllegalArgumentException {
        for (QualificationType q : QualificationType.values())
            if (q.text.equalsIgnoreCase(text))
                return q;
        throw new IllegalArgumentException("Qualification Type not valid! Passed: " + text);
    }

    @Override
    public String toString() {
        return text;
    }
}
