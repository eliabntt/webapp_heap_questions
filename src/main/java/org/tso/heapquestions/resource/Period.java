// Copyright 2018 The StackOverflowers
package org.tso.heapquestions.resource;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Date;

/**
 * Represents a time period.
 */
public class Period extends Resource {
    private final long id;
    private final Date startDate;
    private final Date endDate;

    /**
     * @param id    Identification number.
     * @param start Start date.
     * @param end   End date.
     */
    public Period(final long id, final Date start, final Date end) {
        this.id = id;
        this.startDate = start;
        this.endDate = end;
    }

    /**
     * Gets the ID.
     *
     * @return The ID.
     */
    public final long getId() {
        return id;
    }

    /**
     * Gets the start date.
     *
     * @return The start date.
     */
    public final Date getStartDate() {
        return startDate;
    }

    /**
     * Gets the end date.
     *
     * @return The end date.
     */
    public final Date getEndDate() {
        return endDate;
    }

    /**
     * A JSON representation of the object.
     *
     * @param out The stream to which the JSON representation has to be written.
     * @throws IOException If a JSON object cannot be generated.
     */
    @Override
    public final void toJSON(OutputStream out) throws IOException {
        final JsonGenerator gen = JSON_FACTORY.createGenerator(out);

        gen.writeStartObject();
        gen.writeFieldName("period");
        gen.writeStartObject();

        gen.writeNumberField("id", id);
        gen.writeStringField("startDate", startDate.toString());
        gen.writeStringField("endDate", endDate.toString());

        gen.writeEndObject();
        gen.writeEndObject();
        gen.flush();
    }

    /**
     * Creates a period from its JSON representation.
     *
     * @param in The input stream containing the JSON document.
     * @return The created period.
     * @throws IOException If an error occurs while parsing.
     */
    public static Period fromJSON(final InputStream in) throws IOException {
        long id = -1;
        Date startDate = null;
        Date endDate = null;

        final JsonParser jp = JSON_FACTORY.createParser(in);

        // advance to the next element (if any) while we are not on the start
        // of an element or the element is not a token element
        while (jp.getCurrentToken() != JsonToken.FIELD_NAME || !("period".equals(jp.getCurrentName())))
            if (jp.nextToken() == null)
                throw new IOException("Unable to parse JSON, no period object found.");

        while (jp.nextToken() != JsonToken.END_OBJECT)
            if (jp.getCurrentToken() == JsonToken.FIELD_NAME)
                switch (jp.getCurrentName()) {
                    case "id":
                        jp.nextToken();
                        id = jp.getLongValue();
                        break;
                    case "startDate":
                        jp.nextToken();
                        startDate = Date.valueOf(jp.getText());
                        break;
                    case "endDate":
                        jp.nextToken();
                        endDate = Date.valueOf(jp.getText());
                        break;
                }
        return new Period(id, startDate, endDate);
    }
}
