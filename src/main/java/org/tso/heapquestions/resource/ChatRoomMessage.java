// Copyright 2018 The StackOverflowers
package org.tso.heapquestions.resource;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Timestamp;

/**
 * Represents a {@link Category}-related chat room.
 */
public class ChatRoomMessage extends Resource {
    private final long id;
    private final long sender;
    private final String category;
    private final Timestamp dateTime;
    private final String content;

    /**
     * @param id       Identification number.
     * @param sender   Sender {@link User} ID of the message.
     * @param category Specific category name.
     * @param dateTime Timestamp.
     * @param content  Content of the message.
     */
    public ChatRoomMessage(final long id, final long sender, final String category, final Timestamp dateTime, final String content) {
        this.id = id;
        this.sender = sender;
        this.category = category;
        this.dateTime = dateTime;
        this.content = content;
    }

    /**
     * Gets the ID.
     *
     * @return The ID.
     */
    public final long getId() {
        return id;
    }

    /**
     * Gets the sender ID.
     *
     * @return The sender ID.
     */
    public final long getSender() {
        return sender;
    }

    /**
     * Gets the category name.
     *
     * @return The category name.
     */
    public final String getCategory() {
        return category;
    }

    /**
     * Gets the timestamp.
     *
     * @return The timestamp.
     */
    public final Timestamp getDateTime() {
        return dateTime;
    }

    /**
     * Gets the message's content.
     *
     * @return The content.
     */
    public final String getContent() {
        return content;
    }

    /**
     * A JSON representation of the object.
     *
     * @param out The stream to which the JSON representation has to be written.
     * @throws IOException If a JSON object cannot be generated.
     */
    @Override
    public final void toJSON(OutputStream out) throws IOException {
        final JsonGenerator gen = JSON_FACTORY.createGenerator(out);

        gen.writeStartObject();
        gen.writeFieldName("chatRoom");
        gen.writeStartObject();

        gen.writeNumberField("id", id);
        gen.writeNumberField("senderId", sender);
        gen.writeStringField("category", category);
        gen.writeStringField("dateTime", dateTime.toString());
        gen.writeStringField("content", content);

        gen.writeEndObject();
        gen.writeEndObject();
        gen.flush();
    }

    /**
     * Creates a chat room message from its JSON representation.
     *
     * @param in The input stream containing the JSON document.
     * @return The created chatroom message.
     * @throws IOException If an error occurs while parsing.
     */
    public static ChatRoomMessage fromJSON(final InputStream in) throws IOException {
        long id = -1;
        long sender = -1;
        String category = null;
        Timestamp dateTime = null;
        String content = null;

        final JsonParser jp = JSON_FACTORY.createParser(in);

        // advance to the next element (if any) while we are not on the start
        // of an element or the element is not a token element
        while (jp.getCurrentToken() != JsonToken.FIELD_NAME || !("chatRoom".equals(jp.getCurrentName())))
            if (jp.nextToken() == null)
                throw new IOException("Unable to parse JSON, no chat room message object found.");

        while (jp.nextToken() != JsonToken.END_OBJECT)
            if (jp.getCurrentToken() == JsonToken.FIELD_NAME)
                switch (jp.getCurrentName()) {
                    case "id":
                        jp.nextToken();
                        id = jp.getLongValue();
                        break;
                    case "senderId":
                        jp.nextToken();
                        sender = jp.getLongValue();
                        break;
                    case "category":
                        jp.nextToken();
                        category = jp.getText();
                        break;
                    case "dateTime":
                        jp.nextToken();
                        dateTime = Timestamp.valueOf(jp.getText());
                        break;
                    case "content":
                        jp.nextToken();
                        content = jp.getText();
                        break;
                }
        return new ChatRoomMessage(id, sender, category, dateTime, content);
    }
}
