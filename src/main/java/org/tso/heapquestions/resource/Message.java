// Copyright 2018 The StackOverflowers
package org.tso.heapquestions.resource;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Represents a message or an error message.
 */
public class Message extends Resource {

    /**
     * The message.
     */
    private final String message;

    /**
     * The code of the error, if any.
     */
    private final String errorCode;

    /**
     * Additional details about the error, if any.
     */
    private final String errorDetails;

    /**
     * Indicates whether the message is about an error or not.
     */
    private final boolean isError;


    /**
     * Creates an error message.
     *
     * @param message      The message.
     * @param errorCode    The code of the error.
     * @param errorDetails Additional details about the error.
     */
    public Message(final String message, final String errorCode, final String errorDetails) {
        this.message = message;
        this.errorCode = errorCode;
        this.errorDetails = errorDetails;
        this.isError = true;
    }


    /**
     * Creates a generic message.
     *
     * @param message The message.
     */
    public Message(final String message) {
        this.message = message;
        this.errorCode = null;
        this.errorDetails = null;
        this.isError = false;
    }


    /**
     * Returns the message.
     *
     * @return The message.
     */
    public final String getMessage() {
        return message;
    }

    /**
     * Returns the code of the error, if any.
     *
     * @return The code of the error, if any, {@code null} otherwise.
     */
    public final String getErrorCode() {
        return errorCode;
    }

    /**
     * Returns additional details about the error, if any.
     *
     * @return Additional details about the error, if any, {@code null} otherwise.
     */
    public final String getErrorDetails() {
        return errorDetails;
    }

    /**
     * Indicates whether the message is about an error or not.
     *
     * @return {@code true} if the message is about an error, {@code false} otherwise.
     */
    public final boolean isError() {
        return isError;
    }

    /**
     * A JSON representation of the object.
     *
     * @param out The stream to which the JSON representation has to be written.
     * @throws IOException If a JSON object cannot be generated.
     */
    @Override
    public final void toJSON(final OutputStream out) throws IOException {

        final JsonGenerator jg = JSON_FACTORY.createGenerator(out);

        jg.writeStartObject();
        jg.writeFieldName("message");
        jg.writeStartObject();

        jg.writeStringField("message", message);
        if (errorCode != null)
            jg.writeStringField("errorCode", errorCode);
        if (errorDetails != null)
            jg.writeStringField("errorDetails", errorDetails);

        jg.writeEndObject();
        jg.writeEndObject();
        jg.flush();
    }

    /**
     * Creates a message from its JSON representation.
     *
     * @param in The input stream containing the JSON document.
     * @return The created message.
     * @throws IOException If an error occurs while parsing.
     */
    public static Message fromJSON(final InputStream in) throws IOException {
        String message = null;
        String errorCode = null;
        String errorDetails = null;

        final JsonParser jp = JSON_FACTORY.createParser(in);

        // advance to the next element (if any) while we are not on the start
        // of an element or the element is not a token element
        while (jp.getCurrentToken() != JsonToken.FIELD_NAME || !("message".equals(jp.getCurrentName())))
            if (jp.nextToken() == null)
                throw new IOException("Unable to parse JSON, no message object found.");

        while (jp.nextToken() != JsonToken.END_OBJECT)
            if (jp.getCurrentToken() == JsonToken.FIELD_NAME)
                switch (jp.getCurrentName()) {
                    case "message":
                        jp.nextToken();
                        message = jp.getText();
                        break;
                    case "errorCode":
                        jp.nextToken();
                        errorCode = jp.getText();
                        break;
                    case "errorDetails":
                        jp.nextToken();
                        errorDetails = jp.getText();
                        break;
                }
        return new Message(message, errorCode, errorDetails);
    }
}
