// Copyright 2018 The StackOverflowers
package org.tso.heapquestions.resource;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Social network account (links) for an {@link User}.
 */
public class SocialNetwork extends Resource {
    private final String name;
    private final String website;
    private final byte[] logo;

    /**
     * @param name    Name of the site.
     * @param website The main website.
     * @param logo    The logo.
     */
    public SocialNetwork(final String name, final String website, final byte[] logo) {
        this.name = name;
        this.website = website;
        this.logo = logo;
    }

    /**
     * Gets the name.
     *
     * @return The name.
     */
    public final String getName() {
        return name;
    }

    /**
     * Gets the website.
     *
     * @return The website.
     */
    public final String getWebsite() {
        return website;
    }

    /**
     * Gets the logo.
     *
     * @return The logo.
     */
    public final byte[] getLogo() {
        return logo;
    }

    /**
     * A JSON representation of the object.
     *
     * @param out The stream to which the JSON representation has to be written.
     * @throws IOException If a JSON object cannot be generated.
     */
    @Override
    public final void toJSON(OutputStream out) throws IOException {
        final JsonGenerator gen = JSON_FACTORY.createGenerator(out);

        gen.writeStartObject();
        gen.writeFieldName("socialNetwork");
        gen.writeStartObject();

        gen.writeStringField("name", name);
        gen.writeStringField("website", website);
        gen.writeStringField("logo", Tools.base64encode(logo));

        gen.writeEndObject();
        gen.writeEndObject();
        gen.flush();
    }

    /**
     * Creates a social network from its JSON representation.
     *
     * @param in The input stream containing the JSON document.
     * @return The created social network.
     * @throws IOException If an error occurs while parsing.
     */
    public static SocialNetwork fromJSON(final InputStream in) throws IOException {
        String name = null;
        String website = null;
        byte[] logo = null;

        final JsonParser jp = JSON_FACTORY.createParser(in);

        // advance to the next element (if any) while we are not on the start
        // of an element or the element is not a token element
        while (jp.getCurrentToken() != JsonToken.FIELD_NAME || !("socialNetwork".equals(jp.getCurrentName())))
            if (jp.nextToken() == null)
                throw new IOException("Unable to parse JSON, no social network object found.");

        while (jp.nextToken() != JsonToken.END_OBJECT)
            if (jp.getCurrentToken() == JsonToken.FIELD_NAME)
                switch (jp.getCurrentName()) {
                    case "name":
                        jp.nextToken();
                        name = jp.getText();
                        break;
                    case "website":
                        jp.nextToken();
                        website = jp.getText();
                        break;
                    case "logo":
                        jp.nextToken();
                        logo = jp.getBinaryValue();
                        break;
                }
        return new SocialNetwork(name, website, logo);
    }
}
