// Copyright 2018 The StackOverflowers
package org.tso.heapquestions.resource;

/**
 * Links {@link User} to its {@link PricingPlan} tracking subscription {@link Period}.
 */
public class UserPlanSubscription {
    private final long user;
    private final long period;
    private final int plan;
    private final boolean isActive;

    /**
     * @param user     Specific {@link User} ID.
     * @param period   Time {@link Period} ID.
     * @param plan     Subscribed {@link PricingPlan} ID.
     * @param isActive Whether the plan is active (the last subscribed).
     */
    public UserPlanSubscription(final long user, final long period, final int plan, final boolean isActive) {
        this.user = user;
        this.period = period;
        this.plan = plan;
        this.isActive = isActive;
    }

    /**
     * Gets the user ID.
     *
     * @return The user ID.
     */
    public final long getUser() {
        return user;
    }

    /**
     * Gets the time period ID.
     *
     * @return The period ID.
     */
    public final long getPeriod() {
        return period;
    }

    /**
     * Gets the pricing plan ID.
     *
     * @return The plan ID.
     */
    public final int getPlan() {
        return plan;
    }

    /**
     * Gets whether the subscribed plan is active (it's the last subscribed)
     *
     * @return Whether the plan is active or not.
     */
    public final boolean isActive() {
        return isActive;
    }
}
