// Copyright 2018 The StackOverflowers
package org.tso.heapquestions.resource;

/**
 * Links {@link Submission} to {@link Company}.
 */
public class SubmissionCompany {
    private final long submission;
    private final int company;

    /**
     * @param submission The {@link Submission} ID.
     * @param company    The {@link Company} ID.
     */
    public SubmissionCompany(final long submission, final int company) {
        this.submission = submission;
        this.company = company;
    }

    /**
     * Gets the submission ID.
     *
     * @return The submission ID.
     */
    public final long getSubmission() {
        return submission;
    }

    /**
     * Gets the company ID.
     *
     * @return The company ID.
     */
    public final int getCompany() {
        return company;
    }
}
