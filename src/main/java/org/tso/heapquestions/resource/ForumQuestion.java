// Copyright 2018 The StackOverflowers
package org.tso.heapquestions.resource;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Timestamp;

/**
 * Represents a question on the forum.
 */
public class ForumQuestion extends Resource {
    private final long id;
    private final long user;
    private final String category;
    private final Timestamp dateTime; // TIMESTAMPTZ postgres type
    private final Boolean closed;
    private final String title;
    private final String content;
    private final ItemState state;

    /**
     * @param id       Identification number.
     * @param user     Related {@link User} ID.
     * @param category Related {@link Category} name.
     * @param dateTime Timestamp.
     * @param closed   Whether it's considered as closed.
     * @param title    Question's title.
     * @param content  Question's content.
     * @param state    Question's state.
     */
    public ForumQuestion(final long id, final long user, final String category,
                         final Timestamp dateTime, final Boolean closed, final String title,
                         final String content, final ItemState state) {

        this.id = id;
        this.user = user;
        this.category = category;
        this.dateTime = dateTime;
        this.closed = closed;
        this.title = title;
        this.content = content;
        this.state = state;
    }

    /**
     * Gets the ID.
     *
     * @return The ID.
     */
    public final long getId() {
        return id;
    }

    /**
     * Gets the user ID.
     *
     * @return The user ID.
     */
    public final long getUser() {
        return user;
    }

    /**
     * Gets the category name.
     *
     * @return The category name.
     */
    public final String getCategory() {
        return category;
    }

    /**
     * Gets the timestamp.
     *
     * @return The timestamp.
     */
    public final Timestamp getDateTime() {
        return dateTime;
    }

    /**
     * Gets if the question is marked as closed.
     *
     * @return The closed-marking.
     */
    public final boolean isClosed() {
        return closed;
    }

    /**
     * Gets the title.
     *
     * @return The title.
     */
    public final String getTitle() {
        return title;
    }

    /**
     * Gets the content.
     *
     * @return The content.
     */
    public final String getContent() {
        return content;
    }

    /**
     * Gets the state.
     *
     * @return The state.
     */
    public final ItemState getState() {
        return state;
    }

    /**
     * A JSON representation of the object.
     *
     * @param out The stream to which the JSON representation has to be written.
     * @throws IOException If a JSON object cannot be generated.
     */
    @Override
    public final void toJSON(OutputStream out) throws IOException {
        final JsonGenerator gen = JSON_FACTORY.createGenerator(out);

        gen.writeStartObject();
        gen.writeFieldName("forumQuestion");
        gen.writeStartObject();

        gen.writeNumberField("id", id);
        gen.writeNumberField("userId", user);
        gen.writeStringField("category", category);
        gen.writeStringField("dateTime", dateTime.toString());
        gen.writeBooleanField("closed", closed);
        gen.writeStringField("title", title);
        gen.writeStringField("content", content);
        gen.writeStringField("state", state.toString());

        gen.writeEndObject();
        gen.writeEndObject();
        gen.flush();
    }

    /**
     * Creates a forum question from its JSON representation.
     *
     * @param in The input stream containing the JSON document.
     * @return The created question.
     * @throws IOException If an error occurs while parsing.
     */
    public static ForumQuestion fromJSON(final InputStream in) throws IOException {
        long id = -1, user = -1;
        Timestamp dateTime = null;
        String title = null, content = null, category = null;
        ItemState state = null;
        Boolean isClosed = null;

        final JsonParser jp = JSON_FACTORY.createParser(in);

        // advance to the next element (if any) while we are not on the start
        // of an element or the element is not a token element
        while (jp.getCurrentToken() != JsonToken.FIELD_NAME || !("forumQuestion".equals(jp.getCurrentName())))
            if (jp.nextToken() == null)
                throw new IOException("Unable to parse JSON, no forumQuestion object found.");

        while (jp.nextToken() != JsonToken.END_OBJECT)
            if (jp.getCurrentToken() == JsonToken.FIELD_NAME)
                switch (jp.getCurrentName()) {
                    case "id":
                        jp.nextToken();
                        id = jp.getLongValue();
                        break;
                    case "userId":
                        jp.nextToken();
                        user = jp.getLongValue();
                        break;
                    case "title":
                        jp.nextToken();
                        title = jp.getText();
                        break;
                    case "dateTime":
                        jp.nextToken();
                        dateTime = Timestamp.valueOf(jp.getText());
                        break;
                    case "closed":
                        jp.nextToken();
                        isClosed = jp.getValueAsBoolean();
                        break;
                    case "state":
                        jp.nextToken();
                        state = ItemState.fromString(jp.getText());
                        break;
                    case "content":
                        jp.nextToken();
                        content = jp.getText();
                        break;
                    case "category":
                        jp.nextToken();
                        category = jp.getText();
                        break;
                }
        return new ForumQuestion(id, user, category, dateTime, isClosed, title, content, state);
    }
}
