// Copyright 2018 The StackOverflowers
package org.tso.heapquestions.resource;

/**
 * Represents a {{@link ForumQuestion} | {@link Submission} | {@link Comment}} state.
 */
public enum ItemState {
    Visible("Visible"),
    Edited("Edited"),
    Moderated("Moderated"),
    Deleted("Deleted");

    private final String text;

    ItemState(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public static ItemState fromString(String text) throws IllegalArgumentException {
        for (ItemState i : ItemState.values())
            if (i.text.equalsIgnoreCase(text))
                return i;
        throw new IllegalArgumentException("Item State not valid! Passed: " + text);
    }

    @Override
    public String toString() {
        return text;
    }
}
