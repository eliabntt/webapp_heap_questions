// Copyright 2018 The StackOverflowers
package org.tso.heapquestions.resource;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Timestamp;

/**
 * Represents a comment for a {@link ForumQuestion} or a {@link Submission}.
 */
public class Comment extends Resource {
    private final long id;
    private final long userId;
    private final Long question;
    private final Long submission;
    private final Long parentComment;
    private final Timestamp dateTime;
    private final int downVote;
    private final int upVote;
    private final boolean notify;
    private final boolean isAnswer;
    private final String content;
    private final ItemState state;

    /**
     * @param id            Identification number.
     * @param userId        Writer ({@link User} ID) of the comment.
     * @param question      Related {@link ForumQuestion} ID, if any.
     * @param submission    Related {@link Submission} ID, if any.
     * @param parentComment Parent {@link Comment} ID, if any.
     * @param dateTime      Timestamp.
     * @param downVote      Number of downvotes.
     * @param upVote        NUmber of upvotes.
     * @param notify        Whether the user has to be notified of updates.
     * @param isAnswer      Comment is marked as answer.
     * @param content       Content of the comment.
     * @param state         State of the comment.
     */
    public Comment(final long id, final long userId, final Long question,
                   final Long submission, final Long parentComment, final Timestamp dateTime,
                   final int downVote, final int upVote, final boolean notify,
                   final boolean isAnswer, final String content, final ItemState state) {
        this.id = id;
        this.userId = userId;
        this.question = question;
        this.submission = submission;
        this.parentComment = parentComment;
        this.dateTime = dateTime;
        this.downVote = downVote;
        this.upVote = upVote;
        this.notify = notify;
        this.isAnswer = isAnswer;
        this.content = content;
        this.state = state;
    }

    /**
     * Gets the ID.
     *
     * @return The ID.
     */
    public final long getId() {
        return id;
    }

    /**
     * Gets the user ID.
     *
     * @return The user ID.
     */
    public final long getUser() {
        return userId;
    }

    /**
     * Gets the related forum question ID, if present.
     *
     * @return The question ID.
     */
    public final Long getQuestion() {
        return question;
    }

    /**
     * Gets the related submission ID, if present.
     *
     * @return The submission ID.
     */
    public final Long getSubmission() {
        return submission;
    }

    /**
     * Gets the parent comment ID, if present.
     *
     * @return The parent comment ID.
     */
    public final Long getParentComment() {
        return parentComment;
    }

    /**
     * Gets the timestamp.
     *
     * @return The timestamp.
     */
    public final Timestamp getDateTime() {
        return dateTime;
    }

    /**
     * Gets the number of downvotes.
     *
     * @return The number.
     */
    public final int getDownVote() {
        return downVote;
    }

    /**
     * Gets the number of upvotes.
     *
     * @return The number.
     */
    public final int getUpVote() {
        return upVote;
    }

    /**
     * Gets whether the user has to be notified of updates.
     *
     * @return The notification setting.
     */
    public final boolean isNotify() {
        return notify;
    }

    /**
     * Gets if comment is marked as answer.
     *
     * @return Whether the comment is marked as answer.
     */
    public final boolean isAnswer() {
        return isAnswer;
    }

    /**
     * Gets the comment's content.
     *
     * @return The content.
     */
    public final String getContent() {
        return content;
    }

    /**
     * Gets the comment's state.
     *
     * @return The state.
     */
    public final ItemState getState() {
        return state;
    }

    /**
     * A JSON representation of the object.
     *
     * @param out The stream to which the JSON representation has to be written.
     * @throws IOException If a JSON object cannot be generated.
     */
    @Override
    public final void toJSON(OutputStream out) throws IOException {
        final JsonGenerator gen = JSON_FACTORY.createGenerator(out);

        gen.writeStartObject();
        gen.writeFieldName("comment");
        gen.writeStartObject();

        gen.writeNumberField("id", id);
        gen.writeNumberField("userId", userId);
        gen.writeNumberField("question", question);
        gen.writeNumberField("submission", submission);
        gen.writeNumberField("parentComment", parentComment);
        gen.writeStringField("dateTime", dateTime.toString());
        gen.writeNumberField("downVote", downVote);
        gen.writeNumberField("upVote", upVote);
        gen.writeBooleanField("notify", notify);
        gen.writeBooleanField("isAnswer", isAnswer);
        gen.writeStringField("content", content);
        gen.writeStringField("state", state.toString());

        gen.writeEndObject();
        gen.writeEndObject();
        gen.flush();
    }

    /**
     * Creates a comment from its JSON representation.
     *
     * @param in The input stream containing the JSON document.
     * @return The created comment.
     * @throws IOException If an error occurs while parsing.
     */
    public static Comment fromJSON(final InputStream in) throws IOException {
        long id = -1, userId = -1;
        Long question = null, submission = null, parentComment = null;
        Timestamp dateTime = null;
        int downVote = -1, upVote = -1;
        boolean notify = false;
        boolean isAnswer = false;
        String content = null;
        ItemState state = null;

        final JsonParser jp = JSON_FACTORY.createParser(in);

        // advance to the next element (if any) while we are not on the start
        // of an element or the element is not a token element
        while (jp.getCurrentToken() != JsonToken.FIELD_NAME || !("comment".equals(jp.getCurrentName())))
            if (jp.nextToken() == null)
                throw new IOException("Unable to parse JSON, no comment object found.");

        while (jp.nextToken() != JsonToken.END_OBJECT)
            if (jp.getCurrentToken() == JsonToken.FIELD_NAME)
                switch (jp.getCurrentName()) {
                    case "id":
                        jp.nextToken();
                        id = jp.getLongValue();
                        break;
                    case "userId":
                        jp.nextToken();
                        userId = jp.getLongValue();
                        break;
                    case "question":
                        jp.nextToken();
                        question = jp.getLongValue();
                        break;
                    case "submission":
                        jp.nextToken();
                        submission = jp.getLongValue();
                        break;
                    case "parentComment":
                        jp.nextToken();
                        parentComment = jp.getLongValue();
                        break;
                    case "dateTime":
                        jp.nextToken();
                        dateTime = Timestamp.valueOf(jp.getText());
                        break;
                    case "downVote":
                        jp.nextToken();
                        downVote = jp.getIntValue();
                        break;
                    case "upVote":
                        jp.nextToken();
                        upVote = jp.getIntValue();
                        break;
                    case "notify":
                        jp.nextToken();
                        notify = jp.getValueAsBoolean();
                        break;
                    case "isAnswer":
                        jp.nextToken();
                        isAnswer = jp.getValueAsBoolean();
                        break;
                    case "content":
                        jp.nextToken();
                        content = jp.getText();
                        break;
                    case "state":
                        jp.nextToken();
                        state = ItemState.fromString(jp.getText());
                        break;
                }
        return new Comment(id, userId, question, submission, parentComment, dateTime, downVote, upVote, notify, isAnswer, content, state);
    }
}
