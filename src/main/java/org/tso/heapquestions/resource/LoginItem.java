// Copyright 2018 The StackOverflowers
package org.tso.heapquestions.resource;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Represents parameters for login purposes.
 */
public class LoginItem extends Resource {
    private final String userData;
    private final String password;
    private final boolean remember;

    /**
     * @param userData   User's username or email.
     * @param password   User's password.
     * @param rememberMe Whether to remember the login.
     */
    public LoginItem(final String userData, final String password, final boolean rememberMe) {
        this.userData = userData;
        this.password = password;
        this.remember = rememberMe;
    }

    /**
     * Gets the username or the email.
     *
     * @return The username or the email.
     */
    public final String getUserData() {
        return userData;
    }

    /**
     * Gets the password.
     *
     * @return The password.
     */
    public final String getPassword() {
        return password;
    }

    /**
     * Gets the remember flag.
     *
     * @return The remember flag.
     */
    public final boolean doRemember() {
        return remember;
    }

    /**
     * JSON representation of the object.
     *
     * @param out The stream to which the JSON representation has to be written to.
     * @throws IOException If a JSON object cannot be generated.
     */
    @Override
    public void toJSON(OutputStream out) throws IOException {
        final JsonGenerator gen = JSON_FACTORY.createGenerator(out);

        gen.writeStartObject();
        gen.writeFieldName("LoginItem");
        gen.writeStartObject();

        gen.writeStringField("userData", userData);
        gen.writeStringField("password", password);
        gen.writeBooleanField("remember", remember);

        gen.writeEndObject();
        gen.writeEndObject();
        gen.flush();
    }

    /**
     * Creates a login item from its JSON representation.
     *
     * @param in The input stream containing the JSON document.
     * @return The extracted login data.
     * @throws IOException If an error occurs while parsing.
     */
    public static LoginItem fromJSON(final InputStream in) throws IOException {
        String userdata = null;
        String password = null;
        boolean remember = false;

        final JsonParser jp = JSON_FACTORY.createParser(in);

        // advance to the next element (if any) while we are not on the start
        // of an element or the element is not a token element
        while (jp.getCurrentToken() != JsonToken.FIELD_NAME || !("LoginItem".equals(jp.getCurrentName())))
            if (jp.nextToken() == null)
                throw new IOException("Unable to parse JSON: no login item object found.");

        while (jp.nextToken() != JsonToken.END_OBJECT)
            if (jp.getCurrentToken() == JsonToken.FIELD_NAME)
                switch (jp.getCurrentName()) {
                    case "userData":
                        jp.nextToken();
                        userdata = jp.getText();
                        break;
                    case "password":
                        jp.nextToken();
                        password = jp.getText();
                        break;
                    case "remember":
                        jp.nextToken();
                        remember = jp.getBooleanValue();
                        break;
                }
        return new LoginItem(userdata, password, remember);
    }
}
