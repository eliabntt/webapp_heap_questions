// Copyright 2018 The StackOverflowers
package org.tso.heapquestions.resource;

/**
 * Links {@link User} to {@link ForumQuestion}.
 */
public class FavoriteQuestionList {
    private final long user;
    private final long question;
    private final boolean notify;

    /**
     * @param user     The {@link User} ID.
     * @param question The {@link ForumQuestion} ID.
     * @param notify   Whether to notify about updates.
     */
    public FavoriteQuestionList(final long user, final long question, final boolean notify) {
        this.user = user;
        this.question = question;
        this.notify = notify;
    }

    /**
     * Gets the user ID.
     *
     * @return The user ID.
     */
    public final long getUser() {
        return user;
    }

    /**
     * Gets the question ID.
     *
     * @return The question ID.
     */
    public final long getQuestion() {
        return question;
    }

    /**
     * Gets if the user wants to be notified about question's updates.
     *
     * @return The notification setting.
     */
    public final boolean doNotify() {
        return notify;
    }
}
