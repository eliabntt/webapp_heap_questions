// Copyright 2018 The StackOverflowers
package org.tso.heapquestions.resource;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Represents a company.
 */
public class Company extends Resource {
    private final int id;
    private final String name;
    private final String description;
    private final String website;
    private final byte[] logo;
    private final OperativeAreas operativeArea;

    /**
     * @param id          Identification number.
     * @param name        Name of the company.
     * @param description Brief description.
     * @param website     Company's website.
     * @param logo        Company's logo.
     * @param area        Operative area.
     */
    public Company(final int id, final String name, final String description,
                   final String website, final byte[] logo, final OperativeAreas area) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.website = website;
        this.logo = logo;
        this.operativeArea = area;
    }

    /**
     * Gets the ID.
     *
     * @return The ID.
     */
    public final int getId() {
        return id;
    }

    /**
     * Gets the name.
     *
     * @return The name.
     */
    public final String getName() {
        return name;
    }

    /**
     * Gets the description.
     *
     * @return The description.
     */
    public final String getDescription() {
        return description;
    }

    /**
     * Gets the website.
     *
     * @return Th website.
     */
    public final String getWebsite() {
        return website;
    }

    /**
     * Gets the logo.
     *
     * @return The logo.
     */
    public final byte[] getLogo() {
        return logo;
    }

    /**
     * Gets the operative area.
     *
     * @return The area.
     */
    public final OperativeAreas getOperativeArea() {
        return operativeArea;
    }

    /**
     * A JSON representation of the object.
     *
     * @param out The stream to which the JSON representation has to be written.
     * @throws IOException If a JSON object cannot be generated.
     */
    @Override
    public final void toJSON(OutputStream out) throws IOException {
        final JsonGenerator gen = JSON_FACTORY.createGenerator(out);

        gen.writeStartObject();
        gen.writeFieldName("company");
        gen.writeStartObject();
        gen.writeNumberField("id", id);
        gen.writeStringField("name", name);
        gen.writeStringField("description", description);
        gen.writeStringField("website", website);
        gen.writeStringField("logo", Tools.base64encode(logo));
        gen.writeStringField("operativeArea", operativeArea.toString());

        gen.writeEndObject();
        gen.writeEndObject();
        gen.flush();
        gen.close();
    }

    /**
     * Creates a company from its JSON representation.
     *
     * @param in The input stream containing the JSON document.
     * @return The created company.
     * @throws IOException If an error occurs while parsing.
     */

    public static Company fromJSON(InputStream in) throws IOException {
        int id = -1;
        String name = null;
        String description = null;
        String website = null;
        byte[] logo = null;
        OperativeAreas operativeArea = OperativeAreas.Unspecified;
        //so, if we don't pass the operative area it will be the default

        final JsonParser jp = JSON_FACTORY.createParser(in);

        // advance to the next element (if any) while we are not on the start
        // of an element or the element is not a token element
        while (jp.getCurrentToken() != JsonToken.FIELD_NAME || !("company".equals(jp.getCurrentName())))
            if (jp.nextToken() == null)
                throw new IOException("Unable to parse JSON: no company object found.");

        while (jp.nextToken() != JsonToken.END_OBJECT)
            if (jp.getCurrentToken() == JsonToken.FIELD_NAME)
                switch (jp.getCurrentName()) {
                    case "id":
                        jp.nextToken();
                        id = jp.getIntValue();
                        break;
                    case "name":
                        jp.nextToken();
                        name = jp.getText().toLowerCase();
                        break;
                    case "description":
                        jp.nextToken();
                        description = jp.getText();
                        break;
                    case "website":
                        jp.nextToken();
                        website = jp.getText();
                        break;
                    case "logo":
                        jp.nextToken();
                        logo = Tools.base64decode(jp.getText());
                        break;
                    case "operativeArea":
                        jp.nextToken();
                        operativeArea = OperativeAreas.fromString(jp.getText());
                        break;
                }

        return new Company(id, name, description,website, logo, operativeArea);



    }
}
