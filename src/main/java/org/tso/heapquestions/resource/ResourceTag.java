// Copyright 2018 The StackOverflowers
package org.tso.heapquestions.resource;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Links a {@link Submission} or a {@link ForumQuestion} (a Resource) to {@link Tag}.
 */
public class ResourceTag extends Resource {
    private final long resourceId;
    private final String tag;

    /**
     * @param resId The {@link Submission}/{@link ForumQuestion} ID.
     * @param tag   The {@link Tag} value.
     */
    public ResourceTag(final long resId, final String tag) {
        this.resourceId = resId;
        this.tag = tag;
    }

    /**
     * Gets the resourceId ID.
     *
     * @return The resourceId ID.
     */
    public final long getResourceId() {
        return resourceId;
    }

    /**
     * Gets the tag.
     *
     * @return The tag.
     */
    public final String getTag() {
        return tag;
    }

    /**
     * A JSON representation of the object.
     *
     * @param out The stream to which the JSON representation has to be written.
     * @throws IOException If a JSON object cannot be generated.
     */
    public final void toJSON(OutputStream out) throws IOException {
        final JsonGenerator gen = JSON_FACTORY.createGenerator(out);

        gen.writeStartObject();
        gen.writeFieldName("resourceTag");
        gen.writeStartObject();

        gen.writeNumberField("id", resourceId);
        gen.writeStringField("tag", tag);

        gen.writeEndObject();
        gen.writeEndObject();
        gen.flush();
    }

    /**
     * Creates an object from its JSON representation.
     *
     * @param in The input stream containing the JSON document.
     * @return The created resource-tag object.
     * @throws IOException If an error occurs while parsing.
     */
    public static ResourceTag fromJSON(final InputStream in) throws IOException {
        long id = -1;
        String tag = null;
        final JsonParser jp = JSON_FACTORY.createParser(in);

        // advance to the next element (if any) while we are not on the start
        // of an element or the element is not a token element
        while (jp.getCurrentToken() != JsonToken.FIELD_NAME || !("resourceTag".equals(jp.getCurrentName())))
            if (jp.nextToken() == null)
                throw new IOException("Unable to parse JSON, no object found.");

        while (jp.nextToken() != JsonToken.END_OBJECT)
            if (jp.getCurrentToken() == JsonToken.FIELD_NAME)
                switch (jp.getCurrentName()) {
                    case "id":
                        jp.nextToken();
                        id = jp.getLongValue();
                        break;
                    case "tag":
                        jp.nextToken();
                        tag = jp.getText();
                        break;
                }
        return new ResourceTag(id, tag);
    }
}
