// Copyright 2018 The StackOverflowers
package org.tso.heapquestions.resource;

import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;

/**
 * @deprecated DO NOT USE. Represents extended information for an {@link User}.
 */
public class UserExtra extends Resource {
    /**
     * Identification number.
     */
    private final User user;

    /**
     * First name.
     */
    private final String name;

    /**
     * Last name.
     */
    private final String surname;

    /**
     * Generic living place.
     */
    private final String location;

    /**
     * Date of birth.
     */
    private final Date birthDate;

    /**
     * Platform join date.
     */
    private final Date joinDate; // DATE postgres type

    /**
     * Avatar picture.
     */
    private final byte[] profilePic; // BYTEA postgres type

    /**
     * Whether the user has admin rights on the platform.
     */
    private final boolean isAdmin;

    /**
     * @param user       The basic {@code User} the extra info refers to.
     * @param name       First name.
     * @param surname    Last name.
     * @param location   Living place.
     * @param birthDate  Date of birth.
     * @param joinDate   Platform's join date.
     * @param profilePic Avatar picture.
     * @param isAdmin    Whether the user is an admin.
     */
    public UserExtra(final User user, final String name, final String surname,
                     final String location, final Date birthDate, final Date joinDate,
                     final byte[] profilePic, final boolean isAdmin) {
        this.user = user;
        this.name = name;
        this.surname = surname;
        this.location = location;
        this.birthDate = birthDate;
        this.joinDate = joinDate;
        this.profilePic = profilePic;
        this.isAdmin = isAdmin;
    }

    /**
     * Gets the ID.
     *
     * @return The identification number.
     */
    public final User getUser() {
        return user;
    }

    /**
     * Gets the first name.
     *
     * @return The first name.
     */
    public final String getName() {
        return name;
    }

    /**
     * Gets the last name.
     *
     * @return The last name.
     */
    public final String getSurname() {
        return surname;
    }

    /**
     * Gets the birth date.
     *
     * @return The birth date.
     */
    public Date getBirthDate() {
        return birthDate;
    }

    /**
     * Gets the date the user has joined the platform.
     *
     * @return The join date.
     */
    public Date getJoinDate() {
        return joinDate;
    }

    /**
     * Gets the living place.
     *
     * @return The living place.
     */
    public final String getLocation() {
        return location;
    }

    /**
     * Gets the avatar.
     *
     * @return The profile picture (avatar).
     */
    public final byte[] getProfilePic() {
        return profilePic;
    }

    /**
     * Administration rights checker.
     *
     * @return Whether the user is an admin.
     */
    public final boolean isAdmin() {
        return isAdmin;
    }

    /**
     * JSON representation of the object.
     *
     * @param out The stream to which the JSON representation has to be written to.
     * @throws IOException If a JSON object cannot be generated.
     */
    @Override
    public final void toJSON(final OutputStream out) throws IOException {
        final JsonGenerator gen = JSON_FACTORY.createGenerator(out);

        gen.writeStartObject();
        gen.writeFieldName("userExtra");
        gen.writeStartObject();

        gen.writeNumberField("id", user.getId());
        gen.writeStringField("name", name);
        gen.writeStringField("surname", surname);
        gen.writeStringField("location", location);
        gen.writeStringField("birthDate", birthDate.toString());
        gen.writeStringField("joinDate", joinDate.toString());
        gen.writeStringField("profilePicture", Tools.base64encode(profilePic));
        gen.writeBooleanField("isAdmin", isAdmin);

        gen.writeEndObject();
        gen.writeEndObject();
        gen.flush();
    }
}
