// Copyright 2018 The StackOverflowers
package org.tso.heapquestions.resource;

import java.security.MessageDigest;
import java.util.Base64;

/**
 * Utility methods container.
 */
public class Tools {
    /**
     * SHA-256 hashing function.
     *
     * @param base {@link String} to start from.
     * @return The encoded {@link String}.
     * @throws RuntimeException If encoding is not possible.
     */
    public static String sha256(String base) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(base.getBytes("UTF-8"));
            StringBuilder hexString = new StringBuilder();

            for (byte aHash : hash) {
                String hex = Integer.toHexString(0xff & aHash);
                if (hex.length() == 1)
                    hexString.append('0');
                hexString.append(hex);
            }
            return hexString.toString();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     * Base64 encoding of raw content (byte[] array) to {@link String}.
     *
     * @param input The content as byte array.
     * @return The corresponding encoded {@link String}.
     */
    public static String base64encode(byte[] input) {
        try {
            return Base64.getEncoder().encodeToString(input);
        } catch (Exception ignored) {
            return null;
        }
    }

    /**
     * Base64 decoding for raw content from a {@link String} to a byte[] array.
     *
     * @param input The content as a {@link String}.
     * @return The corresponding (encoded) byte array.
     */
    public static byte[] base64decode(String input) {
        try {
            return Base64.getDecoder().decode(input);
        } catch (Exception ignored) {
            return null;
        }
    }
}
