// Copyright 2018 The StackOverflowers
package org.tso.heapquestions.resource;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Date;

/**
 * Represents a scholar/academical qualification for an {@link User}.
 */
public class Qualification extends Resource {
    private final long user;
    private final QualificationType qualification;
    private final String description;
    private final Date achieveDate;
    private final String grade;

    /**
     * @param user          Corresponding {@link User} ID.
     * @param qualification Qualification type.
     * @param description   Brief description.
     * @param achieveDate   Achieve/award date.
     * @param grade         Obtained grade.
     */
    public Qualification(final long user, final QualificationType qualification, final String description, final Date achieveDate, final String grade) {
        this.user = user;
        this.qualification = qualification;
        this.description = description;
        this.achieveDate = achieveDate;
        this.grade = grade;
    }

    /**
     * Gets the user ID.
     *
     * @return The user ID.
     */
    public final long getUser() {
        return user;
    }

    /**
     * Gets the qualification type.
     *
     * @return The qualification.
     */
    public final QualificationType getQualification() {
        return qualification;
    }

    /**
     * Gets the description.
     *
     * @return The description.
     */
    public final String getDescription() {
        return description;
    }

    /**
     * Gets the achieve date.
     *
     * @return The date.
     */
    public final Date getAchieveDate() {
        return achieveDate;
    }

    /**
     * Gets the grade.
     *
     * @return The grade.
     */
    public final String getGrade() {
        return grade;
    }

    /**
     * A JSON representation of the object.
     *
     * @param out The stream to which the JSON representation has to be written.
     * @throws IOException If a JSON object cannot be generated.
     */
    @Override
    public final void toJSON(OutputStream out) throws IOException {
        final JsonGenerator gen = JSON_FACTORY.createGenerator(out);

        gen.writeStartObject();
        gen.writeFieldName("qualification");
        gen.writeStartObject();

        gen.writeNumberField("userId", user);
        gen.writeStringField("qualification", qualification.toString());
        gen.writeStringField("description", description);
        gen.writeStringField("achieveDate", achieveDate.toString());
        gen.writeStringField("grade", grade);

        gen.writeEndObject();
        gen.writeEndObject();
        gen.flush();
    }

    /**
     * Creates an qualification from its JSON representation.
     *
     * @param in The input stream containing the JSON document.
     * @return The created qualification.
     * @throws IOException If an error occurs while parsing.
     */
    public static Qualification fromJSON(final InputStream in) throws IOException {
        long user = -1;
        QualificationType qualification = null;
        String description = null;
        Date achieveDate = null;
        String grade = null;

        final JsonParser jp = JSON_FACTORY.createParser(in);

        // advance to the next element (if any) while we are not on the start
        // of an element or the element is not a token element
        while (jp.getCurrentToken() != JsonToken.FIELD_NAME || !("qualification".equals(jp.getCurrentName())))
            if (jp.nextToken() == null)
                throw new IOException("Unable to parse JSON: no qualification object found.");

        while (jp.nextToken() != JsonToken.END_OBJECT)
            if (jp.getCurrentToken() == JsonToken.FIELD_NAME)
                switch (jp.getCurrentName()) {
                    case "userId":
                        jp.nextToken();
                        user = jp.getLongValue();
                        break;
                    case "qualification":
                        jp.nextToken();
                        qualification = QualificationType.fromString(jp.getText());
                        break;
                    case "description":
                        jp.nextToken();
                        description = jp.getText();
                        break;
                    case "achieveDate":
                        jp.nextToken();
                        achieveDate = Date.valueOf(jp.getText());
                        break;
                    case "grade":
                        jp.nextToken();
                        grade = jp.getText();
                        break;
                    default:
                        break;
                }
        return new Qualification(user, qualification, description, achieveDate, grade);
    }
}
