// Copyright 2018 The StackOverflowers

/**
 * Provides resources for representing and exchanging data about the main entities of the 
 * application.
 */
package org.tso.heapquestions.resource;