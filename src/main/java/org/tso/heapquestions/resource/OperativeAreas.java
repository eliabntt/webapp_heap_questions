// Copyright 2018 The StackOverflowers
package org.tso.heapquestions.resource;

/**
 * Represents some operative areas for {@link Company}.
 */
public enum OperativeAreas {
    HR("Human Resources"),
    Marketing_Promotion("Marketing/Promotion"),
    Customer_Service_Support("Customer Service Support"),
    Sales("Sales"),
    Accounting_Finance("Accounting and Finance"),
    Distribution("Distribution"),
    Automotive("Automotive"),
    RD("Research & Development"),
    Admin_Management("Administrative/Management"),
    Production("Production"),
    Operations("Operations"),
    IT_Support("IT Support"),
    Purchasing("Purchasing"),
    Legal("Legal Department"),
    Other("Other"),
    Unspecified("Unspecified");

    private final String text;

    OperativeAreas(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public static OperativeAreas fromString(String text) throws IllegalArgumentException {
        for (OperativeAreas o : OperativeAreas.values())
            if (o.text.equalsIgnoreCase(text))
                return o;
        throw new IllegalArgumentException("Operative Area not valid! Passed: " + text);
    }

    @Override
    public String toString() {
        return text;
    }
}
