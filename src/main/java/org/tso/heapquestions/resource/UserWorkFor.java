// Copyright 2018 The StackOverflowers
package org.tso.heapquestions.resource;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static org.tso.heapquestions.resource.Resource.JSON_FACTORY;

/**
 * Represents working periods and links {@link User} to {@link Company} for a certain {@link Period}.
 */
public class UserWorkFor {
    private final long user;
    private final String company;
    private final int period;

    /**
     * @param user    The {@link User} ID.
     * @param company The {@link Company} name.
     * @param period  The working {@link Period} ID.
     */
    public UserWorkFor(final long user, final String company, final int period) {
        this.user = user;
        this.company = company;
        this.period = period;
    }

    /**
     * Gets the user ID.
     *
     * @return The user ID.
     */
    public final long getUser() {
        return user;
    }

    /**
     * Gets the company name.
     *
     * @return The company name.
     */
    public final String getCompany() {
        return company;
    }

    /**
     * Gets the period ID.
     *
     * @return The period ID.
     */
    public final int getPeriod() {
        return period;
    }

    /**
     * JSON representation of the object.
     *
     * @param out The stream to which the JSON representation has to be written to.
     * @throws IOException If a JSON object cannot be generated.
     */
    public final void toJSON(final OutputStream out) throws IOException {
        final JsonGenerator gen = JSON_FACTORY.createGenerator(out);

        gen.writeStartObject();
        gen.writeFieldName("userWorkFor");
        gen.writeStartObject();

        gen.writeNumberField("userId", user);
        gen.writeStringField("company", company);
        gen.writeNumberField("period", period);

        gen.writeEndObject();
        gen.writeEndObject();
        gen.flush();
    }

    /**
     * Creates an UserWorkFor resource from its JSON representation.
     *
     * @param in The input stream containing the JSON document.
     * @return The created qualification.
     * @throws IOException If an error occurs while parsing.
     */
    public static UserWorkFor fromJSON(final InputStream in) throws IOException {
        long user = -1;
        String company = null;
        int period = -1;

        final JsonParser jp = JSON_FACTORY.createParser(in);

        // advance to the next element (if any) while we are not on the start
        // of an element or the element is not a token element
        while (jp.getCurrentToken() != JsonToken.FIELD_NAME || !("userWorkFor".equals(jp.getCurrentName())))
            if (jp.nextToken() == null)
                throw new IOException("Unable to parse JSON: no userWorkFor object found.");

        while (jp.nextToken() != JsonToken.END_OBJECT)
            if (jp.getCurrentToken() == JsonToken.FIELD_NAME)
                switch (jp.getCurrentName()) {
                    case "userId":
                        jp.nextToken();
                        user = jp.getLongValue();
                        break;
                    case "company":
                        jp.nextToken();
                        company = jp.getText();
                        break;
                    case "period":
                        jp.nextToken();
                        period = jp.getIntValue();
                        break;

                    default:
                        break;
                }
        return new UserWorkFor(user, company, period);
    }
}
