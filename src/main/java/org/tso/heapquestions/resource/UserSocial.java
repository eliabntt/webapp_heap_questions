// Copyright 2018 The StackOverflowers
package org.tso.heapquestions.resource;

/**
 * Links {@link User} to {@link SocialNetwork}.
 */
public class UserSocial {
    private final long user;
    private final String social;
    private final String link;

    /**
     *
     * @param user The {@link User} ID.
     * @param social The {@link SocialNetwork} name.
     * @param link The account (last part of the full link).
     */
    public UserSocial(final long user, final String social, final String link) {
        this.user = user;
        this.social = social;
        this.link = link;
    }

    /**
     * Gets the user ID.
     * @return The user ID.
     */
    public final long getUser() {
        return user;
    }

    /**
     * Gets the social network name.
     * @return The SN name.
     */
    public final String getSocial() {
        return social;
    }

    /**
     * Gets the last part of the link.
     * @return The link.
     */
    public final String getLink() {
        return link;
    }
}
