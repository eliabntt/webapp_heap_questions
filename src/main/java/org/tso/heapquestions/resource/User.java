// Copyright 2018 The StackOverflowers
package org.tso.heapquestions.resource;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Date;

/**
 * Represents an user of the platform.
 */
public class User extends Resource {
    private final boolean extraFeatures;
    /**
     * Identification number.
     */
    private final Long id;

    /**
     * Public username.
     */
    private final String username;

    /**
     * Encoded password.
     */
    private final String password;

    /**
     * E-mail.
     */
    private final String email;

    /**
     * Whether the user is an admin.
     */
    private final Boolean admin;

    /**
     * Name.
     */
    private final String name;

    /**
     * Surname.
     */
    private final String surname;

    /**
     * Birth date.
     */
    private final Date birthDate;

    /**
     * Platform's join date.
     */
    private final Date joinDate;

    /**
     * Location.
     */
    private final String location;

    /**
     * Profile picture.
     */
    private final byte[] profilePicture;

    /**
     * Constructor for simple user.
     *
     * @param id       Identification number.
     * @param username Username.
     * @param password Encoded password.
     * @param email    E-mail.
     * @deprecated This constructor shall have no use on the app.
     */
    public User(final long id, final String username, final String password, final String email) {
        this.extraFeatures = false;
        this.id = id;
        this.username = username;
        this.password = password;
        this.email = email;
        this.admin = false;
        this.name = null;
        this.surname = null;
        this.birthDate = null;
        this.joinDate = null;
        this.location = null;
        this.profilePicture = null;
    }

    /**
     * @param id             Identification number.
     * @param username       Username.
     * @param password       Encoded password.
     * @param email          E-mail.
     * @param admin          Administrative privileges.
     * @param name           Name.
     * @param surname        Surname.
     * @param birthDate      Birth date.
     * @param joinDate       Join date.
     * @param location       Location.
     * @param profilePicture User's picture.
     */
    public User(final Long id, final String username, final String password, final String email,
                final Boolean admin, final String name, final String surname,
                final Date birthDate, final Date joinDate,
                final String location, final byte[] profilePicture) {
        this.extraFeatures = joinDate != null; // that's a secure field to do comparison on:
        this.id = id;                          // since we set that at user creation, not having it means a
        this.username = username;              // transmission of partial information through the app, perfectly fine
        this.email = email;
        this.password = password;
        this.admin = admin;
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
        this.joinDate = joinDate;
        this.location = location;
        this.profilePicture = profilePicture;
    }

    public User(final String username, final byte[] profilePicture) {
        this(null, username, null, null, null, null, null, null, null, null, profilePicture);
    }

    /**
     * Gets the ID.
     *
     * @return The identification number.
     */
    public final long getId() {
        return id;
    }

    /**
     * Gets the username.
     *
     * @return The username.
     */
    public final String getUsername() {
        return username;
    }

    /**
     * Gets the encoded password.
     *
     * @return The password, encoded with SHA-256.
     */
    public final String getEncodedPassword() {
        return password;
    }

    /**
     * Password check function.
     *
     * @param toCheck String to check password correspondence on.
     * @return Whether the password is valid.
     */
    public final boolean checkPassword(final String toCheck) {
        final String sha256hex = Tools.sha256(toCheck);
        return sha256hex.equals(password);
    }

    /**
     * Gets the e-mail.
     *
     * @return The e-mail.
     */
    public final String getEmail() {
        return email;
    }

    /**
     * Administration rights checker.
     *
     * @return Whether the user is an admin.
     */
    public final boolean isAdmin() {
        return admin;
    }

    /**
     * Gets the first name.
     *
     * @return The first name.
     */
    public final String getName() {
        return name;
    }

    /**
     * Gets the last name.
     *
     * @return The last name.
     */
    public final String getSurname() {
        return surname;
    }

    /**
     * Gets the birth date.
     *
     * @return The birth date.
     */
    public Date getBirthDate() {
        return birthDate;
    }

    /**
     * Gets the date the user has joined the platform.
     *
     * @return The join date.
     */
    public Date getJoinDate() {
        return joinDate;
    }

    /**
     * Gets the living place.
     *
     * @return The living place.
     */
    public final String getLocation() {
        return location;
    }

    /**
     * Gets the avatar.
     *
     * @return The profile picture (avatar).
     */
    public final byte[] getProfilePicture() {
        return profilePicture;
    }

    /**
     * JSON representation of the object.
     *
     * @param out The stream to which the JSON representation has to be written to.
     * @throws IOException If a JSON object cannot be generated.
     */
    @Override
    public final void toJSON(final OutputStream out) throws IOException {
        final JsonGenerator gen = JSON_FACTORY.createGenerator(out);

        gen.writeStartObject();
        gen.writeFieldName("user");
        gen.writeStartObject();

        if (id != null) {
            gen.writeNumberField("id", id);
            gen.writeStringField("username", username);
            gen.writeStringField("password", password);
            gen.writeStringField("email", email);
        } else {
            gen.writeStringField("username", username);
            gen.writeStringField("profilePicture", Tools.base64encode(profilePicture));
        }

        if (extraFeatures) {
            gen.writeBooleanField("isAdmin", admin);
            gen.writeStringField("name", name);
            gen.writeStringField("surname", surname);
            gen.writeStringField("birthDate", birthDate.toString());
            gen.writeStringField("joinDate", joinDate.toString());
            gen.writeStringField("location", location);
            gen.writeStringField("profilePicture", Tools.base64encode(profilePicture));
        }

        gen.writeEndObject();
        gen.writeEndObject();
        gen.flush();
    }

    /**
     * Creates an user from its JSON representation.
     *
     * @param in The input stream containing the JSON document.
     * @return The created user.
     * @throws IOException If an error occurs while parsing.
     */
    public static User fromJSON(final InputStream in) throws IOException {
        long id = -1;
        String username = null, password = null, email = null;
        String name = null, surname = null, location = null;
        Date birthDate = null, joinDate = null;
        boolean admin = false;
        byte[] profilePic = null;

        final JsonParser jp = JSON_FACTORY.createParser(in);

        // advance to the next element (if any) while we are not on the start
        // of an element or the element is not a token element
        while (jp.getCurrentToken() != JsonToken.FIELD_NAME || !("user".equals(jp.getCurrentName())))
            if (jp.nextToken() == null)
                throw new IOException("Unable to parse JSON: no user object found.");

        while (jp.nextToken() != JsonToken.END_OBJECT)
            if (jp.getCurrentToken() == JsonToken.FIELD_NAME)
                switch (jp.getCurrentName()) {
                    case "id":
                        jp.nextToken();
                        id = jp.getLongValue();
                        break;
                    case "username":
                        jp.nextToken();
                        username = jp.getText();
                        break;
                    case "password":
                        jp.nextToken();
                        password = jp.getText();
                        break;
                    case "email":
                        jp.nextToken();
                        email = jp.getText();
                        break;
                    case "isAdmin":
                        jp.nextToken();
                        admin = jp.getBooleanValue();
                        break;
                    case "name":
                        jp.nextToken();
                        name = jp.getText();
                        break;
                    case "surname":
                        jp.nextToken();
                        surname = jp.getText();
                        break;
                    case "birthDate":
                        jp.nextToken();
                        birthDate = Date.valueOf(jp.getText());
                        break;
                    case "joinDate":
                        jp.nextToken();
                        joinDate = Date.valueOf(jp.getText());
                        break;
                    case "location":
                        jp.nextToken();
                        location = jp.getText();
                        break;
                    case "profilePicture":
                        jp.nextToken();
                        profilePic = Tools.base64decode(jp.getText());
                        break;
                    default:
                        break;
                }
        return new User(id, username, password, email, admin, name, surname, birthDate, joinDate, location, profilePic);
    }
}
