// Copyright 2018 The StackOverflowers
package org.tso.heapquestions.resource;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Represents subscription plans' details.
 */
public class PricingPlan extends Resource {
    private final int id;
    private final PlanLevel level;
    private final double price;
    private final String description;

    /**
     * @param id          Plan ID.
     * @param level       Plan level/type.
     * @param price       Assigned price.
     * @param description A brief description.
     */
    public PricingPlan(final int id, final PlanLevel level, final double price, final String description) {
        this.id = id;
        this.level = level;
        this.price = price;
        this.description = description;
    }

    /**
     * Gets the ID.
     *
     * @return The ID.
     */
    public int getId() {
        return id;
    }

    /**
     * Gets the level/type.
     *
     * @return The level.
     */
    public final PlanLevel getLevel() {
        return level;
    }

    /**
     * Gets the assigned price.
     *
     * @return The price.
     */
    public final double getPrice() {
        return price;
    }

    /**
     * Gets the description.
     *
     * @return The description.
     */
    public final String getDescription() {
        return description;
    }

    /**
     * A JSON representation of the object.
     *
     * @param out The stream to which the JSON representation has to be written.
     * @throws IOException If a JSON object cannot be generated.
     */
    @Override
    public final void toJSON(OutputStream out) throws IOException {
        final JsonGenerator gen = JSON_FACTORY.createGenerator(out);

        gen.writeStartObject();
        gen.writeFieldName("pricingPlan");
        gen.writeStartObject();

        gen.writeNumberField("id", id);
        gen.writeStringField("level", level.toString());
        gen.writeNumberField("price", price);
        gen.writeStringField("description", description);

        gen.writeEndObject();
        gen.writeEndObject();
        gen.flush();
    }

    /**
     * Creates a pricing plan from its JSON representation.
     *
     * @param in The input stream containing the JSON document.
     * @return The created pricing plan.
     * @throws IOException If an error occurs while parsing.
     */
    public static PricingPlan fromJSON(final InputStream in) throws IOException {
        int id = -1;
        PlanLevel level = null;
        double price = -1;
        String description = null;

        final JsonParser jp = JSON_FACTORY.createParser(in);

        // advance to the next element (if any) while we are not on the start
        // of an element or the element is not a token element
        while (jp.getCurrentToken() != JsonToken.FIELD_NAME || !("pricingPlan".equals(jp.getCurrentName())))
            if (jp.nextToken() == null)
                throw new IOException("Unable to parse JSON, no pricing plan object found.");

        while (jp.nextToken() != JsonToken.END_OBJECT)
            if (jp.getCurrentToken() == JsonToken.FIELD_NAME)
                switch (jp.getCurrentName()) {
                    case "id":
                        jp.nextToken();
                        id = jp.getIntValue();
                        break;
                    case "level":
                        jp.nextToken();
                        level = PlanLevel.fromString(jp.getText());
                        break;
                    case "price":
                        jp.nextToken();
                        price = jp.getDoubleValue();
                        break;
                    case "description":
                        jp.nextToken();
                        description = jp.getText();
                        break;
                }
        return new PricingPlan(id, level, price, description);
    }
}
