// Copyright 2018 The StackOverflowers
package org.tso.heapquestions.resource;

/**
 * Represents the difficulty of a {@link Submission}.
 */
public enum Difficulties {
    Easy("Easy"),
    Medium("Medium"),
    Hard("Hard"),
    Extreme("Extreme");

    private final String text;

    Difficulties(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public static Difficulties fromString(String text) throws IllegalArgumentException {
        for (Difficulties d : Difficulties.values())
            if (d.text.equalsIgnoreCase(text))
                return d;
        throw new IllegalArgumentException("Difficulty not valid! Passed: " + text);
    }

    @Override
    public String toString() {
        return text;
    }
}
