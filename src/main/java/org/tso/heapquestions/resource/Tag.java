// Copyright 2018 The StackOverflowers
package org.tso.heapquestions.resource;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.List;

/**
 * Represents a tag for a {@link ForumQuestion} or a {@link Submission}.
 */
public class Tag extends Resource {
    private final List<String> values;

    /**
     * @param value The tag values.
     */
    public Tag(final List<String> value) {
        this.values = value;
    }

    /**
     * Gets the tag values.
     *
     * @return The value.
     */
    public final List<String> getValue() {
        return values;
    }

    /**
     * A JSON representation of the object.
     *
     * @param out The stream to which the JSON representation has to be written.
     * @throws IOException If a JSON object cannot be generated.
     */
    @Override
    public final void toJSON(OutputStream out) throws IOException {
        final JsonGenerator gen = JSON_FACTORY.createGenerator(out);

        gen.writeStartObject();
        gen.writeFieldName("tag");

        gen.writeStartObject();
        gen.writeFieldName("values");

        gen.writeStartArray();
        for (String value : values)
            gen.writeString(value);
        gen.writeEndArray();

        gen.writeEndObject();
        gen.writeEndObject();
        gen.flush();
    }

    /**
     * Creates a list of tags from a JSON representation.
     *
     * @param in The input stream containing the JSON document.
     * @return The list of tags.
     * @throws IOException If an error occurs while parsing.
     */
    public static List<String> fromJSON(InputStream in) throws IOException {
        final JsonParser jp = JSON_FACTORY.createParser(in);
        ObjectMapper mapper = new ObjectMapper();
        List<String> output = null;

        // advance to the next element (if any) while we are not on the start
        // of an element or the element is not a token element
        while (jp.getCurrentToken() != JsonToken.FIELD_NAME || !("tag".equals(jp.getCurrentName())))
            if (jp.nextToken() == null)
                throw new IOException("Unable to parse JSON, no tag object found.");

        // parse values using the object mapper (a list is expected)
        while (jp.nextToken() != JsonToken.END_OBJECT)
            if (jp.getCurrentToken() == JsonToken.FIELD_NAME)
                if (jp.currentName().equals("values")) {
                    jp.nextToken();
                    output = Arrays.asList(mapper.readValue(jp, String[].class));
                }
        return output;
    }
}
