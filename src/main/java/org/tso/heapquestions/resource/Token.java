// Copyright 2018 The StackOverflowers
package org.tso.heapquestions.resource;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Timestamp;

/**
 * Represents a set of login credentials to access contents on the web application.
 */
public class Token extends Resource {
    /**
     * The user id associated with the token.
     */
    private final long userId;

    /**
     * Whether the user is an admin.
     */
    private final boolean isAdmin;

    /**
     * The username associated with the token.
     */
    private final String username;

    /**
     * The generated token.
     */
    private final String token;

    /**
     * The timestamp indicating the token expiration time.
     */
    private final Timestamp expireDate;

    /**
     * @param userId     ID of the logging user.
     * @param username   Username of the logging user.
     * @param token      Token assigned to the user.
     * @param expireDate Expiration time for the assigned token.
     * @param isAdmin    Whether the user is an admin.
     */
    public Token(final long userId, final String username, final String token, final Timestamp expireDate, final boolean isAdmin) {
        this.userId = userId;
        this.token = token;
        this.username = username;
        this.expireDate = expireDate;
        this.isAdmin = isAdmin;
    }

    /**
     * Get the user id of the object.
     *
     * @return The user id.
     */
    public final long getUserId() {
        return userId;
    }

    /**
     * Get the username of the object.
     *
     * @return The username.
     */
    public final String getUsername() {
        return username;
    }

    /**
     * Get the assigned token of the object.
     *
     * @return The token.
     */
    public final String getToken() {
        return token;
    }

    /**
     * Get the expiration date of the object.
     *
     * @return The expiration date.
     */
    public final Timestamp getExpiration() {
        return expireDate;
    }

    /**
     * Get if the user is admin.
     *
     * @return True if admin, false otherwise.
     */
    public final boolean isAdmin() {
        return isAdmin;
    }

    /**
     * A JSON representation of the object.
     *
     * @param out The stream to which the JSON representation has to be written.
     * @throws IOException If a JSON object cannot be generated.
     */
    @Override
    public final void toJSON(OutputStream out) throws IOException {
        final JsonGenerator gen = JSON_FACTORY.createGenerator(out);

        gen.writeStartObject();
        gen.writeFieldName("token");
        gen.writeStartObject();

        gen.writeNumberField("userId", userId);
        gen.writeStringField("username", username);
        gen.writeStringField("tokenValue", token);
        gen.writeStringField("expires", expireDate.toString());
        gen.writeBooleanField("isAdmin", isAdmin);

        gen.writeEndObject();
        gen.writeEndObject();
        gen.flush();
    }

    /**
     * Creates a token from its JSON representation.
     *
     * @param in The input stream containing the JSON document.
     * @return The created token.
     * @throws IOException If an error occurs while parsing.
     */
    public static Token fromJSON(final InputStream in) throws IOException {
        long userId = -1;
        String username = null;
        String token = null;
        Timestamp expiration = null;
        boolean isAdmin = false;

        final JsonParser jp = JSON_FACTORY.createParser(in);

        // advance to the next element (if any) while we are not on the start
        // of an element or the element is not a token element
        while (jp.getCurrentToken() != JsonToken.FIELD_NAME || !("token".equals(jp.getCurrentName())))
            if (jp.nextToken() == null)
                throw new IOException("Unable to parse JSON: no token object found.");

        while (jp.nextToken() != JsonToken.END_OBJECT)
            if (jp.getCurrentToken() == JsonToken.FIELD_NAME)
                switch (jp.getCurrentName()) {
                    case "userId":
                        jp.nextToken();
                        userId = jp.getLongValue();
                        break;
                    case "username":
                        jp.nextToken();
                        username = jp.getText();
                        break;
                    case "tokenValue":
                        jp.nextToken();
                        token = jp.getText();
                        break;
                    case "expires":
                        jp.nextToken();
                        expiration = Timestamp.valueOf(jp.getText());
                        break;
                    case "isAdmin":
                        jp.nextToken();
                        isAdmin = jp.getValueAsBoolean();
                        break;
                    default:
                        break;
                }
        return new Token(userId, username, token, expiration, isAdmin);
    }
}
