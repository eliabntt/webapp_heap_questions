// Copyright 2018 The StackOverflowers
package org.tso.heapquestions.resource;

/**
 * Links {@link User} to {@link Submission}.
 */
public class FavoriteSubmissionList {
    private final long user;
    private final long submission;
    private final boolean notify;

    /**
     * @param user       The {@link User} ID.
     * @param submission The {@link Submission} ID.
     * @param notify     Whether to notify about updates.
     */
    public FavoriteSubmissionList(final long user, final long submission, final boolean notify) {
        this.user = user;
        this.submission = submission;
        this.notify = notify;
    }

    /**
     * Gets the user ID.
     *
     * @return The user ID.
     */
    public final long getUser() {
        return user;
    }

    /**
     * Gets the submission ID.
     *
     * @return The submission ID.
     */
    public final long getSubmission() {
        return submission;
    }

    /**
     * Gets if the user wants to be notified about question's updates.
     *
     * @return The notification setting.
     */
    public final boolean doNotify() {
        return notify;
    }
}
