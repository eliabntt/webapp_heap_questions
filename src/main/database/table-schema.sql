-- Copyright 2018 The StackOverflowers
-- create schema (dbstud-compatible)
DROP SCHEMA IF EXISTS tso CASCADE;
CREATE SCHEMA tso;

CREATE TABLE tso.user (
  id        BIGSERIAL     PRIMARY KEY,
  mail      VARCHAR(256)  UNIQUE NOT NULL,
  username  VARCHAR(128)  UNIQUE NOT NULL,
  password  CHAR(64)      NOT NULL
);
COMMENT ON TABLE tso.user IS 'Platform''s user';

CREATE TABLE tso.userExtended (
  id				  BIGINT        PRIMARY KEY,
  isAdmin			BOOLEAN       NOT NULL DEFAULT FALSE,
  name			  VARCHAR(128)  NOT NULL,
  surname			VARCHAR(128)  NOT NULL,
  birthDate		DATE          NOT NULL,
  joinDate		DATE          NOT NULL DEFAULT current_date,
  location		VARCHAR(128)  NOT NULL,
  profilePic	BYTEA,
  FOREIGN KEY (id) REFERENCES tso.user(id)
    ON DELETE CASCADE ON UPDATE CASCADE
);
COMMENT ON TABLE tso.userExtended IS 'Extended informations for users';

CREATE TABLE tso.token (
  token			  CHAR(64)		  PRIMARY KEY,
  userId			BIGINT			  NOT NULL,
  expireDate	TIMESTAMPTZ		NOT NULL,
  FOREIGN KEY (userId) REFERENCES tso.user(id)
    ON DELETE CASCADE ON UPDATE CASCADE
);
COMMENT ON TABLE tso.token IS 'Container of user-assigned login tokens';

CREATE TYPE tso.qualificationType AS ENUM (
  'High School', 'Bachelors Degree', 'Masters Degree', 'PhD', 'Other'
);

CREATE TABLE tso.qualification (
  userId			  BIGINT                  NOT NULL,
  qualification	tso.qualificationType   NOT NULL,
  description   VARCHAR(256)            NOT NULL,
  achieveDate		DATE                    NOT NULL,
  grade			    VARCHAR(128),
  PRIMARY KEY (userId, qualification),
  FOREIGN KEY (userId) REFERENCES tso.user(id)
    ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT validDate CHECK(achieveDate < current_date)
);
COMMENT ON TABLE tso.qualification IS 'User''s qualifications, academic title or similar';

CREATE TABLE tso.note (
  id	  			BIGSERIAL	  	PRIMARY KEY,
  userId			BIGINT			  NOT NULL,
  dateTime		TIMESTAMPTZ		NOT NULL DEFAULT DATE_TRUNC('second', current_timestamp),
  content			TEXT			    NOT NULL,
  FOREIGN KEY (userId) REFERENCES tso.user(id)
    ON DELETE CASCADE ON UPDATE CASCADE
);
COMMENT ON TABLE tso.note IS 'Notes, similar to Gists, associated to their authors';

-- some areas for companies
CREATE TYPE tso.operativeAreas AS ENUM (
  'Human Resources', 'Marketing/Promotion', 'Customer Service Support',
  'Sales', 'Accounting and Finance', 'Distribution', 'Automotive',
  'Research & Development', 'Administrative/Management', 'Production',
  'Operations', 'IT Support', 'Purchasing',
  'Legal Department', 'Other', 'Unspecified'
);

CREATE TABLE tso.company (
  id		        SERIAL			  PRIMARY KEY,
  name	        VARCHAR(128)	UNIQUE NOT NULL,
  description 	TEXT,
  website		  	TEXT,
  logo			    BYTEA,
  operativeArea	tso.operativeAreas	NOT NULL DEFAULT 'Unspecified'
);
COMMENT ON TABLE tso.company is 'Companies and their relevant data';

CREATE TABLE tso.period (
  id				  BIGSERIAL		PRIMARY KEY,
  startDate		DATE		    NOT NULL DEFAULT current_date,
  endDate			DATE,
  CONSTRAINT checkPeriod CHECK(startDate<endDate)
);
COMMENT ON TABLE tso.period IS 'Generic time period';

-- period is used twice for different things so we can't collate all to one table
CREATE TABLE tso.userWorkFor (
  userId    BIGINT    NOT NULL,
  company   INTEGER   NOT NULL,
  period    BIGINT    NOT NULL,
  PRIMARY KEY (userId, company, period),
  FOREIGN KEY (userId) REFERENCES tso.user(id)
    ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (company) REFERENCES tso.company(id)
    ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (period) REFERENCES tso.period(id)
    ON DELETE CASCADE ON UPDATE CASCADE
);
COMMENT ON TABLE tso.userWorkFor IS 'Tracks users working for a company on a certain time period';

CREATE TABLE tso.socialNetwork (
  name  			VARCHAR(128)	PRIMARY KEY,
  website			TEXT	    		NOT NULL,
  logo  			BYTEA
);
COMMENT ON TABLE tso.socialNetwork IS 'Social network links associated to users';

CREATE TABLE tso.userSocial (
  userId			BIGINT  			NOT NULL,
  social			VARCHAR(128)	NOT NULL,
  link	  		TEXT		    	NOT NULL,
  PRIMARY KEY (userId, social),
  FOREIGN KEY (userId) REFERENCES tso.user(id)
    ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (social) REFERENCES tso.socialNetwork(name)
    ON DELETE CASCADE ON UPDATE CASCADE
);
COMMENT ON TABLE tso.userSocial IS 'Links users to their social networks, if they agreed';

CREATE TYPE tso.planLevel AS ENUM (
  'Free', 'Advanced', 'Pro', 'Annual', 'Lifetime'
);

CREATE TABLE tso.pricingPlan (
  id          SERIAL        PRIMARY KEY,
  level	  		tso.planLevel   	NOT NULL,
  price	    	DECIMAL(12,2)	NOT NULL,
  description TEXT,
  CHECK(price >= 0),
  UNIQUE (level,price),
  CONSTRAINT onlyNotFree CHECK (level <> 'Free')
);
COMMENT ON TABLE tso.pricingPlan IS 'Platform''s pricing plans';

CREATE TABLE tso.userPlanSubscription (
  userId		BIGINT  			NOT NULL,
  period		BIGINT  			NOT NULL,
  planId		INT         	NOT NULL,
  isActive	BOOLEAN 			NOT NULL DEFAULT FALSE,
  PRIMARY KEY (userId, period, planId),
  FOREIGN KEY (userId) REFERENCES tso.user(id)
    ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (period) REFERENCES tso.period(id)
    ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (planId) REFERENCES tso.pricingPlan(id)
    ON DELETE RESTRICT ON UPDATE CASCADE
  --Not need to check adding only if not free because of previous check
);
COMMENT ON TABLE tso.userPlanSubscription IS 'Links users and subscribed plans, tracking periods';

CREATE TABLE tso.category (
  name        VARCHAR(128)	PRIMARY KEY,
  description	TEXT
);
COMMENT ON TABLE tso.category IS 'Categories for forum and submissions';

-- for questions and submissions
CREATE TYPE tso.difficulties AS ENUM (
  'Easy', 'Medium', 'Hard', 'Extreme'
);

CREATE TYPE tso.itemState AS ENUM (
  'Visible', 'Edited', 'Moderated', 'Deleted'
);

CREATE TABLE tso.submission (
  id			  	BIGSERIAL         PRIMARY KEY,
  userId			BIGINT,
  title 		  VARCHAR(256)      NOT NULL,
  dateTime		TIMESTAMPTZ       NOT NULL DEFAULT current_timestamp,
  difficulty  tso.difficulties  NOT NULL,
  state			  tso.itemState 	  NOT NULL DEFAULT 'Visible',
  content			TEXT	      		  NOT NULL,
  solution		TEXT,
  category		VARCHAR(128)  	  NOT NULL,
  minPlanToVis	tso.planLevel		NOT NULL,
  FOREIGN KEY (userId) REFERENCES tso.user(id)
    ON DELETE SET NULL ON UPDATE CASCADE,
  FOREIGN KEY (category) REFERENCES tso.category(name)
    ON DELETE RESTRICT ON UPDATE CASCADE,
  UNIQUE (title,content,solution)
);
COMMENT ON TABLE tso.submission is 'Interview questions submission';

CREATE TABLE tso.submissionCompanies (
  submissionId  BIGINT,
  companyId     INT,
  PRIMARY KEY(submissionId, companyId),
  FOREIGN KEY (submissionId) REFERENCES tso.submission(id)
    ON DELETE CASCADE ON UPDATE CASCADE ,
  FOREIGN KEY (companyId) REFERENCES tso.company(id)
    ON DELETE CASCADE ON UPDATE CASCADE
);
COMMENT ON TABLE tso.submissionCompanies is 'Links questions asked by companies';

CREATE TABLE tso.favoritesSubmissionList (
  userId	  	BIGINT    NOT NULL,
  submission	BIGINT    NOT NULL,
  notify	  	BOOLEAN   NOT NULL DEFAULT FALSE,
  PRIMARY KEY (userId, submission),
  FOREIGN KEY (userId) REFERENCES tso.user(id)
    ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (submission) REFERENCES tso.submission(id)
    ON DELETE CASCADE ON UPDATE CASCADE
);
COMMENT ON TABLE tso.favoritesSubmissionList IS 'Favorite list of submissions, per user';

CREATE TABLE tso.forumQuestion (
  id		  	BIGSERIAL 		PRIMARY KEY,
  userId		BIGINT,
  category	VARCHAR(128)	NOT NULL,
  dateTime	TIMESTAMPTZ		NOT NULL DEFAULT DATE_TRUNC('second',current_timestamp),
  closed		BOOLEAN 			NOT NULL DEFAULT FALSE,
  title 		VARCHAR(256)	NOT NULL,
  content		TEXT		    	NOT NULL,
  state		  tso.itemState	NOT NULL DEFAULT 'Visible',
  FOREIGN KEY (userId) REFERENCES tso.user(id)
    ON DELETE SET NULL ON UPDATE CASCADE,
  FOREIGN KEY (category) REFERENCES tso.category(name)
    ON DELETE RESTRICT ON UPDATE CASCADE,
  UNIQUE (title,content)
);
COMMENT ON TABLE tso.forumQuestion IS 'Forum questions';

CREATE TABLE tso.favoritesQuestionList (
  userId		BIGINT    NOT NULL,
  question	BIGINT    NOT NULL,
  notify		BOOLEAN   NOT NULL DEFAULT FALSE,
  PRIMARY KEY (userId, question),
  FOREIGN KEY (userId) REFERENCES tso.user(id)
    ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (question) REFERENCES tso.forumQuestion(id)
    ON DELETE CASCADE ON UPDATE CASCADE
);
COMMENT ON TABLE tso.favoritesQuestionList IS 'Favorite list of forum questions, per user';

CREATE TABLE tso.tag (
  value VARCHAR(128) PRIMARY KEY
);
COMMENT ON TABLE tso.tag IS 'Tags for submissions and forum questions';

CREATE TABLE tso.submissionTags (
  submission	BIGINT	 	  	NOT NULL,
  tag			    VARCHAR(128) 	NOT NULL,
  PRIMARY KEY (submission, tag),
  FOREIGN KEY (submission) REFERENCES tso.submission(id)
    ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (tag) REFERENCES tso.tag(value)
    ON DELETE CASCADE ON UPDATE CASCADE
);
COMMENT ON TABLE tso.submissionTags IS 'Links submissions to their tags';

CREATE TABLE tso.forumTags (
  question	 BIGINT       NOT NULL,
  tag   		 VARCHAR(128) NOT NULL,
  PRIMARY KEY (question, tag),
  FOREIGN KEY (question) REFERENCES tso.forumQuestion(id)
    ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (tag) REFERENCES tso.tag(value)
    ON DELETE CASCADE ON UPDATE CASCADE
);
COMMENT ON TABLE tso.forumTags IS 'Links forum questions to their tags';

CREATE TABLE tso.comment (
  id	    		  BIGSERIAL		PRIMARY KEY,
  userId	  	  BIGINT,
  question  	  BIGINT,
  submission	  BIGINT,
  parentComment	BIGINT,
  dateTime  	  TIMESTAMPTZ	NOT NULL DEFAULT DATE_TRUNC('second',current_timestamp),
  downvote  	  INTEGER			NOT NULL DEFAULT 0,
  upvote		    INTEGER			NOT NULL DEFAULT 0,
  notify	    	BOOLEAN			NOT NULL DEFAULT FALSE,
  isAnswer  	  BOOLEAN			NOT NULL DEFAULT FALSE,
  content	  	  TEXT			  NOT NULL,
  state		    tso.itemState	NOT NULL DEFAULT 'Visible',
  FOREIGN KEY (userId) REFERENCES tso.user(id)
    ON DELETE SET NULL ON UPDATE CASCADE,
  FOREIGN KEY (question) REFERENCES tso.forumQuestion(id)
    ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (submission) REFERENCES tso.submission(id)
    ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (parentComment) REFERENCES tso.comment(id)
    ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT check_comment CHECK ((question IS NOT NULL AND submission IS NULL) OR (submission IS NOT NULL AND question IS NULL )),
  -- CHECK THE COMMENT TO BE OF A QUESTION OR OF A SUBMISSION, ALSO VALID FOR SUBCOMMENTS, NO XOR IN POSTGRESQL
  CONSTRAINT only_false CHECK ((submission IS NOT NULL AND isAnswer = false) OR (submission IS NULL))
  -- SUBMISSION DOES NOT HAVE AN ANSWER, SO ITS COMMENTS CANNOT BE MARKED AS SUCH
);
COMMENT ON TABLE tso.comment IS 'Comments on submissions or forum questions';
CREATE UNIQUE INDEX check_answer_question ON tso.comment (question) WHERE (isAnswer = true AND (state = 'Visible' or state = 'Edited'));

CREATE TABLE tso.privateChat (
  msgId 			BIGSERIAL		NOT NULL,
  senderId		BIGINT			NOT NULL,
  receiverId	BIGINT			NOT NULL,
  dateTime		TIMESTAMPTZ	NOT NULL DEFAULT DATE_TRUNC('second',current_timestamp),
  content			TEXT			  NOT NULL,
  PRIMARY KEY (msgId, senderId, receiverId),
  FOREIGN KEY (senderId) REFERENCES tso.user(id)
    ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (receiverId) REFERENCES tso.user(id)
    ON DELETE CASCADE ON UPDATE CASCADE
);
COMMENT ON TABLE tso.privateChat IS 'Private chats between two users';

CREATE TABLE tso.chatRoom (
  msgId 			BIGSERIAL	  	NOT NULL,
  senderId		BIGINT  			NOT NULL,
  category		VARCHAR(128)	NOT NULL,
  dateTime	  TIMESTAMPTZ	  NOT NULL DEFAULT DATE_TRUNC('second',current_timestamp),
  content		  TEXT		      NOT NULL,
  PRIMARY KEY (msgId, senderId, category),
  FOREIGN KEY (senderId) REFERENCES tso.user(id)
    ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (category) REFERENCES tso.category(name)
    ON DELETE CASCADE ON UPDATE CASCADE
);
COMMENT ON TABLE tso.chatRoom IS 'Category-specific chat rooms';