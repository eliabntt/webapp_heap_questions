--ADD company
SELECT tso.addCompany('Google','Google','www.google.it',NULL,'Unspecified');
SELECT tso.addCompany('Auchan','Auchan','www.auchan.it',NULL,'Distribution');
SELECT tso.addCompany('BMW', 'BMW', 'www.bmw.de',NULL,'Automotive');

--ADD plan
INSERT INTO tso.pricingPlan (id, level, price, description) VALUES
	(DEFAULT, 'Advanced',10,'Advanced'),
	(DEFAULT, 'Pro',100,'Full'),
	(DEFAULT, 'Advanced',2,'Advanced discounted');

--ADD socialnetwork
INSERT INTO tso.socialNetwork VALUES ('LinkedIn','www.linkedin.com',NULL),
  ('Facebook','www.facebook.com', NULL),
  ('Twitter','www.twitter.com',NULL);

--ADD category
INSERT INTO tso.category (name, description) VALUES
  ('General','General matters'),
  ('Programming','Programming problem'),
  ('Behavior','Behavior question'),
  ('Operating system','General question on operating system');

--ADD users
SELECT tso.adduser('mario@exaple.com', 'mariorossi','011', 'mario','rossi', '07-09-1994','Vicenza',NULL);
SELECT tso.adduser('giovanni@exaple.com', 'foo','01111', 'giovanni','verdi', '02-10-1964','Milano',NULL);
SELECT tso.adduser('marta@exaple.com', 'softwerist','0100', 'marta','bonetto', '4-12-2006','Londra',NULL);
SELECT tso.adduser('claudio@exaple.com', 'italia06','011010', 'claudio','almenari', '05-04-2001','Napoli',NULL);
SELECT tso.adduser('facchin@exaple.com', 'piante','010101', 'facchin','de grandi', '07-09-1994','Germania',NULL);
SELECT tso.adduser('calcio@exaple.com', 'puzzling','0110010', 'mario','rossi', '14-02-1959','Venezia',NULL);
SELECT tso.adduser('alessia@exaple.com', 'shining','010010101', 'alessia','gubbini', '03-10-1958','San Remo',NULL);
SELECT tso.adduser('paolo@exaple.com', 'door','011001010', 'giovanni','paolo', '12-06-1986','Udine',NULL);
SELECT tso.adduser('genoveffa@exaple.com', 'ac/dc','01010101', 'genoveffa','zaroccolo', '04-03-1982','Shangai',NULL);
SELECT tso.adduser('caramelle@exaple.com', 'gloves','0100100011', 'kevin','bacon', '07-02-1944','Houston',NULL);
UPDATE tso.userExtended SET isAdmin = TRUE WHERE location = 'Milano' or location = 'San Remo';

--ADD qualification
SELECT tso.addQualification((SELECT id FROM tso.user WHERE username = 'softwerist'), 'PhD', 'Physics phd', '12-07-2017','110');
SELECT tso.addQualification((SELECT id FROM tso.user WHERE username = 'ac/dc'), 'Other', 'CAD advance course', '13-05-2001','passed');
SELECT tso.addQualification((SELECT id FROM tso.user WHERE username = 'softwerist'), 'Masters Degree', 'ICT', '12-07-2014','110L');
SELECT tso.addQualification((SELECT id FROM tso.user WHERE username = 'shining'), 'High School', 'Classic', '12-07-2002','80');

--ADD note
SELECT tso.addNote((SELECT id FROM tso.user WHERE username = 'gloves'),'devo vedere meglio il sito');

--ADD period
SELECT tso.addPeriod('12-07-2015','12-08-2015');
SELECT tso.addPeriod('13-04-2018',NULL);
SELECT tso.addPeriod('22-04-2006', NULL);
SELECT tso.addPeriod('11-03-2015','22-08-2017');

--ADD userWork
SELECT tso.addUserWorkFor((SELECT id FROM tso.user WHERE username = 'softwerist'), (SELECT id FROM tso.company WHERE name = 'Google'), (SELECT id FROM tso.period WHERE startDate = '12-07-2015' AND endDate = '12-08-2015'));
SELECT tso.addUserWorkFor((SELECT id FROM tso.user WHERE username = 'softwerist'), (SELECT id FROM tso.company WHERE name = 'BMW'), (SELECT id FROM tso.period WHERE startDate = '13-04-2018' AND endDate is NULL));
SELECT tso.addUserWorkFor((SELECT id FROM tso.user WHERE username = 'puzzling'), (SELECT id FROM tso.company WHERE name = 'Auchan'), (SELECT id FROM tso.period WHERE startDate = '22-04-2006' AND endDate is NULL));

--ADD usersocial
SELECT tso.addUserSocial((SELECT id FROM tso.user WHERE username = 'piante'), 'LinkedIn', 'www.linkedin.com/piaante');

--ADD userplansubscription
SELECT tso.addUserPlanSubscription((SELECT id FROM tso.user WHERE username = 'italia06'),(SELECT id FROM tso.period WHERE startDate = '11-03-2015' AND endDate = '22-08-2017'),(SELECT id FROM tso.pricingPlan WHERE level='Advanced' and price=10) , TRUE);
SELECT tso.addUserPlanSubscription((SELECT id FROM tso.user WHERE username = 'piante'),(SELECT id FROM tso.period WHERE startDate = '11-03-2015' AND endDate = '22-08-2017'),(SELECT id FROM tso.pricingPlan WHERE level='Advanced' and price=2) , TRUE);

--ADD forumQuestion
SELECT tso.addForumQuestion ((SELECT id FROM tso.user WHERE username = 'shining'),'General','False','Array','How can I declare an arry?', 'Visible');
SELECT tso.addForumQuestion ((SELECT id FROM tso.user WHERE username = 'italia06'),'Programming','True','For','How can I do a for?', 'Visible');

--ADD submission
SELECT tso.addSubmission ((SELECT id FROM tso.user WHERE username = 'piante'),'First submission','Easy','Visible','First submission content', '','General','Free');
SELECT tso.addSubmission ((SELECT id FROM tso.user WHERE username = 'foo'),'Second submission','Hard','Deleted','Second submission content','','Programming','Advanced');

--ADD companies
SELECT tso.addSubmissionCompany((SELECT id AS subId FROM tso.submission WHERE title = 'First submission'), (SELECT id AS compId FROM tso.company WHERE name = 'Google'));
SELECT tso.addSubmissionCompany((SELECT id AS subId FROM tso.submission WHERE title = 'First submission'), (SELECT id AS compId FROM tso.company WHERE name = 'BMW'));

--ADD Comments
SELECT tso.addComment ((SELECT id FROM tso.user WHERE username = 'italia06'),(SELECT id FROM tso.forumQuestion WHERE title = 'Array'), NULL, NULL, True, False, 'prova a cercare su google');
SELECT tso.addComment ((SELECT id FROM tso.user WHERE username = 'shining'),(SELECT id FROM tso.forumQuestion WHERE title = 'Array'), NULL, (SELECT id FROM tso.comment WHERE content = 'prova a cercare su google'), False, False, 'simpatico');
SELECT tso.addComment ((SELECT id FROM tso.user WHERE username = 'shining'),(SELECT id FROM tso.forumQuestion WHERE title = 'For'), NULL, NULL, True, True, 'for(int i = 0; i< max; i++){}');

--ADD tag to submission
SELECT tso.addSubmissionTags((SELECT id FROM tso.submission WHERE title = 'First submission'), 'programmin');

--ADD tag to question
SELECT tso.addForumTags((SELECT id FROM tso.forumquestion WHERE title = 'For'), 'programming');
