--
-- PostgreSQL database dump
--

--
-- Data for Name: category; Type: TABLE DATA; Schema: tso; Owner: sql
--

INSERT INTO tso.category (name, description) VALUES ('General', 'General matters');
INSERT INTO tso.category (name, description) VALUES ('Programming', 'Programming problem');
INSERT INTO tso.category (name, description) VALUES ('Behavior', 'Behavior question');
INSERT INTO tso.category (name, description) VALUES ('Operating system', 'General question on operating system');

--
-- Data for Name: user; Type: TABLE DATA; Schema: tso; Owner: sql
--

INSERT INTO tso."user" (id, mail, username, password) VALUES (1, 'mario@exaple.com', 'mariorossi', '011                                                             ');
INSERT INTO tso."user" (id, mail, username, password) VALUES (2, 'giovanni@exaple.com', 'foo', '01111                                                           ');
INSERT INTO tso."user" (id, mail, username, password) VALUES (3, 'marta@exaple.com', 'softwerist', '0100                                                            ');
INSERT INTO tso."user" (id, mail, username, password) VALUES (4, 'claudio@exaple.com', 'italia06', '011010                                                          ');
INSERT INTO tso."user" (id, mail, username, password) VALUES (5, 'facchin@exaple.com', 'piante', '010101                                                          ');
INSERT INTO tso."user" (id, mail, username, password) VALUES (6, 'calcio@exaple.com', 'puzzling', '0110010                                                         ');
INSERT INTO tso."user" (id, mail, username, password) VALUES (7, 'alessia@exaple.com', 'shining', '010010101                                                       ');
INSERT INTO tso."user" (id, mail, username, password) VALUES (8, 'paolo@exaple.com', 'door', '011001010                                                       ');
INSERT INTO tso."user" (id, mail, username, password) VALUES (9, 'genoveffa@exaple.com', 'ac/dc', '01010101                                                        ');
INSERT INTO tso."user" (id, mail, username, password) VALUES (10, 'caramelle@exaple.com', 'gloves', '0100100011                                                      ');

--
-- Data for Name: chatroom; Type: TABLE DATA; Schema: tso; Owner: sql
--

--
-- Data for Name: forumquestion; Type: TABLE DATA; Schema: tso; Owner: sql
--

INSERT INTO tso.forumquestion (id, userid, category, datetime, closed, title, content, state) VALUES (1, 7, 'General', '2018-05-15 12:49:12+00', false, 'Array', 'How can I declare an arry?', 'Visible');
INSERT INTO tso.forumquestion (id, userid, category, datetime, closed, title, content, state) VALUES (2, 4, 'Programming', '2018-05-15 12:49:12+00', true, 'For', 'How can I do a for?', 'Visible');

--
-- Data for Name: submission; Type: TABLE DATA; Schema: tso; Owner: sql
--

INSERT INTO tso.submission (id, userid, title, datetime, difficulty, state, content, solution, category, minplantovis) VALUES (1, 5, 'First submission', '2018-05-15 12:49:12.29714+00', 'Easy', 'Visible', 'First submission content', '', 'General', 'Free');
INSERT INTO tso.submission (id, userid, title, datetime, difficulty, state, content, solution, category, minplantovis) VALUES (2, 2, 'Second submission', '2018-05-15 12:49:12.29714+00', 'Hard', 'Deleted', 'Second submission content', '', 'Programming', 'Advanced');

--
-- Data for Name: comment; Type: TABLE DATA; Schema: tso; Owner: sql
--

INSERT INTO tso.comment (id, userid, question, submission, parentcomment, datetime, downvote, upvote, notify, isanswer, content, state) VALUES (1, 4, 1, NULL, NULL, '2018-05-15 12:49:12+00', 0, 0, false, false, 'prova a cercare su google', 'Visible');
INSERT INTO tso.comment (id, userid, question, submission, parentcomment, datetime, downvote, upvote, notify, isanswer, content, state) VALUES (2, 7, 1, NULL, 1, '2018-05-15 12:49:12+00', 0, 0, false, false, 'simpatico', 'Visible');
INSERT INTO tso.comment (id, userid, question, submission, parentcomment, datetime, downvote, upvote, notify, isanswer, content, state) VALUES (3, 7, 2, NULL, NULL, '2018-05-15 12:49:12+00', 0, 0, false, true, 'for(int i = 0; i< max; i++){}', 'Visible');

--
-- Data for Name: company; Type: TABLE DATA; Schema: tso; Owner: sql
--

INSERT INTO tso.company (id, name, description, website, logo, operativearea) VALUES (1, 'Google', 'Google', 'www.google.it', NULL, 'Unspecified');
INSERT INTO tso.company (id, name, description, website, logo, operativearea) VALUES (2, 'Auchan', 'Auchan', 'www.auchan.it', NULL, 'Distribution');
INSERT INTO tso.company (id, name, description, website, logo, operativearea) VALUES (3, 'BMW', 'BMW', 'www.bmw.de', NULL, 'Automotive');

--
-- Data for Name: favoritesquestionlist; Type: TABLE DATA; Schema: tso; Owner: sql
--

--
-- Data for Name: favoritessubmissionlist; Type: TABLE DATA; Schema: tso; Owner: sql
--

--
-- Data for Name: tag; Type: TABLE DATA; Schema: tso; Owner: sql
--

INSERT INTO tso.tag (value) VALUES ('programmin');
INSERT INTO tso.tag (value) VALUES ('programming');

--
-- Data for Name: forumtags; Type: TABLE DATA; Schema: tso; Owner: sql
--

INSERT INTO tso.forumtags (question, tag) VALUES (2, 'programming');

--
-- Data for Name: note; Type: TABLE DATA; Schema: tso; Owner: sql
--

INSERT INTO tso.note (id, userid, datetime, content) VALUES (1, 10, '2018-05-15 12:49:12+00', 'devo vedere meglio il sito');

--
-- Data for Name: period; Type: TABLE DATA; Schema: tso; Owner: sql
--

INSERT INTO tso.period (id, startdate, enddate) VALUES (1, '2015-07-12', '2015-08-12');
INSERT INTO tso.period (id, startdate, enddate) VALUES (2, '2018-04-13', NULL);
INSERT INTO tso.period (id, startdate, enddate) VALUES (3, '2006-04-22', NULL);
INSERT INTO tso.period (id, startdate, enddate) VALUES (4, '2015-03-11', '2017-08-22');

--
-- Data for Name: pricingplan; Type: TABLE DATA; Schema: tso; Owner: sql
--

INSERT INTO tso.pricingplan (id, level, price, description) VALUES (1, 'Advanced', 10.00, 'Advanced');
INSERT INTO tso.pricingplan (id, level, price, description) VALUES (2, 'Pro', 100.00, 'Full');
INSERT INTO tso.pricingplan (id, level, price, description) VALUES (3, 'Advanced', 2.00, 'Advanced discounted');

--
-- Data for Name: privatechat; Type: TABLE DATA; Schema: tso; Owner: sql
--

--
-- Data for Name: qualification; Type: TABLE DATA; Schema: tso; Owner: sql
--

INSERT INTO tso.qualification (userid, qualification, description, achievedate, grade) VALUES (3, 'PhD', 'Physics phd', '2017-07-12', '110');
INSERT INTO tso.qualification (userid, qualification, description, achievedate, grade) VALUES (9, 'Other', 'CAD advance course', '2001-05-13', 'passed');
INSERT INTO tso.qualification (userid, qualification, description, achievedate, grade) VALUES (3, 'Masters Degree', 'ICT', '2014-07-12', '110L');
INSERT INTO tso.qualification (userid, qualification, description, achievedate, grade) VALUES (7, 'High School', 'Classic', '2002-07-12', '80');

--
-- Data for Name: socialnetwork; Type: TABLE DATA; Schema: tso; Owner: sql
--

INSERT INTO tso.socialnetwork (name, website, logo) VALUES ('LinkedIn', 'www.linkedin.com', NULL);
INSERT INTO tso.socialnetwork (name, website, logo) VALUES ('Facebook', 'www.facebook.com', NULL);
INSERT INTO tso.socialnetwork (name, website, logo) VALUES ('Twitter', 'www.twitter.com', NULL);

--
-- Data for Name: submissioncompanies; Type: TABLE DATA; Schema: tso; Owner: sql
--

INSERT INTO tso.submissioncompanies (submissionid, companyid) VALUES (1, 1);
INSERT INTO tso.submissioncompanies (submissionid, companyid) VALUES (1, 3);

--
-- Data for Name: submissiontags; Type: TABLE DATA; Schema: tso; Owner: sql
--

INSERT INTO tso.submissiontags (submission, tag) VALUES (1, 'programmin');

--
-- Data for Name: token; Type: TABLE DATA; Schema: tso; Owner: sql
--

--
-- Data for Name: userextended; Type: TABLE DATA; Schema: tso; Owner: sql
--

INSERT INTO tso.userextended (id, isadmin, name, surname, birthdate, joindate, location, profilepic) VALUES (1, false, 'mario', 'rossi', '1994-09-07', '2018-05-15', 'Vicenza', NULL);
INSERT INTO tso.userextended (id, isadmin, name, surname, birthdate, joindate, location, profilepic) VALUES (3, false, 'marta', 'bonetto', '2006-12-04', '2018-05-15', 'Londra', NULL);
INSERT INTO tso.userextended (id, isadmin, name, surname, birthdate, joindate, location, profilepic) VALUES (4, false, 'claudio', 'almenari', '2001-04-05', '2018-05-15', 'Napoli', NULL);
INSERT INTO tso.userextended (id, isadmin, name, surname, birthdate, joindate, location, profilepic) VALUES (5, false, 'facchin', 'de grandi', '1994-09-07', '2018-05-15', 'Germania', NULL);
INSERT INTO tso.userextended (id, isadmin, name, surname, birthdate, joindate, location, profilepic) VALUES (6, false, 'mario', 'rossi', '1959-02-14', '2018-05-15', 'Venezia', NULL);
INSERT INTO tso.userextended (id, isadmin, name, surname, birthdate, joindate, location, profilepic) VALUES (8, false, 'giovanni', 'paolo', '1986-06-12', '2018-05-15', 'Udine', NULL);
INSERT INTO tso.userextended (id, isadmin, name, surname, birthdate, joindate, location, profilepic) VALUES (9, false, 'genoveffa', 'zaroccolo', '1982-03-04', '2018-05-15', 'Shangai', NULL);
INSERT INTO tso.userextended (id, isadmin, name, surname, birthdate, joindate, location, profilepic) VALUES (10, false, 'kevin', 'bacon', '1944-02-07', '2018-05-15', 'Houston', NULL);
INSERT INTO tso.userextended (id, isadmin, name, surname, birthdate, joindate, location, profilepic) VALUES (2, true, 'giovanni', 'verdi', '1964-10-02', '2018-05-15', 'Milano', NULL);
INSERT INTO tso.userextended (id, isadmin, name, surname, birthdate, joindate, location, profilepic) VALUES (7, true, 'alessia', 'gubbini', '1958-10-03', '2018-05-15', 'San Remo', NULL);

--
-- Data for Name: userplansubscription; Type: TABLE DATA; Schema: tso; Owner: sql
--

INSERT INTO tso.userplansubscription (userid, period, planid, isactive) VALUES (4, 4, 1, true);
INSERT INTO tso.userplansubscription (userid, period, planid, isactive) VALUES (5, 4, 3, true);

--
-- Data for Name: usersocial; Type: TABLE DATA; Schema: tso; Owner: sql
--

INSERT INTO tso.usersocial (userid, social, link) VALUES (5, 'LinkedIn', 'www.linkedin.com/piaante');

--
-- Data for Name: userworkfor; Type: TABLE DATA; Schema: tso; Owner: sql
--

INSERT INTO tso.userworkfor (userid, company, period) VALUES (3, 1, 1);
INSERT INTO tso.userworkfor (userid, company, period) VALUES (3, 3, 2);
INSERT INTO tso.userworkfor (userid, company, period) VALUES (6, 2, 3);

--
-- PostgreSQL database dump complete
--
