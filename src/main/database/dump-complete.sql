--
-- PostgreSQL database dump
--

--
-- Name: tso; Type: SCHEMA; Schema: -; Owner: webdb
--

CREATE SCHEMA tso;

ALTER SCHEMA tso OWNER TO webdb;

--
-- Name: difficulties; Type: TYPE; Schema: tso; Owner: webdb
--

CREATE TYPE tso.difficulties AS ENUM (
    'Easy',
    'Medium',
    'Hard',
    'Extreme'
);


ALTER TYPE tso.difficulties OWNER TO webdb;

--
-- Name: itemstate; Type: TYPE; Schema: tso; Owner: webdb
--

CREATE TYPE tso.itemstate AS ENUM (
    'Visible',
    'Edited',
    'Moderated',
    'Deleted'
);


ALTER TYPE tso.itemstate OWNER TO webdb;

--
-- Name: operativeareas; Type: TYPE; Schema: tso; Owner: webdb
--

CREATE TYPE tso.operativeareas AS ENUM (
    'Human Resources',
    'Marketing/Promotion',
    'Customer Service Support',
    'Sales',
    'Accounting and Finance',
    'Distribution',
    'Automotive',
    'Research & Development',
    'Administrative/Management',
    'Production',
    'Operations',
    'IT Support',
    'Purchasing',
    'Legal Department',
    'Other',
    'Unspecified'
);


ALTER TYPE tso.operativeareas OWNER TO webdb;

--
-- Name: planlevel; Type: TYPE; Schema: tso; Owner: webdb
--

CREATE TYPE tso.planlevel AS ENUM (
    'Free',
    'Advanced',
    'Pro',
    'Annual',
    'Lifetime'
);


ALTER TYPE tso.planlevel OWNER TO webdb;

--
-- Name: qualificationtype; Type: TYPE; Schema: tso; Owner: webdb
--

CREATE TYPE tso.qualificationtype AS ENUM (
    'High School',
    'Bachelors Degree',
    'Masters Degree',
    'PhD',
    'Other'
);


ALTER TYPE tso.qualificationtype OWNER TO webdb;

--
-- Name: addchatroom(bigint, character varying, text); Type: FUNCTION; Schema: tso; Owner: webdb
--

CREATE FUNCTION tso.addchatroom(_senderid bigint, _category character varying, _content text) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN 
  INSERT INTO tso.chatRoom(msgId, senderId, category, dateTime, content)
    VALUES(DEFAULT, _senderId, _category, DEFAULT, _content);
END $$;


ALTER FUNCTION tso.addchatroom(_senderid bigint, _category character varying, _content text) OWNER TO webdb;

--
-- Name: addcomment(bigint, bigint, bigint, bigint, boolean, boolean, text); Type: FUNCTION; Schema: tso; Owner: webdb
--

CREATE FUNCTION tso.addcomment(_userid bigint, _question bigint, _submission bigint, _parentcomment bigint, _notify boolean, _isanswer boolean, _content text) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN 
  INSERT INTO tso.comment(id, userId, question, submission, parentComment, dateTime, downvote, upvote, notify, isAnswer, content)
    VALUES(DEFAULT, _userId, _question, _submission, _parentComment, DEFAULT, DEFAULT, DEFAULT, _notify, _isAnswer, _content);
END $$;


ALTER FUNCTION tso.addcomment(_userid bigint, _question bigint, _submission bigint, _parentcomment bigint, _notify boolean, _isanswer boolean, _content text) OWNER TO webdb;

--
-- Name: addcompany(character varying, text, text, bytea, tso.operativeareas); Type: FUNCTION; Schema: tso; Owner: webdb
--

CREATE FUNCTION tso.addcompany(_name character varying, _description text, _website text, _logo bytea, _operativearea tso.operativeareas) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN 
  INSERT INTO tso.company(id, name, description, website, logo, operativeArea)
    VALUES( DEFAULT, _name, _description, _website, _logo, _operativeArea);
END $$;


ALTER FUNCTION tso.addcompany(_name character varying, _description text, _website text, _logo bytea, _operativearea tso.operativeareas) OWNER TO webdb;

--
-- Name: addfavoritesquestionlist(bigint, bigint, boolean); Type: FUNCTION; Schema: tso; Owner: webdb
--

CREATE FUNCTION tso.addfavoritesquestionlist(_userid bigint, _question bigint, _notify boolean) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN 
  INSERT INTO tso.favoritesQuestionList(userId, question, notify)
    VALUES(_userId, _question, _notify);
END $$;


ALTER FUNCTION tso.addfavoritesquestionlist(_userid bigint, _question bigint, _notify boolean) OWNER TO webdb;

--
-- Name: addfavoritessubmissionlist(bigint, bigint, boolean); Type: FUNCTION; Schema: tso; Owner: webdb
--

CREATE FUNCTION tso.addfavoritessubmissionlist(_userid bigint, _submission bigint, _notify boolean) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN 
  INSERT INTO tso.favoritesSubmissionList(userId, submission, notify)
    VALUES(_userId, _submission, _notify);
END $$;


ALTER FUNCTION tso.addfavoritessubmissionlist(_userid bigint, _submission bigint, _notify boolean) OWNER TO webdb;

--
-- Name: addforumquestion(bigint, character varying, boolean, character varying, text, tso.itemstate); Type: FUNCTION; Schema: tso; Owner: webdb
--

CREATE FUNCTION tso.addforumquestion(_userid bigint, _category character varying, _closed boolean, _title character varying, _content text, _state tso.itemstate) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN 
  INSERT INTO tso.forumQuestion(id, userId, category, dateTime, closed, title, content, state)
    VALUES(DEFAULT, _userId, _category, DEFAULT, _closed, _title,_content,_state);
END $$;


ALTER FUNCTION tso.addforumquestion(_userid bigint, _category character varying, _closed boolean, _title character varying, _content text, _state tso.itemstate) OWNER TO webdb;

--
-- Name: addforumtags(bigint, character varying); Type: FUNCTION; Schema: tso; Owner: webdb
--

CREATE FUNCTION tso.addforumtags(_question bigint, _tag character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
  INSERT INTO tso.tag
  (value)
    SELECT _tag
    WHERE
      NOT EXISTS (
          SELECT value FROM tso.tag WHERE value = _tag
      );
  INSERT INTO tso.forumTags(question, tag)
  VALUES(_question, _tag);
END $$;


ALTER FUNCTION tso.addforumtags(_question bigint, _tag character varying) OWNER TO webdb;

--
-- Name: addnote(bigint, text); Type: FUNCTION; Schema: tso; Owner: webdb
--

CREATE FUNCTION tso.addnote(_userid bigint, _content text) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN 
  INSERT INTO tso.note(id, userId, dateTime, content)
    VALUES( DEFAULT, _userId, DEFAULT, _content);
END $$;


ALTER FUNCTION tso.addnote(_userid bigint, _content text) OWNER TO webdb;

--
-- Name: addperiod(date, date); Type: FUNCTION; Schema: tso; Owner: webdb
--

CREATE FUNCTION tso.addperiod(_startdate date, _enddate date) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN 
  INSERT INTO tso.period(id, startDate, endDate)
    VALUES( DEFAULT, _startDate, _endDate);
END $$;


ALTER FUNCTION tso.addperiod(_startdate date, _enddate date) OWNER TO webdb;

--
-- Name: addprivatechat(bigint, bigint, text); Type: FUNCTION; Schema: tso; Owner: webdb
--

CREATE FUNCTION tso.addprivatechat(_senderid bigint, _receiverid bigint, _content text) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN 
  INSERT INTO tso.privateChat(msgId, senderId, receiverId, dateTime, content)
    VALUES(DEFAULT, _senderId, _receiverId, DEFAULT, _content);
END $$;


ALTER FUNCTION tso.addprivatechat(_senderid bigint, _receiverid bigint, _content text) OWNER TO webdb;

--
-- Name: addqualification(bigint, tso.qualificationtype, character varying, date, character varying); Type: FUNCTION; Schema: tso; Owner: webdb
--

CREATE FUNCTION tso.addqualification(_userid bigint, _qualification tso.qualificationtype, _description character varying, _achievedate date, _grade character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN 
  INSERT INTO tso.qualification (userId, qualification, description, achieveDate, grade)
		VALUES(_userId, _qualification, _description, _achieveDate, _grade);
END $$;


ALTER FUNCTION tso.addqualification(_userid bigint, _qualification tso.qualificationtype, _description character varying, _achievedate date, _grade character varying) OWNER TO webdb;

--
-- Name: addsubmission(bigint, character varying, tso.difficulties, tso.itemstate, text, text, character varying, tso.planlevel); Type: FUNCTION; Schema: tso; Owner: webdb
--

CREATE FUNCTION tso.addsubmission(_userid bigint, _title character varying, _difficulty tso.difficulties, _state tso.itemstate, _content text, _solution text, _category character varying, _minplantovis tso.planlevel) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN 
  INSERT INTO tso.submission(id, userId, title, dateTime, difficulty, state, content, solution, category, minPlanToVis)
    VALUES(DEFAULT, _userId, _title, DEFAULT, _difficulty, _state, _content, _solution, _category, _minPlanToVis);
END $$;


ALTER FUNCTION tso.addsubmission(_userid bigint, _title character varying, _difficulty tso.difficulties, _state tso.itemstate, _content text, _solution text, _category character varying, _minplantovis tso.planlevel) OWNER TO webdb;

--
-- Name: addsubmissioncompany(bigint, integer); Type: FUNCTION; Schema: tso; Owner: webdb
--

CREATE FUNCTION tso.addsubmissioncompany(_submissionid bigint, _companyid integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
 INSERT INTO tso.submissionCompanies(submissionId, companyId)
    VALUES(_submissionId, _companyID);
END $$;


ALTER FUNCTION tso.addsubmissioncompany(_submissionid bigint, _companyid integer) OWNER TO webdb;

--
-- Name: addsubmissiontags(bigint, character varying); Type: FUNCTION; Schema: tso; Owner: webdb
--

CREATE FUNCTION tso.addsubmissiontags(_submission bigint, _tag character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
  INSERT INTO tso.tag
  (value)
    SELECT _tag
    WHERE
      NOT EXISTS (
          SELECT value FROM tso.tag WHERE value = _tag
      );
  INSERT INTO tso.submissionTags(submission, tag)
    VALUES(_submission, _tag);
END $$;


ALTER FUNCTION tso.addsubmissiontags(_submission bigint, _tag character varying) OWNER TO webdb;

--
-- Name: addtoken(character, bigint, timestamp with time zone); Type: FUNCTION; Schema: tso; Owner: webdb
--

CREATE FUNCTION tso.addtoken(_token character, _userid bigint, _expiredate timestamp with time zone) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
  INSERT INTO tso.token(token, userId, expireDate)
    VALUES(_token, _userId, _expireDate);
END $$;


ALTER FUNCTION tso.addtoken(_token character, _userid bigint, _expiredate timestamp with time zone) OWNER TO webdb;

--
-- Name: adduser(character varying, character varying, character, character varying, character varying, date, character varying, bytea); Type: FUNCTION; Schema: tso; Owner: webdb
--

CREATE FUNCTION tso.adduser(_mail character varying, _username character varying, _password character, _name character varying, _surname character varying, _birthdate date, _location character varying, _profilepic bytea) RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE curtime timestamptz := now();
BEGIN
  --create the base user
  INSERT INTO tso.user(id, mail, username, password)
    VALUES(DEFAULT, _mail, _username, _password);
  --fill also the extended information
  INSERT INTO tso.userextended (id, isAdmin, name, surname, birthDate, joinDate, location, profilePic)
    VALUES((SELECT id FROM tso.user WHERE username=_username), DEFAULT, _name, _surname, _birthDate, curtime, _location, _profilePic);
END $$;


ALTER FUNCTION tso.adduser(_mail character varying, _username character varying, _password character, _name character varying, _surname character varying, _birthdate date, _location character varying, _profilepic bytea) OWNER TO webdb;

--
-- Name: adduserplansubscription(bigint, bigint, integer, boolean); Type: FUNCTION; Schema: tso; Owner: webdb
--

CREATE FUNCTION tso.adduserplansubscription(_userid bigint, _period bigint, _planid integer, _isactive boolean) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
--If adding a plan that has to be active then set previous one as non-active

  IF _isActive = TRUE THEN
    WITH period_id AS
      (
      UPDATE tso.userPlanSubscription SET isActive = FALSE WHERE userId = _userId RETURNING period
      )
    --End the previous period
    UPDATE tso.period SET endDate = current_date WHERE id = (SELECT period FROM period_id);
  END IF;
  INSERT INTO tso.userPlanSubscription(userId, period, planId, isActive)
    VALUES(_userId, _period,_planId,_isActive);
END $$;


ALTER FUNCTION tso.adduserplansubscription(_userid bigint, _period bigint, _planid integer, _isactive boolean) OWNER TO webdb;

--
-- Name: addusersocial(bigint, character varying, text); Type: FUNCTION; Schema: tso; Owner: webdb
--

CREATE FUNCTION tso.addusersocial(_userid bigint, _social character varying, _link text) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN 
  INSERT INTO tso.userSocial(userId, social, link)
    VALUES(_userId, _social, _link);
END $$;


ALTER FUNCTION tso.addusersocial(_userid bigint, _social character varying, _link text) OWNER TO webdb;

--
-- Name: adduserworkfor(bigint, integer, bigint); Type: FUNCTION; Schema: tso; Owner: webdb
--

CREATE FUNCTION tso.adduserworkfor(_userid bigint, _company integer, _period bigint) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN 
  INSERT INTO tso.userWorkFor(userId, company, period)
    VALUES(_userId, _company, _period);
END $$;


ALTER FUNCTION tso.adduserworkfor(_userid bigint, _company integer, _period bigint) OWNER TO webdb;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: category; Type: TABLE; Schema: tso; Owner: webdb
--

CREATE TABLE tso.category (
    name character varying(128) NOT NULL,
    description text
);


ALTER TABLE tso.category OWNER TO webdb;

--
-- Name: TABLE category; Type: COMMENT; Schema: tso; Owner: webdb
--

COMMENT ON TABLE tso.category IS 'Categories for forum and submissions';


--
-- Name: chatroom; Type: TABLE; Schema: tso; Owner: webdb
--

CREATE TABLE tso.chatroom (
    msgid bigint NOT NULL,
    senderid bigint NOT NULL,
    category character varying(128) NOT NULL,
    datetime timestamp with time zone DEFAULT date_trunc('second'::text, now()) NOT NULL,
    content text NOT NULL
);


ALTER TABLE tso.chatroom OWNER TO webdb;

--
-- Name: TABLE chatroom; Type: COMMENT; Schema: tso; Owner: webdb
--

COMMENT ON TABLE tso.chatroom IS 'Category-specific chat rooms';


--
-- Name: chatroom_msgid_seq; Type: SEQUENCE; Schema: tso; Owner: webdb
--

CREATE SEQUENCE tso.chatroom_msgid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tso.chatroom_msgid_seq OWNER TO webdb;

--
-- Name: chatroom_msgid_seq; Type: SEQUENCE OWNED BY; Schema: tso; Owner: webdb
--

ALTER SEQUENCE tso.chatroom_msgid_seq OWNED BY tso.chatroom.msgid;


--
-- Name: comment; Type: TABLE; Schema: tso; Owner: webdb
--

CREATE TABLE tso.comment (
    id bigint NOT NULL,
    userid bigint NOT NULL,
    question bigint,
    submission bigint,
    parentcomment bigint,
    datetime timestamp with time zone DEFAULT date_trunc('second'::text, now()) NOT NULL,
    downvote integer DEFAULT 0 NOT NULL,
    upvote integer DEFAULT 0 NOT NULL,
    notify boolean DEFAULT false NOT NULL,
    isanswer boolean DEFAULT false NOT NULL,
    content text NOT NULL,
    state tso.itemstate DEFAULT 'Visible'::tso.itemstate NOT NULL,
    CONSTRAINT check_comment CHECK ((((question IS NOT NULL) AND (submission IS NULL)) OR ((submission IS NOT NULL) AND (question IS NULL))))
);


ALTER TABLE tso.comment OWNER TO webdb;

--
-- Name: TABLE comment; Type: COMMENT; Schema: tso; Owner: webdb
--

COMMENT ON TABLE tso.comment IS 'Comments on submissions or forum questions';


--
-- Name: comment_id_seq; Type: SEQUENCE; Schema: tso; Owner: webdb
--

CREATE SEQUENCE tso.comment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tso.comment_id_seq OWNER TO webdb;

--
-- Name: comment_id_seq; Type: SEQUENCE OWNED BY; Schema: tso; Owner: webdb
--

ALTER SEQUENCE tso.comment_id_seq OWNED BY tso.comment.id;


--
-- Name: company; Type: TABLE; Schema: tso; Owner: webdb
--

CREATE TABLE tso.company (
    id integer NOT NULL,
    name character varying(128) NOT NULL,
    description text,
    website text,
    logo bytea,
    operativearea tso.operativeareas DEFAULT 'Unspecified'::tso.operativeareas NOT NULL
);


ALTER TABLE tso.company OWNER TO webdb;

--
-- Name: TABLE company; Type: COMMENT; Schema: tso; Owner: webdb
--

COMMENT ON TABLE tso.company IS 'Companies and their relevant data';


--
-- Name: company_id_seq; Type: SEQUENCE; Schema: tso; Owner: webdb
--

CREATE SEQUENCE tso.company_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tso.company_id_seq OWNER TO webdb;

--
-- Name: company_id_seq; Type: SEQUENCE OWNED BY; Schema: tso; Owner: webdb
--

ALTER SEQUENCE tso.company_id_seq OWNED BY tso.company.id;


--
-- Name: favoritesquestionlist; Type: TABLE; Schema: tso; Owner: webdb
--

CREATE TABLE tso.favoritesquestionlist (
    userid bigint NOT NULL,
    question bigint NOT NULL,
    notify boolean DEFAULT false NOT NULL
);


ALTER TABLE tso.favoritesquestionlist OWNER TO webdb;

--
-- Name: TABLE favoritesquestionlist; Type: COMMENT; Schema: tso; Owner: webdb
--

COMMENT ON TABLE tso.favoritesquestionlist IS 'Favorite list of forum questions, per user';


--
-- Name: favoritessubmissionlist; Type: TABLE; Schema: tso; Owner: webdb
--

CREATE TABLE tso.favoritessubmissionlist (
    userid bigint NOT NULL,
    submission bigint NOT NULL,
    notify boolean DEFAULT false NOT NULL
);


ALTER TABLE tso.favoritessubmissionlist OWNER TO webdb;

--
-- Name: TABLE favoritessubmissionlist; Type: COMMENT; Schema: tso; Owner: webdb
--

COMMENT ON TABLE tso.favoritessubmissionlist IS 'Favorite list of submissions, per user';


--
-- Name: forumquestion; Type: TABLE; Schema: tso; Owner: webdb
--

CREATE TABLE tso.forumquestion (
    id bigint NOT NULL,
    userid bigint NOT NULL,
    category character varying(128) NOT NULL,
    datetime timestamp with time zone DEFAULT date_trunc('second'::text, now()) NOT NULL,
    closed boolean DEFAULT false NOT NULL,
    title character varying(256) NOT NULL,
    content text NOT NULL,
    state tso.itemstate DEFAULT 'Visible'::tso.itemstate NOT NULL
);


ALTER TABLE tso.forumquestion OWNER TO webdb;

--
-- Name: TABLE forumquestion; Type: COMMENT; Schema: tso; Owner: webdb
--

COMMENT ON TABLE tso.forumquestion IS 'Forum questions';


--
-- Name: forumquestion_id_seq; Type: SEQUENCE; Schema: tso; Owner: webdb
--

CREATE SEQUENCE tso.forumquestion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tso.forumquestion_id_seq OWNER TO webdb;

--
-- Name: forumquestion_id_seq; Type: SEQUENCE OWNED BY; Schema: tso; Owner: webdb
--

ALTER SEQUENCE tso.forumquestion_id_seq OWNED BY tso.forumquestion.id;


--
-- Name: forumtags; Type: TABLE; Schema: tso; Owner: webdb
--

CREATE TABLE tso.forumtags (
    question bigint NOT NULL,
    tag character varying(128) NOT NULL
);


ALTER TABLE tso.forumtags OWNER TO webdb;

--
-- Name: TABLE forumtags; Type: COMMENT; Schema: tso; Owner: webdb
--

COMMENT ON TABLE tso.forumtags IS 'Links forum questions to their tags';


--
-- Name: note; Type: TABLE; Schema: tso; Owner: webdb
--

CREATE TABLE tso.note (
    id bigint NOT NULL,
    userid bigint NOT NULL,
    datetime timestamp with time zone DEFAULT date_trunc('second'::text, now()) NOT NULL,
    content text NOT NULL
);


ALTER TABLE tso.note OWNER TO webdb;

--
-- Name: TABLE note; Type: COMMENT; Schema: tso; Owner: webdb
--

COMMENT ON TABLE tso.note IS 'Notes, similar to Gists, associated to their authors';


--
-- Name: note_id_seq; Type: SEQUENCE; Schema: tso; Owner: webdb
--

CREATE SEQUENCE tso.note_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tso.note_id_seq OWNER TO webdb;

--
-- Name: note_id_seq; Type: SEQUENCE OWNED BY; Schema: tso; Owner: webdb
--

ALTER SEQUENCE tso.note_id_seq OWNED BY tso.note.id;


--
-- Name: period; Type: TABLE; Schema: tso; Owner: webdb
--

CREATE TABLE tso.period (
    id bigint NOT NULL,
    startdate date DEFAULT ('now'::text)::date NOT NULL,
    enddate date,
    CONSTRAINT checkperiod CHECK ((startdate < enddate))
);


ALTER TABLE tso.period OWNER TO webdb;

--
-- Name: TABLE period; Type: COMMENT; Schema: tso; Owner: webdb
--

COMMENT ON TABLE tso.period IS 'Generic time period';


--
-- Name: period_id_seq; Type: SEQUENCE; Schema: tso; Owner: webdb
--

CREATE SEQUENCE tso.period_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tso.period_id_seq OWNER TO webdb;

--
-- Name: period_id_seq; Type: SEQUENCE OWNED BY; Schema: tso; Owner: webdb
--

ALTER SEQUENCE tso.period_id_seq OWNED BY tso.period.id;


--
-- Name: pricingplan; Type: TABLE; Schema: tso; Owner: webdb
--

CREATE TABLE tso.pricingplan (
    id integer NOT NULL,
    level tso.planlevel NOT NULL,
    price numeric(12,2) NOT NULL,
    description text,
    CONSTRAINT onlynotfree CHECK ((level <> 'Free'::tso.planlevel)),
    CONSTRAINT pricingplan_price_check CHECK ((price >= (0)::numeric))
);


ALTER TABLE tso.pricingplan OWNER TO webdb;

--
-- Name: TABLE pricingplan; Type: COMMENT; Schema: tso; Owner: webdb
--

COMMENT ON TABLE tso.pricingplan IS 'Platform''s pricing plans';


--
-- Name: pricingplan_id_seq; Type: SEQUENCE; Schema: tso; Owner: webdb
--

CREATE SEQUENCE tso.pricingplan_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tso.pricingplan_id_seq OWNER TO webdb;

--
-- Name: pricingplan_id_seq; Type: SEQUENCE OWNED BY; Schema: tso; Owner: webdb
--

ALTER SEQUENCE tso.pricingplan_id_seq OWNED BY tso.pricingplan.id;


--
-- Name: privatechat; Type: TABLE; Schema: tso; Owner: webdb
--

CREATE TABLE tso.privatechat (
    msgid bigint NOT NULL,
    senderid bigint NOT NULL,
    receiverid bigint NOT NULL,
    datetime timestamp with time zone DEFAULT date_trunc('second'::text, now()) NOT NULL,
    content text NOT NULL
);


ALTER TABLE tso.privatechat OWNER TO webdb;

--
-- Name: TABLE privatechat; Type: COMMENT; Schema: tso; Owner: webdb
--

COMMENT ON TABLE tso.privatechat IS 'Private chats between two users';


--
-- Name: privatechat_msgid_seq; Type: SEQUENCE; Schema: tso; Owner: webdb
--

CREATE SEQUENCE tso.privatechat_msgid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tso.privatechat_msgid_seq OWNER TO webdb;

--
-- Name: privatechat_msgid_seq; Type: SEQUENCE OWNED BY; Schema: tso; Owner: webdb
--

ALTER SEQUENCE tso.privatechat_msgid_seq OWNED BY tso.privatechat.msgid;


--
-- Name: qualification; Type: TABLE; Schema: tso; Owner: webdb
--

CREATE TABLE tso.qualification (
    userid bigint NOT NULL,
    qualification tso.qualificationtype NOT NULL,
    description character varying(256) NOT NULL,
    achievedate date NOT NULL,
    grade character varying(128),
    CONSTRAINT validdate CHECK ((achievedate < ('now'::text)::date))
);


ALTER TABLE tso.qualification OWNER TO webdb;

--
-- Name: TABLE qualification; Type: COMMENT; Schema: tso; Owner: webdb
--

COMMENT ON TABLE tso.qualification IS 'User''s qualifications, academic title or similar';


--
-- Name: socialnetwork; Type: TABLE; Schema: tso; Owner: webdb
--

CREATE TABLE tso.socialnetwork (
    name character varying(128) NOT NULL,
    website text NOT NULL,
    logo bytea
);


ALTER TABLE tso.socialnetwork OWNER TO webdb;

--
-- Name: TABLE socialnetwork; Type: COMMENT; Schema: tso; Owner: webdb
--

COMMENT ON TABLE tso.socialnetwork IS 'Social network links associated to users';


--
-- Name: submission; Type: TABLE; Schema: tso; Owner: webdb
--

CREATE TABLE tso.submission (
    id bigint NOT NULL,
    userid bigint,
    title character varying(256) NOT NULL,
    datetime timestamp with time zone DEFAULT now() NOT NULL,
    difficulty tso.difficulties NOT NULL,
    state tso.itemstate DEFAULT 'Visible'::tso.itemstate NOT NULL,
    content text NOT NULL,
    solution text,
    category character varying(128) NOT NULL,
    minplantovis tso.planlevel NOT NULL
);


ALTER TABLE tso.submission OWNER TO webdb;

--
-- Name: TABLE submission; Type: COMMENT; Schema: tso; Owner: webdb
--

COMMENT ON TABLE tso.submission IS 'Interview questions submission';


--
-- Name: submission_id_seq; Type: SEQUENCE; Schema: tso; Owner: webdb
--

CREATE SEQUENCE tso.submission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tso.submission_id_seq OWNER TO webdb;

--
-- Name: submission_id_seq; Type: SEQUENCE OWNED BY; Schema: tso; Owner: webdb
--

ALTER SEQUENCE tso.submission_id_seq OWNED BY tso.submission.id;


--
-- Name: submissioncompanies; Type: TABLE; Schema: tso; Owner: webdb
--

CREATE TABLE tso.submissioncompanies (
    submissionid bigint NOT NULL,
    companyid integer NOT NULL
);


ALTER TABLE tso.submissioncompanies OWNER TO webdb;

--
-- Name: TABLE submissioncompanies; Type: COMMENT; Schema: tso; Owner: webdb
--

COMMENT ON TABLE tso.submissioncompanies IS 'Links questions asked by companies';


--
-- Name: submissiontags; Type: TABLE; Schema: tso; Owner: webdb
--

CREATE TABLE tso.submissiontags (
    submission bigint NOT NULL,
    tag character varying(128) NOT NULL
);


ALTER TABLE tso.submissiontags OWNER TO webdb;

--
-- Name: TABLE submissiontags; Type: COMMENT; Schema: tso; Owner: webdb
--

COMMENT ON TABLE tso.submissiontags IS 'Links submissions to their tags';


--
-- Name: tag; Type: TABLE; Schema: tso; Owner: webdb
--

CREATE TABLE tso.tag (
    value character varying(128) NOT NULL
);


ALTER TABLE tso.tag OWNER TO webdb;

--
-- Name: TABLE tag; Type: COMMENT; Schema: tso; Owner: webdb
--

COMMENT ON TABLE tso.tag IS 'Tags for submissions and forum questions';


--
-- Name: token; Type: TABLE; Schema: tso; Owner: webdb
--

CREATE TABLE tso.token (
    token character(64) NOT NULL,
    userid bigint NOT NULL,
    expiredate timestamp with time zone NOT NULL
);


ALTER TABLE tso.token OWNER TO webdb;

--
-- Name: TABLE token; Type: COMMENT; Schema: tso; Owner: webdb
--

COMMENT ON TABLE tso.token IS 'Container of user-assigned login tokens';


--
-- Name: user; Type: TABLE; Schema: tso; Owner: webdb
--

CREATE TABLE tso."user" (
    id bigint NOT NULL,
    mail character varying(256) NOT NULL,
    username character varying(128) NOT NULL,
    password character(64) NOT NULL
);


ALTER TABLE tso."user" OWNER TO webdb;

--
-- Name: TABLE "user"; Type: COMMENT; Schema: tso; Owner: webdb
--

COMMENT ON TABLE tso."user" IS 'Platform''s user';


--
-- Name: user_id_seq; Type: SEQUENCE; Schema: tso; Owner: webdb
--

CREATE SEQUENCE tso.user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tso.user_id_seq OWNER TO webdb;

--
-- Name: user_id_seq; Type: SEQUENCE OWNED BY; Schema: tso; Owner: webdb
--

ALTER SEQUENCE tso.user_id_seq OWNED BY tso."user".id;


--
-- Name: userextended; Type: TABLE; Schema: tso; Owner: webdb
--

CREATE TABLE tso.userextended (
    id bigint NOT NULL,
    isadmin boolean DEFAULT false NOT NULL,
    name character varying(128) NOT NULL,
    surname character varying(128) NOT NULL,
    birthdate date NOT NULL,
    joindate date DEFAULT ('now'::text)::date NOT NULL,
    location character varying(128) NOT NULL,
    profilepic bytea
);


ALTER TABLE tso.userextended OWNER TO webdb;

--
-- Name: TABLE userextended; Type: COMMENT; Schema: tso; Owner: webdb
--

COMMENT ON TABLE tso.userextended IS 'Extended informations for users';


--
-- Name: userplansubscription; Type: TABLE; Schema: tso; Owner: webdb
--

CREATE TABLE tso.userplansubscription (
    userid bigint NOT NULL,
    period bigint NOT NULL,
    planid integer NOT NULL,
    isactive boolean DEFAULT false NOT NULL
);


ALTER TABLE tso.userplansubscription OWNER TO webdb;

--
-- Name: TABLE userplansubscription; Type: COMMENT; Schema: tso; Owner: webdb
--

COMMENT ON TABLE tso.userplansubscription IS 'Links users and subscribed plans, tracking periods';


--
-- Name: usersocial; Type: TABLE; Schema: tso; Owner: webdb
--

CREATE TABLE tso.usersocial (
    userid bigint NOT NULL,
    social character varying(128) NOT NULL,
    link text NOT NULL
);


ALTER TABLE tso.usersocial OWNER TO webdb;

--
-- Name: TABLE usersocial; Type: COMMENT; Schema: tso; Owner: webdb
--

COMMENT ON TABLE tso.usersocial IS 'Links users to their social networks, if they agreed';


--
-- Name: userworkfor; Type: TABLE; Schema: tso; Owner: webdb
--

CREATE TABLE tso.userworkfor (
    userid bigint NOT NULL,
    company integer NOT NULL,
    period bigint NOT NULL
);


ALTER TABLE tso.userworkfor OWNER TO webdb;

--
-- Name: TABLE userworkfor; Type: COMMENT; Schema: tso; Owner: webdb
--

COMMENT ON TABLE tso.userworkfor IS 'Tracks users working for a company on a certain time period';


--
-- Name: chatroom msgid; Type: DEFAULT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso.chatroom ALTER COLUMN msgid SET DEFAULT nextval('tso.chatroom_msgid_seq'::regclass);


--
-- Name: comment id; Type: DEFAULT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso.comment ALTER COLUMN id SET DEFAULT nextval('tso.comment_id_seq'::regclass);


--
-- Name: company id; Type: DEFAULT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso.company ALTER COLUMN id SET DEFAULT nextval('tso.company_id_seq'::regclass);


--
-- Name: forumquestion id; Type: DEFAULT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso.forumquestion ALTER COLUMN id SET DEFAULT nextval('tso.forumquestion_id_seq'::regclass);


--
-- Name: note id; Type: DEFAULT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso.note ALTER COLUMN id SET DEFAULT nextval('tso.note_id_seq'::regclass);


--
-- Name: period id; Type: DEFAULT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso.period ALTER COLUMN id SET DEFAULT nextval('tso.period_id_seq'::regclass);


--
-- Name: pricingplan id; Type: DEFAULT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso.pricingplan ALTER COLUMN id SET DEFAULT nextval('tso.pricingplan_id_seq'::regclass);


--
-- Name: privatechat msgid; Type: DEFAULT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso.privatechat ALTER COLUMN msgid SET DEFAULT nextval('tso.privatechat_msgid_seq'::regclass);


--
-- Name: submission id; Type: DEFAULT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso.submission ALTER COLUMN id SET DEFAULT nextval('tso.submission_id_seq'::regclass);


--
-- Name: user id; Type: DEFAULT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso."user" ALTER COLUMN id SET DEFAULT nextval('tso.user_id_seq'::regclass);


--
-- Data for Name: category; Type: TABLE DATA; Schema: tso; Owner: webdb
--

INSERT INTO tso.category (name, description) VALUES ('General', 'General matters');
INSERT INTO tso.category (name, description) VALUES ('Programming', 'Programming problem');
INSERT INTO tso.category (name, description) VALUES ('Behavior', 'Behavior question');
INSERT INTO tso.category (name, description) VALUES ('Operating system', 'General question on operating system');


--
-- Data for Name: chatroom; Type: TABLE DATA; Schema: tso; Owner: webdb
--



--
-- Data for Name: comment; Type: TABLE DATA; Schema: tso; Owner: webdb
--

INSERT INTO tso.comment (id, userid, question, submission, parentcomment, datetime, downvote, upvote, notify, isanswer, content, state) VALUES (1, 4, 1, NULL, NULL, '2018-05-15 12:49:12+00', 0, 0, true, false, 'prova a cercare su google', 'Visible');
INSERT INTO tso.comment (id, userid, question, submission, parentcomment, datetime, downvote, upvote, notify, isanswer, content, state) VALUES (2, 7, 1, NULL, 1, '2018-05-15 12:49:12+00', 0, 0, false, false, 'simpatico', 'Visible');
INSERT INTO tso.comment (id, userid, question, submission, parentcomment, datetime, downvote, upvote, notify, isanswer, content, state) VALUES (3, 7, 2, NULL, NULL, '2018-05-15 12:49:12+00', 0, 0, true, true, 'for(int i = 0; i< max; i++){}', 'Visible');


--
-- Data for Name: company; Type: TABLE DATA; Schema: tso; Owner: webdb
--

INSERT INTO tso.company (id, name, description, website, logo, operativearea) VALUES (1, 'Google', 'Google', 'www.google.it', NULL, 'Unspecified');
INSERT INTO tso.company (id, name, description, website, logo, operativearea) VALUES (2, 'Auchan', 'Auchan', 'www.auchan.it', NULL, 'Distribution');
INSERT INTO tso.company (id, name, description, website, logo, operativearea) VALUES (3, 'BMW', 'BMW', 'www.bmw.de', NULL, 'Automotive');


--
-- Data for Name: favoritesquestionlist; Type: TABLE DATA; Schema: tso; Owner: webdb
--



--
-- Data for Name: favoritessubmissionlist; Type: TABLE DATA; Schema: tso; Owner: webdb
--



--
-- Data for Name: forumquestion; Type: TABLE DATA; Schema: tso; Owner: webdb
--

INSERT INTO tso.forumquestion (id, userid, category, datetime, closed, title, content, state) VALUES (1, 7, 'General', '2018-05-15 12:49:12+00', false, 'Array', 'How can I declare an arry?', 'Visible');
INSERT INTO tso.forumquestion (id, userid, category, datetime, closed, title, content, state) VALUES (2, 4, 'Programming', '2018-05-15 12:49:12+00', true, 'For', 'How can I do a for?', 'Visible');


--
-- Data for Name: forumtags; Type: TABLE DATA; Schema: tso; Owner: webdb
--

INSERT INTO tso.forumtags (question, tag) VALUES (2, 'programming');


--
-- Data for Name: note; Type: TABLE DATA; Schema: tso; Owner: webdb
--

INSERT INTO tso.note (id, userid, datetime, content) VALUES (1, 10, '2018-05-15 12:49:12+00', 'devo vedere meglio il sito');


--
-- Data for Name: period; Type: TABLE DATA; Schema: tso; Owner: webdb
--

INSERT INTO tso.period (id, startdate, enddate) VALUES (1, '2015-07-12', '2015-08-12');
INSERT INTO tso.period (id, startdate, enddate) VALUES (2, '2018-04-13', NULL);
INSERT INTO tso.period (id, startdate, enddate) VALUES (3, '2006-04-22', NULL);
INSERT INTO tso.period (id, startdate, enddate) VALUES (4, '2015-03-11', '2017-08-22');


--
-- Data for Name: pricingplan; Type: TABLE DATA; Schema: tso; Owner: webdb
--

INSERT INTO tso.pricingplan (id, level, price, description) VALUES (1, 'Advanced', 10.00, 'Advanced');
INSERT INTO tso.pricingplan (id, level, price, description) VALUES (2, 'Pro', 100.00, 'Full');
INSERT INTO tso.pricingplan (id, level, price, description) VALUES (3, 'Advanced', 2.00, 'Advanced discounted');


--
-- Data for Name: privatechat; Type: TABLE DATA; Schema: tso; Owner: webdb
--



--
-- Data for Name: qualification; Type: TABLE DATA; Schema: tso; Owner: webdb
--

INSERT INTO tso.qualification (userid, qualification, description, achievedate, grade) VALUES (3, 'PhD', 'Physics phd', '2017-07-12', '110');
INSERT INTO tso.qualification (userid, qualification, description, achievedate, grade) VALUES (9, 'Other', 'CAD advance course', '2001-05-13', 'passed');
INSERT INTO tso.qualification (userid, qualification, description, achievedate, grade) VALUES (3, 'Masters Degree', 'ICT', '2014-07-12', '110L');
INSERT INTO tso.qualification (userid, qualification, description, achievedate, grade) VALUES (7, 'High School', 'Classic', '2002-07-12', '80');


--
-- Data for Name: socialnetwork; Type: TABLE DATA; Schema: tso; Owner: webdb
--

INSERT INTO tso.socialnetwork (name, website, logo) VALUES ('LinkedIn', 'www.linkedin.com', NULL);
INSERT INTO tso.socialnetwork (name, website, logo) VALUES ('Facebook', 'www.facebook.com', NULL);
INSERT INTO tso.socialnetwork (name, website, logo) VALUES ('Twitter', 'www.twitter.com', NULL);


--
-- Data for Name: submission; Type: TABLE DATA; Schema: tso; Owner: webdb
--

INSERT INTO tso.submission (id, userid, title, datetime, difficulty, state, content, solution, category, minplantovis) VALUES (1, 5, 'First submission', '2018-05-15 12:49:12.29714+00', 'Easy', 'Visible', 'First submission content', '', 'General', 'Free');
INSERT INTO tso.submission (id, userid, title, datetime, difficulty, state, content, solution, category, minplantovis) VALUES (2, 2, 'Second submission', '2018-05-15 12:49:12.29714+00', 'Hard', 'Deleted', 'Second submission content', '', 'Programming', 'Advanced');


--
-- Data for Name: submissioncompanies; Type: TABLE DATA; Schema: tso; Owner: webdb
--

INSERT INTO tso.submissioncompanies (submissionid, companyid) VALUES (1, 1);
INSERT INTO tso.submissioncompanies (submissionid, companyid) VALUES (1, 3);


--
-- Data for Name: submissiontags; Type: TABLE DATA; Schema: tso; Owner: webdb
--

INSERT INTO tso.submissiontags (submission, tag) VALUES (1, 'programmin');


--
-- Data for Name: tag; Type: TABLE DATA; Schema: tso; Owner: webdb
--

INSERT INTO tso.tag (value) VALUES ('programmin');
INSERT INTO tso.tag (value) VALUES ('programming');


--
-- Data for Name: token; Type: TABLE DATA; Schema: tso; Owner: webdb
--



--
-- Data for Name: user; Type: TABLE DATA; Schema: tso; Owner: webdb
--

INSERT INTO tso."user" (id, mail, username, password) VALUES (1, 'mario@exaple.com', 'mariorossi', '011                                                             ');
INSERT INTO tso."user" (id, mail, username, password) VALUES (2, 'giovanni@exaple.com', 'foo', '01111                                                           ');
INSERT INTO tso."user" (id, mail, username, password) VALUES (3, 'marta@exaple.com', 'softwerist', '0100                                                            ');
INSERT INTO tso."user" (id, mail, username, password) VALUES (4, 'claudio@exaple.com', 'italia06', '011010                                                          ');
INSERT INTO tso."user" (id, mail, username, password) VALUES (5, 'facchin@exaple.com', 'piante', '010101                                                          ');
INSERT INTO tso."user" (id, mail, username, password) VALUES (6, 'calcio@exaple.com', 'puzzling', '0110010                                                         ');
INSERT INTO tso."user" (id, mail, username, password) VALUES (7, 'alessia@exaple.com', 'shining', '010010101                                                       ');
INSERT INTO tso."user" (id, mail, username, password) VALUES (8, 'paolo@exaple.com', 'door', '011001010                                                       ');
INSERT INTO tso."user" (id, mail, username, password) VALUES (9, 'genoveffa@exaple.com', 'ac/dc', '01010101                                                        ');
INSERT INTO tso."user" (id, mail, username, password) VALUES (10, 'caramelle@exaple.com', 'gloves', '0100100011                                                      ');


--
-- Data for Name: userextended; Type: TABLE DATA; Schema: tso; Owner: webdb
--

INSERT INTO tso.userextended (id, isadmin, name, surname, birthdate, joindate, location, profilepic) VALUES (1, false, 'mario', 'rossi', '1994-09-07', '2018-05-15', 'Vicenza', NULL);
INSERT INTO tso.userextended (id, isadmin, name, surname, birthdate, joindate, location, profilepic) VALUES (3, false, 'marta', 'bonetto', '2006-12-04', '2018-05-15', 'Londra', NULL);
INSERT INTO tso.userextended (id, isadmin, name, surname, birthdate, joindate, location, profilepic) VALUES (4, false, 'claudio', 'almenari', '2001-04-05', '2018-05-15', 'Napoli', NULL);
INSERT INTO tso.userextended (id, isadmin, name, surname, birthdate, joindate, location, profilepic) VALUES (5, false, 'facchin', 'de grandi', '1994-09-07', '2018-05-15', 'Germania', NULL);
INSERT INTO tso.userextended (id, isadmin, name, surname, birthdate, joindate, location, profilepic) VALUES (6, false, 'mario', 'rossi', '1959-02-14', '2018-05-15', 'Venezia', NULL);
INSERT INTO tso.userextended (id, isadmin, name, surname, birthdate, joindate, location, profilepic) VALUES (8, false, 'giovanni', 'paolo', '1986-06-12', '2018-05-15', 'Udine', NULL);
INSERT INTO tso.userextended (id, isadmin, name, surname, birthdate, joindate, location, profilepic) VALUES (9, false, 'genoveffa', 'zaroccolo', '1982-03-04', '2018-05-15', 'Shangai', NULL);
INSERT INTO tso.userextended (id, isadmin, name, surname, birthdate, joindate, location, profilepic) VALUES (10, false, 'kevin', 'bacon', '1944-02-07', '2018-05-15', 'Houston', NULL);
INSERT INTO tso.userextended (id, isadmin, name, surname, birthdate, joindate, location, profilepic) VALUES (2, true, 'giovanni', 'verdi', '1964-10-02', '2018-05-15', 'Milano', NULL);
INSERT INTO tso.userextended (id, isadmin, name, surname, birthdate, joindate, location, profilepic) VALUES (7, true, 'alessia', 'gubbini', '1958-10-03', '2018-05-15', 'San Remo', NULL);


--
-- Data for Name: userplansubscription; Type: TABLE DATA; Schema: tso; Owner: webdb
--

INSERT INTO tso.userplansubscription (userid, period, planid, isactive) VALUES (4, 4, 1, true);
INSERT INTO tso.userplansubscription (userid, period, planid, isactive) VALUES (5, 4, 3, true);


--
-- Data for Name: usersocial; Type: TABLE DATA; Schema: tso; Owner: webdb
--

INSERT INTO tso.usersocial (userid, social, link) VALUES (5, 'LinkedIn', 'www.linkedin.com/piaante');


--
-- Data for Name: userworkfor; Type: TABLE DATA; Schema: tso; Owner: webdb
--

INSERT INTO tso.userworkfor (userid, company, period) VALUES (3, 1, 1);
INSERT INTO tso.userworkfor (userid, company, period) VALUES (3, 3, 2);
INSERT INTO tso.userworkfor (userid, company, period) VALUES (6, 2, 3);


--
-- Name: chatroom_msgid_seq; Type: SEQUENCE SET; Schema: tso; Owner: webdb
--

SELECT pg_catalog.setval('tso.chatroom_msgid_seq', 1, false);


--
-- Name: comment_id_seq; Type: SEQUENCE SET; Schema: tso; Owner: webdb
--

SELECT pg_catalog.setval('tso.comment_id_seq', 3, true);


--
-- Name: company_id_seq; Type: SEQUENCE SET; Schema: tso; Owner: webdb
--

SELECT pg_catalog.setval('tso.company_id_seq', 3, true);


--
-- Name: forumquestion_id_seq; Type: SEQUENCE SET; Schema: tso; Owner: webdb
--

SELECT pg_catalog.setval('tso.forumquestion_id_seq', 2, true);


--
-- Name: note_id_seq; Type: SEQUENCE SET; Schema: tso; Owner: webdb
--

SELECT pg_catalog.setval('tso.note_id_seq', 1, true);


--
-- Name: period_id_seq; Type: SEQUENCE SET; Schema: tso; Owner: webdb
--

SELECT pg_catalog.setval('tso.period_id_seq', 4, true);


--
-- Name: pricingplan_id_seq; Type: SEQUENCE SET; Schema: tso; Owner: webdb
--

SELECT pg_catalog.setval('tso.pricingplan_id_seq', 3, true);


--
-- Name: privatechat_msgid_seq; Type: SEQUENCE SET; Schema: tso; Owner: webdb
--

SELECT pg_catalog.setval('tso.privatechat_msgid_seq', 1, false);


--
-- Name: submission_id_seq; Type: SEQUENCE SET; Schema: tso; Owner: webdb
--

SELECT pg_catalog.setval('tso.submission_id_seq', 2, true);


--
-- Name: user_id_seq; Type: SEQUENCE SET; Schema: tso; Owner: webdb
--

SELECT pg_catalog.setval('tso.user_id_seq', 10, true);


--
-- Name: category category_pkey; Type: CONSTRAINT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso.category
    ADD CONSTRAINT category_pkey PRIMARY KEY (name);


--
-- Name: chatroom chatroom_pkey; Type: CONSTRAINT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso.chatroom
    ADD CONSTRAINT chatroom_pkey PRIMARY KEY (msgid, senderid, category);


--
-- Name: comment comment_pkey; Type: CONSTRAINT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso.comment
    ADD CONSTRAINT comment_pkey PRIMARY KEY (id);


--
-- Name: company company_pkey; Type: CONSTRAINT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso.company
    ADD CONSTRAINT company_pkey PRIMARY KEY (id);


--
-- Name: favoritesquestionlist favoritesquestionlist_pkey; Type: CONSTRAINT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso.favoritesquestionlist
    ADD CONSTRAINT favoritesquestionlist_pkey PRIMARY KEY (userid, question);


--
-- Name: favoritessubmissionlist favoritessubmissionlist_pkey; Type: CONSTRAINT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso.favoritessubmissionlist
    ADD CONSTRAINT favoritessubmissionlist_pkey PRIMARY KEY (userid, submission);


--
-- Name: forumquestion forumquestion_pkey; Type: CONSTRAINT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso.forumquestion
    ADD CONSTRAINT forumquestion_pkey PRIMARY KEY (id);


--
-- Name: forumtags forumtags_pkey; Type: CONSTRAINT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso.forumtags
    ADD CONSTRAINT forumtags_pkey PRIMARY KEY (question, tag);


--
-- Name: note note_pkey; Type: CONSTRAINT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso.note
    ADD CONSTRAINT note_pkey PRIMARY KEY (id);


--
-- Name: period period_pkey; Type: CONSTRAINT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso.period
    ADD CONSTRAINT period_pkey PRIMARY KEY (id);


--
-- Name: pricingplan pricingplan_level_price_key; Type: CONSTRAINT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso.pricingplan
    ADD CONSTRAINT pricingplan_level_price_key UNIQUE (level, price);


--
-- Name: pricingplan pricingplan_pkey; Type: CONSTRAINT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso.pricingplan
    ADD CONSTRAINT pricingplan_pkey PRIMARY KEY (id);


--
-- Name: privatechat privatechat_pkey; Type: CONSTRAINT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso.privatechat
    ADD CONSTRAINT privatechat_pkey PRIMARY KEY (msgid, senderid, receiverid);


--
-- Name: qualification qualification_pkey; Type: CONSTRAINT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso.qualification
    ADD CONSTRAINT qualification_pkey PRIMARY KEY (userid, qualification);


--
-- Name: socialnetwork socialnetwork_pkey; Type: CONSTRAINT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso.socialnetwork
    ADD CONSTRAINT socialnetwork_pkey PRIMARY KEY (name);


--
-- Name: submission submission_pkey; Type: CONSTRAINT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso.submission
    ADD CONSTRAINT submission_pkey PRIMARY KEY (id);


--
-- Name: submissioncompanies submissioncompanies_pkey; Type: CONSTRAINT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso.submissioncompanies
    ADD CONSTRAINT submissioncompanies_pkey PRIMARY KEY (submissionid, companyid);


--
-- Name: submissiontags submissiontags_pkey; Type: CONSTRAINT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso.submissiontags
    ADD CONSTRAINT submissiontags_pkey PRIMARY KEY (submission, tag);


--
-- Name: tag tag_pkey; Type: CONSTRAINT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso.tag
    ADD CONSTRAINT tag_pkey PRIMARY KEY (value);


--
-- Name: token token_pkey; Type: CONSTRAINT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso.token
    ADD CONSTRAINT token_pkey PRIMARY KEY (token);


--
-- Name: user user_mail_key; Type: CONSTRAINT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso."user"
    ADD CONSTRAINT user_mail_key UNIQUE (mail);


--
-- Name: user user_pkey; Type: CONSTRAINT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso."user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);


--
-- Name: user user_username_key; Type: CONSTRAINT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso."user"
    ADD CONSTRAINT user_username_key UNIQUE (username);


--
-- Name: userextended userextended_pkey; Type: CONSTRAINT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso.userextended
    ADD CONSTRAINT userextended_pkey PRIMARY KEY (id);


--
-- Name: userplansubscription userplansubscription_pkey; Type: CONSTRAINT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso.userplansubscription
    ADD CONSTRAINT userplansubscription_pkey PRIMARY KEY (userid, period, planid);


--
-- Name: usersocial usersocial_pkey; Type: CONSTRAINT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso.usersocial
    ADD CONSTRAINT usersocial_pkey PRIMARY KEY (userid, social);


--
-- Name: userworkfor userworkfor_pkey; Type: CONSTRAINT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso.userworkfor
    ADD CONSTRAINT userworkfor_pkey PRIMARY KEY (userid, company, period);


--
-- Name: chatroom chatroom_category_fkey; Type: FK CONSTRAINT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso.chatroom
    ADD CONSTRAINT chatroom_category_fkey FOREIGN KEY (category) REFERENCES tso.category(name) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: chatroom chatroom_senderid_fkey; Type: FK CONSTRAINT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso.chatroom
    ADD CONSTRAINT chatroom_senderid_fkey FOREIGN KEY (senderid) REFERENCES tso."user"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: comment comment_parentcomment_fkey; Type: FK CONSTRAINT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso.comment
    ADD CONSTRAINT comment_parentcomment_fkey FOREIGN KEY (parentcomment) REFERENCES tso.comment(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: comment comment_question_fkey; Type: FK CONSTRAINT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso.comment
    ADD CONSTRAINT comment_question_fkey FOREIGN KEY (question) REFERENCES tso.forumquestion(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: comment comment_submission_fkey; Type: FK CONSTRAINT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso.comment
    ADD CONSTRAINT comment_submission_fkey FOREIGN KEY (submission) REFERENCES tso.submission(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: comment comment_userid_fkey; Type: FK CONSTRAINT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso.comment
    ADD CONSTRAINT comment_userid_fkey FOREIGN KEY (userid) REFERENCES tso."user"(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: favoritesquestionlist favoritesquestionlist_question_fkey; Type: FK CONSTRAINT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso.favoritesquestionlist
    ADD CONSTRAINT favoritesquestionlist_question_fkey FOREIGN KEY (question) REFERENCES tso.forumquestion(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: favoritesquestionlist favoritesquestionlist_userid_fkey; Type: FK CONSTRAINT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso.favoritesquestionlist
    ADD CONSTRAINT favoritesquestionlist_userid_fkey FOREIGN KEY (userid) REFERENCES tso."user"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: favoritessubmissionlist favoritessubmissionlist_submission_fkey; Type: FK CONSTRAINT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso.favoritessubmissionlist
    ADD CONSTRAINT favoritessubmissionlist_submission_fkey FOREIGN KEY (submission) REFERENCES tso.submission(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: favoritessubmissionlist favoritessubmissionlist_userid_fkey; Type: FK CONSTRAINT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso.favoritessubmissionlist
    ADD CONSTRAINT favoritessubmissionlist_userid_fkey FOREIGN KEY (userid) REFERENCES tso."user"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: forumquestion forumquestion_category_fkey; Type: FK CONSTRAINT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso.forumquestion
    ADD CONSTRAINT forumquestion_category_fkey FOREIGN KEY (category) REFERENCES tso.category(name) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: forumquestion forumquestion_userid_fkey; Type: FK CONSTRAINT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso.forumquestion
    ADD CONSTRAINT forumquestion_userid_fkey FOREIGN KEY (userid) REFERENCES tso."user"(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: forumtags forumtags_question_fkey; Type: FK CONSTRAINT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso.forumtags
    ADD CONSTRAINT forumtags_question_fkey FOREIGN KEY (question) REFERENCES tso.forumquestion(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: forumtags forumtags_tag_fkey; Type: FK CONSTRAINT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso.forumtags
    ADD CONSTRAINT forumtags_tag_fkey FOREIGN KEY (tag) REFERENCES tso.tag(value) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: note note_userid_fkey; Type: FK CONSTRAINT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso.note
    ADD CONSTRAINT note_userid_fkey FOREIGN KEY (userid) REFERENCES tso."user"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: privatechat privatechat_receiverid_fkey; Type: FK CONSTRAINT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso.privatechat
    ADD CONSTRAINT privatechat_receiverid_fkey FOREIGN KEY (receiverid) REFERENCES tso."user"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: privatechat privatechat_senderid_fkey; Type: FK CONSTRAINT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso.privatechat
    ADD CONSTRAINT privatechat_senderid_fkey FOREIGN KEY (senderid) REFERENCES tso."user"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: qualification qualification_userid_fkey; Type: FK CONSTRAINT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso.qualification
    ADD CONSTRAINT qualification_userid_fkey FOREIGN KEY (userid) REFERENCES tso."user"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: submission submission_category_fkey; Type: FK CONSTRAINT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso.submission
    ADD CONSTRAINT submission_category_fkey FOREIGN KEY (category) REFERENCES tso.category(name) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: submission submission_userid_fkey; Type: FK CONSTRAINT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso.submission
    ADD CONSTRAINT submission_userid_fkey FOREIGN KEY (userid) REFERENCES tso."user"(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: submissioncompanies submissioncompanies_companyid_fkey; Type: FK CONSTRAINT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso.submissioncompanies
    ADD CONSTRAINT submissioncompanies_companyid_fkey FOREIGN KEY (companyid) REFERENCES tso.company(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: submissioncompanies submissioncompanies_submissionid_fkey; Type: FK CONSTRAINT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso.submissioncompanies
    ADD CONSTRAINT submissioncompanies_submissionid_fkey FOREIGN KEY (submissionid) REFERENCES tso.submission(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: submissiontags submissiontags_submission_fkey; Type: FK CONSTRAINT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso.submissiontags
    ADD CONSTRAINT submissiontags_submission_fkey FOREIGN KEY (submission) REFERENCES tso.submission(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: submissiontags submissiontags_tag_fkey; Type: FK CONSTRAINT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso.submissiontags
    ADD CONSTRAINT submissiontags_tag_fkey FOREIGN KEY (tag) REFERENCES tso.tag(value) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: token token_userid_fkey; Type: FK CONSTRAINT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso.token
    ADD CONSTRAINT token_userid_fkey FOREIGN KEY (userid) REFERENCES tso."user"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: userextended userextended_id_fkey; Type: FK CONSTRAINT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso.userextended
    ADD CONSTRAINT userextended_id_fkey FOREIGN KEY (id) REFERENCES tso."user"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: userplansubscription userplansubscription_period_fkey; Type: FK CONSTRAINT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso.userplansubscription
    ADD CONSTRAINT userplansubscription_period_fkey FOREIGN KEY (period) REFERENCES tso.period(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: userplansubscription userplansubscription_planid_fkey; Type: FK CONSTRAINT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso.userplansubscription
    ADD CONSTRAINT userplansubscription_planid_fkey FOREIGN KEY (planid) REFERENCES tso.pricingplan(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: userplansubscription userplansubscription_userid_fkey; Type: FK CONSTRAINT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso.userplansubscription
    ADD CONSTRAINT userplansubscription_userid_fkey FOREIGN KEY (userid) REFERENCES tso."user"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: usersocial usersocial_social_fkey; Type: FK CONSTRAINT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso.usersocial
    ADD CONSTRAINT usersocial_social_fkey FOREIGN KEY (social) REFERENCES tso.socialnetwork(name) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: usersocial usersocial_userid_fkey; Type: FK CONSTRAINT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso.usersocial
    ADD CONSTRAINT usersocial_userid_fkey FOREIGN KEY (userid) REFERENCES tso."user"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: userworkfor userworkfor_company_fkey; Type: FK CONSTRAINT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso.userworkfor
    ADD CONSTRAINT userworkfor_company_fkey FOREIGN KEY (company) REFERENCES tso.company(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: userworkfor userworkfor_period_fkey; Type: FK CONSTRAINT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso.userworkfor
    ADD CONSTRAINT userworkfor_period_fkey FOREIGN KEY (period) REFERENCES tso.period(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: userworkfor userworkfor_userid_fkey; Type: FK CONSTRAINT; Schema: tso; Owner: webdb
--

ALTER TABLE ONLY tso.userworkfor
    ADD CONSTRAINT userworkfor_userid_fkey FOREIGN KEY (userid) REFERENCES tso."user"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--
